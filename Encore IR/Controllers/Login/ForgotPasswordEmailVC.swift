//
//  ForgotPasswordEmailVC.swift
//  Encore Direct
//
//  Created by Jaimin Patel on 31/05/19.
//  Copyright © 2019 ravi. All rights reserved.
//

import UIKit

class ForgotPasswordEmailVC: UIViewController {

    
    @IBOutlet weak var objScrollView: UIScrollView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var viewForgotPasswordDetail: UIView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var btnBackToSignIn: UIButton!
    @IBOutlet weak var lblEmailTitle: UILabel!
    @IBOutlet weak var imgEmail: UIImageView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    
    var IsForgotPassword = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupInitialView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if IsForgotPassword == false {
            btnBackToSignIn.isHidden = true
        } else {
            btnBackToSignIn.isHidden = false
            
            if UserDefaults.standard.object(forKey: kUserEmail) != nil  &&  UserDefaults.standard.object(forKey: kPassword) != nil{
                
                txtEmail.text = UserDefaults.standard.string(forKey: kUserEmail)
                
            }
        }
    }
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        
        btnSubmit.titleLabel?.font = UIFont(name:DINPROBold, size: 18.0)
        btnSubmit.titleLabel?.text = strSubmit
        
        txtEmail.textColor = fontLightTextColor
        txtEmail.font = UIFont(name: D_DIN, size: 15)
        txtEmail.placeholder = strEmail
        
        lblEmailTitle.font = UIFont(name: DINPRO, size: 15)
        lblEmailTitle.text = strEmail
        
        
    }
    
    func forgotApiResponse(strOtp:String, strUser:String){
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
        objVC.strOtpPass = strOtp
        objVC.strUserIdPass = strUser
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    func MoveToLogin(){
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        objVC.strUserEmail = txtEmail.text!
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func btnSubmitPressed(_ sender: Any) {
        
        
        if (txtEmail.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: msgEnterEmail, viewcontrolelr: self)
        }
        else if txtEmail.text?.isEmail == false
        {
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: msgEnterValidEmail, viewcontrolelr: self)
        }
        else if IsForgotPassword == false{
            self.userExistApiCall()
        }
        else{
            self.forgotApiCall()
        }
    }
    
    @IBAction func btnBackToSignInPressed(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
        
    }

}
