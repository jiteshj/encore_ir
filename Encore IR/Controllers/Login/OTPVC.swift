//
//  OTPVC.swift
//  Encore IR
//
//  Created by ravi on 04/05/19.
//  Copyright © 2019 ravi. All rights reserved.
//

import UIKit

class OTPVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var objScrollView: UIScrollView!
    
    @IBOutlet weak var viewTop: UIView!
    
    @IBOutlet weak var imgBackground: UIImageView!
    
    @IBOutlet weak var viewOTPDetail: UIView!
    @IBOutlet weak var lblEnterOTP: UILabel!
    
    @IBOutlet weak var txtOTP1: UITextField!
    @IBOutlet weak var txtOTP2: UITextField!
    @IBOutlet weak var txtOTP3: UITextField!
    @IBOutlet weak var txtOTP4: UITextField!
    @IBOutlet weak var txtOTP5: UITextField!
    @IBOutlet weak var txtOTP6: UITextField!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    
    
    @IBOutlet weak var viewBottom: UIView!
    
    @IBOutlet weak var btnBackToSignIn: UIButton!
    
    
    var strOtpPass = ""
    var strUserIdPass = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.\
        self.setupInitialView()
    
    }
    
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        
        btnSubmit.titleLabel?.font = UIFont(name:DINPROBold, size: 18.0)
        btnSubmit.titleLabel?.text = strSubmit
        
        lblEnterOTP.textColor = otpFonttitleColor
        lblEnterOTP.font = UIFont(name:MEDIUEM_FONT, size: 14.0)
        
        btnBackToSignIn.titleLabel?.font = UIFont(name:DINPROBold, size: 12.0)
        btnBackToSignIn.titleLabel?.text = strBackToSignin
        
        txtOTP1.delegate = self
        txtOTP2.delegate = self
        txtOTP3.delegate = self
        txtOTP4.delegate = self
        txtOTP5.delegate = self
        txtOTP6.delegate = self
        
        txtOTP1.textColor = otpFontColor
        txtOTP1.font = UIFont(name:MEDIUEM_FONT, size: 22.0)

        txtOTP2.textColor = otpFontColor
        txtOTP2.font = UIFont(name:MEDIUEM_FONT, size: 22.0)
        
        txtOTP3.textColor = otpFontColor
        txtOTP3.font = UIFont(name:MEDIUEM_FONT, size: 22.0)
        
        txtOTP4.textColor = otpFontColor
        txtOTP4.font = UIFont(name:MEDIUEM_FONT, size: 22.0)
        
        txtOTP5.textColor = otpFontColor
        txtOTP5.font = UIFont(name:MEDIUEM_FONT, size: 22.0)
        
        txtOTP6.textColor = otpFontColor
        txtOTP6.font = UIFont(name:MEDIUEM_FONT, size: 22.0)

        
        txtOTP1.addTarget(self, action: #selector(textFieldEditingDidChange), for: UIControlEvents.editingChanged)
        txtOTP2.addTarget(self, action: #selector(textFieldEditingDidChange), for: UIControlEvents.editingChanged)
        txtOTP3.addTarget(self, action: #selector(textFieldEditingDidChange), for: UIControlEvents.editingChanged)
        txtOTP4.addTarget(self, action: #selector(textFieldEditingDidChange), for: UIControlEvents.editingChanged)
        txtOTP5.addTarget(self, action: #selector(textFieldEditingDidChange), for: UIControlEvents.editingChanged)
        txtOTP6.addTarget(self, action: #selector(textFieldEditingDidChange), for: UIControlEvents.editingChanged)
        
        
    }
    
    
    
    @IBAction func btnSubmitPressed(_ sender: Any) {
        let strOTPCombine = "\(txtOTP1.text!)\(txtOTP2.text!)\(txtOTP3.text!)\(txtOTP4.text!)\(txtOTP5.text!)\(txtOTP6.text!)"
        
        self.view.endEditing(true)
        if (txtOTP1.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: msgEnterOTP, viewcontrolelr: self)
        }
        else if (txtOTP2.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: msgEnterOTP, viewcontrolelr: self)
        }
        else if (txtOTP3.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: msgEnterOTP, viewcontrolelr: self)
        }
        else if (txtOTP4.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: msgEnterOTP, viewcontrolelr: self)
        }
        else if (txtOTP5.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: msgEnterOTP, viewcontrolelr: self)
        }
        else if (txtOTP6.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: msgEnterOTP, viewcontrolelr: self)
        }
        else if strOtpPass != strOTPCombine{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: msgOTPMismatch, viewcontrolelr: self)
        }else{
            let objVC = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
            objVC.strUserid = strUserIdPass
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    @IBAction func btnBackToSignInPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK:- UITextField Delegate Method
    
    @objc func textFieldEditingDidChange(textField: UITextField) {
        
        let text = textField.text
        
        if (text?.utf16.count)! >= 1{
            switch textField{
            case txtOTP1:
                txtOTP2.becomeFirstResponder()
            case txtOTP2:
                txtOTP3.becomeFirstResponder()
            case txtOTP3:
                txtOTP4.becomeFirstResponder()
            case txtOTP4:
                txtOTP5.becomeFirstResponder()
            case txtOTP5:
                txtOTP6.becomeFirstResponder()
            case txtOTP6:
                txtOTP6.resignFirstResponder()
            default:
                break
            }
        }
        
        
        if  text?.count == 0 {
            switch textField{
            case txtOTP1:
                txtOTP1.becomeFirstResponder()
            case txtOTP2:
                txtOTP1.becomeFirstResponder()
            case txtOTP3:
                txtOTP2.becomeFirstResponder()
            case txtOTP4:
                txtOTP3.becomeFirstResponder()
            case txtOTP5:
                txtOTP4.becomeFirstResponder()
            case txtOTP6:
                txtOTP5.becomeFirstResponder()
            default:
                break
            }
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) {
            return true
        }
        
      
                
        if (textField.text?.length)! > 0{
            return false
        }
        return string.isNumber
      
        
    }
    
}
