//
//  LoginVC.swift
//  Encore IR
//
//  Created by ravi on 04/05/19.
//  Copyright © 2019 ravi. All rights reserved.
//

import UIKit
import SwiftGifOrigin
import LocalAuthentication


enum BiometricType: String {
    case none
    case touchID
    case faceID
}

extension LAContext {
    
    
    var biometricType: BiometricType {
        var error: NSError?
        
        guard self.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) else {
            // Capture these recoverable error thru Crashlytics
            return .none
        }
        
        if #available(iOS 11.0, *) {
            switch self.biometryType {
            case .none:
                return .none
            case .touchID:
                return .touchID
            case .faceID:
                return .faceID
            }
        } else {
            return  self.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) ? .touchID : .none
        }
    }
}

class LoginVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var objScrollview: UIScrollView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var viewLoginDetail: UIView!
    @IBOutlet weak var btnSignInFacialId: UIButton!
    @IBOutlet weak var btnCheckRemember: UIButton!
    @IBOutlet weak var lblRememberMe: UILabel!
    @IBOutlet weak var btnForgotPassword: UIButton!
    
    @IBOutlet weak var lblForgotPassword: UILabel!
    @IBOutlet weak var imgGifLoad: UIImageView!
    @IBOutlet weak var imgGifLoadBack: UIImageView!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblUsernameTitle: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var imgPassword: UIImageView!
    @IBOutlet weak var txtPassWord: UITextField!
    
    @IBOutlet weak var imgCheckBox: UIImageView!
    
    @IBOutlet weak var imgFingureTouch: UIImageView!
    @IBOutlet weak var btnFingureTouch: UIButton!
    
    @IBOutlet weak var ConstEmailHeight: NSLayoutConstraint!
    
    @IBOutlet weak var ConstPassHeight: NSLayoutConstraint!
    
    @IBOutlet weak var ConstBtnRememberHeight: NSLayoutConstraint!
    
    @IBOutlet weak var ConstRemImgHeight: NSLayoutConstraint!
    
    @IBOutlet weak var viewUsername: UIView!
    
    @IBOutlet weak var viewPassword: UIView!
    
    @IBOutlet var viewTerms: UIView!
    @IBOutlet var lblTitleTerms: UILabel!
    @IBOutlet var txtDetailTerms: UITextView!
    
    @IBOutlet var btnDecline: UIButton!
    
    @IBOutlet var btnIAgree: UIButton!
    
    //MARK:- Variables
//    var IsRememberMe = false
    var strUserEmail = ""
    var IsUserRegister = false
    var strUserId = ""
    
    
    //MARK:- UIView Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //
        btnSignInFacialId.isHidden = true
        btnFingureTouch.isHidden = true
        imgFingureTouch.isHidden = true
        
        
        if appDelegateObj.isShowToAnimation{
            imgLogo.isHidden = true
            
            imgGifLoad.image = UIImage.gif(name: "blank-text")
            
            imgGifLoad.animationRepeatCount = 1
            
            imgGifLoad.isHidden = false
            imgGifLoad.alpha = 1.0
            imgGifLoadBack.alpha = 1.0
            
            UIView.animate(withDuration: 7, delay: 0.0, options: [], animations: {
                
                self.imgGifLoad.alpha = 0.0
                self.imgGifLoadBack.alpha = 0.0
                
            }) { (finished: Bool) in
                
                self.imgGifLoad.isHidden = true
                self.imgGifLoadBack.isHidden = true
            }
            
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 6) {
                self.imgLogo.isHidden = false
            }
            
            appDelegateObj.isShowToAnimation = false
        }
        else{
            self.imgGifLoad.isHidden = true
            self.imgGifLoadBack.isHidden = true
            self.imgLogo.isHidden = false
        }
        
        //isShowSplashScreen
        if appDelegateObj.isShowSplashScreen{

            UIView.animate(withDuration: 2, delay: 0.0, options: [], animations: {
                self.imgLogo.isHidden = false
                self.imgGifLoadBack.isHidden = false

                
            }) { (finished: Bool) in
                
                self.imgGifLoadBack.isHidden = true
            }
            
            appDelegateObj.isShowSplashScreen = false

        }
        else{
            self.imgGifLoadBack.isHidden = true
        }
        
        
        // Do any additional setup after loading the view.
        self.setupInitialView()
        
        
    }
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        
        
        
        viewBottom.backgroundColor = fontTextColor
        
        
        txtUsername.textColor = fontLightTextColor
        txtUsername.font = UIFont(name: D_DIN, size: 15)
        txtUsername.placeholder = strEmail
        txtUsername.text = ""
        
        
        txtPassWord.textColor = fontLightTextColor
        txtPassWord.font = UIFont(name: D_DIN, size: 15)
        txtPassWord.isSecureTextEntry = true
        txtPassWord.placeholder = strPassword
        txtPassWord.text = ""
        
        lblUsernameTitle.text = strUserName
        txtPassWord.font = UIFont(name: DINPRO, size: 15)
        
        lblPassword.text = strPassword
        txtPassWord.font = UIFont(name: DINPRO, size: 15)
        
        let titleString = NSMutableAttributedString(string: strForgotPassword)
        let fontBig = UIFont(name:DINPRO, size: 13.0)
        titleString.addAttribute(NSAttributedStringKey.font, value: fontBig!, range: NSMakeRange(0, strForgotPassword.count))
        titleString.addAttribute(NSAttributedStringKey.foregroundColor, value: wBlueMainTitleColor, range: NSMakeRange(0, strForgotPassword.count))
        
        titleString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSMakeRange(0, strForgotPassword.count))
        lblForgotPassword.attributedText = titleString
        lblForgotPassword.adjustsFontSizeToFitWidth = true
        
//        btnForgotPassword.underlineButton(text: strForgotPassword)
//        btnForgotPassword.titleLabel!.adjustsFontSizeToFitWidth = true
        
        lblRememberMe.font = UIFont(name:DINPRO, size: 12.0)
        lblRememberMe.text = strRememberMe
        
        btnSignInFacialId.cornerRadius = 5
        btnSignInFacialId.borderWidth = 1
        btnSignInFacialId.borderColor = UIColor.white
        btnSignInFacialId.titleLabel?.font = UIFont(name:DINPROBold, size: 12.0)
        
        btnLogin.titleLabel?.font = UIFont(name:DINPROBold, size: 18.0)
        btnLogin.titleLabel?.text = strSignin
        
        
        
        #if arch(i386) || arch(x86_64)
        //simulator
        self.txtUsername.text = "shine@gmail.com"
        self.txtPassWord.text = "12345678"
        #else
        //device
        #endif
        
        if UserDefaults.standard.object(forKey: kUserEmail) != nil  &&  UserDefaults.standard.object(forKey: kPassword) != nil{
            
//            let strEmail = UserDefaults.standard.string(forKey: kUserEmail)
//            if strEmail == strUserEmail{
//                txtUsername.text = UserDefaults.standard.string(forKey: kUserEmail)
//                txtPassWord.text = UserDefaults.standard.string(forKey: kPassword)
//                imgCheckBox.image = UIImage(named: "checkbox-tick")
//                appDelegateObj.IsRememberMe = true
//
//            }else{
//                txtUsername.text = strUserEmail
//                txtPassWord.text = ""
//                imgCheckBox.image = UIImage(named: "checkbox")
//                appDelegateObj.IsRememberMe = false
//
//            }
            
        }else{
            txtUsername.text = strUserEmail
            txtPassWord.text = ""
            imgCheckBox.image = UIImage(named: "checkbox")
            appDelegateObj.IsRememberMe = false

        }
        
        if IsUserRegister == false{
            
            ConstPassHeight.constant = 0
            ConstRemImgHeight.constant = 0
            ConstBtnRememberHeight.constant = 0
            viewPassword.isHidden = true
            btnForgotPassword.isHidden = true
            lblForgotPassword.isHidden = true
        }
        else{
            ConstPassHeight.constant = 55
            ConstRemImgHeight.constant = 15
            ConstBtnRememberHeight.constant = 30
            viewPassword.isHidden = false
            btnForgotPassword.isHidden = false
            lblForgotPassword.isHidden = false
        }
        
        btnIAgree.layer.cornerRadius = 10
        btnIAgree.layer.masksToBounds = true
        btnIAgree.setTitle("I Agree", for: .normal)
        btnIAgree.backgroundColor = InvestTextColor
        btnIAgree.setTitleColor(UIColor.white, for: .normal)
        
        btnDecline.layer.cornerRadius = 10
        btnDecline.layer.masksToBounds = true
        btnDecline.setTitle("Decline", for: .normal)
        btnDecline.backgroundColor = lineViewColor
        btnDecline.setTitleColor(UIColor.white, for: .normal)
        
        viewTerms.layer.cornerRadius = 5
        viewTerms.clipsToBounds = true
        
        txtDetailTerms.isEditable = false
        
        viewTerms.isHidden = true
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        if imgCheckBox.image == UIImage(named: "checkbox-tick")
        {
            appDelegateObj.IsRememberMe = true
        }
        else{
            appDelegateObj.IsRememberMe = false
        }
        
        //var currentType = LAContext().biometricType
        
        self.checkTouchIdAvaibility()
    }
    func checkTouchIdAvaibility(){
        
        if self.isKeyPresentInUserDefaults(key: kTouchAuthUserId){
            let currentType = LAContext().biometricType
            if currentType.rawValue == BiometricType.none.rawValue{
                self.btnSignInFacialId.isHidden = true
                self.btnFingureTouch.isHidden = true
                self.imgFingureTouch.isHidden = true
                
            }else if currentType.rawValue == BiometricType.faceID.rawValue{
                self.btnSignInFacialId.isHidden = false
                self.btnFingureTouch.isHidden = true
                self.imgFingureTouch.isHidden = true
                
            }else if currentType.rawValue == BiometricType.touchID.rawValue{
                self.btnSignInFacialId.isHidden = true
                self.btnFingureTouch.isHidden = false
                self.imgFingureTouch.isHidden = false
            }
        }
    }
    
    func MoveToDashboard(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let navigationController = storyboard.instantiateViewController(withIdentifier: "NavigationController") as! UINavigationController
        navigationController.setViewControllers([storyboard.instantiateViewController(withIdentifier: "TabbarVC")], animated: false)
        
        let mainViewController = storyboard.instantiateInitialViewController() as! MainViewController
        mainViewController.rootViewController = navigationController
        mainViewController.leftViewPresentationStyle = .slideAbove
        
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = mainViewController
        
        UIView.transition(with: window, duration: 0.1, options: [.transitionCrossDissolve], animations: nil, completion: nil)
    }
    
    func forgotApiResponse(strOtp:String, strUser:String){
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
        objVC.strOtpPass = strOtp
        objVC.strUserIdPass = strUser
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    func checkUserRegisterResponce(){
        if IsUserRegister == false{
            
            ConstPassHeight.constant = 0
            ConstRemImgHeight.constant = 0
            ConstBtnRememberHeight.constant = 0
            viewPassword.isHidden = true
            btnForgotPassword.isHidden = true
            lblForgotPassword.isHidden = true
        }
        else{
            ConstPassHeight.constant = 55
            ConstRemImgHeight.constant = 15
            ConstBtnRememberHeight.constant = 30
            viewPassword.isHidden = false
            btnForgotPassword.isHidden = false
            lblForgotPassword.isHidden = false
            
            if UserDefaults.standard.object(forKey: kUserEmail) != nil  &&  UserDefaults.standard.object(forKey: kPassword) != nil{
                
                let strEmail = UserDefaults.standard.string(forKey: kUserEmail)
                if strEmail == txtUsername.text{
//                    txtUsername.text = UserDefaults.standard.string(forKey: kUserEmail)
                    txtPassWord.text = UserDefaults.standard.string(forKey: kPassword)
                    imgCheckBox.image = UIImage(named: "checkbox-tick")
                    appDelegateObj.IsRememberMe = true
                    
                }else{
//                    txtUsername.text = strUserEmail
                    txtPassWord.text = ""
                    imgCheckBox.image = UIImage(named: "checkbox")
                    appDelegateObj.IsRememberMe = false
                    
                }
                
            }else{
//                txtUsername.text = strUserEmail
                txtPassWord.text = ""
                imgCheckBox.image = UIImage(named: "checkbox")
                appDelegateObj.IsRememberMe = false
                
            }
            
        }
    }
    
    
    func afterTouchAuthLogin(){
        var strLoginId = ""
        strLoginId = UserDefaults.standard.string(forKey: kTouchAuthUserId)!
        
        var strLoginPass = ""
        strLoginPass = UserDefaults.standard.string(forKey: kTouchAuthPassword)!
        
        self.loginApiCall(strUserName: strLoginId, strPassword: strLoginPass)
    }
    
    //MARK:- IBActions
    @IBAction func btnLoginPressed(_ sender: Any) {
        
        self.view.endEditing(true)

        if IsUserRegister == false {
            
            if (txtUsername.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
                self.ShowAlertDisplay(titleObj: strAppName, messageObj: msgEnterUserName, viewcontrolelr: self)
            }else{
                self.userExistApiCall()
            }
        } else {
            
            if (txtUsername.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
                self.ShowAlertDisplay(titleObj: strAppName, messageObj: msgEnterUserName, viewcontrolelr: self)
            } else if (txtPassWord.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
                self.ShowAlertDisplay(titleObj: strAppName, messageObj: msgEnterPassword, viewcontrolelr: self)
            }
            else{
                self.loginApiCall(strUserName: txtUsername.text!, strPassword: txtPassWord.text!)
                
            }
        }
        
    }
    
    @IBAction func btnSignInFacialPressed(_ sender: Any) {
        
        let context = LAContext()
        
        var error: NSError?
        
        if context.canEvaluatePolicy(
            LAPolicy.deviceOwnerAuthenticationWithBiometrics,
            error: &error) {
            
            // Device can use biometric authentication
            context.evaluatePolicy(
                LAPolicy.deviceOwnerAuthenticationWithBiometrics,
                localizedReason: "Access requires authentication",
                reply: {(success, error) in
                    DispatchQueue.main.async {
                        
                        if let err = error {
                            
                            switch err._code {
                                
                            case LAError.Code.systemCancel.rawValue:
                                self.notifyUser("Session cancelled",
                                                err: err.localizedDescription)
                                
                            default:
                                self.notifyUser("Authentication failed",
                                                err: err.localizedDescription)
                            }
                            
                        } else {
                            self.afterTouchAuthLogin()
                            self.notifyUser("Authentication Successful",
                                            err: "You now have full access")
                        }
                    }
            })
            
        }
    }
    func notifyUser(_ msg: String, err: String?) {
        
    }
    @IBAction func btnCheckRemPressed(_ sender: Any) {
        
        if imgCheckBox.image == UIImage(named: "checkbox")
        {
            imgCheckBox.image = UIImage(named: "checkbox-tick")
            appDelegateObj.IsRememberMe = true
        }
        else
        {
            imgCheckBox.image = UIImage(named: "checkbox")
            appDelegateObj.IsRememberMe = false
            
            UserDefaults.standard.removeObject(forKey: kUserEmail)
            UserDefaults.standard.removeObject(forKey: kPassword)
            UserDefaults.standard.synchronize()
            
        }
        
    }
    
    @IBAction func btnForgotPassPressed(_ sender: Any) {
        
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordEmailVC") as! ForgotPasswordEmailVC
//        objVC.strUserid = strUserIdPass
        objVC.IsForgotPassword = true
        self.navigationController?.pushViewController(objVC, animated: true)
        
    }
    
    
    @IBAction func btnTuchIdPress(_ sender: Any){
        let myContext = LAContext()
        let myLocalizedReasonString = "Biometric Authntication"
        var authError: NSError?
        if #available(iOS 8.0, macOS 10.12.1, *) {
            if myContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
                myContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: myLocalizedReasonString) { success, evaluateError in
                    
                    DispatchQueue.main.async {
                        if success {
                            // User authenticated successfully, take appropriate action
                            self.afterTouchAuthLogin()
                        }
                    }
                }
            }
        }
    }
    
    
    @IBAction func btnDeclinePressed(_ sender: Any) {
        self.imgLogo.isHidden = false
        self.viewTerms.isHidden = true
    }
    
    @IBAction func btnIAgreePressed(_ sender: Any) {
        self.imgLogo.isHidden = false
        self.viewTerms.isHidden = true
        self.agreeTerms()
    }
    
    
}


