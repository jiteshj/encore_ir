//
//  ForgotPasswordVC.swift
//  Encore IR
//
//  Created by ravi on 04/05/19.
//  Copyright © 2019 ravi. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {
    
    @IBOutlet weak var objScrollView: UIScrollView!
    
    @IBOutlet weak var viewTop: UIView!
    
    @IBOutlet weak var imgBackground: UIImageView!
    
    @IBOutlet weak var viewForgotPasswordDetail: UIView!
    
    @IBOutlet weak var txtNewPassword: SkyFloatingLabelTextFieldWithIcon!
    
    @IBOutlet weak var txtConfPassword: SkyFloatingLabelTextFieldWithIcon!
    
    
    
    @IBOutlet weak var btnGetStarted: UIButton!
    
    @IBOutlet weak var viewBottom: UIView!
    
    @IBOutlet weak var btnBackToSignIn: UIButton!
    
    
    @IBOutlet weak var lblNewPassTitle: UILabel!
    
    @IBOutlet weak var imgNewPass: UIImageView!
    
    @IBOutlet weak var txtNewPass: UITextField!
    
    @IBOutlet weak var lblConfPassTitle: UILabel!
    
    @IBOutlet weak var imgConfPass: UIImageView!
    
    @IBOutlet weak var txtConfPass: UITextField!
    
    var strUserid = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.setupInitialView()
    }
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        
        //        txtNewPassword.placeholder = "New Password"
        //        txtNewPassword.title = "New Password"
        //        txtNewPassword.titleColor = fontTextColor
        //        txtNewPassword.selectedTitleColor = fontTextColor
        //        txtNewPassword.selectedLineColor = lineViewColor
        //        txtNewPassword.iconType = .image
        //        txtNewPassword.iconMarginBottom = 8.0
        //        txtNewPassword.iconImageView.image = UIImage(named: "password")
        //        txtNewPassword.text = "********"
        //        txtNewPassword.textColor = fontLightTextColor
        //        txtNewPassword.font = UIFont(name: D_DIN, size: 15)
        //        txtNewPassword.setTitleVisible(true)
        //        txtNewPassword.isSecureTextEntry = true
        //
        //
        //        txtConfPassword.placeholder = "Confirm Password"
        //        txtConfPassword.title = "Confirm Password"
        //        txtConfPassword.titleColor = fontTextColor
        //        txtConfPassword.selectedTitleColor = fontTextColor
        //        txtConfPassword.selectedLineColor = lineViewColor
        //        txtConfPassword.iconType = .image
        //        txtConfPassword.iconMarginBottom = 8.0
        //        txtConfPassword.iconImageView.image = UIImage(named: "password")
        //        txtConfPassword.text = "********"
        //        txtConfPassword.textColor = fontLightTextColor
        //        txtConfPassword.font = UIFont(name: D_DIN, size: 15)
        //        txtConfPassword.setTitleVisible(true)
        //        txtConfPassword.isSecureTextEntry = true
        
        viewBottom.backgroundColor = fontTextColor
        
        btnGetStarted.titleLabel?.font = UIFont(name:DINPROBold, size: 18.0)
        btnGetStarted.titleLabel?.text = strGetStared
        
        
        btnBackToSignIn.titleLabel?.font = UIFont(name:DINPROBold, size: 12.0)
        btnBackToSignIn.titleLabel?.text = strBackToSignin
        
        txtNewPass.textColor = fontLightTextColor
        txtNewPass.font = UIFont(name: D_DIN, size: 15)
        txtNewPass.isSecureTextEntry = true
        txtNewPass.placeholder = strNewPass
        
        lblNewPassTitle.font = UIFont(name: DINPRO, size: 15)
        lblNewPassTitle.text = strNewPass
        
        txtConfPass.textColor = fontLightTextColor
        txtConfPass.font = UIFont(name: D_DIN, size: 15)
        txtConfPass.isSecureTextEntry = true
        txtConfPass.placeholder = strConfPass
        
        lblConfPassTitle.font = UIFont(name: DINPRO, size: 15)
        lblConfPassTitle.text = strConfPass
        
    }
    
    @IBAction func btnGetStartedPressed(_ sender: Any) {
        self.view.endEditing(true)
        if (txtNewPass.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: msgEnterNewPassword, viewcontrolelr: self)
        } else if (txtConfPass.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: msgEnterConfirmassword, viewcontrolelr: self)
        }
        else if txtNewPass.text != txtConfPass.text{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: msgPasswordMismatch, viewcontrolelr: self)
        }
        else{
            self.resetApiCall()
        }
    }
    func resetApiResponse(strMsg:String){
        self.moveToLogin(strM: strMsg)
    }
    func moveToLogin(strM:String){
        let alert = UIAlertController(title: strAppName, message: strM, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: strOK, style: UIAlertActionStyle.destructive, handler: { action in
            self.navigationController?.popToRootViewController(animated: true)
        }))
        
        let activeVc = UIApplication.shared.keyWindow?.rootViewController
        activeVc?.present(alert, animated: true, completion: nil)
    }
    @IBAction func btnBackToSignInPressed(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
        
    }
    
    
    
}

