//
//  TabbarVC.swift
//  Encore IR
//
//  Created by ravi on 06/05/19.
//  Copyright © 2019 ravi. All rights reserved.
//

import UIKit

class TabbarVC: UITabBarController, UITabBarControllerDelegate {
    
    var newsVC: NewsVC!
    var myAssetsVC: MyAssetsVC!
    var newDealVC: NewDealVC!
    var libraryVC: LibraryVC!
    var freshLaunch = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupInitialView()
        
        appDelegateObj.tabBarController = self
        
    }
    

    //MARK:- Class Methods and Functions
    func setupInitialView(){
        self.delegate = self
        for tabBarItem in tabBar.items! {
            tabBarItem.title = ""
            tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if freshLaunch == true {
            freshLaunch = false
            appDelegateObj.tabBarController!.selectedIndex = 1
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
//        var tabFrame = self.tabBar.frame
//        // - 40 is editable , the default value is 49 px, below lowers the tabbar and above increases the tab bar size
//        tabFrame.size.height = 100
//        tabFrame.origin.y = self.view.frame.size.height - 100
//        self.tabBar.frame = tabFrame
//
        
//        var tabFrame:CGRect = self.tabBar.frame
//        tabFrame.origin.y = 20 + self.navigationController!.navigationBar.frame.size.height
//        tabFrame.size.height = 30
//        self.tabBar.frame = tabFrame
//        self.tabBar.backgroundColor = UIColor.clear
//
//        let topB = CALayer()
//        topB.frame = CGRect(x: 0, y:30.0, width: self.view.frame.size.width, height: 1.0)
        
//        topB.backgroundColor = orangeThemeColor.cgColor
//        self.tabBar.layer.addSublayer(topB)
        
    }
    
    //MARK: UITabbar Delegate
//    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
//        if viewController.isKind(of: ActionViewController.self) {
//            let vc =  ActionViewController()
//            vc.modalPresentationStyle = .overFullScreen
//            self.present(vc, animated: true, completion: nil)
//            return false
//        }
//        return true
//    }

}

class TabBar: UITabBar {
    
//    override func sizeThatFits(_ size: CGSize) -> CGSize {
//        var sizeThatFits = super.sizeThatFits(size)
//        sizeThatFits.height = 60
//
//        return sizeThatFits
//    }
}
