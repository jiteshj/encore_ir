//
//  InvestmentSummaryVC.swift
//  EncoreIR
//
//  Created by Jaimin Patel on 20/05/19.
//  Copyright © 2019 ravi. All rights reserved.
//

import UIKit
import ImageSlideshow

class QuaterlyUpdatesCell : UITableViewCell{
    
    @IBOutlet weak var viewBack: UIView!
//    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    
    @IBOutlet weak var imgChart: UIImageView!
    
    @IBOutlet weak var btnChart: UIButton!
    
    @IBOutlet weak var viewChart: UIView!
    
    @IBOutlet weak var viewChartBG: UIView!
    
    @IBOutlet weak var viewChartHeight: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewBack.layer.cornerRadius = 5
        self.viewBack.layer.borderWidth = 0.5
        self.viewBack.layer.masksToBounds = true
       
        
        self.viewChartBG.layer.cornerRadius = viewChartBG.frame.width/2
        self.viewChartBG.layer.masksToBounds = true
        
    
    }
    
}


class InvestmentSummaryVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK:- Outlets
    @IBOutlet weak var imgViewbackground: UIImageView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var imgFontBackgroung: UIImageView!
    @IBOutlet weak var imgFontBig: UIImageView!
    
    @IBOutlet weak var btnLargeFont: UIButton!
    @IBOutlet weak var tblInvesrmentSummary: UITableView!
    @IBOutlet weak var collHeader: UICollectionView!
    @IBOutlet var viewHeaderColl: UIView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var btnMinFont: UIButton!
    @IBOutlet weak var btnMaxFont: UIButton!
    
    
    @IBOutlet var lblTitleDate: UILabel!
    
    
    //MARK:- Variable
    var arrayRowTitle = NSMutableArray()
    var objSection = ObjSectionDetail()
    var arrImage = [UIImage]()
    var strChartName = ""
    var strImageUrl = ""
    var IsPDf = false
    var arrPdf = NSMutableArray()
    var fontsizeDetail : CGFloat = 0
    var slideshowTransitioningDelegate: ZoomAnimatedTransitioningDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupInitialView()

    }
 
    override func viewWillAppear(_ animated: Bool) {
        self.setUpThemeColor()
    }
    
    
    //MARK:- Class Method and Function
    func setupInitialView(){
        lblTopTitle.text  = strInvSumm
        self.AssetInvSummApi(strAId: objSection.a_id, strSection_isDynamicTF: objSection.section_isDynamic, strSection_id: objSection.section_id, strC_type: objSection.c_type, strIs_group: objSection.is_group)

        viewContainer.isHidden = true

        
    }
    func setUpThemeColor(){
        if appDelegateObj.IsThemeLight == true{
            
            print("Ligth Theme mode")
            imgViewbackground.image = UIImage(named: "Wht_background")
            imgBack.image = UIImage(named: "Wht_Back")
            self.view.backgroundColor = wviewBgDefaultThemeColor
            lblTopTitle.textColor = wTopTitleFontColor
            
            imgFontBackgroung.image = UIImage(named: "Wht_LargeFontBG")
            imgFontBig.image = UIImage(named: "Wht_LargeFont")
            
            btnMinFont.setTitleColor(wBlueMainTitleColor, for: .normal)
            btnMaxFont.setTitleColor(wBlueMainTitleColor, for: .normal)
            
            
            
        }
        else{
            print("Dark Theme mode")
            self.view.backgroundColor = TabViewColor
            imgViewbackground.image = UIImage(named: "background")
            imgBack.image = UIImage(named: "Back")
            lblTopTitle.textColor = TopTitleFontColor
            
            imgFontBackgroung.image = UIImage(named: "LargeFontBG")
            imgFontBig.image = UIImage(named: "FontBig")
            
            btnMinFont.setTitleColor(UIColor.white, for: .normal)
            btnMaxFont.setTitleColor(UIColor.white, for: .normal)
      
            
        }
        
        self.setupFontSize()
    }
    
    func setupFontSize(){
        lblTopTitle.font = lblTopTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 20.0))
        btnMinFont.titleLabel?.font = UIFont(name:DINPRO, size: GlobalMethods.setThemeFontSize(objectSize: 16.0))
        btnMaxFont.titleLabel?.font = UIFont(name:DINPRO, size: GlobalMethods.setThemeFontSize(objectSize: 25.0))
        
        fontsizeDetail = GlobalMethods.setThemeFontSize(objectSize: 0.0)
    }

    @objc func btnDocTbl(sender:UIButton)  {
        

        
        if IsPDf == false{
            
           /* // Your action
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ObjVC = storyboard.instantiateViewController(withIdentifier: "FullScreenImageVC") as! FullScreenImageVC
            
            print(arrImage)
            ObjVC.arrSlide = arrImage
            //        self.navigationController?.pushViewController(ObjVC, animated: true)
            
            ObjVC.modalPresentationStyle = .overCurrentContext
            present(ObjVC, animated: false, completion: nil)*/
            
            
            var arryModel = [Model]()
            
            for element in arrImage {
                //imageLabel.image = element
                arryModel.append(Model(image: element, title: ""))
            }
            
            
            let fullScreenController = FullScreenSlideshowViewController()
            
            fullScreenController.closeButton.layer.cornerRadius = fullScreenController.closeButton.bounds.height/2
            fullScreenController.closeButton.layer.borderWidth = 1
            fullScreenController.closeButton.layer.borderColor = UIColor.darkGray.cgColor
            fullScreenController.closeButton.backgroundColor = UIColor.lightGray
            fullScreenController.closeButton.layer.masksToBounds = true
            
            if appDelegateObj.IsThemeLight == true{
                fullScreenController.backgroundColor = wviewBgDefaultThemeColor
            }
            else{
                fullScreenController.backgroundColor = TabViewColor
            }
            
            fullScreenController.inputs = arryModel.map { $0.inputSource }
            
            
            fullScreenController.initialPage = 0
            
            present(fullScreenController, animated: true, completion: nil)
            
        }else{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ObjVC = storyboard.instantiateViewController(withIdentifier: "DocumentVC") as! DocumentVC
            
            print(arrPdf)
            ObjVC.strUrlPath = arrPdf.lastObject as! String
            ObjVC.strDocTitle = strChartName.firstUppercased
            self.navigationController?.pushViewController(ObjVC, animated: true)
            
        }
    }

    
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func btnFontLargePressed(_ sender: Any) {
        
        if viewContainer.isHidden == true {
            viewContainer.isHidden = false
        } else {
            viewContainer.isHidden = true
        }
        
    }
    
    @IBAction func btnMinFontPressed(_ sender: Any) {
        print("font --\(fontsizeDetail)")
        
        if fontsizeDetail < 0 {
            //            fontsizeTitle = 15
            //            fontsizeDetail = 13
        }
        else{
            fontsizeDetail = fontsizeDetail - 3
            tblInvesrmentSummary.reloadData()
        }
        
    }
    
    
    @IBAction func btnMaxFontPressed(_ sender: Any) {
        print("font ++\(fontsizeDetail)")
        
        if fontsizeDetail > 6 {
            //            fontsizeTitle = 15
            //            fontsizeDetail = 13
        }
        else{
            fontsizeDetail = fontsizeDetail + 3
            tblInvesrmentSummary.reloadData()
        }
        
    }
    
    //MARK:- UITableView Methods
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayRowTitle.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if indexPath.row == 0{
            let tablecell = tableView.dequeueReusableCell(withIdentifier: "QuaterlyUpdatesCell", for: indexPath) as! QuaterlyUpdatesCell


            tablecell.lblDetail.text = arrayRowTitle[indexPath.row] as? String
            

            tablecell.lblDetail.font = UIFont(name: MEDIUEM_FONT, size: CGFloat(14 + fontsizeDetail))

            
            if appDelegateObj.IsThemeLight{
                tablecell.viewBack.layer.borderColor = wBlueMainTitleColor.cgColor
                tablecell.viewBack.backgroundColor = UIColor.white
                tablecell.lblDetail.textColor = wDarkGrayColor
                tablecell.viewChartBG.backgroundColor = wBlueMainTitleColor
            }else{
                tablecell.viewBack.layer.borderColor = kCellBackColor.cgColor
                tablecell.viewBack.backgroundColor = kCellBackColor
                tablecell.lblDetail.textColor = UIColor.white
                tablecell.viewChartBG.backgroundColor = PdfBGColor
            }
            
            
            if arrImage.count != 0 || arrPdf.count != 0{
                tablecell.btnChart.tag = indexPath.row
                tablecell.btnChart.addTarget(self, action: #selector(self.btnDocTbl), for: .touchUpInside)
                
                strChartName = strQuarterlyUpdates
                
                tablecell.viewChartHeight.constant = 44
                tablecell.viewChart.isHidden = false
                tablecell.isHidden = false

            }
            else{
                tablecell.viewChartHeight.constant = 0
                tablecell.viewChart.isHidden = true
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    tablecell.isHidden = true
                }


            }
            
            return tablecell
        }
        else if indexPath.row == 1{
            let tablecell1 = tableView.dequeueReusableCell(withIdentifier: "cellRecentUpdate", for: indexPath) as! cellRecentUpdate

            tablecell1.lblTitle.text = strLEGAL_ISSUE_AND_CLAIMS
            
            tablecell1.lblDetail.text = arrayRowTitle[indexPath.row] as? String
            
            tablecell1.lblTitle.font = UIFont(name: DINPROBold, size: CGFloat(14 + fontsizeDetail))
            tablecell1.lblDetail.font = UIFont(name: MEDIUEM_FONT, size: CGFloat(14 + fontsizeDetail))

            
            if appDelegateObj.IsThemeLight{
                tablecell1.viewBack.layer.borderColor = wBlueMainTitleColor.cgColor
                tablecell1.viewBack.backgroundColor = UIColor.white
                tablecell1.lblTitle.textColor = wBlueMainTitleColor
                tablecell1.lblDetail.textColor = wDarkGrayColor
                tablecell1.viewChartBG.backgroundColor = wBlueMainTitleColor
            }else{
                tablecell1.viewBack.layer.borderColor = kCellBackColor.cgColor
                tablecell1.viewBack.backgroundColor = kCellBackColor
                tablecell1.lblTitle.textColor = UIColor.white
                tablecell1.lblDetail.textColor = UIColor.white
                tablecell1.viewChartBG.backgroundColor = PdfBGColor
            }
            
            tablecell1.viewChartHeight.constant = 0
            tablecell1.viewChart.isHidden = true
            
            if arrayRowTitle[indexPath.row] as? String == ""{
                tablecell1.isHidden = true
            }
            else{
                tablecell1.isHidden = false
            }

            return tablecell1
        }
        else{
            
            let cellDetail = tableView.dequeueReusableCell(withIdentifier: "cellFinTerm", for: indexPath) as! cellFinTerm

            cellDetail.lblTitle.text = arrayRowTitle[indexPath.row] as? String
            cellDetail.lblTitle.font = UIFont(name: DINPROBItalic, size: CGFloat(13 + fontsizeDetail))

            cellDetail.lblTitle.numberOfLines = 0
            
            if appDelegateObj.IsThemeLight{
                
                cellDetail.viewBack.backgroundColor = UIColor.white
                cellDetail.lblTitle.textColor = wDarkGrayColor
                
            }else{
                
                cellDetail.viewBack.backgroundColor = kCellBackColor
                cellDetail.lblTitle.textColor = UIColor.white
                
            }

            if arrayRowTitle[indexPath.row] as? String == ""{
                cellDetail.isHidden = true
            }
            
            return cellDetail
        }
    
        
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            if arrayRowTitle[indexPath.row] as? String == ""{
                return 0
            }else{
                return UITableViewAutomaticDimension
            }
        }
        else if indexPath.row == 1{
            if arrayRowTitle[indexPath.row] as? String == ""{
                return 0
            }else{
                return UITableViewAutomaticDimension
            }
        }
        else{
            
            if arrayRowTitle[indexPath.row] as? String == ""{
                return 0
            }else{
                return UITableViewAutomaticDimension
            }
        }
    }

}
