//
//  NewsVC.swift
//  Encore IR
//
//  Created by ravi on 06/05/19.
//  Copyright © 2019 ravi. All rights reserved.
//

import UIKit
import LGSideMenuController
import SDWebImage
import FSPagerView

class NewsTableCellTop: UITableViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblDetail: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var btnBookmark: UIButton!
    
    @IBOutlet weak var btnShare: UIButton!
    
    
    @IBOutlet weak var imgBookmark: UIImageView!
    
    @IBOutlet weak var imgShare: UIImageView!
    
    @IBOutlet weak var imgRibbon: UIImageView!
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        // do your thing

        
        lblTitle.font = UIFont(name: MEDIUEM_FONT, size: GlobalMethods.setThemeFontSize(objectSize: 15.0))
        lblDetail.font = UIFont(name: DINPROLight, size: GlobalMethods.setThemeFontSize(objectSize: 14.0))
        lblDate.font = UIFont(name: DINPROBold, size: GlobalMethods.setThemeFontSize(objectSize: 14.0))
        if appDelegateObj.IsThemeLight == true{
            lblTitle.textColor = wTopTitleFontColor
            lblDetail.textColor = wDetailFontColor
            lblDate.textColor = wDateFontColor
            imgShare.image = UIImage(named: "Wht_Share")
        }
        else{
            
            lblTitle.textColor = TopTitleFontColor
            lblDetail.textColor = TopTitleFontColor
            lblDate.textColor = DateFontColor
            imgShare.image = UIImage(named: "share")
            
        }
        
    }
}


class NewsCollViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblDetail: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var btnBookmark: UIButton!
    
    @IBOutlet weak var pagecontrol: UIPageControl!
    
    
    @IBOutlet weak var imgBookmark: UIImageView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        // do your thing
//        self.imgView.layer.cornerRadius = 10
//        self.imgView.clipsToBounds = true
        
        lblTitle.font = UIFont(name: MEDIUEM_FONT, size: 16)
        lblDetail.font = UIFont(name: DINPROLight, size: 14)
        lblDate.font = UIFont(name: DINPROBold, size: 14)
        if appDelegateObj.IsThemeLight == true{
            
            lblTitle.textColor = wTopTitleFontColor
         
            lblDetail.textColor = wDetailFontColor
         
            lblDate.textColor = wDateFontColor
       
            
            
        }
        else{
            
            lblTitle.textColor = TopTitleFontColor
     
            lblDetail.textColor = TopTitleFontColor
      
            lblDate.textColor = DateFontColor
      

        }
    }
    
}

class NewsTableCell: UITableViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblDetail: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var btnBookmark: UIButton!
    
    @IBOutlet weak var btnShare: UIButton!
    
    @IBOutlet weak var imgBookmark: UIImageView!
    
    @IBOutlet weak var imgShare: UIImageView!
    
    @IBOutlet weak var imgRibbon: UIImageView!
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        // do your thing
        self.imgView.layer.cornerRadius = 10
        self.imgView.clipsToBounds = true
        
        lblTitle.font = UIFont(name: MEDIUEM_FONT, size: 16)
        lblDetail.font = UIFont(name: DINPROLight, size: 14)
        lblDate.font = UIFont(name: DINPROBold, size: 14)
        
        if appDelegateObj.IsThemeLight == true{
            
            lblTitle.textColor = wTopTitleFontColor
        
            lblDetail.textColor = wDetailFontColor
            lblDate.textColor = wDateFontColor
      
            imgShare.image = UIImage(named: "Wht_Share")
        }
        else{
            
            lblTitle.textColor = TopTitleFontColor
            
            lblDetail.textColor = TopTitleFontColor
         
            lblDate.textColor = DateFontColor
       
            imgShare.image = UIImage(named: "share")
            
        }
    }
    
}

class NewsTableCellRev: UITableViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnBookmark: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var imgBookmark: UIImageView!
    @IBOutlet weak var imgShare: UIImageView!
    @IBOutlet weak var imgRibbonRight: UIImageView!
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        // do your thing
        self.imgView.layer.cornerRadius = 10
        self.imgView.clipsToBounds = true
        lblTitle.font = UIFont(name: MEDIUEM_FONT, size: 16)
        lblDetail.font = UIFont(name: DINPROLight, size: 14)
        lblDate.font = UIFont(name: DINPROBold, size: 14)
        
        if appDelegateObj.IsThemeLight == true{
            
            lblTitle.textColor = wTopTitleFontColor
         
            lblDetail.textColor = wDetailFontColor
        
            lblDate.textColor = wDateFontColor
    
            imgShare.image = UIImage(named: "Wht_Share")
        }
        else{
            
            lblTitle.textColor = TopTitleFontColor
            
            lblDetail.textColor = TopTitleFontColor
  
            lblDate.textColor = DateFontColor 
            imgShare.image = UIImage(named: "share")
            
        }
        
    }
    
}


class NewsVC: UIViewController, UITableViewDataSource,UITableViewDelegate, UISearchBarDelegate {

    //MARK:- Outlets

    @IBOutlet weak var viewTop: UIView!
    
    
    @IBOutlet weak var lblTopTitle: UILabel!
    
    @IBOutlet weak var viewSearch: UISearchBar!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var imgSearch: UIImageView!

    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var imgViewBackground: UIImageView!
    @IBOutlet weak var objTableView: UITableView!
    @IBOutlet weak var lblCounterNews: UILabel!
    @IBOutlet weak var lblCounterAssets: UILabel!
    @IBOutlet weak var lblCounterDeals: UILabel!
    @IBOutlet weak var lblCounterLibrary: UILabel!
    @IBOutlet weak var viewFooter: UIView!
    @IBOutlet weak var imgFooterBG: UIImageView!
    @IBOutlet weak var imgNewsFooter: UIImageView!
    @IBOutlet weak var imgAssetFooter: UIImageView!
    @IBOutlet weak var imgDealFooter: UIImageView!
    @IBOutlet weak var imgLibFooter: UIImageView!
    @IBOutlet weak var imgLoginBackground: UIImageView!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet var imgMenu: UIImageView!
    
    
    
    //MARK:- Variable
    var imgCollection = [UIImage]()
    var strPage = ""
    var arrDealDetail = [AssetProject]()
    var arrNews_detail = [Subnews_detail]()
    var objRefreshControl = UIRefreshControl()
    var pageAdded = 0
    var isPagingNeed = true
    var TotalFeed = 1
    
    var arrNews_detailCopy = [Subnews_detail]()
    var textFieldInsideSearchBar = UITextField()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //isShowSplashScreen
        if appDelegateObj.isShowSplashScreen{
            
            UIView.animate(withDuration: 2, delay: 0.0, options: [], animations: {
                self.imgLogo.isHidden = false
                self.imgLoginBackground.isHidden = false
                
                
            }) { (finished: Bool) in
                self.imgLogo.isHidden = true
                self.imgLoginBackground.isHidden = true
            }
            
            appDelegateObj.isShowSplashScreen = false
            
        }
        else{
            self.imgLogo.isHidden = true
            self.imgLoginBackground.isHidden = true
        }
        
       // self.objTableView.tableHeaderView = viewCollectionHeader
        objTableView.tableHeaderView = UIView()
        self.setupInitialView()
        
        objRefreshControl.attributedTitle = NSAttributedString(string: "")
        objRefreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        objRefreshControl.tintColor = UIColor.clear
        objTableView.addSubview(objRefreshControl)
        
        NotificationCenter.default.addObserver(self, selector: #selector(refresh), name: NSNotification.Name(rawValue: kRefreshNews), object: nil)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        self.getUpdateCounter()

        if appDelegateObj.IsThemeLight == true{
           
            imgViewBackground.image = UIImage(named: "Wht_background")
            self.view.backgroundColor = wviewBgDefaultThemeColor
            lblTopTitle.textColor = wTopTitleFontColor
            //lblTopTitle.font = UIFont(name: wRubic, size: 20)
            imgSearch.image = UIImage(named: "searchBlue")
            imgMenu.image = UIImage(named: "menuBlue")
            imgFooterBG.image = UIImage(named: "Wht_FooterBG")
            imgNewsFooter.image = UIImage(named: "Wht_NewsSelected")
            imgAssetFooter.image = UIImage(named: "Wht_Asset")
            imgDealFooter.image = UIImage(named: "Wht_Deal")
            imgLibFooter.image = UIImage(named: "Wht_Library")
            
            lblCounterNews.textColor = UIColor.black
            lblCounterNews.backgroundColor = counterBgColor
            
            lblCounterAssets.textColor = UIColor.black
            lblCounterAssets.backgroundColor = counterBgColor
            
            lblCounterDeals.textColor = UIColor.black
            lblCounterDeals.backgroundColor = counterBgColor
            
            lblCounterLibrary.textColor = UIColor.black
            lblCounterLibrary.backgroundColor = counterBgColor
            
            textFieldInsideSearchBar.textColor = wBlueMainTitleColor

        }
        else{
            print("Dark Theme mode")
            self.view.backgroundColor = TabViewColor
            imgViewBackground.image = UIImage(named: "background")
            lblTopTitle.textColor = TopTitleFontColor
          //  lblTopTitle.font = UIFont(name: MEDIUEM_FONT, size: 20)
            imgSearch.image = UIImage(named: "Search-1")
            imgMenu.image = UIImage(named: "menu")
            imgFooterBG.image = UIImage(named: "footer-background")
            imgNewsFooter.image = UIImage(named: "News-Selected")
            imgAssetFooter.image = UIImage(named: "My-Assets-icon")
            imgDealFooter.image = UIImage(named: "New-Deals-icon")
            imgLibFooter.image = UIImage(named: "Library-icon")
            
            lblCounterNews.textColor = UIColor.black
            lblCounterNews.backgroundColor = counterBgColor
            
            lblCounterAssets.textColor = UIColor.black
            lblCounterAssets.backgroundColor = counterBgColor
            
            lblCounterDeals.textColor = UIColor.black
            lblCounterDeals.backgroundColor = counterBgColor
            
            lblCounterLibrary.textColor = UIColor.black
            lblCounterLibrary.backgroundColor = counterBgColor
            
            textFieldInsideSearchBar.textColor = UIColor.white
            
        }

        objTableView.reloadData()
      //  objCollectionView.reloadData()
        
        self.setupFontSize()

    }
    
    override func viewWillLayoutSubviews() {
        
        
        //New Notification Counter
        if appDelegateObj.ObjCounter.new_news != "0" {
            lblCounterNews.isHidden = false
            lblCounterNews.text = appDelegateObj.ObjCounter.new_news
            
        }else{
            lblCounterNews.isHidden = true
        }
       
        lblCounterNews.layer.cornerRadius = lblCounterNews.frame.width/2
        lblCounterNews.layer.masksToBounds = true
        
        
        if appDelegateObj.ObjCounter.new_asset != "0" {
            lblCounterAssets.isHidden = false
            lblCounterAssets.text = appDelegateObj.ObjCounter.new_asset
            
        }else{
            lblCounterAssets.isHidden = true
        }
        lblCounterAssets.text = appDelegateObj.ObjCounter.new_asset
       
        lblCounterAssets.layer.cornerRadius = lblCounterAssets.frame.width/2
        lblCounterAssets.layer.masksToBounds = true
        
        if appDelegateObj.ObjCounter.new_deal != "0" {
            lblCounterDeals.isHidden = false
            lblCounterDeals.text = appDelegateObj.ObjCounter.new_deal
            
        }else{
            lblCounterDeals.isHidden = true
        }
        lblCounterDeals.text = appDelegateObj.ObjCounter.new_deal
        
        lblCounterDeals.layer.cornerRadius = lblCounterDeals.frame.width/2
        lblCounterDeals.layer.masksToBounds = true
        
        if appDelegateObj.ObjCounter.new_docs != "0" {
            lblCounterLibrary.isHidden = false
            lblCounterLibrary.text = appDelegateObj.ObjCounter.new_docs
            
        }else{
            lblCounterLibrary.isHidden = true
        }
        lblCounterLibrary.text = appDelegateObj.ObjCounter.new_docs
       
        lblCounterLibrary.layer.cornerRadius = lblCounterLibrary.frame.width/2
        lblCounterLibrary.layer.masksToBounds = true
        
    }
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
//        self.view.backgroundColor = TabViewColor
        
//        self.view.backgroundColor = viewBgDefaultThemeColor
//        self.viewFooter.backgroundColor = viewFooterDefaultThemeColor
        
        objTableView.estimatedRowHeight = 300
        objTableView.rowHeight = UITableViewAutomaticDimension

        objTableView.tableFooterView = UIView()
        
        strPage = "0"
        
        imgCollection = [UIImage(named: "MainNews"),UIImage(named: "MainNews"),UIImage(named: "MainNews"),UIImage(named: "MainNews"),UIImage(named: "MainNews")] as! [UIImage]
        
        self.getNewsFeed()
        
        //        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Right", style: .plain, target: self, action: #selector(showRightView(sender:)))
        
//        objCollectionView.isPagingEnabled = true
        
        self.getProfileDetail()

//        let nc = NotificationCenter.default
//        nc.post(name: Notification.Name(kPostUpdateSideMenuProfile), object: nil)
//
//        let nc1 = NotificationCenter.default
//        nc1.post(name: Notification.Name(kGetProfileUpdate), object: nil)
        
        
        viewSearch.isHidden = true
        
        textFieldInsideSearchBar = viewSearch.value(forKey: "searchField") as! UITextField
        textFieldInsideSearchBar.font = UIFont(name: D_DIN, size: 15)
        
        
    }
    
    func setupFontSize(){
        lblTopTitle.font = lblTopTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 20.0))
       
    }

    
    @objc func refresh(_ sender: Any) {
        //  your code to refresh tableView
        print("Refresh start")
        
        self.getNewsFeed()

        
        objRefreshControl.endRefreshing()

    }
    
    @objc func btnBookmarkTbl(sender:UIButton)  {
        
        var strFlagPass = "0"
//        if arrNews_detail[sender.tag].is_bookmark != nil {
//            strFlagPass = "0"
//        }
        
        appDelegateObj.strPostId = arrNews_detail[sender.tag].n_id
        appDelegateObj.strPostTypeId = "1" //news type = 1

        if arrNews_detail[sender.tag].is_bookmark == "1"
        {
            strFlagPass = "0"
            self.removeBookmarkApi(strPostId: appDelegateObj.strPostId, strPostTypeId: appDelegateObj.strPostTypeId)
        }
        else if arrNews_detail[sender.tag].is_bookmark == "0"
        {
            strFlagPass = "1"
            self.addBookmarkApi(strPostId: appDelegateObj.strPostId, strPostTypeId: appDelegateObj.strPostTypeId)

        }
        
        arrNews_detail[sender.tag].is_bookmark = strFlagPass
        
        DispatchQueue.main.async(execute: {
//            let indexPath = IndexPath(item: sender.tag, section: 0)
//            self.objTableView.reloadRows(at: [indexPath], with: .automatic)
            self.objTableView.reloadData()
        })
        
    }

    @objc func btnShareTbl(sender:UIButton)  {
                
        SDWebImageManager.shared.loadImage(
            with: URL(string: arrNews_detail[sender.tag].n_image),
            options: .highPriority,
            progress: nil) { (image, data, error, cacheType, isFinished, imageUrl) in
                print(isFinished)
                let text = self.arrNews_detail[sender.tag].n_headline
                let textDetail = self.arrNews_detail[sender.tag].n_p_dec

                let shareAll = [image! , text, textDetail] as [Any]
                let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                self.present(activityViewController, animated: true, completion: nil)
                
        }
    
        
        
    }
    
   
    
    @objc func btnShareTblColl(sender:UIButton)  {
        
//        var imgPath = String()
//        imgPath = arrDealDetail[sender.tag].a_asset_img
//
//        let text = imgPath
//        let textShare = [ text ]
//        let activityViewController = UIActivityViewController(activityItems: textShare , applicationActivities: nil)
//        activityViewController.popoverPresentationController?.sourceView = self.view
//        self.present(activityViewController, animated: true, completion: nil)
        
        SDWebImageManager.shared.loadImage(
            with: URL(string: arrDealDetail[sender.tag].a_asset_img),
            options: .highPriority,
            progress: nil) { (image, data, error, cacheType, isFinished, imageUrl) in
                print(isFinished)
                let text = self.arrDealDetail[sender.tag].a_name
                let textDetail = self.arrDealDetail[sender.tag].a_description
                
                let shareAll = [image! , text, textDetail] as [Any]
                let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                self.present(activityViewController, animated: true, completion: nil)
                
        }
    }

    
    //MARK:- IBActions
    @IBAction func btnMyAssetsPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 1
    }
    @IBAction func btnMyNewDealsPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 2
    }
    @IBAction func btnLibraryPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 3
    }
    
    
    @IBAction func btnMenuPressed(_ sender: Any) {
        showLeftViewAnimated(sender)
    }
    
    @IBAction func btnSearchPressed(_ sender: Any) {
        if self.arrNews_detail.count > 0{
            lblTopTitle.isHidden = true
            viewSearch.isHidden = false
            imgSearch.isHidden = true
            btnSearch.isHidden = true
          //  objTableView.tableHeaderView = UIView()
            arrNews_detailCopy = self.arrNews_detail
        }
    }
    
    //MARK:- UISearchbar Methods
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        self.closeSearchView()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        viewSearch.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.filterTextArray(withString: searchText)
    }
    
    func filterTextArray(withString searchText: String) {
       
        var result = [Subnews_detail]()
       
        result = arrNews_detailCopy.filter{ $0.n_headline.range(of: searchText, options: [.caseInsensitive]) != nil
            || $0.n_p_dec.range(of: searchText, options: [.caseInsensitive]) != nil }
        
        print(result.count)
        arrNews_detail = result
        objTableView.reloadData()
    }
    func closeSearchView(){
        viewSearch.endEditing(true)
        lblTopTitle.isHidden = false
        viewSearch.isHidden = true
        imgSearch.isHidden = false
        btnSearch.isHidden = false
    //    self.objTableView.tableHeaderView = viewCollectionHeader
        self.arrNews_detail = arrNews_detailCopy
        
        objTableView.reloadData()
    }
    
    //MARK:- UITableView Methods
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrNews_detail.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let tablecell = tableView.dequeueReusableCell(withIdentifier: "NewsTableCellTop", for: indexPath) as! NewsTableCellTop
        
        tablecell.lblTitle.text = arrNews_detail[indexPath.row].n_headline
        tablecell.lblTitle.numberOfLines = 2
        
        
        tablecell.lblDetail.text = arrNews_detail[indexPath.row].n_p_dec
        tablecell.lblDetail.numberOfLines = 4
        
        //        tablecell.lblDate.text = arrNews_detail[indexPath.row].n_created_date
        tablecell.lblDate.text = getDateFromTimeStamp(timestamp: Double(arrNews_detail[indexPath.row].n_created_date)!, strFormat: "MMMM dd, yyyy")
        // tablecell.lblDate.textColor = InvestTextColor
        
        let imgPath = arrNews_detail[indexPath.row].n_image
        tablecell.imgView.sd_setImage(with: URL(string: imgPath), completed: nil)
        
        if arrNews_detail[indexPath.row].is_bookmark == "0" {
            if appDelegateObj.IsThemeLight == true{
                tablecell.imgBookmark.image = UIImage(named: "Wht_Bookmark")
            }
            else{
                tablecell.imgBookmark.image = UIImage(named: "Bookmark")
            }
            
        }else{
            
            if appDelegateObj.IsThemeLight == true{
                tablecell.imgBookmark.image = UIImage(named: "Wht_BookmarkFill")
            }
            else{
                tablecell.imgBookmark.image = UIImage(named: "Bookmark-fill")
            }
        }
        
        if arrNews_detail[indexPath.row].is_viewed == "0" {
            tablecell.imgRibbon.isHidden = false
            if appDelegateObj.IsThemeLight == true{
                tablecell.imgRibbon.image = UIImage(named: "Wht_Ribbon-with-text")
            }
            else{
                tablecell.imgRibbon.image = UIImage(named: "Ribbon-with-text")
            }
        }else{
            tablecell.imgRibbon.isHidden = true
        }
        
        
        tablecell.btnBookmark.tag = indexPath.row
        tablecell.btnBookmark.addTarget(self, action: #selector(self.btnBookmarkTbl), for: .touchUpInside)
        
        tablecell.btnShare.tag = indexPath.row
        tablecell.btnShare.addTarget(self, action: #selector(self.btnShareTbl), for: .touchUpInside)
        
        
        tablecell.preservesSuperviewLayoutMargins = false
        tablecell.separatorInset = UIEdgeInsets.zero
        tablecell.layoutMargins = UIEdgeInsets.zero
        
        
        return tablecell
        
      
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        

        
        DispatchQueue.global(qos: .background).async {
            // Background Thread
            if self.arrNews_detail[indexPath.row].is_viewed == "0"{
                
                let copyObj = self.arrNews_detail[indexPath.row]
                copyObj.is_viewed = "1"
                var copyArray = self.arrNews_detail
                copyArray.remove(at: indexPath.row)
                copyArray.insert(copyObj, at: indexPath.row)
                self.arrNews_detail = copyArray
                self.objTableView.reloadData()
                
            }

            
            DispatchQueue.main.async {
                // Run UI Updates or call completion block
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let ObjVC = storyboard.instantiateViewController(withIdentifier: "NewsDetailsVC") as! NewsDetailsVC
                ObjVC.isNewsDetail = true
                ObjVC.objNews_detail = self.arrNews_detail[indexPath.row]
                self.navigationController?.pushViewController(ObjVC, animated: true)

            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if self.viewSearch.isHidden == true{
            if indexPath.row == arrNews_detail.count - 3 {
                isPagingNeed = false
            }
            
            if !isPagingNeed && TotalFeed > 9 {
    
                isPagingNeed = true
                
                self.pageAdded += 10
                
                let strNewPage = "\(pageAdded)"
                print("page added \(pageAdded)")
                print("TotalFeed \(TotalFeed)  \(arrNews_detail.count)")
                
                self.getMoreNewsFeed(strMorePage: strNewPage)
              
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) { // Change `2.0` to the desired number of seconds.
                
                if self.arrNews_detail.count > indexPath.row{
                    if self.arrNews_detail[indexPath.row].is_viewed == "0" {
                        
                        let copyObj = self.arrNews_detail[indexPath.row]
                        copyObj.is_viewed = "1"
                        var copyArray = self.arrNews_detail
                        copyArray.remove(at: indexPath.row)
                        copyArray.insert(copyObj, at: indexPath.row)
                        self.arrNews_detail = copyArray
                        self.objTableView.reloadData()
                        
                        self.IsReadPostApi(strPostId: self.arrNews_detail[indexPath.row].n_id, strPostTypeId: "2")
                        
                    }
                }
            }
        }
    }
}
