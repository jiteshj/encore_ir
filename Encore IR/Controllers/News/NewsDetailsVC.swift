//
//  NewsDetailsVC.swift
//  Encore IR
//
//  Created by ravi on 09/05/19.
//  Copyright © 2019 ravi. All rights reserved.
//

import UIKit
import SDWebImage
import SafariServices
import AVKit
import Photos
import ImageSlideshow

class NewsDetailsVC: UIViewController, UIPopoverControllerDelegate, UIAlertViewDelegate, UIPopoverPresentationControllerDelegate, UIScrollViewDelegate {

    //MARK:- Outlets

    @IBOutlet weak var imgBackground: UIImageView!
    
    
    @IBOutlet weak var viewTop: UIView!
    
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var imgBookmarkTop: UIImageView!
    
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var btnChangeFont: UIButton!
    
    @IBOutlet weak var btnBookmark: UIButton!
    
    @IBOutlet weak var btnShare: UIButton!
    
    @IBOutlet weak var imgFontChange: UIImageView!
    
    @IBOutlet weak var imgNewsdetail: UIImageView!
    
    @IBOutlet weak var lblTitleNews: UILabel!

    @IBOutlet weak var btnReadMore: UIButton!
    
    @IBOutlet weak var txtDetailNews: UILabel!
    
    @IBOutlet weak var viewContainer: UIView!
    
    @IBOutlet weak var btnMinFont: UIButton!
    
    @IBOutlet weak var btnMaxFont: UIButton!
    
    @IBOutlet weak var objScrollview: UIScrollView!
    
    @IBOutlet weak var viewNewsDetail: UIView!
    
    @IBOutlet weak var btnPlayVideo: UIButton!
    
    @IBOutlet weak var viewVideoPlay: UIView!
    
    @IBOutlet weak var imgPlayVideo: UIImageView!
    
    @IBOutlet weak var imgBackButton: UIImageView!
    
    @IBOutlet weak var imgShareButton: UIImageView!
    
    @IBOutlet weak var imgContainerBG: UIImageView!

    
    //MARK:- Variable
    var objDealDetail = SubDeal_detail()
    var objNews_detail = Subnews_detail()
    var isNewsDetail = true
    var imgCollection = [UIImage]()
    var imgPass = UIImage()

    var fontsizeTitle = 16
    var fontsizeDetail = 14
    var strN_id = ""
    
    var myPlayer = AVPlayer()
    
    var slideshowTransitioningDelegate: ZoomAnimatedTransitioningDelegate? = nil

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //FOUR-STORY PARKING GARAGE FOR BEDFORD PLAZA OK’ D, NEW CINEMA FINALIZING PLANS

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imgNewsdetail.isUserInteractionEnabled = true
        imgNewsdetail.addGestureRecognizer(tapGestureRecognizer)

        if strN_id != ""{
            self.newsDetailApi(strNId: strN_id)
        }else{
            self.setupInitialView()
        }
    }

    override func viewDidAppear(_ animated: Bool) {

        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            let contentHeight = self.txtDetailNews.bounds.height + self.lblTitleNews.bounds.height + self.imgNewsdetail.bounds.height + self.btnReadMore.bounds.height + 100
            
            self.objScrollview.contentSize = CGSize(width: self.view.frame.size.width, height: contentHeight)
        }
       
    }
    
    override func viewDidLayoutSubviews()
    {
      //  objScrollview.delegate = self
        
        let contentHeight = txtDetailNews.bounds.height + lblTitleNews.bounds.height + imgNewsdetail.bounds.height + btnReadMore.bounds.height + 100
        
        objScrollview.contentSize = CGSize(width: self.view.frame.size.width, height: contentHeight)
        
    }
    
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        
//        txtDetailNews.isEditable = false
//        viewVideoPlay.backgroundColor = PlayVideoThumbColor
        
        lblTitleNews.numberOfLines = 0
        lblTitleNews.font = UIFont(name: MEDIUEM_FONT, size: CGFloat(fontsizeTitle))
        
        txtDetailNews.font = UIFont(name: DINPRO, size: CGFloat(fontsizeDetail))
        txtDetailNews.numberOfLines = 0
        

        if isNewsDetail == true{
            
            self.IsReadPostApi(strPostId: objNews_detail.n_id, strPostTypeId: "2")
            
            lblTitleNews.text = objNews_detail.n_headline.capitalizedFirst()
            txtDetailNews.text = objNews_detail.n_p_dec
            
            
            if objNews_detail.is_bookmark == "1"{
//                imgBookmarkTop.image = UIImage.init(named: "Bookmark-fill")

                if appDelegateObj.IsThemeLight == true{
                    imgBookmarkTop.image = UIImage(named: "Wht_BookmarkFill")
                }
                else{
                    imgBookmarkTop.image = UIImage(named: "Bookmark-fill")
                }

            }else{
//                imgBookmarkTop.image = UIImage.init(named: "Bookmark")
                if appDelegateObj.IsThemeLight == true{
                    imgBookmarkTop.image = UIImage(named: "Wht_Bookmark")
                }
                else{
                    imgBookmarkTop.image = UIImage(named: "Bookmark")
                }
                
            }

            let imgPath = objNews_detail.n_image
            imgNewsdetail.sd_setImage(with: URL(string: imgPath), completed: nil)

            if objNews_detail.n_link != "" {
                btnReadMore.isHidden = false
            }else{
                btnReadMore.isHidden = true
            }
            
            if objNews_detail.n_video != "" || objNews_detail.n_video_link != ""{
                viewVideoPlay.isHidden = false
            }else{
                viewVideoPlay.isHidden = true
            }
            
            if appDelegateObj.IsThemeLight == true{
                print("Ligth Theme mode")
                imgBackground.image = UIImage(named: "Wht_background")
                self.view.backgroundColor = wviewBgDefaultThemeColor
                lblTopTitle.textColor = wBlueMainTitleColor
             //   lblTopTitle.font = UIFont(name: wRubic, size: 20)
                
                imgBackButton.image = UIImage(named: "Wht_Back")
                imgShareButton.image = UIImage(named: "Wht_Share")
                
                imgContainerBG.image = UIImage(named: "Wht_LargeFontBG")
                imgFontChange.image = UIImage(named: "Wht_LargeFont")
                
                
                let titleString = NSMutableAttributedString(string: strReadMore)
                let fontBig = UIFont(name: DINPRO, size: CGFloat(fontsizeDetail))
                titleString.addAttribute(NSAttributedStringKey.font, value: fontBig!, range: NSMakeRange(0, strReadMore.count))
                titleString.addAttribute(NSAttributedStringKey.foregroundColor, value: wDetailFontColor, range: NSMakeRange(0, strReadMore.count))
                
                titleString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSMakeRange(0, strReadMore.count))
                btnReadMore.setAttributedTitle(titleString, for: .normal)
                btnReadMore.titleLabel!.adjustsFontSizeToFitWidth = true
                
                lblTitleNews.textColor = wBlueMainTitleColor
                txtDetailNews.textColor = wDetailFontColor
                
                
                btnMinFont.setTitleColor(wBlueMainTitleColor, for: .normal)
                btnMaxFont.setTitleColor(wBlueMainTitleColor, for: .normal)
        
                viewVideoPlay.backgroundColor = PlayVideoThumbColor
                
            }
            else{
                print("Dark Theme mode")
                self.view.backgroundColor = TabViewColor
                imgBackground.image = UIImage(named: "background")
                lblTopTitle.textColor = TopTitleFontColor
              //  lblTopTitle.font = UIFont(name: MEDIUEM_FONT, size: 20)
                
                imgBackButton.image = UIImage(named: "Back")
                imgShareButton.image = UIImage(named: "share")
                
                imgContainerBG.image = UIImage(named: "LargeFontBG")
                imgFontChange.image = UIImage(named: "FontBig")
                
                let titleString = NSMutableAttributedString(string: strReadMore)
                let fontBig = UIFont(name: DINPRO, size: CGFloat(fontsizeDetail))
                titleString.addAttribute(NSAttributedStringKey.font, value: fontBig!, range: NSMakeRange(0, strReadMore.count))
                titleString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.white, range: NSMakeRange(0, strReadMore.count))
                
                titleString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSMakeRange(0, strReadMore.count))
                btnReadMore.setAttributedTitle(titleString, for: .normal)
                btnReadMore.titleLabel!.adjustsFontSizeToFitWidth = true
                
                lblTitleNews.textColor = DarkThemefontColor
                txtDetailNews.textColor = DarkThemefontColor
                
                btnMinFont.setTitleColor(UIColor.white, for: .normal)
                btnMaxFont.setTitleColor(UIColor.white, for: .normal)
              
                
                viewVideoPlay.backgroundColor = PlayVideoThumbColor
                
                
            }
            
        }
        else{
            
//            self.IsReadPostApi(strPostId: objDealDetail.a_id, strPostTypeId: "0")
//
//            lblTitleNews.text = objDealDetail.a_name.uppercased()
//
//            if objDealDetail.is_bookmark == "1"{
//                imgBookmarkTop.image = UIImage.init(named: "Bookmark-fill")
//
//            }else{
//                imgBookmarkTop.image = UIImage.init(named: "Bookmark")
//            }
//
//            txtDetailNews.text = objDealDetail.a_description
//
//            let imgPath = objDealDetail.a_asset_img
//            imgNewsdetail.sd_setImage(with: URL(string: imgPath), completed: nil)

        }
        
       
        viewContainer.isHidden = true
        
        self.setupFontSize()

    }
    
    func setupFontSize(){
        lblTopTitle.font = lblTopTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 20.0))
        lblTitleNews.font = lblTopTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 16.0))
        txtDetailNews.font = txtDetailNews.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 16.0))
        
        btnReadMore.titleLabel?.font = UIFont(name:DINPRO, size: GlobalMethods.setThemeFontSize(objectSize: 15.0))
        
        btnMinFont.titleLabel?.font = UIFont(name:DINPRO, size: GlobalMethods.setThemeFontSize(objectSize: 16.0))
        btnMaxFont.titleLabel?.font = UIFont(name:DINPRO, size: GlobalMethods.setThemeFontSize(objectSize: 25.0))
    }

    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        
      /*  self.imgCollection.removeAll()

        // Your action
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ObjVC = storyboard.instantiateViewController(withIdentifier: "FullScreenImageVC") as! FullScreenImageVC
        
        imgPass = imgNewsdetail.image!
        imgCollection.append(imgPass)
        
        ObjVC.arrSlide = imgCollection
        //        self.navigationController?.pushViewController(ObjVC, animated: true)
        
        ObjVC.modalPresentationStyle = .overCurrentContext
        present(ObjVC, animated: false, completion: nil)*/
        
        
        var arryModel = [Model]()
        arryModel.append(Model(image: imgNewsdetail.image!, title: ""))
        
        let fullScreenController = FullScreenSlideshowViewController()
        
        fullScreenController.closeButton.layer.cornerRadius = fullScreenController.closeButton.bounds.height/2
        fullScreenController.closeButton.layer.borderWidth = 1
        fullScreenController.closeButton.layer.borderColor = UIColor.darkGray.cgColor
        fullScreenController.closeButton.backgroundColor = UIColor.lightGray
        fullScreenController.closeButton.layer.masksToBounds = true
        
        if appDelegateObj.IsThemeLight == true{
            fullScreenController.backgroundColor = wviewBgDefaultThemeColor
        }
        else{
            fullScreenController.backgroundColor = TabViewColor
        }
        
        fullScreenController.inputs = arryModel.map { $0.inputSource }
        fullScreenController.initialPage = 0
       
        slideshowTransitioningDelegate = ZoomAnimatedTransitioningDelegate(imageView: imgNewsdetail, slideshowController: fullScreenController)
            fullScreenController.transitioningDelegate = slideshowTransitioningDelegate
       
        fullScreenController.slideshow.currentPageChanged = { [weak self] page in
            self?.slideshowTransitioningDelegate?.referenceImageView = self!.imgNewsdetail
        }
        
        present(fullScreenController, animated: true, completion: nil)

    }
    
    //MARK:- IBActions

    @IBAction func btnBackPressed(_ sender: Any) {
//self.dismiss(animated: true, completion: nil)
        
        
        self.navigationController?.popViewController(animated: true)

    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController!) -> UIModalPresentationStyle {
        // Return no adaptive presentation style, use default presentation behaviour
        return .none
    }
    
    @IBAction func btnChangeFontPressed(_ sender: Any) {
        
        if viewContainer.isHidden == true {
            viewContainer.isHidden = false
        } else {
            viewContainer.isHidden = true
        }
        

    }
    
    
    @IBAction func btnBookmarkPressed(_ sender: Any) {
        
        
        if isNewsDetail == true{
            
            appDelegateObj.strPostId = objNews_detail.n_id
            appDelegateObj.strPostTypeId = "1" //news type = 1
            
            if objNews_detail.is_bookmark == "1"
            {
                self.removeBookmarkApi(strPostId: appDelegateObj.strPostId, strPostTypeId: appDelegateObj.strPostTypeId)
                btnBookmark.setImage(UIImage(named: "Bookmark"), for: .normal)
                objNews_detail.is_bookmark = "0"
            }
            else if objNews_detail.is_bookmark == "0"
            {
                self.addBookmarkApi(strPostId: appDelegateObj.strPostId, strPostTypeId: appDelegateObj.strPostTypeId)
                btnBookmark.setImage(UIImage(named: "Bookmark-fill"), for: .normal)
                objNews_detail.is_bookmark = "1"
            }
            
            
        }
        else{
            
            appDelegateObj.strPostId = objDealDetail.a_id
            appDelegateObj.strPostTypeId = objDealDetail.n_type
            
            if objDealDetail.is_bookmark == "1"
            {
                self.removeBookmarkApi(strPostId: appDelegateObj.strPostId, strPostTypeId: appDelegateObj.strPostTypeId)
                btnBookmark.setImage(UIImage(named: "Bookmark"), for: .normal)
                objDealDetail.is_bookmark = "0"
            }
            else if objDealDetail.is_bookmark == "0"
            {
                self.addBookmarkApi(strPostId: appDelegateObj.strPostId, strPostTypeId: appDelegateObj.strPostTypeId)
                btnBookmark.setImage(UIImage(named: "Bookmark-fill"), for: .normal)
                objDealDetail.is_bookmark = "1"
            }
            
        }
        
        
        self.setupInitialView()

        let nc = NotificationCenter.default
        nc.post(name: Notification.Name(kRefreshNews), object: nil)

    }
    
    
    @IBAction func btnSharePressed(_ sender: Any) {
        
        if isNewsDetail == true{
            

            
            SDWebImageManager.shared.loadImage(
                with: URL(string: objNews_detail.n_image),
                options: .highPriority,
                progress: nil) { (image, data, error, cacheType, isFinished, imageUrl) in
                    print(isFinished)
                    let text = self.objNews_detail.n_headline
                    let textDetail = self.objNews_detail.n_p_dec
                    
                    let shareAll = [image! , text, textDetail] as [Any]
                    let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = self.view
                    self.present(activityViewController, animated: true, completion: nil)
                    
            }
            
        }
        else{
            
            SDWebImageManager.shared.loadImage(
                with: URL(string: objDealDetail.a_asset_img),
                options: .highPriority,
                progress: nil) { (image, data, error, cacheType, isFinished, imageUrl) in
                    print(isFinished)
                    let text = self.objDealDetail.a_name
                    let textDetail = self.objDealDetail.a_description
                    
                    let shareAll = [image! , text, textDetail] as [Any]
                    let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = self.view
                    self.present(activityViewController, animated: true, completion: nil)
                    
            }
            
        }
        
        
    }
    
    
    
    @IBAction func btnReadMorePressed(_ sender: Any) {
        print("read more...")
        
        var strURL = objNews_detail.n_link
//        var strURL = "abd.com"

        
        if strURL.lowercased().hasPrefix("http://")==false && strURL.lowercased().hasPrefix("https://")==false{
            strURL = "http://".appendingFormat(strURL)
        }
        
        let url = URL(string: strURL)
        
        let svc = SFSafariViewController(url: url!)
        present(svc, animated: true, completion: nil)
        
        
    }
    
    
    @IBAction func btnMinFontPressed(_ sender: Any) {
        print("text--")
        
        let contentHeight = txtDetailNews.frame.height + lblTitleNews.frame.height + imgNewsdetail.frame.height + btnReadMore.frame.height + 400
        
        objScrollview.contentSize = CGSize(width: self.view.frame.size.width, height: contentHeight)


        if fontsizeTitle < Int(GlobalMethods.setThemeFontSize(objectSize: 16.0)){
            return
        }
        else{
            fontsizeTitle = fontsizeTitle - 3
            fontsizeDetail = fontsizeDetail - 3
        }
        
        lblTitleNews.font = UIFont(name: MEDIUEM_FONT, size: GlobalMethods.setThemeFontSize(objectSize: CGFloat(fontsizeTitle)))
        txtDetailNews.font = UIFont(name: DINPRO, size: GlobalMethods.setThemeFontSize(objectSize: CGFloat(fontsizeDetail)))
        
        //lblTitleNews.font = UIFont(name: MEDIUEM_FONT, size: CGFloat(fontsizeTitle))
       // txtDetailNews.font = UIFont(name: DINPRO, size: CGFloat(fontsizeDetail))
        
        if appDelegateObj.IsThemeLight == true{
            
            let titleString = NSMutableAttributedString(string: strReadMore)
            let fontBig = UIFont(name: DINPRO, size: CGFloat(fontsizeDetail))
            titleString.addAttribute(NSAttributedStringKey.font, value: fontBig!, range: NSMakeRange(0, strReadMore.count))
            titleString.addAttribute(NSAttributedStringKey.foregroundColor, value: wDetailFontColor, range: NSMakeRange(0, strReadMore.count))
            
            titleString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSMakeRange(0, strReadMore.count))
            btnReadMore.setAttributedTitle(titleString, for: .normal)
            btnReadMore.titleLabel!.adjustsFontSizeToFitWidth = true

            
        }
        else{
            let titleString = NSMutableAttributedString(string: strReadMore)
            let fontBig = UIFont(name: DINPRO, size: CGFloat(fontsizeDetail))
            titleString.addAttribute(NSAttributedStringKey.font, value: fontBig!, range: NSMakeRange(0, strReadMore.count))
            titleString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.white, range: NSMakeRange(0, strReadMore.count))
            
            titleString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSMakeRange(0, strReadMore.count))
            btnReadMore.setAttributedTitle(titleString, for: .normal)
            btnReadMore.titleLabel!.adjustsFontSizeToFitWidth = true

        }
        
        
        
    }
    
    
    @IBAction func btnMaxFontPressed(_ sender: Any) {
       

        let contentHeight = txtDetailNews.frame.height + lblTitleNews.frame.height + imgNewsdetail.frame.height + btnReadMore.frame.height + 400
        
        objScrollview.contentSize = CGSize(width: self.view.frame.size.width, height: contentHeight)

        print("\(txtDetailNews.frame.height)")
        

        if fontsizeTitle > Int(GlobalMethods.setThemeFontSize(objectSize: 25.0)){
            return
        }
        else{
            fontsizeTitle = fontsizeTitle + 3
            fontsizeDetail = fontsizeDetail + 3
        }
        
        lblTitleNews.font = UIFont(name: MEDIUEM_FONT, size: GlobalMethods.setThemeFontSize(objectSize: CGFloat(fontsizeTitle)))
        txtDetailNews.font = UIFont(name: DINPRO, size: GlobalMethods.setThemeFontSize(objectSize: CGFloat(fontsizeDetail)))
        
//        lblTitleNews.font = UIFont(name: MEDIUEM_FONT, size: CGFloat(fontsizeTitle))
//        txtDetailNews.font = UIFont(name: DINPRO, size: CGFloat(fontsizeDetail))
        
        if appDelegateObj.IsThemeLight == true{
            
            let titleString = NSMutableAttributedString(string: strReadMore)
            let fontBig = UIFont(name: DINPRO, size: CGFloat(fontsizeDetail))
            titleString.addAttribute(NSAttributedStringKey.font, value: fontBig!, range: NSMakeRange(0, strReadMore.count))
            titleString.addAttribute(NSAttributedStringKey.foregroundColor, value: wDetailFontColor, range: NSMakeRange(0, strReadMore.count))
            
            titleString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSMakeRange(0, strReadMore.count))
            btnReadMore.setAttributedTitle(titleString, for: .normal)
            btnReadMore.titleLabel!.adjustsFontSizeToFitWidth = true
            
            
        }
        else{
            let titleString = NSMutableAttributedString(string: strReadMore)
            let fontBig = UIFont(name: DINPRO, size: CGFloat(fontsizeDetail))
            titleString.addAttribute(NSAttributedStringKey.font, value: fontBig!, range: NSMakeRange(0, strReadMore.count))
            titleString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.white, range: NSMakeRange(0, strReadMore.count))
            
            titleString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSMakeRange(0, strReadMore.count))
            btnReadMore.setAttributedTitle(titleString, for: .normal)
            btnReadMore.titleLabel!.adjustsFontSizeToFitWidth = true
            
        }
        
    }
    
    
    @IBAction func btnPlayVideoPressed(_ sender: Any) {
        
        var stringArray = [String]()
        
        if objNews_detail.n_video != "" {
            stringArray = objNews_detail.n_video.components(separatedBy: ".")
            
            var isValidVideo = true
            if stringArray.count > 1{
                if stringArray[1] == "3gp"{
                    isValidVideo = false
                }
            }
            
            if isValidVideo == true{
                var vidURL = ""
                vidURL = "\(objNews_detail.n_video)"
                print(vidURL)
                
                let videoURL = URL(string: vidURL)
                myPlayer = AVPlayer(url: videoURL!)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = myPlayer
                let topController = UIApplication.topViewController()
                topController?.present(playerViewController, animated: true, completion: {
                    playerViewController.player!.play()
                })
            }
            else
            {
                self.ShowAlertDisplay(titleObj: strAppName, messageObj: "This video not supported", viewcontrolelr: self)
            }

        }
        else if objNews_detail.n_video_link != ""{
            stringArray = objNews_detail.n_video_link.components(separatedBy: ".")
            
            var isValidVideo = true
            if stringArray.count > 1{
                if stringArray[1] == "3gp"{
                    isValidVideo = false
                }
            }
            
            if isValidVideo == true{
                var vidURL = ""
                vidURL = "\(objNews_detail.n_video_link)"
                print(vidURL)
                
                let videoURL = URL(string: vidURL)
                myPlayer = AVPlayer(url: videoURL!)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = myPlayer
                let topController = UIApplication.topViewController()
                topController?.present(playerViewController, animated: true, completion: {
                    playerViewController.player!.play()
                })
            }
            else
            {
                self.ShowAlertDisplay(titleObj: strAppName, messageObj: "This video not supported", viewcontrolelr: self)
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: "This video not supported", viewcontrolelr: self)
            return
        }
        
        
        
    }
    
}
