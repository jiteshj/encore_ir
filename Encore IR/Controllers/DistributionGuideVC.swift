//
//  DistributionGuideVC.swift
//  EncoreIR
//
//  Created by Trivedi Sagar on 5/14/19.
//  Copyright © 2019 ravi. All rights reserved.
//

import UIKit
import ImageSlideshow

class DistributionGuideVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDelegate, UITableViewDataSource {
    
    //MARK:- Outlets
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var imgFontBackgroung: UIImageView!
    @IBOutlet weak var imgFontBig: UIImageView!
    @IBOutlet weak var btnFontLarge: UIButton!
    @IBOutlet weak var objCollectionAssets: UICollectionView!
    @IBOutlet weak var viewCollection: UIView!
    @IBOutlet weak var tblDetail: UITableView!
    
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var btnMinFont: UIButton!
    @IBOutlet weak var btnMaxFont: UIButton!

    
    //MARK:- Variable
    var arrayRowTitle = NSMutableArray()
    var objSection = ObjSectionDetail()
    var IsGroupDis = false
    var arrImage = [UIImage]()
    var strChartName = ""
    var strImageUrl = ""
    var IsPDf = false
    var arrPdf = NSMutableArray()
    var fontsizeDetail : CGFloat = 0

    var slideshowTransitioningDelegate: ZoomAnimatedTransitioningDelegate? = nil
    
    //MARK:- UIView Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
//        tblDetail.tableHeaderView = viewCollection
    
//        arrayRowTitle = ["DESCRIPTION","DEVELOPMENT TIMELINE","PROPOSED EXIT STRATEGY"]
        self.setupInitialView()
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.setUpThemeColor()
    }
    
    //MARK:- Class Method and Function
    func setupInitialView(){
        lblTopTitle.text  = strDistribuGuidance
        
        tblDetail.estimatedRowHeight = 200
        tblDetail.rowHeight = UITableViewAutomaticDimension
        
        self.AssetDisGuidenceApi(strAId: objSection.a_id, strSection_isDynamicTF: objSection.section_isDynamic, strSection_id: objSection.section_id, strC_type: objSection.c_type, strIs_group: objSection.is_group)

        self.viewContainer.isHidden = true
        
    }
    func setUpThemeColor(){
        if appDelegateObj.IsThemeLight == true{
            
            print("Ligth Theme mode")
            imgBackground.image = UIImage(named: "Wht_background")
            imgBack.image = UIImage(named: "Wht_Back")
            self.view.backgroundColor = wviewBgDefaultThemeColor
            lblTopTitle.textColor = wTopTitleFontColor
            
            imgFontBackgroung.image = UIImage(named: "Wht_LargeFontBG")
            imgFontBig.image = UIImage(named: "Wht_LargeFont")
            
            btnMinFont.setTitleColor(wBlueMainTitleColor, for: .normal)
            btnMaxFont.setTitleColor(wBlueMainTitleColor, for: .normal)
            
        }
        else{
            print("Dark Theme mode")
            self.view.backgroundColor = TabViewColor
            imgBackground.image = UIImage(named: "background")
            imgBack.image = UIImage(named: "Back")
            lblTopTitle.textColor = TopTitleFontColor
            
            imgFontBackgroung.image = UIImage(named: "LargeFontBG")
            imgFontBig.image = UIImage(named: "FontBig")
            
            btnMinFont.setTitleColor(UIColor.white, for: .normal)
            btnMaxFont.setTitleColor(UIColor.white, for: .normal)
            
        }
        self.setupFontSize()
    }
    func setupFontSize(){
        lblTopTitle.font = lblTopTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 20.0))
        btnMinFont.titleLabel?.font = UIFont(name:DINPRO, size: GlobalMethods.setThemeFontSize(objectSize: 16.0))
        btnMaxFont.titleLabel?.font = UIFont(name:DINPRO, size: GlobalMethods.setThemeFontSize(objectSize: 25.0))
        
        fontsizeDetail = GlobalMethods.setThemeFontSize(objectSize: 0.0)
    }
    @objc func btnDocTbl(sender:UIButton)  {
        
        if IsPDf == false{
            
          /*  // Your action
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ObjVC = storyboard.instantiateViewController(withIdentifier: "FullScreenImageVC") as! FullScreenImageVC
            
            print(arrImage)
            ObjVC.arrSlide = arrImage
            //        self.navigationController?.pushViewController(ObjVC, animated: true)
            
            ObjVC.modalPresentationStyle = .overCurrentContext
            present(ObjVC, animated: false, completion: nil)*/
            
            
            var arryModel = [Model]()
            
            for element in arrImage {
                //imageLabel.image = element
                arryModel.append(Model(image: element, title: ""))
            }
            
            
            let fullScreenController = FullScreenSlideshowViewController()
            
            fullScreenController.closeButton.layer.cornerRadius = fullScreenController.closeButton.bounds.height/2
            fullScreenController.closeButton.layer.borderWidth = 1
            fullScreenController.closeButton.layer.borderColor = UIColor.darkGray.cgColor
            fullScreenController.closeButton.backgroundColor = UIColor.lightGray
            fullScreenController.closeButton.layer.masksToBounds = true
            
            if appDelegateObj.IsThemeLight == true{
                fullScreenController.backgroundColor = wviewBgDefaultThemeColor
            }
            else{
                fullScreenController.backgroundColor = TabViewColor
            }
            
            fullScreenController.inputs = arryModel.map { $0.inputSource }
            
            
            fullScreenController.initialPage = 0
            
            
            
            
            present(fullScreenController, animated: true, completion: nil)
            
        }else{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ObjVC = storyboard.instantiateViewController(withIdentifier: "DocumentVC") as! DocumentVC
            
            print(arrPdf)
            ObjVC.strUrlPath = arrPdf.lastObject as! String
            ObjVC.strDocTitle = strChartName.firstUppercased
            self.navigationController?.pushViewController(ObjVC, animated: true)
            
        }
        
        
    }
    
    func displayDisGuidence(value : DistGuidenceDetail){
        
        
        if IsGroupDis == true{
            
            let strOne = value.d_disclaimer
            let strTwo = value.d_general_update
            let strThree = value.d_distribution_guidance
            let strFour = value.d_praposed_stratagy
            let strFive = value.d_footnotes
            
            self.arrayRowTitle.removeAllObjects()
            
            self.arrayRowTitle.add(strOne)
            self.arrayRowTitle.add(strTwo)
            self.arrayRowTitle.add(strThree)
            self.arrayRowTitle.add(strFour)
            self.arrayRowTitle.add(strFive)

            
            self.tblDetail.reloadData()
            self.strChartName = value.d_chart_img.doc_name //(responseValue?.data.recent_update.q_chart_imgs.doc_name)!
            self.strImageUrl =  value.d_chart_img.document    //(responseValue?.data.recent_update.q_chart_imgs.document)!
            
            if self.strImageUrl != ""{
                let fullNameArr = self.strImageUrl.components(separatedBy: "||")
                self.arrImage.removeAll()
                self.arrPdf.removeAllObjects()

                for img in fullNameArr {
                    
                    let url = URL(string:img)
                    
                    let strExtension = url?.pathExtension
                    if strExtension == "pdf"{
                        self.IsPDf = true
                        self.arrPdf.add(img)
                    }else{
//                        let data = try? Data(contentsOf: url!)
//                        let image: UIImage = UIImage(data: data!)!
//                        self.arrImage.append(image)
                        if let data = try? Data(contentsOf: url!) {
                            let image: UIImage = UIImage(data: data)!
                            self.arrImage.append(image)
                            
                        }
                    }
                    
                }
            }
        }
        else{
            
            let strOne = value.d_disclaimer
            let strTwo = value.d_general_update
            let strThree = value.d_development_timeline
            let strFour = value.d_praposed_stratagy
            let strFive = value.d_distribution_guidance
            let strSix = value.d_footnotes

            self.arrayRowTitle.removeAllObjects()
            
            self.arrayRowTitle.add(strOne)
            self.arrayRowTitle.add(strTwo)
            self.arrayRowTitle.add(strThree)
            self.arrayRowTitle.add(strFour)
            self.arrayRowTitle.add(strFive)
            self.arrayRowTitle.add(strSix)

            
            self.tblDetail.reloadData()
            self.strChartName = value.d_chart_img.doc_name //(responseValue?.data.recent_update.q_chart_imgs.doc_name)!
            self.strImageUrl =  value.d_chart_img.document    //(responseValue?.data.recent_update.q_chart_imgs.document)!
            
            if self.strImageUrl != ""{
                let fullNameArr = self.strImageUrl.components(separatedBy: "||")
                self.arrImage.removeAll()
                self.arrPdf.removeAllObjects()
                for img in fullNameArr {
                    
                    let url = URL(string:img)
                    
                    let strExtension = url?.pathExtension
                    if strExtension?.lowercased() == "pdf"{
                        self.IsPDf = true
                        self.arrPdf.add(img)
                    }else{
//                        let data = try? Data(contentsOf: url!)
//                        let image: UIImage = UIImage(data: data!)!
//                        self.arrImage.append(image)
                        if let data = try? Data(contentsOf: url!) {
                            let image: UIImage = UIImage(data: data)!
                            self.arrImage.append(image)
                            
                        }
                    }
                    
                }
            }
            
        }
        
        
    }
    
    //MARK:- IBActions
    @IBAction func btnBackSel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnLargeFontPressed(_ sender: Any) {
        
        if viewContainer.isHidden == true {
            viewContainer.isHidden = false
        } else {
            viewContainer.isHidden = true
        }
    }
    
    
    @IBAction func btnMinFontPressed(_ sender: Any) {
        print("font --\(fontsizeDetail)")
        
        if fontsizeDetail < 0 {
            //            fontsizeTitle = 15
            //            fontsizeDetail = 13
        }
        else{
            fontsizeDetail = fontsizeDetail - 3
            tblDetail.reloadData()
        }
        
    }
    
    
    @IBAction func btnMaxFontPressed(_ sender: Any) {
        print("font ++\(fontsizeDetail)")
        
        if fontsizeDetail > 6 {
            //            fontsizeTitle = 15
            //            fontsizeDetail = 13
        }
        else{
            fontsizeDetail = fontsizeDetail + 3
            tblDetail.reloadData()
        }
        
    }
    
    
    //MARK:- UICollection Delegate Method
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellAssetDetail", for: indexPath) as! cellAssetDetail
        Cell.imgNews.image = UIImage(named: "MainNews")
        Cell.pageControl.numberOfPages = 5
        Cell.btnCounter.setTitle("+25", for: .normal)
        return Cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellAssetDetail", for: indexPath) as! cellAssetDetail
        Cell.pageControl.currentPage = indexPath.row
    }
    
    //MARK:- UITableView Methods
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if IsGroupDis == true{
            return arrayRowTitle.count

        }else{
            return arrayRowTitle.count
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if IsGroupDis == true{
            if indexPath.row == 0{
                let tablecell = tableView.dequeueReusableCell(withIdentifier: "cellQuotes", for: indexPath) as! cellQuotes
                
                tablecell.lblTitle.text =  arrayRowTitle[indexPath.row] as? String
                tablecell.lblTitle.numberOfLines = 0
                tablecell.lblTitle.font = UIFont(name: DINPROBItalic, size: CGFloat(13 + fontsizeDetail))
                
                if appDelegateObj.IsThemeLight{
                    tablecell.viewBack.backgroundColor = UIColor.white
                    tablecell.lblTitle.textColor = wBlueMainTitleColor
                }else{
                    tablecell.viewBack.backgroundColor = kCellBackColor
                    tablecell.lblTitle.textColor = UIColor.white
                }
                
                
                tablecell.constLblLeading.constant = 8
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    tablecell.isHidden = true
                }
                
                return tablecell
                
            }
            else if indexPath.row == 1{
                
                let tablecell = tableView.dequeueReusableCell(withIdentifier: "cellQuotes", for: indexPath) as! cellQuotes
                
                tablecell.lblTitle.text =  arrayRowTitle[indexPath.row] as? String
                tablecell.lblTitle.numberOfLines = 0
                tablecell.lblTitle.font = UIFont(name: DINPROBold, size: CGFloat(13 + fontsizeDetail))
                
                if appDelegateObj.IsThemeLight{
                    tablecell.viewBack.backgroundColor = UIColor.white
                    tablecell.lblTitle.textColor = wBlueMainTitleColor
                }else{
                    tablecell.viewBack.backgroundColor = kCellBackColor
                    tablecell.lblTitle.textColor = UIColor.white
                }

                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    tablecell.isHidden = true
                }
                
                return tablecell
                
            }
            else if indexPath.row == 2{
                
                let tablecell1 = tableView.dequeueReusableCell(withIdentifier: "cellRecentUpdate", for: indexPath) as! cellRecentUpdate
                
                tablecell1.lblTitle.text = strDistGuidance
                
                tablecell1.lblDetail.text = arrayRowTitle[indexPath.row] as? String
                tablecell1.lblDetail.numberOfLines = 0
                
                tablecell1.lblTitle.font = UIFont(name: DINPROBold, size: CGFloat(14 + fontsizeDetail))
                tablecell1.lblDetail.font = UIFont(name: MEDIUEM_FONT, size: CGFloat(14 + fontsizeDetail))
                
                if appDelegateObj.IsThemeLight{
                    tablecell1.viewBack.layer.borderColor = wBlueMainTitleColor.cgColor
                    tablecell1.viewBack.backgroundColor = UIColor.white
                    tablecell1.lblTitle.textColor = wBlueMainTitleColor
                    tablecell1.lblDetail.textColor = wDarkGrayColor
                    tablecell1.viewChartBG.backgroundColor = wBlueMainTitleColor
                }else{
                    tablecell1.viewBack.layer.borderColor = kCellBackColor.cgColor
                    tablecell1.viewBack.backgroundColor = kCellBackColor
                    tablecell1.lblTitle.textColor = UIColor.white
                    tablecell1.lblDetail.textColor = UIColor.white
                    tablecell1.viewChartBG.backgroundColor = PdfBGColor
                }
                
                
                if arrImage.count != 0 || arrPdf.count != 0{
                    tablecell1.btnChart.tag = indexPath.row
                    tablecell1.btnChart.addTarget(self, action: #selector(self.btnDocTbl), for: .touchUpInside)
                    
                    strChartName = strDistGuidance
                    
                    tablecell1.viewChartHeight.constant = 44
                    tablecell1.viewChart.isHidden = false
                    tablecell1.isHidden = false

                }
                else{
                    tablecell1.viewChartHeight.constant = 0
                    tablecell1.viewChart.isHidden = true
                    
                    if arrayRowTitle[indexPath.row] as? String == ""{
                        tablecell1.isHidden = true
                    }
                    else{
                        tablecell1.isHidden = false
                    }
                }

                
                return tablecell1
                
            }
            else if indexPath.row == 3{
                
                let tablecell1 = tableView.dequeueReusableCell(withIdentifier: "cellRecentUpdate", for: indexPath) as! cellRecentUpdate
                
                tablecell1.lblTitle.text = strPropExitStategy
                
                tablecell1.lblDetail.text = arrayRowTitle[indexPath.row] as? String
                tablecell1.lblDetail.numberOfLines = 0
                
                tablecell1.lblTitle.font = UIFont(name: DINPROBold, size: CGFloat(14 + fontsizeDetail))
                tablecell1.lblDetail.font = UIFont(name: MEDIUEM_FONT, size: CGFloat(14 + fontsizeDetail))

                if appDelegateObj.IsThemeLight{
                    tablecell1.viewBack.layer.borderColor = wBlueMainTitleColor.cgColor
                    tablecell1.viewBack.backgroundColor = UIColor.white
                    tablecell1.lblTitle.textColor = wBlueMainTitleColor
                    tablecell1.lblDetail.textColor = wDarkGrayColor
                    tablecell1.viewChartBG.backgroundColor = wBlueMainTitleColor
                }else{
                    tablecell1.viewBack.layer.borderColor = kCellBackColor.cgColor
                    tablecell1.viewBack.backgroundColor = kCellBackColor
                    tablecell1.lblTitle.textColor = UIColor.white
                    tablecell1.lblDetail.textColor = UIColor.white
                    tablecell1.viewChartBG.backgroundColor = PdfBGColor
                }
                
                tablecell1.viewChartHeight.constant = 0
                tablecell1.viewChart.isHidden = true
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    tablecell1.isHidden = true
                }
                else{
                    tablecell1.isHidden = false
                }
                return tablecell1
                
            }
            else if indexPath.row == 4{
               
                let tablecell = tableView.dequeueReusableCell(withIdentifier: "cellFinTerm", for: indexPath) as! cellFinTerm
                tablecell.lblTitle.text = arrayRowTitle[indexPath.row] as? String
                tablecell.lblTitle.font = UIFont(name: DINPROBItalic, size: CGFloat(13 + fontsizeDetail))
                tablecell.lblTitle.numberOfLines = 0
                
                if appDelegateObj.IsThemeLight{
                    
                    tablecell.viewBack.backgroundColor = UIColor.white
                    tablecell.lblTitle.textColor = wDarkGrayColor
                    
                }else{
                   
                    tablecell.viewBack.backgroundColor = kCellBackColor
                    tablecell.lblTitle.textColor = UIColor.white
                    
                }
                
                return tablecell
                
            }

            else{
                let tablecell = tableView.dequeueReusableCell(withIdentifier: "cellFinTerm", for: indexPath) as! cellFinTerm
                
                if appDelegateObj.IsThemeLight{
                    
                    tablecell.viewBack.backgroundColor = UIColor.white
                    tablecell.lblTitle.textColor = wDarkGrayColor
                    
                }else{
                    
                    tablecell.viewBack.backgroundColor = kCellBackColor
                    tablecell.lblTitle.textColor = UIColor.white
                    
                }
                return tablecell
            }
            
            
        }
        else{
            if indexPath.row == 0{
                let tablecell = tableView.dequeueReusableCell(withIdentifier: "cellQuotes", for: indexPath) as! cellQuotes
                
                tablecell.lblTitle.text =  arrayRowTitle[indexPath.row] as? String
                tablecell.lblTitle.numberOfLines = 0
                tablecell.lblTitle.font = UIFont(name: DINPROBItalic, size: CGFloat(13 + fontsizeDetail))
                
                tablecell.viewBack.backgroundColor = UIColor.clear
                tablecell.constLblLeading.constant = 8
                
                if appDelegateObj.IsThemeLight{
                    tablecell.viewBack.backgroundColor = UIColor.white
                    tablecell.lblTitle.textColor = wBlueMainTitleColor
                }else{
                    tablecell.viewBack.backgroundColor = kCellBackColor
                    tablecell.lblTitle.textColor = UIColor.white
                }
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    tablecell.isHidden = true
                }
                
                return tablecell
                
            }
            else if indexPath.row == 1{
                
                let tablecell = tableView.dequeueReusableCell(withIdentifier: "cellQuotes", for: indexPath) as! cellQuotes
                
                tablecell.lblTitle.text =  arrayRowTitle[indexPath.row] as? String
                tablecell.lblTitle.numberOfLines = 0
                tablecell.lblTitle.font = UIFont(name: DINPROBold, size: CGFloat(13 + fontsizeDetail))
                
                if appDelegateObj.IsThemeLight{
                    tablecell.viewBack.backgroundColor = UIColor.white
                    tablecell.lblTitle.textColor = wBlueMainTitleColor
                }else{
                    tablecell.viewBack.backgroundColor = kCellBackColor
                    tablecell.lblTitle.textColor = UIColor.white
                }
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    tablecell.isHidden = true
                }
                
                return tablecell
                
            }
            else if indexPath.row == 2{
                
                let tablecell = tableView.dequeueReusableCell(withIdentifier: "cellRecentUpdate", for: indexPath) as! cellRecentUpdate
                
                tablecell.lblTitle.text = strDevTimeline
                tablecell.lblDetail.text = arrayRowTitle[indexPath.row] as? String
                tablecell.lblDetail.numberOfLines = 0
                
                tablecell.lblTitle.font = UIFont(name: DINPROBold, size: CGFloat(14 + fontsizeDetail))
                tablecell.lblDetail.font = UIFont(name: MEDIUEM_FONT, size: CGFloat(14 + fontsizeDetail))
                
                if appDelegateObj.IsThemeLight{
                    tablecell.viewBack.layer.borderColor = wBlueMainTitleColor.cgColor
                    tablecell.viewBack.backgroundColor = UIColor.white
                    tablecell.lblTitle.textColor = wBlueMainTitleColor
                    tablecell.lblDetail.textColor = wDarkGrayColor
                    tablecell.viewChartBG.backgroundColor = wBlueMainTitleColor
                }else{
                    tablecell.viewBack.layer.borderColor = kCellBackColor.cgColor
                    tablecell.viewBack.backgroundColor = kCellBackColor
                    tablecell.lblTitle.textColor = UIColor.white
                    tablecell.lblDetail.textColor = UIColor.white
                    tablecell.viewChartBG.backgroundColor = PdfBGColor
                }
                
                
                if arrImage.count != 0 || arrPdf.count != 0{
                    tablecell.btnChart.tag = indexPath.row
                    tablecell.btnChart.addTarget(self, action: #selector(self.btnDocTbl), for: .touchUpInside)
                    
                    strChartName = strDevTimeline
                    
                    tablecell.viewChartHeight.constant = 44
                    tablecell.viewChart.isHidden = false
                    tablecell.isHidden = false

                }
                else{
                    tablecell.viewChartHeight.constant = 0
                    tablecell.viewChart.isHidden = true
                    
                    if arrayRowTitle[indexPath.row] as? String == ""{
                        tablecell.isHidden = true
                    }
                    else{
                        tablecell.isHidden = false
                    }
                }
                
                return tablecell
                
            }
            else if indexPath.row == 3{
                
                let tablecell1 = tableView.dequeueReusableCell(withIdentifier: "cellRecentUpdate", for: indexPath) as! cellRecentUpdate
                
                tablecell1.lblTitle.text = strPropExitStategy
                
                tablecell1.lblDetail.text = arrayRowTitle[indexPath.row] as? String
                tablecell1.lblDetail.numberOfLines = 0
                
                tablecell1.lblTitle.font = UIFont(name: DINPROBold, size: CGFloat(14 + fontsizeDetail))
                tablecell1.lblDetail.font = UIFont(name: MEDIUEM_FONT, size: CGFloat(14 + fontsizeDetail))
                
                
                if appDelegateObj.IsThemeLight{
                    tablecell1.viewBack.layer.borderColor = wBlueMainTitleColor.cgColor
                    tablecell1.viewBack.backgroundColor = UIColor.white
                    tablecell1.lblTitle.textColor = wBlueMainTitleColor
                    tablecell1.lblDetail.textColor = wDarkGrayColor
                    tablecell1.viewChartBG.backgroundColor = wBlueMainTitleColor
                }else{
                    tablecell1.viewBack.layer.borderColor = kCellBackColor.cgColor
                    tablecell1.viewBack.backgroundColor = kCellBackColor
                    tablecell1.lblTitle.textColor = UIColor.white
                    tablecell1.lblDetail.textColor = UIColor.white
                    tablecell1.viewChartBG.backgroundColor = PdfBGColor
                }
                
                tablecell1.viewChartHeight.constant = 0
                tablecell1.viewChart.isHidden = true
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    tablecell1.isHidden = true
                }
                else{
                    tablecell1.isHidden = false
                }
                return tablecell1
                
            }
            else if indexPath.row == 4{
                
                let tablecell1 = tableView.dequeueReusableCell(withIdentifier: "cellRecentUpdate", for: indexPath) as! cellRecentUpdate
                
                tablecell1.lblTitle.text = strDistGuidance
                
                tablecell1.lblDetail.text = arrayRowTitle[indexPath.row] as? String
                tablecell1.lblDetail.numberOfLines = 0
                
                tablecell1.lblTitle.font = UIFont(name: DINPROBold, size: CGFloat(14 + fontsizeDetail))
                tablecell1.lblDetail.font = UIFont(name: MEDIUEM_FONT, size: CGFloat(14 + fontsizeDetail))
                
                if appDelegateObj.IsThemeLight{
                    tablecell1.viewBack.layer.borderColor = wBlueMainTitleColor.cgColor
                    tablecell1.viewBack.backgroundColor = UIColor.white
                    tablecell1.lblTitle.textColor = wBlueMainTitleColor
                    tablecell1.lblDetail.textColor = wDarkGrayColor
                    tablecell1.viewChartBG.backgroundColor = wBlueMainTitleColor
                }else{
                    tablecell1.viewBack.layer.borderColor = kCellBackColor.cgColor
                    tablecell1.viewBack.backgroundColor = kCellBackColor
                    tablecell1.lblTitle.textColor = UIColor.white
                    tablecell1.lblDetail.textColor = UIColor.white
                    tablecell1.viewChartBG.backgroundColor = PdfBGColor
                }
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    tablecell1.isHidden = true
                }
                else{
                    tablecell1.isHidden = false
                }
                
                tablecell1.viewChartHeight.constant = 0
                tablecell1.viewChart.isHidden = true
                
                return tablecell1
                
            }
            else if indexPath.row == 5{
                
                let tablecell = tableView.dequeueReusableCell(withIdentifier: "cellFinTerm", for: indexPath) as! cellFinTerm
                tablecell.lblTitle.text = arrayRowTitle[indexPath.row] as? String
                tablecell.lblTitle.font = UIFont(name: DINPROBItalic, size: CGFloat(13 + fontsizeDetail))
                tablecell.lblTitle.numberOfLines = 0
                
                if appDelegateObj.IsThemeLight{
                    
                    tablecell.viewBack.backgroundColor = UIColor.white
                    tablecell.lblTitle.textColor = wDarkGrayColor
                    
                }else{
                    
                    tablecell.viewBack.backgroundColor = kCellBackColor
                    tablecell.lblTitle.textColor = UIColor.white
                    
                }
                
                return tablecell
                
            }
            else{
                let tablecell = tableView.dequeueReusableCell(withIdentifier: "cellFinTerm", for: indexPath) as! cellFinTerm
                
                if appDelegateObj.IsThemeLight{
                    
                    tablecell.viewBack.backgroundColor = UIColor.white
                    tablecell.lblTitle.textColor = wDarkGrayColor
                    
                }else{
                   
                    tablecell.viewBack.backgroundColor = kCellBackColor
                    tablecell.lblTitle.textColor = UIColor.white
                    
                }
                
                return tablecell
            }
            
            
        }
        
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if IsGroupDis == true{
            if indexPath.row == 0{
                
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    return 0
                }else{
                    return UITableViewAutomaticDimension
                }
                
            }
            else if indexPath.row == 1{
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    return 0
                }else{
                    return UITableViewAutomaticDimension
                }
                
            }
            else if indexPath.row == 2{
                
                if arrayRowTitle[indexPath.row] as? String == "" && arrImage.count == 0 && arrPdf.count == 0{
                    return 0
                }else{
                    return UITableViewAutomaticDimension
                }
                
            }else if indexPath.row == 3{
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    return 0
                }else{
                    return UITableViewAutomaticDimension
                }
                
            }else if indexPath.row == 4{
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    return 0
                }else{
                    return UITableViewAutomaticDimension
                }
                
            }
            else{
                return UITableViewAutomaticDimension
            }
            
            
        }
        else{
            if indexPath.row == 0{
                if arrayRowTitle[indexPath.row] as? String == ""{
                    return 0
                }else{
                    return UITableViewAutomaticDimension
                }
                
            }
            else if indexPath.row == 1{
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    return 0
                }else{
                    return UITableViewAutomaticDimension
                }
                
            }
            else if indexPath.row == 2{
                
                if arrayRowTitle[indexPath.row] as? String == "" && arrImage.count == 0 && arrPdf.count == 0{
                    return 0
                }else{
                    return UITableViewAutomaticDimension
                }
                
            }
            else if indexPath.row == 3{
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    return 0
                }else{
                    return UITableViewAutomaticDimension
                }
                
            }
            else if indexPath.row == 4{
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    return 0
                }else{
                    return UITableViewAutomaticDimension
                }
                
            }
            else if indexPath.row == 5{
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    return 0
                }else{
                    return UITableViewAutomaticDimension
                }
                
            }
            else{
                return UITableViewAutomaticDimension
            }
            
            
        }
        
    }
    
}

extension DistributionGuideVC: UICollectionViewDelegateFlowLayout {
    
    private func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let approximateHeight = collectionView.frame.height
        
        let numberOfCell: CGFloat = 1   //you need to give a type as CGFloat
        let cellWidth = UIScreen.main.bounds.size.width / numberOfCell
        //        return CGSizeMake(cellWidth, cellWidth)
        return CGSize(width: cellWidth, height: approximateHeight)
        
    }
    
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    //        return 0
    //    }
    //
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    //        return 0
    //    }
    //
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    //        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    //    }
    
}
