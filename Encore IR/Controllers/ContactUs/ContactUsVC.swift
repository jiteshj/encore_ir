//
//  ContactUsVC.swift
//  Encore IR
//
//  Created by ravi on 08/05/19.
//  Copyright © 2019 ravi. All rights reserved.
//

import UIKit
import LGSideMenuController
import ImageSlideshow


class ContactUsVC: UIViewController {

    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var viewTop: UIView!
    
    @IBOutlet weak var imgMenu: UIImageView!
    @IBOutlet weak var lblTopTitle: UILabel!
    
    @IBOutlet weak var btnMenu: UIButton!
    
    @IBOutlet weak var viewContactUs: UIView!

    @IBOutlet weak var imgUserBorder: UIImageView!
    
    @IBOutlet weak var lblNameTitle: UILabel!
    
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblContact: UILabel!
    
    @IBOutlet weak var viewContBG: UIView!
    
    @IBOutlet weak var viewMobContact: UIView!
    @IBOutlet weak var viewOfficeContact: UIView!
    
    
    @IBOutlet weak var lblOfficeContact: UILabel!
    
    @IBOutlet weak var lblCounterNews: UILabel!
    @IBOutlet weak var lblCounterAssets: UILabel!
    @IBOutlet weak var lblCounterDeals: UILabel!
    @IBOutlet weak var lblCounterLibrary: UILabel!

    @IBOutlet weak var btnMobCall: UIButton!
    
    @IBOutlet weak var btnOfficeCall: UIButton!
    
    @IBOutlet weak var btnEmail: UIButton!
    
 
    
    @IBOutlet weak var viewSupportBG: UIView!
    @IBOutlet weak var lblSupportContactTitle: UILabel!
    @IBOutlet weak var lblIRTeamTitle: UILabel!
    @IBOutlet weak var lblAppSupportTitle: UILabel!
    @IBOutlet weak var lblIRTeam: UILabel!
    @IBOutlet weak var lblAppSupport: UILabel!
    
    @IBOutlet weak var imgFooterBG: UIImageView!
    @IBOutlet weak var imgNewsFooter: UIImageView!
    @IBOutlet weak var imgAssetFooter: UIImageView!
    @IBOutlet weak var imgDealFooter: UIImageView!
    @IBOutlet weak var imgLibFooter: UIImageView!

    
    var imgCollection = [UIImage]()
    var imgPass = UIImage()
    var slideshowTransitioningDelegate: ZoomAnimatedTransitioningDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.ContactUsDetail), name: NSNotification.Name(rawValue: kContactUsDetails), object: nil)

        
        self.setupInitialView()
        self.tabBarController?.moreNavigationController.isNavigationBarHidden = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imgUserBorder.isUserInteractionEnabled = true
        imgUserBorder.addGestureRecognizer(tapGestureRecognizer)

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getProfileDetail()
        self.setupTheme()
        self.setupFontSize()
    }
    
    override func viewWillLayoutSubviews() {
    
        imgUserBorder.layer.borderWidth = 2
        imgUserBorder.layer.masksToBounds = false
        
        if appDelegateObj.IsThemeLight == true{
            imgUserBorder.layer.borderColor = wBoxBorderColor.cgColor
        }
        else{
            imgUserBorder.layer.borderColor = UIColor.white.cgColor
        }
        imgUserBorder.layer.cornerRadius = imgUserBorder.frame.height/2
        imgUserBorder.clipsToBounds = true
        
        
        //New Notification Counter
        if appDelegateObj.ObjCounter.new_news != "0" {
            lblCounterNews.isHidden = false
            lblCounterNews.text = appDelegateObj.ObjCounter.new_news
            
        }else{
            lblCounterNews.isHidden = true
        }
        
        lblCounterNews.layer.cornerRadius = lblCounterNews.frame.width/2
        lblCounterNews.layer.masksToBounds = true
        
        
        if appDelegateObj.ObjCounter.new_asset != "0" {
            lblCounterAssets.isHidden = false
            lblCounterAssets.text = appDelegateObj.ObjCounter.new_asset
            
        }else{
            lblCounterAssets.isHidden = true
        }
        lblCounterAssets.text = appDelegateObj.ObjCounter.new_asset
        
        lblCounterAssets.layer.cornerRadius = lblCounterAssets.frame.width/2
        lblCounterAssets.layer.masksToBounds = true
        
        if appDelegateObj.ObjCounter.new_deal != "0" {
            lblCounterDeals.isHidden = false
            lblCounterDeals.text = appDelegateObj.ObjCounter.new_deal
            
        }else{
            lblCounterDeals.isHidden = true
        }
        lblCounterDeals.text = appDelegateObj.ObjCounter.new_deal
        
        lblCounterDeals.layer.cornerRadius = lblCounterDeals.frame.width/2
        lblCounterDeals.layer.masksToBounds = true
        
        if appDelegateObj.ObjCounter.new_docs != "0" {
            lblCounterLibrary.isHidden = false
            lblCounterLibrary.text = appDelegateObj.ObjCounter.new_docs
            
        }else{
            lblCounterLibrary.isHidden = true
        }
        lblCounterLibrary.text = appDelegateObj.ObjCounter.new_docs
        
        lblCounterLibrary.layer.cornerRadius = lblCounterLibrary.frame.width/2
        lblCounterLibrary.layer.masksToBounds = true
        
    }
    
    @objc func ContactUsDetail()
    {
        
        let imgPath = appDelegateObj.loginCordDetails.c_image
        imgUserBorder.sd_setImage(with: URL(string: imgPath), placeholderImage: UIImage(named: "userDefault"))

        
        lblNameTitle.text = "\(appDelegateObj.loginCordDetails.c_fname) \(appDelegateObj.loginCordDetails.c_lname)"
        lblSubTitle.text = "(\(strContactDesignation))"

        
        lblEmail.attributedText = NSAttributedString(string: appDelegateObj.loginCordDetails.c_email, attributes:
            [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        
        lblContact.attributedText = NSAttributedString(string: appDelegateObj.loginCordDetails.c_contact, attributes:
            [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        
        lblOfficeContact.attributedText = NSAttributedString(string: appDelegateObj.loginCordDetails.c_o_contact , attributes:
            [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        
        if appDelegateObj.loginCordDetails.c_o_contact != "" {
            viewOfficeContact.isHidden = false
        }else{
            viewOfficeContact.isHidden = true

        }
        
        
    }

    //MARK:- Class Methods and Functions
    func setupInitialView(){
        
        self.getProfileDetail()

        self.view.backgroundColor = TabViewColor

        self.viewContBG.layer.cornerRadius = 5
        self.viewContBG.layer.masksToBounds = true
        
        self.viewSupportBG.layer.cornerRadius = 5
        self.viewSupportBG.layer.masksToBounds = true
        
        
        lblIRTeam.attributedText = NSAttributedString(string: strIrTeamEmail, attributes:
            [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        lblAppSupport.attributedText = NSAttributedString(string: strbccMailReciever, attributes:
            [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        
        
    
    }
    func setupFontSize(){
        
        lblTopTitle.font = lblTopTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 20.0))
        
        lblNameTitle.font = lblNameTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 17.0))
        lblSubTitle.font = lblSubTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 13.0))
        lblEmail.font = lblEmail.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 15.0))
        lblContact.font = lblContact.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 13.0))
        
        lblOfficeContact.font = lblOfficeContact.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 13.0))
        
        lblSupportContactTitle.font = lblSupportContactTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 14.0))
        lblIRTeamTitle.font = lblIRTeamTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 12.0))
        lblAppSupportTitle.font = lblAppSupportTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 12.0))
        lblIRTeam.font = lblIRTeam.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 12.0))
        lblAppSupport.font = lblAppSupport.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 12.0))
       
    }
    func setupTheme(){

        viewContBG.layer.borderWidth = 1
        viewSupportBG.layer.borderWidth = 1
        if appDelegateObj.IsThemeLight == true{
            viewContBG.layer.borderColor = wBoxBorderColor.cgColor
            self.viewContBG.backgroundColor = wviewBgDefaultThemeColor
            
            viewSupportBG.layer.borderColor = wBoxBorderColor.cgColor
            self.viewSupportBG.backgroundColor = wviewBgDefaultThemeColor
        }
        else{
            viewContBG.layer.borderColor = kCellBackColor.cgColor
            self.viewContBG.backgroundColor = kCellBackColor
            
            viewSupportBG.layer.borderColor = kCellBackColor.cgColor
            self.viewSupportBG.backgroundColor = kCellBackColor
        }
        viewContBG.layer.masksToBounds = true
        viewSupportBG.layer.masksToBounds = true
        
        if appDelegateObj.IsThemeLight == true{
            print("Ligth Theme mode")
            
            
            imgBackground.image = UIImage(named: "Wht_background")
            self.view.backgroundColor = wviewBgDefaultThemeColor
            lblTopTitle.textColor = wBlueMainTitleColor
            
            lblCounterNews.textColor = UIColor.black
            lblCounterNews.backgroundColor = counterBgColor
            
            lblCounterAssets.textColor = UIColor.black
            lblCounterAssets.backgroundColor = counterBgColor
            
            lblCounterDeals.textColor = UIColor.black
            lblCounterDeals.backgroundColor = counterBgColor
            
            lblCounterLibrary.textColor = UIColor.black
            lblCounterLibrary.backgroundColor = counterBgColor
            
            lblNameTitle.textColor = wBlueMainTitleColor
            lblSubTitle.textColor = wLightGrayColor
            lblEmail.textColor = wBlueMainTitleColor
            lblContact.textColor = wBlueMainTitleColor
            
            lblSupportContactTitle.textColor = wDarkGrayColor
            lblIRTeamTitle.textColor = wDarkGrayColor
            lblAppSupportTitle.textColor = wDarkGrayColor
            lblIRTeam.textColor = wBlueMainTitleColor
            lblAppSupport.textColor = wBlueMainTitleColor

            imgMenu.image = UIImage(named: "menuBlue")
            imgFooterBG.image = UIImage(named: "Wht_FooterBG")
            imgNewsFooter.image = UIImage(named: "Wht_News")
            imgAssetFooter.image = UIImage(named: "Wht_Asset")
            imgDealFooter.image = UIImage(named: "Wht_Deal")
            imgLibFooter.image = UIImage(named: "Wht_Library")
            
        }
        else{
            print("Dark Theme mode")
           
            
            self.view.backgroundColor = TabViewColor
            imgBackground.image = UIImage(named: "background")
            lblTopTitle.textColor = TopTitleFontColor
            
            lblCounterNews.textColor = UIColor.black
            lblCounterNews.backgroundColor = counterBgColor
            
            lblCounterAssets.textColor = UIColor.black
            lblCounterAssets.backgroundColor = counterBgColor
            
            lblCounterDeals.textColor = UIColor.black
            lblCounterDeals.backgroundColor = counterBgColor
            
            lblCounterLibrary.textColor = UIColor.black
            lblCounterLibrary.backgroundColor = counterBgColor
            
            lblNameTitle.textColor = UIColor.white
            lblSubTitle.textColor = UIColor.white
            lblEmail.textColor = UIColor.white
            lblContact.textColor = UIColor.white
            
            lblSupportContactTitle.textColor = UIColor.white
            lblIRTeamTitle.textColor = UIColor.white
            lblAppSupportTitle.textColor = UIColor.white
            lblIRTeam.textColor = UIColor.white
            lblAppSupport.textColor = UIColor.white
            
            imgMenu.image = UIImage(named: "menu")
            imgFooterBG.image = UIImage(named: "footer-background")
            imgNewsFooter.image = UIImage(named: "News-icon")
            imgAssetFooter.image = UIImage(named: "My-Assets-icon")
            imgDealFooter.image = UIImage(named: "New-Deals-icon")
            imgLibFooter.image = UIImage(named: "Library-icon")
            
        }
        
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        
        self.imgCollection.removeAll()
       
        var arryModel = [Model]()
        arryModel.append(Model(image: imgUserBorder.image!, title: ""))
        
        let fullScreenController = FullScreenSlideshowViewController()
        
        fullScreenController.closeButton.layer.cornerRadius = fullScreenController.closeButton.bounds.height/2
        fullScreenController.closeButton.layer.borderWidth = 1
        fullScreenController.closeButton.layer.borderColor = UIColor.darkGray.cgColor
        fullScreenController.closeButton.backgroundColor = UIColor.lightGray
        fullScreenController.closeButton.layer.masksToBounds = true
        
        if appDelegateObj.IsThemeLight == true{
            fullScreenController.backgroundColor = wviewBgDefaultThemeColor
           // fullScreenController.closeButton.setImage(UIImage.init(named: "Wht_Back"), for: .normal)
        }
        else{
            fullScreenController.backgroundColor = TabViewColor
            //fullScreenController.closeButton.setImage(UIImage.init(named: "Back"), for: .normal)
        }
        
        fullScreenController.inputs = arryModel.map { $0.inputSource }
        fullScreenController.initialPage = 0
        
        slideshowTransitioningDelegate = ZoomAnimatedTransitioningDelegate(imageView: imgUserBorder, slideshowController: fullScreenController)
        fullScreenController.transitioningDelegate = slideshowTransitioningDelegate
        
        fullScreenController.slideshow.currentPageChanged = { [weak self] page in
            self?.slideshowTransitioningDelegate?.referenceImageView = self!.imgUserBorder
        }
        
        present(fullScreenController, animated: true, completion: nil)
        
    }
    
    func openEmail(withEmailId email:String){
        if let url = URL(string: "mailto:\(email)?bcc=\(strbccMailReciever)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    //MARK:- IBActions
    @IBAction func btnMenuPressed(_ sender: Any) {
        showLeftViewAnimated(sender)
    }
    @IBAction func btnNewsPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 0
    }
    @IBAction func btnAssetPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 1
    }
    @IBAction func btnDealPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 2
    }
    
    @IBAction func btnLibraryPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 3
    }
    
    
    @IBAction func btnMobCallPressed(_ sender: Any) {
        
        let call = "\(appDelegateObj.loginCordDetails.c_contact)"
//        if let url = URL(string: "tel:\(call)") {
//            if #available(iOS 10.0, *) {
//                UIApplication.shared.open(url)
//            } else {
//                UIApplication.shared.openURL(url)
//            }
//        }else{
//            print("Fail")
//        }
        call.makeAColl()

    }
    
    @IBAction func btnOfficeCallPressed(_ sender: Any) {
//        if let url = URL(string: "tel://\(lblOfficeContact.text!)"), UIApplication.shared.canOpenURL(url) {
//            if #available(iOS 10, *) {
//                UIApplication.shared.open(url)
//            } else {
//                UIApplication.shared.openURL(url)
//            }
//        }
//        else{
//            print("Fail")
//        }
        let call = "\(appDelegateObj.loginCordDetails.c_o_contact)"

        call.makeAColl()
    }
    
    @IBAction func btnEmailPressed(_ sender: Any) {
        
        let email = "\(appDelegateObj.loginCordDetails.c_email)"

        
        if let url = URL(string: "mailto:\(email)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        
    }
    
    @IBAction func btnIRTeamPressed(_ sender: Any) {
        self.openEmail(withEmailId: strIrTeamEmail)
    }
    @IBAction func btnAppSupportPressed(_ sender: Any) {
        self.openEmail(withEmailId: strbccMailReciever)
    }
    
    
    
}
