//
//  FullScreenImageVC.swift
//  EncoreIR
//
//  Created by ravi on 16/05/19.
//  Copyright © 2019 ravi. All rights reserved.
//

import UIKit
import FSPagerView


class FullScreenImageVC: UIViewController, FSPagerViewDelegate,FSPagerViewDataSource, UIGestureRecognizerDelegate, UIScrollViewDelegate {
    
    //MARK:- Outlets
    @IBOutlet weak var viewSlideShow: FSPagerView!{
        didSet {
            self.viewSlideShow.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
            self.viewSlideShow.itemSize = FSPagerView.automaticSize
        }
    }
    
    @IBOutlet weak var pageControl: FSPageControl!{
        didSet {
            self.pageControl.numberOfPages = self.arrSlide.count
            self.pageControl.contentHorizontalAlignment = .right
            self.pageControl.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
            self.pageControl.hidesForSinglePage = true
        }
    }
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var btnDismissView: UIButton!
    @IBOutlet weak var imgBackButton: UIImageView!

    //MARK:- Variable
    var arrSlide = [UIImage]()
    var imgSlide = UIImage()
    var isFromContctUs = Bool()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInitialView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //
        if appDelegateObj.IsThemeLight == true{
            print("Ligth Theme mode")
            imgBackground.image = UIImage(named: "Wht_background")
            self.view.backgroundColor = wviewBgDefaultThemeColor
            
            imgBackButton.image = UIImage(named: "Wht_Back")
            
        }
        else{
            print("Dark Theme mode")
            self.view.backgroundColor = TabViewColor
            imgBackground.image = UIImage(named: "background")
            
            imgBackButton.image = UIImage(named: "Back")
            
        }
        
    }
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        
        self.pageControl.setStrokeColor(UIColor.gray, for: .normal)
        self.pageControl.setStrokeColor(UIColor.gray, for: .selected)
        self.pageControl.setFillColor(UIColor.gray, for: .selected)
        
        self.pageControl.contentHorizontalAlignment = .center
        
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        imgBackground.alpha = 1.00
        
        self.viewTop.isHidden = false
        
        let pinch : UIPinchGestureRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(self.pinch))
        pinch.delegate = self
        pinch.delaysTouchesBegan = true
       // self.viewSlideShow?.addGestureRecognizer(pinch)
        
    }
    
   
    
    @objc func pinch(sender:UIPinchGestureRecognizer) {
        if sender.state == .began || sender.state == .changed {
            

            sender.view?.transform = sender.view!.transform.scaledBy(x: sender.scale, y: sender.scale)
            
            let transform = sender.view!.transform
            let maxScale: CGFloat = 3 //Anything
            let minScale: CGFloat = 1 //Anything
            let scale = sqrt(transform.a * transform.a + transform.c * transform.c)
            if scale > maxScale {
                sender.view?.transform = sender.view!.transform.scaledBy(x: maxScale / scale, y: maxScale / scale)
            }
            else if scale < minScale {
                sender.view?.transform = sender.view!.transform.scaledBy(x: minScale / scale, y: minScale / scale)
            }
            
            sender.scale = 1

            print("zooom")
        }
    }
    
    @IBAction func handlePan(_ gestureRecognizer: UIPanGestureRecognizer) {
        if gestureRecognizer.state == .began || gestureRecognizer.state == .changed {

            let translation = gestureRecognizer.translation(in: self.view)
            // note: 'view' is optional and need to be unwrapped
            gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x + translation.x, y: gestureRecognizer.view!.center.y + translation.y)
            gestureRecognizer.setTranslation(CGPoint.zero, in: self.view)
        }
        
    }

    
    //MARK:- IBAction
    @IBAction func btnBackPressed(_ sender: Any) {

        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnDismissViewPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)

    }
    
    // MARK:- FSPagerViewDataSource
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return arrSlide.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        
        // @IBOutlet weak var imageViewZoom: EFImageViewZoom!
//        self.imageViewZoom._delegate = self
//        self.imageViewZoom.image = UIImage(named: "14bis.png")
//        self.imageViewZoom.contentMode = .left
        
        
        
        
        cell.imageView?.image =  arrSlide[index]
        
        
        if isFromContctUs{
            cell.imageView?.contentMode = .scaleAspectFill
        }
        else{
            cell.imageView?.contentMode = .scaleToFill
        }
       
        cell.imageView?.clipsToBounds = true
        
        cell.contentView.isUserInteractionEnabled = false

        
        return cell
    }
    
    
    // MARK:- FSPagerView Delegate
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.pageControl.currentPage = targetIndex
    }
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        self.pageControl.currentPage = pagerView.currentIndex
    }
}


