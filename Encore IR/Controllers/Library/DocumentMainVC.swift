//
//  DocumentMainVC.swift
//  Encore Direct
//
//  Created by ravi on 29/06/19.
//  Copyright © 2019 ravi. All rights reserved.
//

import UIKit

class cellDocumentCol: UICollectionViewCell {
   
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var imgDocument: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgRibbon: UIImageView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        // do your thing
        
        
        lblTitle.font = UIFont(name: MEDIUEM_FONT, size: GlobalMethods.setThemeFontSize(objectSize: 16.0))
        
    
        
    }

}

class cellDocumentFolderCol: UICollectionViewCell {
    
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var imgFolder: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var imgRibbon: UIImageView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        // do your thing
        
        
        lblTitle.font = UIFont(name: MEDIUEM_FONT, size: GlobalMethods.setThemeFontSize(objectSize: 16.0))
        lblDetail.font = UIFont(name: DINPROLight, size: GlobalMethods.setThemeFontSize(objectSize: 14.0))
     
        
    }
    
}

class DocumentMainVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDataSource, UITableViewDelegate {

    
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var imgMenu: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgTopLayout: UIImageView!

    @IBOutlet weak var collLibraryObj: UICollectionView!
    @IBOutlet weak var tblLibraryObj: UITableView!

    @IBOutlet weak var lblNoDataFound: UILabel!
    
    
    @IBOutlet weak var lblCounterNews: UILabel!
    @IBOutlet weak var lblCounterAssets: UILabel!
    @IBOutlet weak var lblCounterDeals: UILabel!
    @IBOutlet weak var lblCounterLibrary: UILabel!
    
    @IBOutlet weak var imgFooterBG: UIImageView!
    @IBOutlet weak var imgNewsFooter: UIImageView!
    @IBOutlet weak var imgAssetFooter: UIImageView!
    @IBOutlet weak var imgDealFooter: UIImageView!
    @IBOutlet weak var imgLibFooter: UIImageView!
    
    var isGridLayout = false
    var arrayData = [DocumentMainData]()
    
    var objRefreshControl = UIRefreshControl()
    var refresher = UIRefreshControl()
    

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        //   self.tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        self.title = nil
        
        self.setupInitialView()
        self.collLibraryObj.contentInsetAdjustmentBehavior = .always
        
        collLibraryObj.alwaysBounceVertical = true
        refresher.addTarget(self, action: #selector(refreshStream), for: .valueChanged)
        refresher.tintColor = UIColor.clear
        collLibraryObj.addSubview(refresher)
        
        objRefreshControl.attributedTitle = NSAttributedString(string: "")
        objRefreshControl.addTarget(self, action: #selector(refreshTable), for: .valueChanged)
        objRefreshControl.tintColor = UIColor.clear
        tblLibraryObj.addSubview(objRefreshControl)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getUpdateCounter()
        self.setUpThemeColor()
        DispatchQueue.main.async{
            self.tblLibraryObj.reloadData()
            self.collLibraryObj.reloadData()
        }
        
    }
    
    override func viewWillLayoutSubviews() {
        
        //New Notification Counter
        if appDelegateObj.ObjCounter.new_news != "0" {
            lblCounterNews.isHidden = false
            lblCounterNews.text = appDelegateObj.ObjCounter.new_news
            
        }else{
            lblCounterNews.isHidden = true
        }
        
        lblCounterNews.layer.cornerRadius = lblCounterNews.frame.width/2
        lblCounterNews.layer.masksToBounds = true
        
        
        if appDelegateObj.ObjCounter.new_asset != "0" {
            lblCounterAssets.isHidden = false
            lblCounterAssets.text = appDelegateObj.ObjCounter.new_asset
            
        }else{
            lblCounterAssets.isHidden = true
        }
        lblCounterAssets.text = appDelegateObj.ObjCounter.new_asset
       
        lblCounterAssets.layer.cornerRadius = lblCounterAssets.frame.width/2
        lblCounterAssets.layer.masksToBounds = true
        
        if appDelegateObj.ObjCounter.new_deal != "0" {
            lblCounterDeals.isHidden = false
            lblCounterDeals.text = appDelegateObj.ObjCounter.new_deal
            
        }else{
            lblCounterDeals.isHidden = true
        }
        lblCounterDeals.text = appDelegateObj.ObjCounter.new_deal
       
        lblCounterDeals.layer.cornerRadius = lblCounterDeals.frame.width/2
        lblCounterDeals.layer.masksToBounds = true
        
        if appDelegateObj.ObjCounter.new_docs != "0" {
            lblCounterLibrary.isHidden = false
            lblCounterLibrary.text = appDelegateObj.ObjCounter.new_docs
            
        }else{
            lblCounterLibrary.isHidden = true
        }
        lblCounterLibrary.text = appDelegateObj.ObjCounter.new_docs
       
        lblCounterLibrary.layer.cornerRadius = lblCounterLibrary.frame.width/2
        lblCounterLibrary.layer.masksToBounds = true
        
    }
    
    
    func setUpThemeColor(){
        if appDelegateObj.IsThemeLight == true{
            
            print("Ligth Theme mode")
            imgBackground.image = UIImage(named: "Wht_background")
            
            imgTopLayout.image = UIImage.init(named: "Wht_Grid")
            self.view.backgroundColor = wviewBgDefaultThemeColor
            lblTitle.textColor = wTopTitleFontColor
            
            imgMenu.image = UIImage(named: "menuBlue")
            
            
            imgFooterBG.image = UIImage(named: "Wht_FooterBG")
            imgNewsFooter.image = UIImage(named: "Wht_News")
            imgAssetFooter.image = UIImage(named: "Wht_Asset")
            imgDealFooter.image = UIImage(named: "Wht_Deal")
            imgLibFooter.image = UIImage(named: "Wht_LibrarySelected")
            
            
            lblCounterNews.textColor = UIColor.black
            lblCounterNews.backgroundColor = counterBgColor
            
            lblCounterAssets.textColor = UIColor.black
            lblCounterAssets.backgroundColor = counterBgColor
            
            lblCounterDeals.textColor = UIColor.black
            lblCounterDeals.backgroundColor = counterBgColor
            
            lblCounterLibrary.textColor = UIColor.black
            lblCounterLibrary.backgroundColor = counterBgColor
            
            lblNoDataFound.textColor = UIColor.black
            
            
        }
        else{
            print("Dark Theme mode")
            self.view.backgroundColor = TabViewColor
            imgBackground.image = UIImage(named: "background")
            imgTopLayout.image = UIImage.init(named: "Grid View Icon")
            lblTitle.textColor = TopTitleFontColor
            
            imgMenu.image = UIImage(named: "menu")
            
            
            imgFooterBG.image = UIImage(named: "footer-background")
            imgNewsFooter.image = UIImage(named: "News-icon")
            imgAssetFooter.image = UIImage(named: "My-Assets-icon")
            imgDealFooter.image = UIImage(named: "New-Deals-icon")
            imgLibFooter.image = UIImage(named: "Library-Selected")
            
            lblCounterNews.textColor = UIColor.black
            lblCounterNews.backgroundColor = counterBgColor
            
            lblCounterAssets.textColor = UIColor.black
            lblCounterAssets.backgroundColor = counterBgColor
            
            lblCounterDeals.textColor = UIColor.black
            lblCounterDeals.backgroundColor = counterBgColor
            
            lblCounterLibrary.textColor = UIColor.black
            lblCounterLibrary.backgroundColor = counterBgColor
         
            lblNoDataFound.textColor = UIColor.white
        }
        self.setupFontSize()
    }
    
    
    
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        
        
        self.getDocumentFeed()
        
        
        tblLibraryObj.isHidden = false
        collLibraryObj.isHidden = true
        
        lblNoDataFound.isHidden = true
        
   
    }
    func setupFontSize(){
        lblTitle.font = lblTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 20.0))
        lblNoDataFound.font = lblNoDataFound.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 15.0))
        
    }
    
    @objc func refreshStream() {
        
        print("refresh")
        
        self.getDocumentFeed()
        
        
        refresher.endRefreshing()
        
    }
    
    @objc func refreshTable(_ sender: Any) {
      
        self.getDocumentFeed()
        objRefreshControl.endRefreshing()
        
    }
    
    //MARK:- IBActions
    @IBAction func btnMenuPressed(_ sender: Any) {
        showLeftViewAnimated(sender)
    }
    @IBAction func btnNewsPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 0
    }
    @IBAction func btnMyAssetsPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 1
    }
    @IBAction func btnMyNewDealsPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 2
    }

    @IBAction func btnLayoutChngedPressed(_ sender: Any) {
        
        if isGridLayout == false{
            
            self.setViewHideShowWithAnimarion(view: collLibraryObj, hidden: false)
            self.setViewHideShowWithAnimarion(view: tblLibraryObj, hidden: true)
 
            
            if appDelegateObj.IsThemeLight{
                imgTopLayout.image = UIImage.init(named: "Wht_list_Icon")
            }
            else{
                imgTopLayout.image = UIImage.init(named: "List View Icon")
            }
         
            isGridLayout = true
        }
        else{

            self.setViewHideShowWithAnimarion(view: collLibraryObj, hidden: true)
            self.setViewHideShowWithAnimarion(view: tblLibraryObj, hidden: false)
            
            if appDelegateObj.IsThemeLight{
                imgTopLayout.image = UIImage.init(named: "Wht_Grid")
            }else{
                imgTopLayout.image = UIImage.init(named: "Grid View Icon")
            }
      
            isGridLayout = false
        }
        
    }
    
    //MARK:- UITableView Methods
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayData.count;
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let tablecell = tableView.dequeueReusableCell(withIdentifier: "cellLibrarytbl", for: indexPath) as! cellLibrarytbl
        
        var objModel = DocumentMainData()
        objModel = arrayData[indexPath.row]
    
        if objModel.folder_name != "" {
            if appDelegateObj.IsThemeLight == true{
                tablecell.imgLibrary.image = UIImage(named: "folderBlue")
                tablecell.imgRibbon.image = UIImage(named: "Wht_Ribbon-with-textRight")
            }
            else{
                tablecell.imgLibrary.image = UIImage(named: "Folder")
                tablecell.imgRibbon.image = UIImage(named: "Ribbon-with-textRight")
            }
            
            if objModel.is_new == "0" {
            }else{
                tablecell.imgRibbon.isHidden = true
            }
            
            tablecell.lblTitle.text = objModel.folder_name + " (\(String(objModel.data.count)))"
            
            
        }else{
           
            tablecell.lblTitle.numberOfLines = 0
            
            if appDelegateObj.IsThemeLight == true{
                tablecell.imgLibrary.image = UIImage(named: "fileBlue")
                tablecell.imgRibbon.image = UIImage(named: "Wht_Ribbon-with-textRight")
            }else{
                tablecell.imgLibrary.image = UIImage(named: "text-file-icon")
                tablecell.imgRibbon.image = UIImage(named: "Ribbon-with-textRight")
            }
            
            if objModel.is_viewed == "0" {
                tablecell.imgRibbon.isHidden = false
            }else{
                tablecell.imgRibbon.isHidden = true
            }
    
            tablecell.lblTitle.text = objModel.doc_name
            
        }
        
        return tablecell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        var objModel = DocumentMainData()
        objModel = arrayData[indexPath.row]
        
        if objModel.folder_name != "" {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ObjVC = storyboard.instantiateViewController(withIdentifier: "LibraryVC") as! LibraryVC
            
            ObjVC.arrayData = objModel.data
            ObjVC.strFolderTitle = objModel.folder_name
            
            self.navigationController?.pushViewController(ObjVC, animated: true)
            
        }
        else{
            
            if arrayData[indexPath.row].is_viewed == "0"{
                
                let copyObj = arrayData[indexPath.row]
                copyObj.is_viewed = "1"
                var copyArray = arrayData
                copyArray.remove(at: indexPath.row)
                copyArray.insert(copyObj, at: indexPath.row)
                arrayData = copyArray
                DispatchQueue.main.async{
                    self.collLibraryObj.reloadData()
                }
            }
            
            self.IsReadPostApi(strPostId: objModel.doc_id, strPostTypeId: "3")
            
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ObjVC = storyboard.instantiateViewController(withIdentifier: "DocumentVC") as! DocumentVC
            
            print(objModel.document)
            ObjVC.strUrlPath = objModel.document
            ObjVC.strDocTitle = objModel.doc_name
            
            self.navigationController?.pushViewController(ObjVC, animated: true)
            
        }
    }
    
    
    //MARK:- UICollection Delegate Method
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //Folder
        
        var objModel = DocumentMainData()
        objModel = arrayData[indexPath.row]
        

        if objModel.folder_name != "" {
            
            let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellDocumentFolderCol", for: indexPath) as! cellDocumentFolderCol
            if appDelegateObj.IsThemeLight == true{
                Cell.lblTitle.textColor = wTopTitleFontColor
                Cell.lblDetail.textColor = wDetailFontColor
                Cell.imgBackground.image = UIImage(named: "boxLibBlue")
                Cell.imgFolder.image = UIImage(named: "folderBlue")
                Cell.imgRibbon.image = UIImage(named: "Wht_Ribbon-with-textRight")
                
            }
            else{
                Cell.lblTitle.textColor = TopTitleFontColor
                Cell.lblDetail.textColor = TopTitleFontColor
                Cell.imgBackground.image = UIImage(named: "text-list-box")
                Cell.imgFolder.image = UIImage(named: "Folder")
                Cell.imgRibbon.image = UIImage(named: "Ribbon-with-textRight")
            }
        
            if objModel.is_new == "0" {
                Cell.imgRibbon.isHidden = false
            }else{
                Cell.imgRibbon.isHidden = true
            }
         
            Cell.lblTitle.text = objModel.folder_name
            Cell.lblDetail.text = String(objModel.data.count)
         
            return Cell

        }else{
            
            let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellDocumentCol", for: indexPath) as! cellDocumentCol

            Cell.lblTitle.numberOfLines = 0
            
            if appDelegateObj.IsThemeLight{
                Cell.lblTitle.textColor = wTopTitleFontColor
                Cell.imgBackground.image = UIImage(named: "boxLibBlue")
                Cell.imgDocument.image = UIImage(named: "fileBlue")
                Cell.imgRibbon.image = UIImage(named: "Wht_Ribbon-with-textRight")
            }
            else{
                Cell.imgBackground.image = UIImage(named: "text-list-box")
                Cell.lblTitle.textColor = TopTitleFontColor
                Cell.imgDocument.image = UIImage(named: "text-file-icon")
                Cell.imgRibbon.image = UIImage(named: "Ribbon-with-textRight")
            }

            if objModel.is_viewed == "0" {
                Cell.imgRibbon.isHidden = false
            }else{
                Cell.imgRibbon.isHidden = true
            }


            Cell.lblTitle.text = objModel.doc_name
            
            return Cell

            
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var objModel = DocumentMainData()
        objModel = arrayData[indexPath.row]
        
        if objModel.folder_name != "" {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ObjVC = storyboard.instantiateViewController(withIdentifier: "LibraryVC") as! LibraryVC
            
            ObjVC.arrayData = objModel.data
            ObjVC.strFolderTitle = objModel.folder_name
            
            self.navigationController?.pushViewController(ObjVC, animated: true)
            
        }
        else{
            
            if arrayData[indexPath.row].is_viewed == "0"{
                
                let copyObj = arrayData[indexPath.row]
                copyObj.is_viewed = "1"
                var copyArray = arrayData
                copyArray.remove(at: indexPath.row)
                copyArray.insert(copyObj, at: indexPath.row)
                arrayData = copyArray
                DispatchQueue.main.async{
                    self.collLibraryObj.reloadData()
                }
            }
            
            self.IsReadPostApi(strPostId: objModel.doc_id, strPostTypeId: "3")
            
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ObjVC = storyboard.instantiateViewController(withIdentifier: "DocumentVC") as! DocumentVC
            
          
            ObjVC.strUrlPath = objModel.document
            ObjVC.strDocTitle = objModel.doc_name
            
            self.navigationController?.pushViewController(ObjVC, animated: true)
            
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        var objModel = DocumentMainData()
        objModel = arrayData[indexPath.row]
        
        if objModel.folder_name != "" {
            
        }
        else{
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                
                if self.arrayData.count > indexPath.row{
                    if self.arrayData[indexPath.row].is_viewed == "0"{
                        
                        let copyObj = self.arrayData[indexPath.row]
                        copyObj.is_viewed = "1"
                        var copyArray = self.arrayData
                        copyArray.remove(at: indexPath.row)
                        copyArray.insert(copyObj, at: indexPath.row)
                        self.arrayData = copyArray
                        DispatchQueue.main.async{
                            self.collLibraryObj.reloadData()
                        }
                        self.IsReadPostApi(strPostId: copyObj.doc_id, strPostTypeId: "3")
                        
                    }
                }
             
            }
            
        }
        
    }
    
  
    
}

extension DocumentMainVC: UICollectionViewDelegateFlowLayout {
    
   
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let top : CGFloat = 10
        let left : CGFloat = 10
        let bottom : CGFloat = 10
        let right : CGFloat = 10
        
        
    
        
        return UIEdgeInsets(top: top, left: left, bottom: bottom, right: right)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let marginsAndInsets = 10 * 2 + collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right + 10 * CGFloat(2 - 1)
        let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(2)).rounded(.down)
        //        let itemHeight = ((collectionView.bounds.size.height - marginsAndInsets) / CGFloat(2)).rounded(.down)
        
        return CGSize(width: itemWidth, height: itemWidth/1.3)
    }
    
}

