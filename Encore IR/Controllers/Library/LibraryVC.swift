//
//  LibraryVC.swift
//  Encore IR
//
//  Created by ravi on 06/05/19.
//  Copyright © 2019 ravi. All rights reserved.
//

import UIKit
import LGSideMenuController



class cellLibrarytbl: UITableViewCell {
    
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var imgLibrary: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    
    @IBOutlet weak var imgRibbon: UIImageView!
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        // do your thing
        
        if appDelegateObj.IsThemeLight == true{
            lblTitle.textColor = wTopTitleFontColor
            lblDetail.textColor = wDetailFontColor
            imgBackground.image = UIImage.init(named: "rectLibBlue")
        }
        else{
            lblTitle.textColor = TopTitleFontColor
            lblDetail.textColor = TopTitleFontColor
            imgBackground.image = UIImage.init(named: "text-grid-box")
        }
        
        lblTitle.font = lblTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 13.0))
        lblDetail.font = lblDetail.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 13.0))
        
    }
    
}

class cellLibraryCol: UICollectionViewCell {
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var imgLibrary: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    
    @IBOutlet weak var imgRibbon: UIImageView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        // do your thing
        
        if appDelegateObj.IsThemeLight == true{
            lblTitle.textColor = wTopTitleFontColor
            lblDetail.textColor = wDetailFontColor
            imgBackground.image = UIImage.init(named: "rectLibBlue")
            imgLibrary.image = UIImage.init(named: "fileBlue")
        }
        else{
            
            lblTitle.textColor = TopTitleFontColor
            lblDetail.textColor = TopTitleFontColor
            imgBackground.image = UIImage.init(named: "text-list-box")
            imgLibrary.image = UIImage(named: "text-file-icon")
        }
        
        lblTitle.font = lblTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 13.0))
        lblDetail.font = lblDetail.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 13.0))
        
    }
    
}


class LibraryVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource {

    
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblLibraryObj: UITableView!
    @IBOutlet weak var collLibraryObj: UICollectionView!
    
    @IBOutlet weak var btnLayoutChange: UIButton!
    @IBOutlet weak var imgTopLayout: UIImageView!
    
    @IBOutlet weak var lblNoDataFound: UILabel!
    
    var isGridLayout = false
    
    var arrayData = [saprate_dataDetails]()
    var objRefreshControl = UIRefreshControl()
    var refresher = UIRefreshControl()

    var pageAdded = 0
    var isPagingNeed = true
    var TotalFeed = 1
    var strFolderTitle = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
     //   self.tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        self.title = nil

        self.setupInitialView()
        self.collLibraryObj.contentInsetAdjustmentBehavior = .always
        
        isGridLayout = false
        
        objRefreshControl.attributedTitle = NSAttributedString(string: "")
        objRefreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        objRefreshControl.tintColor = UIColor.clear
        tblLibraryObj.addSubview(objRefreshControl)
        
        
        collLibraryObj.alwaysBounceVertical = true
        refresher.addTarget(self, action: #selector(refreshStream), for: .valueChanged)
        refresher.tintColor = UIColor.clear
        collLibraryObj.addSubview(refresher)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getUpdateCounter()
        self.setUpThemeColor()
        DispatchQueue.main.async{
            self.tblLibraryObj.reloadData()
            self.collLibraryObj.reloadData()
        }
    }
    
    override func viewWillLayoutSubviews() {
        

    }
    
    
    func setUpThemeColor(){
        if appDelegateObj.IsThemeLight == true{
            
            print("Ligth Theme mode")
            imgBackground.image = UIImage(named: "Wht_background")
            
            imgTopLayout.image = UIImage.init(named: "Wht_Grid")
            self.view.backgroundColor = wviewBgDefaultThemeColor
            lblTitle.textColor = wTopTitleFontColor
            imgBack.image = UIImage(named: "Wht_Back")
            lblNoDataFound.textColor = UIColor.black
            
            
        }
        else{
            print("Dark Theme mode")
            self.view.backgroundColor = TabViewColor
            imgBackground.image = UIImage(named: "background")
            imgTopLayout.image = UIImage.init(named: "Grid View Icon")
            lblTitle.textColor = TopTitleFontColor
            imgBack.image = UIImage(named: "Back")
            lblNoDataFound.textColor = UIColor.white
        }
        self.setupFontSize()
    }

    //MARK:- Class Methods and Functions
    func setupInitialView(){
        self.view.backgroundColor = TabViewColor
        
        lblTitle.text = strFolderTitle
        
      // btnLayoutChange.setImage(UIImage(named: "Grid View Icon"), for: .normal)
//        imgTopLayout.image = UIImage.init(named: "Grid View Icon")
//
//        tblLibraryObj.isHidden = true
//        collLibraryObj.isHidden = false
        
        
        tblLibraryObj.isHidden = false
        collLibraryObj.isHidden = true

        
//        self.getDocumentFeed()
        
        lblNoDataFound.isHidden = true
       
        
       
    }
    func setupFontSize(){
        lblTitle.font = lblTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 20.0))
        lblNoDataFound.font = lblNoDataFound.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 15.0))
        
    }
    @objc func refresh(_ sender: Any) {
        //  your code to refresh tableView
        print("Refresh start")
        
//        self.getDocumentFeed()
        tblLibraryObj.reloadData()
        collLibraryObj.reloadData()

        
        objRefreshControl.endRefreshing()
        
    }
    
    @objc func refreshStream() {
        
        print("refresh")
        
//        self.getDocumentFeed()
        tblLibraryObj.reloadData()
        collLibraryObj.reloadData()

        
        refresher.endRefreshing()
        
    }

    //MARK:- IBActions
    @IBAction func btnMenuPressed(_ sender: Any) {
//        showLeftViewAnimated(sender)
        self.navigationController?.popViewController(animated: true)

    }
   
   

    @IBAction func btnLayoutChngedPressed(_ sender: Any) {
        
        if isGridLayout == false{
            collLibraryObj.isHidden = false
            tblLibraryObj.isHidden = true
            
            if appDelegateObj.IsThemeLight{
                imgTopLayout.image = UIImage.init(named: "Wht_list_Icon")
            }
            else{
                imgTopLayout.image = UIImage.init(named: "List View Icon")
            }
            
            isGridLayout = true
        }
        else{
            collLibraryObj.isHidden = true
            tblLibraryObj.isHidden = false
            if appDelegateObj.IsThemeLight{
                imgTopLayout.image = UIImage.init(named: "Wht_Grid")
            }else{
                imgTopLayout.image = UIImage.init(named: "Grid View Icon")
            }
            isGridLayout = false
        }
        
    }
    
    //MARK:- UITableView Methods
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayData.count;
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let tablecell = tableView.dequeueReusableCell(withIdentifier: "cellLibrarytbl", for: indexPath) as! cellLibrarytbl
        
        var objModel = saprate_dataDetails()
        objModel = arrayData[indexPath.row]
        
        tablecell.lblTitle.numberOfLines = 2
        
        tablecell.lblTitle.text = objModel.doc_name
        tablecell.lblDetail.text = objModel.doc_type
        
        if appDelegateObj.IsThemeLight == true{
            tablecell.imgLibrary.image = UIImage(named: "fileBlue")
            tablecell.imgRibbon.image = UIImage(named: "Wht_Ribbon-with-textRight")
        }
        else{
            tablecell.imgLibrary.image = UIImage(named: "text-file-icon")
            tablecell.imgRibbon.image = UIImage(named: "Ribbon-with-textRight")
        }
        
        if objModel.is_viewed == "0" {
            tablecell.imgRibbon.isHidden = false
        }else{
            tablecell.imgRibbon.isHidden = true
        }

        tablecell.lblDetail.isHidden = true
        return tablecell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if arrayData[indexPath.row].is_viewed == "0"{
            
            let copyObj = arrayData[indexPath.row]
            copyObj.is_viewed = "1"
            var copyArray = arrayData
            copyArray.remove(at: indexPath.row)
            copyArray.insert(copyObj, at: indexPath.row)
            arrayData = copyArray
            self.tblLibraryObj.reloadData()
            
        }
        
        
        var objModel = saprate_dataDetails()
        objModel = arrayData[indexPath.row]

        self.IsReadPostApi(strPostId: objModel.doc_id, strPostTypeId: "3")
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ObjVC = storyboard.instantiateViewController(withIdentifier: "DocumentVC") as! DocumentVC
        
        print(objModel.document)
        ObjVC.strUrlPath = objModel.document
        ObjVC.strDocTitle = objModel.doc_name
        self.navigationController?.pushViewController(ObjVC, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
//        if indexPath.row == arrayData.count - 3 {
//            isPagingNeed = false
//        }
//
//        if !isPagingNeed && TotalFeed > 9 {
//
//            //            if TotalFeed > 0 {
//            isPagingNeed = true
//
//            self.pageAdded += 10
//
//            let strNewPage = "\(pageAdded)"
//            print("page added \(pageAdded)")
//            print("TotalFeed \(TotalFeed)  \(arrayData.count)")
//
//            self.getMoreDocumentFeed(strMorePage: strNewPage)
//            //            }
//
//        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            if self.arrayData.count > indexPath.row{
                if self.arrayData[indexPath.row].is_viewed == "0" {
                    
                    let copyObj = self.arrayData[indexPath.row]
                    copyObj.is_viewed = "1"
                    var copyArray = self.arrayData
                    copyArray.remove(at: indexPath.row)
                    copyArray.insert(copyObj, at: indexPath.row)
                    self.arrayData = copyArray
                    self.tblLibraryObj.reloadData()
                    
                    self.IsReadPostApi(strPostId: copyObj.doc_id, strPostTypeId: "3")
                }
            }
        }
        
    }
    
//    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//
//        if arrayData[indexPath.row].is_viewed == "0" {
//
//            let copyObj = arrayData[indexPath.row]
//            copyObj.is_viewed = "1"
//            var copyArray = arrayData
//            copyArray.remove(at: indexPath.row)
//            copyArray.insert(copyObj, at: indexPath.row)
//            arrayData = copyArray
//            self.tblLibraryObj.reloadData()
//
//            self.IsReadPostApi(strPostId: copyObj.doc_id, strPostTypeId: "3")
//
//        }else{
//
//        }
//
//    }
    
    //MARK:- UICollection Delegate Method
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellLibraryCol", for: indexPath) as! cellLibraryCol
        
        
    
        var objModel = saprate_dataDetails()
        objModel = arrayData[indexPath.row]
        
        if objModel.is_viewed == "0" {
            Cell.imgRibbon.isHidden = false
            if appDelegateObj.IsThemeLight == true{
                Cell.imgRibbon.image = UIImage(named: "Wht_Ribbon-with-textRight")
            }
            else{
                Cell.imgRibbon.image = UIImage(named: "Ribbon-with-textRight")
            }
        }else{
            Cell.imgRibbon.isHidden = true
        }

        Cell.lblTitle.numberOfLines = 2

        Cell.lblTitle.text = objModel.doc_name
        Cell.lblDetail.text = objModel.doc_type
        
        Cell.lblDetail.isHidden = true

        return Cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if arrayData[indexPath.row].is_viewed == "0"{
            
            let copyObj = arrayData[indexPath.row]
            copyObj.is_viewed = "1"
            var copyArray = arrayData
            copyArray.remove(at: indexPath.row)
            copyArray.insert(copyObj, at: indexPath.row)
            arrayData = copyArray
            self.collLibraryObj.reloadData()
            
        }
        
        var objModel = saprate_dataDetails()
        objModel = arrayData[indexPath.row]
        
        self.IsReadPostApi(strPostId: objModel.doc_id, strPostTypeId: "3")

        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ObjVC = storyboard.instantiateViewController(withIdentifier: "DocumentVC") as! DocumentVC
        
        print(objModel.document)
        ObjVC.strUrlPath = objModel.document
        ObjVC.strDocTitle = objModel.doc_name

        self.navigationController?.pushViewController(ObjVC, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
//        if indexPath.row == arrayData.count - 3 {
//            isPagingNeed = false
//        }
//
//        if !isPagingNeed && TotalFeed > 9 {
//
//            //            if TotalFeed > 0 {
//            isPagingNeed = true
//
//            self.pageAdded += 10
//
//            let strNewPage = "\(pageAdded)"
//            print("page added \(pageAdded)")
//            print("TotalFeed \(TotalFeed)  \(arrayData.count)")
//
//            self.getMoreDocumentFeed(strMorePage: strNewPage)
//            //            }
//
//        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            if self.arrayData.count > indexPath.row{
                if self.arrayData[indexPath.row].is_viewed == "0"{
                    
                    let copyObj = self.arrayData[indexPath.row]
                    copyObj.is_viewed = "1"
                    var copyArray = self.arrayData
                    copyArray.remove(at: indexPath.row)
                    copyArray.insert(copyObj, at: indexPath.row)
                    self.arrayData = copyArray
                    self.collLibraryObj.reloadData()
                    
                    self.IsReadPostApi(strPostId: copyObj.doc_id, strPostTypeId: "3")
                    
                }
            }
        }
    }
    
//    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//
//        if arrayData[indexPath.row].is_viewed == "0"{
//
//            let copyObj = arrayData[indexPath.row]
//            copyObj.is_viewed = "1"
//            var copyArray = arrayData
//            copyArray.remove(at: indexPath.row)
//            copyArray.insert(copyObj, at: indexPath.row)
//            arrayData = copyArray
//            self.collLibraryObj.reloadData()
//
//            self.IsReadPostApi(strPostId: copyObj.doc_id, strPostTypeId: "3")
//
//        }else{
//
//        }
//
//    }
    
}

extension LibraryVC: UICollectionViewDelegateFlowLayout {
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//        let padding: CGFloat = 10
//        let collectionCellwidth = collectionView.frame.size.width - padding
//        let collectionCellHeight = collectionView.frame.size.height - padding
//
//
//        return CGSize(width: collectionCellwidth/2, height: collectionCellHeight/6)
//
//    }
    
//    let inset: CGFloat = 10
//    let minimumLineSpacing: CGFloat = 10
//    let minimumInteritemSpacing: CGFloat = 10
//    let cellsPerRow = 2
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let top : CGFloat = 10
        let left : CGFloat = 10
        let bottom : CGFloat = 10
        let right : CGFloat = 10

        
//        let isEven = section % 2 == 0
//
//        if isEven {
//            top = 5
//            bottom = 5
//            left = 10
//            right = 5
//        } else {
//            top = 5
//            bottom = 5
//            left = 5
//            right = 10
//
//        }
        
        return UIEdgeInsets(top: top, left: left, bottom: bottom, right: right)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let marginsAndInsets = 10 * 2 + collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right + 10 * CGFloat(2 - 1)
        let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(2)).rounded(.down)
//        let itemHeight = ((collectionView.bounds.size.height - marginsAndInsets) / CGFloat(2)).rounded(.down)

        return CGSize(width: itemWidth, height: itemWidth/1.3)
    }
    
}
