//
//  MyAssetsDetailVC.swift
//  Encore IR
//
//  Created by ravi on 09/05/19.
//  Copyright © 2019 ravi. All rights reserved.
//

import UIKit
import GoogleMaps
import ImageSlideshow

struct Model {
    let image: UIImage
    let title: String
    
    var inputSource: InputSource {
        return ImageSource(image: image)
    }
}

class cellAssetDetail: UICollectionViewCell {
    @IBOutlet weak var imgNews: UIImageView!
    @IBOutlet weak var imgBox: UIImageView!
    @IBOutlet weak var btnCounter: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        // do your thing
        let image = UIImage.outlinedEllipse(size: CGSize(width: 7.0, height: 7.0), color: wPageControlThemeColor)
        self.pageControl.pageIndicatorTintColor = UIColor.init(patternImage: image!)
        self.pageControl.currentPageIndicatorTintColor = wPageControlThemeColor
        
        btnCounter.titleLabel?.font = UIFont(name:MEDIUEM_FONT, size: GlobalMethods.setThemeFontSize(objectSize: 15.0))

    }
    
}

class cellDetailTitle: UITableViewCell{
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewBack.layer.cornerRadius = 5
        self.viewBack.layer.borderWidth = 0.5
        self.viewBack.layer.masksToBounds = true
        
        lblTitle.font = lblTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 15.0))
    }
}

class cellMainAsset: UITableViewCell{
    
    @IBOutlet weak var gMap: GMSMapView!
    @IBOutlet weak var viewBack1: UIView!

    @IBOutlet weak var lblAssetOverview: UILabel!
    
    @IBOutlet weak var lblQuarterDate: UILabel!
    @IBOutlet weak var lblSectorTitle: UILabel!
    @IBOutlet weak var lblSector: UILabel!
    
    @IBOutlet weak var lblOwnershipTitle: UILabel!
    @IBOutlet weak var lblOwnership: UILabel!
    
    @IBOutlet weak var lblYearBuildTitle: UILabel!
    
    @IBOutlet weak var lblYearBuild: UILabel!
    
    @IBOutlet weak var lblLocationTitle: UILabel!
    
    @IBOutlet weak var lblLocation: UILabel!
    

    @IBOutlet weak var lblYearRenovaltTitle: UILabel!
    
    @IBOutlet weak var lblYearRenovalt: UILabel!
    
    
    
//    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var lblStatusTitle: UILabel!
    
    @IBOutlet weak var lblStatus: UILabel!
    
    
    @IBOutlet weak var viewSector: UIView!
    @IBOutlet weak var ConstSectorHeight: NSLayoutConstraint!
    
    @IBOutlet weak var viewStatus: UIView!
    
    @IBOutlet weak var ConstStatusHeight: NSLayoutConstraint!
    
    @IBOutlet weak var viewLocation: UIView!
    
    @IBOutlet weak var ConstLocationHeight: NSLayoutConstraint!
    
    @IBOutlet weak var viewOwnership: UIView!
    
    @IBOutlet weak var ConstOwnerShipHeigth: NSLayoutConstraint!
    
    @IBOutlet weak var viewYearBuild: UIView!
    
    @IBOutlet weak var ConstYearBuildHeight: NSLayoutConstraint!
    
    @IBOutlet weak var viewYearRenovation: UIView!
    
    @IBOutlet weak var ConstYearRenovaltHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var constTxtDescHeight: NSLayoutConstraint!

    
    @IBOutlet weak var txtAssetOverviewDesc: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewBack1.layer.cornerRadius = 5
        self.viewBack1.layer.borderWidth = 0.5
        self.viewBack1.layer.masksToBounds = true
        
        
        self.gMap.layer.cornerRadius = 5
        self.gMap.layer.masksToBounds = true
        
        self.gMap.isUserInteractionEnabled = false
        self.lblQuarterDate.isHidden = true
        
        
      
        
        lblAssetOverview.font = lblAssetOverview.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 15.0))
        lblSectorTitle.font = lblSectorTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 12.0))
        lblSector.font = lblSector.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 13.0))
        
        lblOwnershipTitle.font = lblOwnershipTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 12.0))
        lblOwnership.font = lblOwnership.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 13.0))
        
        lblStatusTitle.font = lblStatusTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 12.0))
        lblStatus.font = lblStatus.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 13.0))
        
        lblYearBuildTitle.font = lblYearBuildTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 12.0))
        lblYearBuild.font = lblYearBuild.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 13.0))
        
        lblLocationTitle.font = lblLocationTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 12.0))
        lblLocation.font = lblLocation.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 13.0))
        
        lblYearRenovaltTitle.font = lblYearRenovaltTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 12.0))
        lblYearRenovalt.font = lblYearRenovalt.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 13.0))
    
      
        lblQuarterDate.font = lblQuarterDate.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 12.0))
        
        txtAssetOverviewDesc.font = txtAssetOverviewDesc.font!.withSize(GlobalMethods.setThemeFontSize(objectSize: 15.0))
       

    }
}




class MyAssetsDetailVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDelegate, UITableViewDataSource {

    //MARK:- Outlets
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnBookmark: UIButton!
    @IBOutlet weak var objCollectionAssets: UICollectionView!
    @IBOutlet weak var viewCollection: UIView!
    @IBOutlet weak var tblDetail: UITableView!
    @IBOutlet weak var imgBookmarkTop: UIImageView!
    
    
    //MARK:- Variable
    var projectDetail = project_detailDetail()
    var assetDetail = asset_detailDetail()
    var arrImgUrl = [a_asset_imgDetail]()
    var arrayRowTitle = [project_section_listDetail]()

    var objMyAsset = AssetProject()
    var arrImg = [UIImage]()
    var locAddress = ""
    var strAssetTitle = ""
    var strAssetId = ""
    var IsNewDealDetail = false
    var objRefreshControl = UIRefreshControl()

    
    var centerMapCoordinate = CLLocationCoordinate2D()
    
    var slideshowTransitioningDelegate: ZoomAnimatedTransitioningDelegate? = nil
    
    //MARK:- UIView Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        tblDetail.tableHeaderView = viewCollection
        
        self.setupInitialView()
        
        objRefreshControl.attributedTitle = NSAttributedString(string: "")
        objRefreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        objRefreshControl.tintColor = UIColor.clear
        tblDetail.addSubview(objRefreshControl)

       
        
//        arrayRowTitle = ["","QUARTERLY UPDATES","DISTRIBUTION GUIDANCE","FINANCIALS","NEW SECTION"]
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        self.getAssetsDetail(strType: strAssetId)
        
        self.setUpThemeColor()
        tblDetail.reloadData()
    }
   
   
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        
        tblDetail.estimatedRowHeight = 200
        tblDetail.rowHeight = UITableViewAutomaticDimension

        self.getAssetsDetail(strType: strAssetId)

        if IsNewDealDetail == false{
            self.IsReadPostApi(strPostId: strAssetId, strPostTypeId: "0")
            
            let nc = NotificationCenter.default
            nc.post(name: Notification.Name(kRefreshAssets), object: nil)


        }else{
            self.IsReadPostApi(strPostId: strAssetId, strPostTypeId: "0")
            
            let nc = NotificationCenter.default
            nc.post(name: Notification.Name(kRefreshDeal), object: nil)


        }
        
    }
    
    func setUpThemeColor(){
        if appDelegateObj.IsThemeLight == true{
            
            print("Ligth Theme mode")
            imgBackground.image = UIImage(named: "Wht_background")
            imgBack.image = UIImage(named: "Wht_Back")
            
            self.view.backgroundColor = wviewBgDefaultThemeColor
            lblTopTitle.textColor = wTopTitleFontColor
          
           
        }
        else{
            print("Dark Theme mode")
            self.view.backgroundColor = TabViewColor
            imgBackground.image = UIImage(named: "background")
            imgBack.image = UIImage(named: "Back")
            lblTopTitle.textColor = TopTitleFontColor
            
        }
        self.setupFontSize()
    }
    func setupFontSize(){
        lblTopTitle.font = lblTopTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 20.0))
    }
    @objc func refresh(_ sender: Any) {
        //  your code to refresh tableView
        print("Refresh start")
        
        self.getAssetsDetail(strType: strAssetId)

        objRefreshControl.endRefreshing()
        
    }
    func mapLocationUpdate(){
        
        let lat = Double(assetDetail.latitude)
        let long = Double(assetDetail.longitude)
        self.centerMapCoordinate = CLLocationCoordinate2D(latitude: lat!, longitude: long!)

        lblTopTitle.text = projectDetail.asset_name
        
        

    }
    
    func setUpBookMark(){
      
        if assetDetail.bookmark == "1"{
            imgBookmarkTop.image = UIImage.init(named: "")
            
            if appDelegateObj.IsThemeLight == true{
                imgBookmarkTop.image = UIImage(named: "Wht_BookmarkFill")
            }
            else{
                imgBookmarkTop.image = UIImage(named: "Bookmark-fill")
            }
            
        }else{
           
            if appDelegateObj.IsThemeLight == true{
                imgBookmarkTop.image = UIImage(named: "Wht_Bookmark")
            }
            else{
                imgBookmarkTop.image = UIImage(named: "Bookmark")
            }
        }
    }
    
    
    //MARK:- IBActions
    @IBAction func btnBackSel(_ sender: Any) {
        
       // self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnBookmarkPressed(_ sender: Any) {
        
        if IsNewDealDetail == false{
            
            appDelegateObj.strPostId = strAssetId
            appDelegateObj.strPostTypeId = "2" //asset type = 2
            
            if assetDetail.bookmark == "1"{
                assetDetail.bookmark = "0"
                self.removeBookmarkApi(strPostId: appDelegateObj.strPostId, strPostTypeId: appDelegateObj.strPostTypeId)
                
               
                if appDelegateObj.IsThemeLight == true{
                    imgBookmarkTop.image = UIImage(named: "Wht_Bookmark")
                }
                else{
                    imgBookmarkTop.image = UIImage(named: "Bookmark")
                }
                
                
            }else{
                assetDetail.bookmark = "1"
               
                
                if appDelegateObj.IsThemeLight == true{
                    imgBookmarkTop.image = UIImage(named: "Wht_BookmarkFill")
                }
                else{
                    imgBookmarkTop.image = UIImage(named: "Bookmark-fill")
                }
                
                self.addBookmarkApi(strPostId: appDelegateObj.strPostId, strPostTypeId: appDelegateObj.strPostTypeId)
                
            }
            
            let nc = NotificationCenter.default
            nc.post(name: Notification.Name(kRefreshAssets), object: nil)

            
        }else{
            
            appDelegateObj.strPostId = objMyAsset.a_id
            appDelegateObj.strPostTypeId = "3" // deal type = 3
            
            if assetDetail.bookmark == "1"{
                assetDetail.bookmark = "0"
                self.removeBookmarkApi(strPostId: appDelegateObj.strPostId, strPostTypeId: appDelegateObj.strPostTypeId)
                
                
                if appDelegateObj.IsThemeLight == true{
                    imgBookmarkTop.image = UIImage(named: "Wht_Bookmark")
                }
                else{
                    imgBookmarkTop.image = UIImage(named: "Bookmark")
                }
                
            }else{
                assetDetail.bookmark = "1"
               
                
                if appDelegateObj.IsThemeLight == true{
                    imgBookmarkTop.image = UIImage(named: "Wht_BookmarkFill")
                }
                else{
                    imgBookmarkTop.image = UIImage(named: "Bookmark-fill")
                }
                
                self.addBookmarkApi(strPostId: appDelegateObj.strPostId, strPostTypeId: appDelegateObj.strPostTypeId)
                
            }
            let nc = NotificationCenter.default
            nc.post(name: Notification.Name(kRefreshDeal), object: nil)

        }
        
        
    }
    
    
    
    //MARK:- UICollection Delegate Method
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImgUrl.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellAssetDetail", for: indexPath) as! cellAssetDetail
        
        let imgPath = arrImgUrl[indexPath.row].url
        Cell.imgNews.sd_setImage(with: URL(string: imgPath), completed: nil)

       // arrImg.append(Cell.imgNews.image!)
        
        Cell.pageControl.numberOfPages = arrImgUrl.count
        Cell.btnCounter.setTitle("+\(arrImgUrl.count)", for: .normal)
        
        Cell.btnCounter.isHidden = true
        Cell.imgBox.isHidden = true

        return Cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       /* // Your action
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ObjVC = storyboard.instantiateViewController(withIdentifier: "FullScreenImageVC") as! FullScreenImageVC
        
        
        
        ObjVC.arrSlide = arrImg
        //        self.navigationController?.pushViewController(ObjVC, animated: true)
        
        ObjVC.modalPresentationStyle = .overCurrentContext
        present(ObjVC, animated: false, completion: nil)*/
        
        var arryModel = [Model]()
        
        for element in arrImg {
            //imageLabel.image = element
            arryModel.append(Model(image: element, title: ""))
        }
        
       
        let fullScreenController = FullScreenSlideshowViewController()
        
        fullScreenController.closeButton.layer.cornerRadius = fullScreenController.closeButton.bounds.height/2
        fullScreenController.closeButton.layer.borderWidth = 1
        fullScreenController.closeButton.layer.borderColor = UIColor.darkGray.cgColor
        fullScreenController.closeButton.backgroundColor = UIColor.lightGray
        fullScreenController.closeButton.layer.masksToBounds = true
        
        if appDelegateObj.IsThemeLight == true{
            fullScreenController.backgroundColor = wviewBgDefaultThemeColor
        }
        else{
            fullScreenController.backgroundColor = TabViewColor
        }
        
        fullScreenController.inputs = arryModel.map { $0.inputSource }
        
     
        fullScreenController.initialPage = indexPath.row
   
        
        if let imageView = (collectionView.cellForItem(at: indexPath) as! cellAssetDetail).imgNews {
            slideshowTransitioningDelegate = ZoomAnimatedTransitioningDelegate(imageView: imageView, slideshowController: fullScreenController)
            fullScreenController.transitioningDelegate = slideshowTransitioningDelegate
        }
        
        fullScreenController.slideshow.currentPageChanged = { [weak self] page in
            if let imageView = (collectionView.cellForItem(at: indexPath) as! cellAssetDetail).imgNews {
                self?.slideshowTransitioningDelegate?.referenceImageView = imageView
            }
        }
        
        present(fullScreenController, animated: true, completion: nil)

    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellAssetDetail", for: indexPath) as! cellAssetDetail
        Cell.pageControl.currentPage = indexPath.row
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        let closestCell: UICollectionViewCell = self.objCollectionAssets.visibleCells[0]
        for cell in self.objCollectionAssets.visibleCells as [UICollectionViewCell] {
            let closestCellDelta = abs(closestCell.center.x - self.objCollectionAssets.bounds.size.width / 2.0 - self.objCollectionAssets.contentOffset.x)
            let cellDelta = abs(cell.center.x - self.objCollectionAssets.bounds.size.width/2.0 - self.objCollectionAssets.contentOffset.x)
            if (cellDelta < closestCellDelta){
            }
        }
        let indexPath = self.objCollectionAssets.indexPath(for: closestCell)
        self.objCollectionAssets.scrollToItem(at: indexPath!, at: .centeredHorizontally , animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    
    }
    

    //MARK:- UITableView Methods
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayRowTitle.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        
        if indexPath.row == 0{
            let tablecellMain = tableView.dequeueReusableCell(withIdentifier: "cellMainAsset", for: indexPath) as! cellMainAsset

            
            //map setup
            let marker = GMSMarker()
            marker.position = centerMapCoordinate
//            marker.icon = imageConverted
            marker.map = tablecellMain.gMap
            tablecellMain.gMap.camera = GMSCameraPosition.camera(withTarget: marker.position, zoom: 11)
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap))
            tablecellMain.isUserInteractionEnabled = true
            tablecellMain.addGestureRecognizer(tap)

            
            tablecellMain.lblSector.text = assetDetail.sector
            if assetDetail.sector == ""{
                tablecellMain.ConstSectorHeight.constant = 0
                tablecellMain.viewSector.isHidden = true
            }
            else{
                tablecellMain.ConstSectorHeight.constant = 47
                tablecellMain.viewSector.isHidden = false
            }
            
            tablecellMain.lblOwnership.text = assetDetail.a_ownership
            tablecellMain.lblOwnership.numberOfLines = 2
            if assetDetail.a_ownership == ""{
                tablecellMain.ConstOwnerShipHeigth.constant = 0
                tablecellMain.viewOwnership.isHidden = true
            }
            else{
                tablecellMain.ConstOwnerShipHeigth.constant = 47
                tablecellMain.viewOwnership.isHidden = false
            }
            
            tablecellMain.lblYearBuild.text = assetDetail.a_build_year
            tablecellMain.lblYearBuild.numberOfLines = 2
            if assetDetail.a_build_year == ""{
                tablecellMain.ConstYearBuildHeight.constant = 0
                tablecellMain.viewYearBuild.isHidden = true
            }
            else{
                tablecellMain.ConstYearBuildHeight.constant = 47
                tablecellMain.viewYearBuild.isHidden = false
            }
            let exampleString = assetDetail.a_asset_address
            let wantedString = exampleString.replacingOccurrences(of: ",", with: ", ")
            let strRemoveSpace = wantedString.replacingOccurrences(of: "  ", with: " ")

            tablecellMain.lblLocation.text = strRemoveSpace
            tablecellMain.lblLocation.numberOfLines = 4
            if assetDetail.a_asset_address == ""{
                tablecellMain.ConstLocationHeight.constant = 0
                tablecellMain.viewLocation.isHidden = true
            }
            else{
                tablecellMain.ConstLocationHeight.constant = 47
                tablecellMain.viewLocation.isHidden = false
            }
            
            tablecellMain.lblQuarterDate.text = assetDetail.a_created_date

            tablecellMain.lblStatus.text = assetDetail.a_status
            tablecellMain.lblStatus.numberOfLines = 2
            if assetDetail.a_status == ""{
                tablecellMain.ConstStatusHeight.constant = 0
                tablecellMain.viewStatus.isHidden = true
            }
            else{
                tablecellMain.ConstStatusHeight.constant = 47
                tablecellMain.viewStatus.isHidden = false
            }

            tablecellMain.lblYearRenovalt.text = assetDetail.a_y_renovated
            tablecellMain.lblYearRenovalt.numberOfLines = 2
            if assetDetail.a_y_renovated == ""{
                tablecellMain.ConstYearRenovaltHeight.constant = 0
                tablecellMain.viewYearRenovation.isHidden = true
            }
            else{
                tablecellMain.ConstYearRenovaltHeight.constant = 47
                tablecellMain.viewYearRenovation.isHidden = false
            }

            tablecellMain.txtAssetOverviewDesc.text = assetDetail.a_asset_overview
            tablecellMain.txtAssetOverviewDesc.isEditable = false
            
            if  tablecellMain.txtAssetOverviewDesc.contentSize.height >= 100
            {
//                tablecellMain.txtAssetOverviewDesc.isScrollEnabled = true
                tablecellMain.constTxtDescHeight.constant = 100
            }
            else
            {
                 tablecellMain.constTxtDescHeight.constant = tablecellMain.txtAssetOverviewDesc.contentSize.height
                 tablecellMain.txtAssetOverviewDesc.isScrollEnabled = false
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                tablecellMain.txtAssetOverviewDesc.setContentOffset(.zero, animated: false)
            }
            
            
            if appDelegateObj.IsThemeLight{
                tablecellMain.viewBack1.backgroundColor = wviewBgDefaultThemeColor
                tablecellMain.viewBack1.layer.borderColor = wBlueMainTitleColor.cgColor
                tablecellMain.lblAssetOverview.textColor = wBlueMainTitleColor
                tablecellMain.lblSector.textColor = wDarkGrayColor
                tablecellMain.lblOwnership.textColor = wDarkGrayColor
                tablecellMain.lblStatus.textColor = wDarkGrayColor
                tablecellMain.lblYearBuild.textColor = wDarkGrayColor
                tablecellMain.lblLocation.textColor = wDarkGrayColor
                tablecellMain.txtAssetOverviewDesc.textColor = wDarkGrayColor
                tablecellMain.lblQuarterDate.textColor = wDarkGrayColor
                tablecellMain.lblYearRenovalt.textColor = wDarkGrayColor
                
            }else{
                tablecellMain.viewBack1.backgroundColor = kCellBackColor
                tablecellMain.viewBack1.layer.borderColor = UIColor.clear.cgColor
                tablecellMain.lblAssetOverview.textColor = UIColor.white
                tablecellMain.lblSector.textColor = UIColor.white
                tablecellMain.lblOwnership.textColor = UIColor.white
                tablecellMain.lblStatus.textColor = UIColor.white
                tablecellMain.lblYearBuild.textColor = UIColor.white
                tablecellMain.lblLocation.textColor = UIColor.white
                tablecellMain.txtAssetOverviewDesc.textColor = UIColor.white
                tablecellMain.lblQuarterDate.textColor = UIColor.white
                tablecellMain.lblYearRenovalt.textColor = UIColor.white
                
            }


            tablecellMain.layoutIfNeeded()

            
            return tablecellMain
        }
        else
        {
            let tablecell = tableView.dequeueReusableCell(withIdentifier: "cellDetailTitle", for: indexPath) as! cellDetailTitle

            tablecell.lblTitle.text = arrayRowTitle[indexPath.row].section_display_name.uppercased()
            
            if arrayRowTitle[indexPath.row].hide_Show == "Hide"{
                tablecell.isHidden = true
            }
            
            if appDelegateObj.IsThemeLight{
                tablecell.viewBack.backgroundColor = wviewBgDefaultThemeColor
                tablecell.viewBack.layer.borderColor = wBlueMainTitleColor.cgColor
                tablecell.lblTitle.textColor = wBlueMainTitleColor
                
            }else{
                tablecell.viewBack.backgroundColor = kCellBackColor
                tablecell.viewBack.layer.borderColor = UIColor.clear.cgColor
                tablecell.lblTitle.textColor = UIColor.white
            }
            
            return tablecell
        }
        
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        print("cell clicked")
        
        if indexPath.row == 0{
            return
        }
        print("cell clicked")

        if arrayRowTitle[indexPath.row].section_id == strUnique1{
            print("cell clicked1")

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ObjVC = storyboard.instantiateViewController(withIdentifier: "RecentUpdateVC") as! RecentUpdateVC
            
            let ObjSectionData = ObjSectionDetail()
            ObjSectionData.a_id = projectDetail.a_id
            ObjSectionData.c_type = arrayRowTitle[indexPath.row].c_id
            ObjSectionData.is_group = "0"
            ObjSectionData.section_id = arrayRowTitle[indexPath.row].section_id
            ObjSectionData.section_isDynamic = arrayRowTitle[indexPath.row].section_isDynamic
            ObjSectionData.u_id = appDelegateObj.loginUser.u_id
            
            appDelegateObj.ObjSection = ObjSectionData
            
            ObjVC.objSection = ObjSectionData
            ObjVC.IsGroupDis = false
            
            self.navigationController?.pushViewController(ObjVC, animated: true)
            
        }
        else if arrayRowTitle[indexPath.row].section_id == strUnique5{
            print("cell clicked5")

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ObjVC = storyboard.instantiateViewController(withIdentifier: "DistributionGuideVC") as! DistributionGuideVC
            
            let ObjSectionData = ObjSectionDetail()
            ObjSectionData.a_id = projectDetail.a_id
            ObjSectionData.c_type = arrayRowTitle[indexPath.row].c_id
            ObjSectionData.is_group = "0"
            ObjSectionData.section_id = arrayRowTitle[indexPath.row].section_id
            ObjSectionData.section_isDynamic = arrayRowTitle[indexPath.row].section_isDynamic
            ObjSectionData.u_id = appDelegateObj.loginUser.u_id
            
            appDelegateObj.ObjSection = ObjSectionData
            
            ObjVC.objSection = ObjSectionData
            ObjVC.IsGroupDis = false
            
            
            self.navigationController?.pushViewController(ObjVC, animated: true)
            
        }
        else if arrayRowTitle[indexPath.row].section_id == strUnique3{
            print("cell clicked3")

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ObjVC = storyboard.instantiateViewController(withIdentifier: "FinancialsVC") as! FinancialsVC
            
            let ObjSectionData = ObjSectionDetail()
            ObjSectionData.a_id = projectDetail.a_id
            ObjSectionData.c_type = arrayRowTitle[indexPath.row].c_id
            ObjSectionData.is_group = "0"
            ObjSectionData.section_id = arrayRowTitle[indexPath.row].section_id
            ObjSectionData.section_isDynamic = arrayRowTitle[indexPath.row].section_isDynamic
            ObjSectionData.u_id = appDelegateObj.loginUser.u_id
            
            appDelegateObj.ObjSection = ObjSectionData
            
            ObjVC.objSection = ObjSectionData
            ObjVC.IsGroupDis = false
            
            self.navigationController?.pushViewController(ObjVC, animated: true)
            
            
        }
        else {
            print("cell clicked 0")

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ObjVC = storyboard.instantiateViewController(withIdentifier: "NewSectionVC") as! NewSectionVC
            
            let ObjSectionData = ObjSectionDetail()
            ObjSectionData.a_id = projectDetail.a_id
            ObjSectionData.c_type = arrayRowTitle[indexPath.row].section_type
            ObjSectionData.is_group = "0"
            ObjSectionData.section_id = arrayRowTitle[indexPath.row].section_id
            ObjSectionData.section_isDynamic = arrayRowTitle[indexPath.row].section_isDynamic
            ObjSectionData.u_id = appDelegateObj.loginUser.u_id
            
            appDelegateObj.ObjSection = ObjSectionData
            
            ObjVC.objSection = ObjSectionData
            ObjVC.IsGroupDis = false
            
//            print(arrayRowTitle[indexPath.row])
//            print(arrayRowTitle[indexPath.row].section_display_name)
//            let strTitle :String!  = "\(arrayRowTitle[indexPath.row].section_display_name)"
//            ObjVC.lblTopTitle.text = arrayRowTitle[indexPath.row].section_display_name.uppercased()
            ObjVC.strViewTitle = arrayRowTitle[indexPath.row].section_display_name.uppercased()

            self.navigationController?.pushViewController(ObjVC, animated: true)
        }
        

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0{
            return UITableViewAutomaticDimension
        }
        else{
            if arrayRowTitle[indexPath.row].hide_Show == "Hide"{
                return 0
            }else{
                return UITableViewAutomaticDimension
            }
        }
        
        
    }
    
}

extension MyAssetsDetailVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let approximateHeight = collectionView.frame.height
        
        let numberOfCell: CGFloat = 1   //you need to give a type as CGFloat
        let cellWidth = UIScreen.main.bounds.size.width / numberOfCell
        //        return CGSizeMake(cellWidth, cellWidth)
        return CGSize(width: cellWidth, height: approximateHeight)
        
    }
    
   
    
}
extension MyAssetsDetailVC: UIGestureRecognizerDelegate {

    
    @objc func handleTap(_ gesture: UITapGestureRecognizer){
        
        let tapLocation = gesture.location(in: self.tblDetail)
        if let tapIndexPath = self.tblDetail.indexPathForRow(at: tapLocation)
        {
            if let tappedCell = self.tblDetail.cellForRow(at: tapIndexPath) as? cellMainAsset
            {
                print("Row Selected") // access tapped cell and its subview as
                let touchpoint:CGPoint = gesture.location(in: tappedCell)
                
                if tappedCell.gMap.frame.contains(touchpoint)  {
                    // user tapped gMap
//                    tappedCell.gMap.isUserInteractionEnabled = true
                    print("tap on map")
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let ObjVC = storyboard.instantiateViewController(withIdentifier: "MapPopupVC") as! MapPopupVC
                    ObjVC.strLocationAddress = assetDetail.a_asset_address
                    ObjVC.centerMapCoordinate = centerMapCoordinate
                    
                    self.navigationController?.pushViewController(ObjVC, animated: true)
                    
                }
                else{
                    tappedCell.gMap.isUserInteractionEnabled = false
                }
            
            }
        }
    }
    
}
