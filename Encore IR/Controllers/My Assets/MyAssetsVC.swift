//
//  MyAssetsVC.swift
//  Encore IR
//
//  Created by ravi on 06/05/19.
//  Copyright © 2019 ravi. All rights reserved.
//

import UIKit
import LGSideMenuController
import AlignedCollectionViewFlowLayout


class AssetsTblCell: UITableViewCell {
    
    @IBOutlet weak var btnBookmark: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgCell: UIImageView!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblQuater: UILabel!
    
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var imgRibbon: UIImageView!
    
    @IBOutlet weak var imgBookmark: UIImageView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
    
        if appDelegateObj.IsThemeLight == true{
            
            lblTitle.textColor = wTopTitleFontColor
            lblDetail.textColor = wDarkGrayColor
            lblQuater.textColor = wDarkGrayColor
            lblAmount.textColor = wDarkGrayColor
            lblDate.textColor = wDarkGrayColor
            
        }
        else{
            
            lblTitle.textColor = TopTitleFontColor
            lblDetail.textColor = TopTitleFontColor
            lblQuater.textColor = TopTitleFontColor
            lblAmount.textColor = TopTitleFontColor
            lblDate.textColor = TopTitleFontColor
      
        }
        
        lblTitle.font = lblTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 15.0))
        lblDetail.font = lblDetail.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 13.0))
        lblQuater.font = lblQuater.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 13.0))
        lblAmount.font = lblAmount.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 15.0))
        lblDate.font = lblDate.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 13.0))
        
    }
}
class investmentSummaryCell: UITableViewCell {
    
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var lblAssets: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblFunded: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        // do your thing
        
        if appDelegateObj.IsThemeLight == true{
            lblAssets.textColor = wBlueMainTitleColor
            lblAmount.textColor = wDarkGrayColor
            lblFunded.textColor = wDarkGrayColor
           
        }
        else{
            
            lblAssets.textColor = DarkThemefontColor
            lblAmount.textColor = DarkThemefontColor
            lblFunded.textColor = DarkThemefontColor
           
        }
        
        lblAssets.font = lblAssets.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 13.0))
        lblAmount.font = lblAmount.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 13.0))
        lblFunded.font = lblFunded.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 13.0))
        
    }
    
}
class cellFilter: UICollectionViewCell{
     @IBOutlet var lblTitle: UILabel!
}


class MyAssetsVC: UIViewController, UITableViewDelegate, UITableViewDataSource,  UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate, UISearchBarDelegate{

    
    @IBOutlet weak var ViewTop: UIView!
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var btnMenu: UIButton!
    
    @IBOutlet weak var viewSearch: UISearchBar!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var imgSearch: UIImageView!
    
    
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var tblAssetsobj: UITableView!
    
    @IBOutlet weak var viewRightFilter: UIView!
    @IBOutlet weak var viewRightFilterInside: UIView!
    @IBOutlet weak var collectionFilterProperty: UICollectionView!
    @IBOutlet weak var collectionFilterPeriod: UICollectionView!
    
    @IBOutlet weak var lblFilterTitle: UILabel!
    @IBOutlet weak var lblPropertyTypeTitle: UILabel!
    @IBOutlet weak var lblPeriodTitle: UILabel!
    
    
    @IBOutlet weak var viewCollProperty: UIView!
    @IBOutlet weak var viewCollPeriod: UIView!
    @IBOutlet weak var btnFilterApply: UIButton!
    @IBOutlet weak var btnFilterReset: UIButton!
    
    @IBOutlet weak var lblNoDataFound: UILabel!
    
    @IBOutlet weak var viewTblAssetsobjCopy: UIView!
    @IBOutlet weak var imgTblAssetsBackground: UIImageView!
    @IBOutlet weak var tblAssetsobjCopy: UITableView!

    
    var arrayPropFilter = [FilterPropertyDataList]()
    var arrayPeriodFilter = [FilterQuarterDataList]()
    
    var arrayData = [AssetProject]()
    var IsDetailGroup = false
    
    var strTimeFramePass = ""
    var strProTypePass = ""
    var pageAdded = 0
    var isPagingNeed = true
    var TotalFeed = 1
    
    var arrayDataCopy = [AssetProject]()
    var textFieldInsideSearchBar = UITextField()

    
    @IBOutlet weak var hightFilterCollProConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var hightQarterCollConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblCounterNews: UILabel!
    @IBOutlet weak var lblCounterAssets: UILabel!
    @IBOutlet weak var lblCounterDeals: UILabel!
    @IBOutlet weak var lblCounterLibrary: UILabel!
    
    @IBOutlet weak var imgViewBackground: UIImageView!

    @IBOutlet weak var viewFooter: UIView!
    
    @IBOutlet weak var imgFooterBG: UIImageView!
    
    @IBOutlet weak var imgNewsFooter: UIImageView!
    
    @IBOutlet weak var imgAssetFooter: UIImageView!
    
    @IBOutlet weak var imgDealFooter: UIImageView!
    
    @IBOutlet weak var imgLibFooter: UIImageView!

    @IBOutlet weak var imgMenu: UIImageView!
    @IBOutlet weak var imgFilter: UIImageView!

    @IBOutlet weak var imgFilterBG: UIImageView!
    
    @IBOutlet weak var imgFilterClose: UIImageView!
    
    
    @IBOutlet weak var viewInvestSummary: UIView!
    @IBOutlet weak var lblInvestSumTitle: UILabel!
    @IBOutlet weak var lblMyAssetsTitle: UILabel!
    @IBOutlet weak var lblAmountTitle: UILabel!
    @IBOutlet weak var lblFundedTitle: UILabel!
    @IBOutlet weak var tblSummaryObj: UITableView!
    @IBOutlet weak var viewInvestSum: UIView!
    
    let alignedFlowLayout = AlignedCollectionViewFlowLayout(horizontalAlignment: .left, verticalAlignment: .top)
    
    let alignedFlowLayout2 = AlignedCollectionViewFlowLayout(horizontalAlignment: .left, verticalAlignment: .top)
    var objRefreshControl = UIRefreshControl()
    
    @IBOutlet weak var hightViewInvestConstraint: NSLayoutConstraint!
    @IBOutlet weak var hightMainViewConstraint: NSLayoutConstraint!
    
    var arrAmountData = [LoginAmountData]()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        self.tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        self.title = nil

        self.setupInitialView()        
        tblAssetsobj.tableFooterView = UIView()
        tblAssetsobjCopy.tableFooterView = UIView()
        
        self.getAssetsFeed(strType: "0", strTimeFrame: strTimeFramePass, strProType: strProTypePass, isNeedToCallFilter: true)
        
        lblNoDataFound.isHidden = true
        lblNoDataFound.textColor = UIColor.white
        
        objRefreshControl.attributedTitle = NSAttributedString(string: "")
        objRefreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        objRefreshControl.tintColor = UIColor.clear
        tblAssetsobj.addSubview(objRefreshControl)

        NotificationCenter.default.addObserver(self, selector: #selector(refresh), name: NSNotification.Name(rawValue: kRefreshAssets), object: nil)

        tblAssetsobj.rowHeight = UITableViewAutomaticDimension
        tblAssetsobj.estimatedRowHeight = 255

         NotificationCenter.default.addObserver(self, selector: #selector(pushNotificationHandler(_:)) , name: NSNotification.Name(rawValue: kNewNotification), object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.getUpdateCounter()
        
        self.setUpThemeColor()
     
        tblAssetsobj.reloadData()
        collectionFilterPeriod.reloadData()
        collectionFilterProperty.reloadData()
        tblSummaryObj.reloadData()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.hightViewInvestConstraint.constant = 150 + 75
        }
    }
    
    
    override func viewWillLayoutSubviews() {

        //New Notification Counter
        if appDelegateObj.ObjCounter.new_news != "0" {
            lblCounterNews.isHidden = false
            lblCounterNews.text = appDelegateObj.ObjCounter.new_news
            
        }else{
            lblCounterNews.isHidden = true
        }
       
        lblCounterNews.layer.cornerRadius = lblCounterNews.frame.width/2
        lblCounterNews.layer.masksToBounds = true
        
        
        if appDelegateObj.ObjCounter.new_asset != "0" {
            lblCounterAssets.isHidden = false
            lblCounterAssets.text = appDelegateObj.ObjCounter.new_asset
            
        }else{
            lblCounterAssets.isHidden = true
        }
        lblCounterAssets.text = appDelegateObj.ObjCounter.new_asset
       
        lblCounterAssets.layer.cornerRadius = lblCounterAssets.frame.width/2
        lblCounterAssets.layer.masksToBounds = true
        
        if appDelegateObj.ObjCounter.new_deal != "0" {
            lblCounterDeals.isHidden = false
            lblCounterDeals.text = appDelegateObj.ObjCounter.new_deal
            
        }else{
            lblCounterDeals.isHidden = true
        }
        lblCounterDeals.text = appDelegateObj.ObjCounter.new_deal
       
        lblCounterDeals.layer.cornerRadius = lblCounterDeals.frame.width/2
        lblCounterDeals.layer.masksToBounds = true
        
        if appDelegateObj.ObjCounter.new_docs != "0" {
            lblCounterLibrary.isHidden = false
            lblCounterLibrary.text = appDelegateObj.ObjCounter.new_docs
            
        }else{
            lblCounterLibrary.isHidden = true
        }
        lblCounterLibrary.text = appDelegateObj.ObjCounter.new_docs
        
        lblCounterLibrary.layer.cornerRadius = lblCounterLibrary.frame.width/2
        lblCounterLibrary.layer.masksToBounds = true
        
    }
    
    //MARK:- Class Methods and Functions
    
    func setUpThemeColor(){
        if appDelegateObj.IsThemeLight == true{
            
            print("Ligth Theme mode")
            imgViewBackground.image = UIImage(named: "Wht_background")
            imgTblAssetsBackground.image = UIImage(named: "Wht_background")
            self.view.backgroundColor = wviewBgDefaultThemeColor
            lblTopTitle.textColor = wTopTitleFontColor
           // lblTopTitle.font = UIFont(name: wRubic, size: 20)
            imgMenu.image = UIImage(named: "menuBlue")
            imgSearch.image = UIImage(named: "searchBlue")
            imgFilter.image = UIImage(named: "Wht_Filter")
            imgFooterBG.image = UIImage(named: "Wht_FooterBG")
            imgNewsFooter.image = UIImage(named: "Wht_News")
            imgAssetFooter.image = UIImage(named: "Wht_AssetSelected")
            imgDealFooter.image = UIImage(named: "Wht_Deal")
            imgLibFooter.image = UIImage(named: "Wht_Library")
            imgFilterBG.image = UIImage(named: "Wht_background")
            imgFilterClose.image = UIImage(named: "Wht_close")
            
            lblCounterNews.textColor = UIColor.black
            lblCounterNews.backgroundColor = counterBgColor
            
            lblCounterAssets.textColor = UIColor.black
            lblCounterAssets.backgroundColor = counterBgColor
            
            lblCounterDeals.textColor = UIColor.black
            lblCounterDeals.backgroundColor = counterBgColor
            
            lblCounterLibrary.textColor = UIColor.black
            lblCounterLibrary.backgroundColor = counterBgColor
           
            lblMyAssetsTitle.textColor = wLightGrayColor
            lblAmountTitle.textColor = wLightGrayColor
            lblFundedTitle.textColor = wLightGrayColor
            
            self.viewInvestSum.layer.cornerRadius = 5
            self.viewInvestSum.layer.masksToBounds = true
            self.viewInvestSum.backgroundColor = wviewBgDefaultThemeColor
            viewInvestSum.layer.borderWidth = 1
            
            viewInvestSum.layer.borderColor = wBoxBorderColor.cgColor
            
            viewInvestSum.clipsToBounds = true
            lblInvestSumTitle.textColor = wBlueMainTitleColor
            
            textFieldInsideSearchBar.textColor = wBlueMainTitleColor
            
            lblFilterTitle.textColor = wBlueMainTitleColor
            lblPropertyTypeTitle.textColor = wBlueMainTitleColor
            lblPeriodTitle.textColor = wBlueMainTitleColor
            
            viewCollProperty.layer.cornerRadius = 5
            viewCollProperty.layer.borderWidth = 0.5
            viewCollProperty.layer.borderColor = wBlueMainTitleColor.cgColor
            viewCollProperty.layer.masksToBounds = true
            
            viewCollProperty.backgroundColor = UIColor.clear
            
            viewCollPeriod.layer.cornerRadius = 5
            viewCollPeriod.layer.borderWidth = 0.5
            viewCollPeriod.layer.borderColor = wBlueMainTitleColor.cgColor
            viewCollPeriod.layer.masksToBounds = true
            
            viewCollPeriod.backgroundColor = UIColor.clear
            
            btnFilterApply.backgroundColor = wBlueMainTitleColor
            btnFilterApply.setTitleColor(UIColor.white, for: .normal)
            
            btnFilterReset.backgroundColor = UIColor.darkGray
            btnFilterReset.setTitleColor(UIColor.white, for: .normal)
            
            lblNoDataFound.textColor = UIColor.black
        }
        else{
            print("Dark Theme mode")
            self.view.backgroundColor = TabViewColor
            imgViewBackground.image = UIImage(named: "background")
            imgTblAssetsBackground.image = UIImage(named: "background")
            lblTopTitle.textColor = TopTitleFontColor
          //  lblTopTitle.font = UIFont(name: MEDIUEM_FONT, size: 20)
            imgMenu.image = UIImage(named: "menu")
            imgSearch.image = UIImage(named: "Search-1")
            imgFilter.image = UIImage(named: "Filter Icon")
            imgFooterBG.image = UIImage(named: "footer-background")
            imgNewsFooter.image = UIImage(named: "News-icon")
            imgAssetFooter.image = UIImage(named: "MyAssets-selected")
            imgDealFooter.image = UIImage(named: "New-Deals-icon")
            imgLibFooter.image = UIImage(named: "Library-icon")
            
            lblCounterNews.textColor = UIColor.black
            lblCounterNews.backgroundColor = counterBgColor
            
            lblCounterAssets.textColor = UIColor.black
            lblCounterAssets.backgroundColor = counterBgColor
            
            lblCounterDeals.textColor = UIColor.black
            lblCounterDeals.backgroundColor = counterBgColor
            
            lblCounterLibrary.textColor = UIColor.black
            lblCounterLibrary.backgroundColor = counterBgColor
            
            imgFilterBG.image = UIImage(named: "background")
            imgFilterClose.image = UIImage(named: "closeNew")
            
            lblMyAssetsTitle.textColor = InvestTextColor
            lblAmountTitle.textColor = InvestTextColor
            lblFundedTitle.textColor = InvestTextColor
            
            self.viewInvestSum.layer.cornerRadius = 5
            self.viewInvestSum.layer.masksToBounds = true
            self.viewInvestSum.backgroundColor = kCellBackColor
            viewInvestSum.layer.borderWidth = 0.5
            
            viewInvestSum.layer.borderColor = kCellBackColor.cgColor
            
            viewInvestSum.clipsToBounds = true
            lblInvestSumTitle.textColor = wviewBgDefaultThemeColor
            
            textFieldInsideSearchBar.textColor = UIColor.white
            
            
            lblFilterTitle.textColor = UIColor.white
            lblPropertyTypeTitle.textColor = UIColor.white
            lblPeriodTitle.textColor = UIColor.white
            
            viewCollProperty.layer.cornerRadius = 5
            viewCollProperty.layer.masksToBounds = true
            
            viewCollProperty.backgroundColor = kCellBackColor
            
            viewCollPeriod.layer.cornerRadius = 5
            viewCollPeriod.layer.masksToBounds = true
            
            viewCollPeriod.backgroundColor = kCellBackColor
            btnFilterApply.backgroundColor = InvestTextColor
            btnFilterApply.setTitleColor(UIColor.white, for: .normal)
            
            btnFilterReset.backgroundColor = lineViewColor
            btnFilterReset.setTitleColor(UIColor.white, for: .normal)
            lblNoDataFound.textColor = UIColor.white
            
        }
        
        self.setupFontSize()
    }
    
    
    func setupInitialView(){
     
        viewRightFilter.isHidden = true
       
        btnFilterApply.layer.cornerRadius = 10
        btnFilterApply.layer.masksToBounds = true
        btnFilterApply.setTitle("Apply", for: .normal)
        
        
        btnFilterReset.layer.cornerRadius = 10
        btnFilterReset.layer.masksToBounds = true
        btnFilterReset.setTitle("Reset", for: .normal)
       
        tblSummaryObj.tableFooterView = UIView()
        
        viewSearch.isHidden = true
        viewTblAssetsobjCopy.isHidden = true
        textFieldInsideSearchBar = viewSearch.value(forKey: "searchField") as! UITextField
        textFieldInsideSearchBar.font = UIFont(name: D_DIN, size: 15)
     
    }
    func setupFontSize(){
        lblTopTitle.font = lblTopTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 20.0))
        lblMyAssetsTitle.font = lblMyAssetsTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 14.0))
        lblAmountTitle.font = lblAmountTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 14.0))
        lblFundedTitle.font = lblFundedTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 14.0))
        
        lblInvestSumTitle.font = lblInvestSumTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 14.0))
        
        lblFilterTitle.font = lblFilterTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 20.0))
        lblPropertyTypeTitle.font = lblPropertyTypeTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 14.0))
        lblPeriodTitle.font = lblPeriodTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 14.0))
        lblNoDataFound.font = lblNoDataFound.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 15.0))
        
        btnFilterReset.titleLabel?.font = UIFont(name:MEDIUEM_FONT, size: GlobalMethods.setThemeFontSize(objectSize: 17.0))
        btnFilterApply.titleLabel?.font = UIFont(name:MEDIUEM_FONT, size: GlobalMethods.setThemeFontSize(objectSize: 17.0))
        
    }
    func getAmountData(){
        arrAmountData = appDelegateObj.ProAmountDetail
    }

    @objc func refresh(_ sender: Any) {
        //  your code to refresh tableView
        print("Refresh start")
        
        self.getAssetsFeed(strType: "0", strTimeFrame: strTimeFramePass, strProType: strProTypePass, isNeedToCallFilter: true)

        
        objRefreshControl.endRefreshing()
        
    }
    
    @objc func btnBookmarkColl(sender:UIButton)  {
    
        let objModel = arrayData[sender.tag]
        
        let copyObj = objModel
        
        if copyObj.is_group == "1" {
            
//            appDelegateObj.strPostId = objModel.a_gp_id
//            appDelegateObj.strPostTypeId = "4" //fund type = 4
//
//            if objModel.is_bookmark == "1"{
//                copyObj.is_bookmark = "0"
//                self.removeBookmarkApi(strPostId: appDelegateObj.strPostId, strPostTypeId: appDelegateObj.strPostTypeId)
//
//            }else{
//                copyObj.is_bookmark = "1"
//                self.addBookmarkApi(strPostId: appDelegateObj.strPostId, strPostTypeId: appDelegateObj.strPostTypeId)
//            }
//
//            var copyArray = arrayData
//
//            copyArray.remove(at: sender.tag)
//            copyArray.insert(copyObj, at: sender.tag)
//
//            arrayData = copyArray
//            self.tblAssetsobj.reloadData()
            
            var strFlagPass = "0"
            //        if arrNews_detail[sender.tag].is_bookmark != nil {
            //            strFlagPass = "0"
            //        }
            
            appDelegateObj.strPostId = objModel.a_gp_id
            appDelegateObj.strPostTypeId = "4" //fund type = 4

            if arrayData[sender.tag].is_bookmark == "1"
            {
                strFlagPass = "0"
                self.removeBookmarkApi(strPostId: appDelegateObj.strPostId, strPostTypeId: appDelegateObj.strPostTypeId)
            }
            else if arrayData[sender.tag].is_bookmark == "0"
            {
                strFlagPass = "1"
                self.addBookmarkApi(strPostId: appDelegateObj.strPostId, strPostTypeId: appDelegateObj.strPostTypeId)
                
            }
            
            arrayData[sender.tag].is_bookmark = strFlagPass
            
            DispatchQueue.main.async(execute: {
                //            let indexPath = IndexPath(item: sender.tag, section: 0)
                //            self.objTableView.reloadRows(at: [indexPath], with: .automatic)
                self.tblAssetsobj.reloadData()
            })
            
            
            
        } else {
            
//            appDelegateObj.strPostId = objModel.a_id
//            appDelegateObj.strPostTypeId = "2" //asset type = 2
//
//            if objModel.is_bookmark == "1"{
//                copyObj.is_bookmark = "0"
//                self.removeBookmarkApi(strPostId: appDelegateObj.strPostId, strPostTypeId: appDelegateObj.strPostTypeId)
//
//            }else{
//                copyObj.is_bookmark = "1"
//                self.addBookmarkApi(strPostId: appDelegateObj.strPostId, strPostTypeId: appDelegateObj.strPostTypeId)
//            }
//
//            var copyArray = arrayData
//
//            copyArray.remove(at: sender.tag)
//            copyArray.insert(copyObj, at: sender.tag)
//
//            arrayData = copyArray
//            self.tblAssetsobj.reloadData()
            
            var strFlagPass = "0"
            //        if arrNews_detail[sender.tag].is_bookmark != nil {
            //            strFlagPass = "0"
            //        }
            
            appDelegateObj.strPostId = objModel.a_id
            appDelegateObj.strPostTypeId = "2" //asset type = 2

            if arrayData[sender.tag].is_bookmark == "1"
            {
                strFlagPass = "0"
                self.removeBookmarkApi(strPostId: appDelegateObj.strPostId, strPostTypeId: appDelegateObj.strPostTypeId)
            }
            else if arrayData[sender.tag].is_bookmark == "0"
            {
                strFlagPass = "1"
                self.addBookmarkApi(strPostId: appDelegateObj.strPostId, strPostTypeId: appDelegateObj.strPostTypeId)
                
            }
            
            arrayData[sender.tag].is_bookmark = strFlagPass
            
            DispatchQueue.main.async(execute: {
                //            let indexPath = IndexPath(item: sender.tag, section: 0)
                //            self.objTableView.reloadRows(at: [indexPath], with: .automatic)
                self.tblAssetsobj.reloadData()
            })
            
        }
        
    }
    
    @objc func pushNotificationHandler(_ notification : NSNotification) {
        // self.udateUI() // Add code to show a specific view on tap.
        print("notification view update \(notification.userInfo!)")
        
        let userInfo = notification.userInfo
        
        if let targetValue = userInfo![AnyHashable("postData")] as? String,
            let data = targetValue.data(using: .utf8) {
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String : Any] {
                    
                    print(json.keys)
                    
                    for (key, value) in json {
                        print("key \(key) value2 \(value)")
                        
                    }
                    
                    let type = json["title"] as! String
                    
                    if type == "Assets"{
                       
                        
                        guard json["a_id"] != nil else {
                            print("Assets a_id Nil")
                            
                            return
                            
                        }
                        
                   
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let ObjVC = storyboard.instantiateViewController(withIdentifier: "MyAssetsDetailVC") as! MyAssetsDetailVC
                        let strName = "\(json["a_name"]!)"
                        ObjVC.strAssetTitle = strName
                        //                        ObjVC.objMyAsset = json["title"] as! String
                        let strId = "\(json["a_id"]!)"
                        ObjVC.strAssetId = strId
                        ObjVC.IsNewDealDetail = false
                        self.navigationController?.pushViewController(ObjVC, animated: true)
                        
                        //                        }
                    }
                    
                    if type == "News"{
                        print("News Clicked")
                        
                        guard json["n_id"] != nil else {
                            print("News n_id Nil")
                            
                            return
                            
                        }
                        
                      
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let ObjVC = storyboard.instantiateViewController(withIdentifier: "NewsDetailsVC") as! NewsDetailsVC
                        ObjVC.isNewsDetail = true
                        let strId = "\(json["n_id"]!)"
                        ObjVC.strN_id = strId
                        
                        self.navigationController?.pushViewController(ObjVC, animated: true)
                      
                        
                    }
                    
                    if type == "Fund"{
                        print("Fund Clicked")
                    
                        
                        guard json["g_a_id"] != nil else {
                            print("Fund Nil")
                            
                            return
                            
                        }
                        
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let ObjVC = storyboard.instantiateViewController(withIdentifier: "FundsDetailVC") as! FundsDetailVC
                        //                        ObjVC.objFund = arrayData[indexPath.row]
                        let strId = "\(json["g_a_id"]!)"
                        ObjVC.strFundId = strId
                        self.navigationController?.pushViewController(ObjVC, animated: true)
                        
                        //                        }
                        
                        
                    }
                    if type == "Library"{
                        print("Document Clicked")
                        
                      
                        
                        guard json["document"] != nil else {
                            print("document Nil")
                            
                            return
                            
                        }
                        
                        
                        let strDocId = "\(json["doc_id"]!)"
                        self.IsReadPostApi(strPostId: strDocId, strPostTypeId: "3")
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let ObjVC = storyboard.instantiateViewController(withIdentifier: "DocumentVC") as! DocumentVC
                        
                        
                        let strDocpath = "\(json["document"]!)"
                        
                        if json["doc_name"] != nil{
                            let strDocName = "\(json["doc_name"]!)"
                             ObjVC.strDocTitle = strDocName
                        }
                   
                        ObjVC.strUrlPath = strDocpath
                        self.navigationController?.pushViewController(ObjVC, animated: true)
                    }
                    
                    if type == "New Deals"{
                        
                        print("New Deals Clicked")
                        
                        if "\(json["a_id"]!)" == "" {
                            print("New Deals a_id Nil")
                            
                            return
                        }
                        
                        
                        // Code you want to be delayed
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let ObjVC = storyboard.instantiateViewController(withIdentifier: "MyAssetsDetailVC") as! MyAssetsDetailVC
                        let strName = "\(json["a_name"]!)"
                        ObjVC.strAssetTitle = strName
                        //                        ObjVC.objMyAsset = json["title"] as! String
                        let strId = "\(json["a_id"]!)"
                        ObjVC.strAssetId = strId
                        ObjVC.IsNewDealDetail = true
                        self.navigationController?.pushViewController(ObjVC, animated: true)
                        
                        //                        }
                    }
                    
                }
                else {
                    print("JSON is invalid")
                }
            }
            catch {
                print("Exception converting: \(error)")
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        
        if (scrollView.contentOffset.y <= -50){
            //reach top
            //print("SSS TOP")
            self.getAssetsFeed(strType: "0", strTimeFrame: strTimeFramePass, strProType: strProTypePass, isNeedToCallFilter: false)
        }
        
       
        
        
    }
    
    //MARK:- IBActions
    @IBAction func btnNewsPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 0
    }
    @IBAction func btnMyNewDealsPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 2
    }
    @IBAction func btnLibraryPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 3
    }
    @IBAction func btnMenuPressed(_ sender: Any) {
        showLeftViewAnimated(sender)
    }
    @IBAction func btnFilterPressed(_ sender: Any) {
        self.setViewHideShowWithAnimarionRightToLeft(view: viewRightFilter, hidden: false)
        let oldFrame = viewRightFilterInside.frame
        var updateFrame = viewRightFilterInside.frame
        updateFrame.origin.x = viewRightFilterInside.frame.width
        viewRightFilterInside.frame = updateFrame
        
        UIView.transition(with: self.view, duration: 0.2, options: .curveEaseIn, animations: {
            self.viewRightFilterInside.frame = oldFrame
        })
    }
    @IBAction func btnCloseFilterPressed(_ sender: Any) {
        let oldFrame = viewRightFilterInside.frame
        var updateFrame = viewRightFilterInside.frame
        updateFrame.origin.x = viewRightFilter.frame.width
        
        UIView.transition(with: self.view, duration: 0.2, options: .curveEaseOut, animations: {
            self.viewRightFilterInside.frame = updateFrame
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.setViewHideShowWithAnimarionRightToLeft(view: self.viewRightFilter, hidden: true)
            self.viewRightFilterInside.frame = oldFrame
        }
    }
    @IBAction func btnFilterApplyPressed(_ sender: Any) {
        let filterProp = arrayPropFilter.filter({$0.is_checked})
        let arrStrPro = filterProp.map{$0.p_type_id} as NSArray
        strProTypePass = arrStrPro.componentsJoined(by: ",")
        
        let filetrQarter = arrayPeriodFilter.filter({$0.is_checked})
        let arrStrQ = filetrQarter.map{$0.quarter_id} as NSArray
        strTimeFramePass = arrStrQ.componentsJoined(by: ",")
        
        if strProTypePass.count > 0 || strTimeFramePass.count > 0{
            self.getAssetsFeed(strType: "0", strTimeFrame: strTimeFramePass, strProType: strProTypePass, isNeedToCallFilter: false)
        }
        
        self.btnCloseFilterPressed(self)
    }
    @IBAction func btnFilterResetPressed(_ sender: Any) {
        
        let filterProp = arrayPropFilter.filter({$0.is_checked})
        let arrStrPro = filterProp.map{$0.p_type_id} as NSArray
        strProTypePass = arrStrPro.componentsJoined(by: ",")
        
        let filetrQarter = arrayPeriodFilter.filter({$0.is_checked})
        let arrStrQ = filetrQarter.map{$0.quarter_id} as NSArray
        strTimeFramePass = arrStrQ.componentsJoined(by: ",")
        
        if strProTypePass.count > 0 || strTimeFramePass.count > 0{
            strTimeFramePass = ""
            strProTypePass = ""
            self.getAssetsFeed(strType: "0", strTimeFrame: strTimeFramePass, strProType: strProTypePass, isNeedToCallFilter: true)
        }
        
       self.btnCloseFilterPressed(self)

    }
    
    @IBAction func btnSearchPressed(_ sender: Any) {
        if self.arrayData.count > 0{
            lblTopTitle.isHidden = true
            viewSearch.isHidden = false
            imgSearch.isHidden = true
            btnSearch.isHidden = true
            btnFilter.isHidden = true
            imgFilter.isHidden = true
            viewTblAssetsobjCopy.isHidden = false
            arrayDataCopy = self.arrayData
        }
    }
    
    //MARK:- UISearchbar Methods
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        self.closeSearchView()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        viewSearch.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.filterTextArray(withString: searchText)
    }
    
    func filterTextArray(withString searchText: String) {
        var result = [AssetProject]()
      
        result = arrayDataCopy.filter{ $0.a_name.range(of: searchText, options: [.caseInsensitive]) != nil
            || $0.p_type_name.range(of: searchText, options: [.caseInsensitive]) != nil || $0.group_name.range(of: searchText, options: [.caseInsensitive]) != nil}
        
        print(result.count)
        arrayData = result
        tblAssetsobjCopy.reloadData()
    }
    
    func closeSearchView(){
        viewSearch.endEditing(true)
        lblTopTitle.isHidden = false
        viewSearch.isHidden = true
        imgSearch.isHidden = false
        btnSearch.isHidden = false
        btnFilter.isHidden = false
        imgFilter.isHidden = false
        self.arrayData = arrayDataCopy
        viewTblAssetsobjCopy.isHidden = true
        tblAssetsobjCopy.reloadData()
    }
    
    //MARK:- UITableView Methods
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == tblAssetsobj || tableView == tblAssetsobjCopy{
            return arrayData.count
        }
        else{
            var intRowCount = 0
            
            if arrayData.count > 0{
                intRowCount = arrayData.count + 1
            }
            
            return intRowCount
        }
        
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == tblAssetsobj || tableView == tblAssetsobjCopy{
            let tablecell = tableView.dequeueReusableCell(withIdentifier: "AssetsTblCell", for: indexPath) as! AssetsTblCell
            
            let objModel = arrayData[indexPath.row]
            
            let imgPath = objModel.a_asset_img
            tablecell.imgCell.sd_setImage(with: URL(string: imgPath), completed: nil)
            
            
            tablecell.preservesSuperviewLayoutMargins = false
            tablecell.separatorInset = UIEdgeInsets.zero
            tablecell.layoutMargins = UIEdgeInsets.zero
            
            if objModel.is_bookmark == "0" {
            
                if appDelegateObj.IsThemeLight == true{
                    tablecell.imgBookmark.image = UIImage(named: "Wht_Bookmark")
                }
                else{
                    tablecell.imgBookmark.image = UIImage(named: "Bookmark")
                }
                
            }else{
                if appDelegateObj.IsThemeLight == true{
                    tablecell.imgBookmark.image = UIImage(named: "Wht_BookmarkFill")
                }
                else{
                    tablecell.imgBookmark.image = UIImage(named: "Bookmark-fill")
                }
                
            }
            
            if objModel.is_viewed == "0" {
                tablecell.imgRibbon.isHidden = false
                if appDelegateObj.IsThemeLight == true{
                    tablecell.imgRibbon.image = UIImage(named: "Wht_Ribbon-with-text")
                }
                else{
                    tablecell.imgRibbon.image = UIImage(named: "Ribbon-with-text")
                }
            }else{
                tablecell.imgRibbon.isHidden = true
            }
            
            if objModel.is_group == "0"{
                tablecell.lblTitle.text = objModel.a_name.uppercased()
                tablecell.lblTitle.numberOfLines = 2
                tablecell.lblDetail.text = objModel.p_type_name
                tablecell.lblDetail.numberOfLines = 0
                tablecell.lblQuater.text = "\(objModel.a_city), \(objModel.a_state)"
                
            }
            else{
                tablecell.lblTitle.text = objModel.group_name.uppercased()
                tablecell.lblTitle.numberOfLines = 2
                tablecell.lblDetail.text = ""
                tablecell.lblQuater.text = ""
                
            }
            
            
            
           
            tablecell.lblAmount.text = self.getNumberFormat(strAmount: objModel.amount)
            
            tablecell.lblDate.text = objModel.date_funded
            
            
            tablecell.btnBookmark.tag = indexPath.row
            tablecell.btnBookmark.addTarget(self, action: #selector(self.btnBookmarkColl), for: .touchUpInside)
            
            return tablecell
            
        }
        else
        {
            let tablecell = tableView.dequeueReusableCell(withIdentifier: "investmentSummaryCell", for: indexPath) as! investmentSummaryCell
            
            
            if indexPath.row == arrayData.count{
                var totalAmount = 0.0
                
                for objModel in arrayData{
                    var price = 0.0

                    let myString = objModel.amount
                    if myString.count > 0{
                        price = Double(myString) as! Double
                    }
                    totalAmount = totalAmount + price
                }
                
                tablecell.lblAssets.text = "Total Amount"
               
                tablecell.lblAmount.text = self.getNumberFormat(strAmount: "\(totalAmount)")
                tablecell.lblAssets.textColor = UIColor.white
                tablecell.lblAssets.font = UIFont(name: DINPROBold, size: 13)
                tablecell.lblFunded.text = ""
                
                if appDelegateObj.IsThemeLight{
                    tablecell.lblAssets.textColor = wBlueMainTitleColor
                }else{
                    tablecell.lblAssets.textColor = wDarkGrayColor
                }
                
            }else{
              
                
                if arrayData[indexPath.row].is_group == "0"{
                    tablecell.lblAssets.text = arrayData[indexPath.row].a_name.uppercased()
                }
                else{
                    tablecell.lblAssets.text = arrayData[indexPath.row].group_name.uppercased()
                }
                
             //  tablecell.lblAmount.text = "$" + arrayData[indexPath.row].amount
                tablecell.lblAmount.text = self.getNumberFormat(strAmount: arrayData[indexPath.row].amount)
               tablecell.lblFunded.text = arrayData[indexPath.row].date_funded
                tablecell.lblAssets.textColor = UIColor.white
                tablecell.lblAssets.font = UIFont(name: MEDIUEM_FONT, size: 13)
                
                if appDelegateObj.IsThemeLight{
                    tablecell.lblAssets.textColor = wBlueMainTitleColor
                }else{
                    tablecell.lblAssets.textColor = wDarkGrayColor
                }
                
            }
            
            return tablecell
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if tableView == tblAssetsobj{
            
            self.hightMainViewConstraint.constant = self.tblAssetsobj.contentSize.height + 150 + 75 + 20
            
        
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                
                if self.arrayData.count > indexPath.row{
                    if self.arrayData[indexPath.row].is_viewed == "0" {
                        
                        let copyObj = self.arrayData[indexPath.row]
                        copyObj.is_viewed = "1"
                        var copyArray = self.arrayData
                        copyArray.remove(at: indexPath.row)
                        copyArray.insert(copyObj, at: indexPath.row)
                        self.arrayData = copyArray
                        self.tblAssetsobj.reloadData()
                        
                        if copyObj.is_group == "0" {
                            self.IsReadPostApi(strPostId: copyObj.a_id, strPostTypeId: "0")
                        }
                        else{
                            self.IsReadPostApi(strPostId: copyObj.a_gp_id, strPostTypeId: "1")
                        }
                    }
                }

            }
            
        }
    
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView == tblAssetsobj || tableView == tblAssetsobjCopy{
            if arrayData[indexPath.row].is_viewed == "0"{
                
                let copyObj = arrayData[indexPath.row]
                copyObj.is_viewed = "1"
                var copyArray = arrayData
                copyArray.remove(at: indexPath.row)
                copyArray.insert(copyObj, at: indexPath.row)
                arrayData = copyArray
                self.tblAssetsobj.reloadData()
                
            }
            
            
            let objModel = arrayData[indexPath.row]
            
            if objModel.is_group == "0" {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let ObjVC = storyboard.instantiateViewController(withIdentifier: "MyAssetsDetailVC") as! MyAssetsDetailVC
                ObjVC.strAssetTitle = arrayData[indexPath.row].a_name
                ObjVC.objMyAsset = arrayData[indexPath.row]
                ObjVC.strAssetId = arrayData[indexPath.row].a_id
                ObjVC.IsNewDealDetail = false
                self.navigationController?.pushViewController(ObjVC, animated: true)
                
            }else{
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let ObjVC = storyboard.instantiateViewController(withIdentifier: "FundsDetailVC") as! FundsDetailVC
                ObjVC.objFund = arrayData[indexPath.row]
                self.navigationController?.pushViewController(ObjVC, animated: true)
                
            }
        }
        else{
            
            if indexPath.row == arrayData.count{
                return
            }
            
        
            let objModel = arrayData[indexPath.row]
            
            if objModel.is_group == "0" {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let ObjVC = storyboard.instantiateViewController(withIdentifier: "MyAssetsDetailVC") as! MyAssetsDetailVC
                ObjVC.strAssetTitle = arrayData[indexPath.row].a_name
                ObjVC.objMyAsset = arrayData[indexPath.row]
                ObjVC.strAssetId = arrayData[indexPath.row].a_id
                ObjVC.IsNewDealDetail = false
                self.navigationController?.pushViewController(ObjVC, animated: true)
                
            }else{
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let ObjVC = storyboard.instantiateViewController(withIdentifier: "FundsDetailVC") as! FundsDetailVC
                ObjVC.objFund = arrayData[indexPath.row]
                self.navigationController?.pushViewController(ObjVC, animated: true)
                
            }
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
//    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//
//        if tableView == tblAssetsobj{
//            if arrayData[indexPath.row].is_viewed == "0" {
//
//                let copyObj = arrayData[indexPath.row]
//                copyObj.is_viewed = "1"
//                var copyArray = arrayData
//                copyArray.remove(at: indexPath.row)
//                copyArray.insert(copyObj, at: indexPath.row)
//                arrayData = copyArray
//                self.tblAssetsobj.reloadData()
//
//                if copyObj.is_group == "0" {
//                    self.IsReadPostApi(strPostId: copyObj.a_id, strPostTypeId: "0")
//                }
//                else{
//                    self.IsReadPostApi(strPostId: copyObj.a_gp_id, strPostTypeId: "1")
//                }
//            }
//        }
//
//
//    }
//
    
    //MARK:- UICollectionView Delegate Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collectionFilterProperty{
            return arrayPropFilter.count
        }
        else{
            return arrayPeriodFilter.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellFilter", for: indexPath) as! cellFilter
        
        cell.layer.cornerRadius = 10
        cell.layer.borderWidth = 1
        cell.clipsToBounds = true
        cell.backgroundColor = UIColor.clear
        
        if collectionView == collectionFilterProperty{
            cell.lblTitle.text = arrayPropFilter[indexPath.row].p_type_name
            
            if arrayPropFilter[indexPath.row].is_checked{

                cell.layer.backgroundColor = InvestTextColor.cgColor
                cell.lblTitle.textColor = UIColor.white

                if appDelegateObj.IsThemeLight == true{
                    cell.layer.borderColor = wBlueMainTitleColor.cgColor
                    cell.layer.backgroundColor = wBlueMainTitleColor.cgColor
                    cell.lblTitle.textColor = UIColor.white
                    

                }else{
                    cell.layer.borderColor = InvestTextColor.cgColor
                    cell.layer.backgroundColor = InvestTextColor.cgColor
                    cell.lblTitle.textColor = UIColor.white

                }
            }
            else{
                cell.layer.borderColor =  UIColor.lightGray.cgColor
                cell.lblTitle.textColor = UIColor.lightGray

                if appDelegateObj.IsThemeLight == true{
                    cell.layer.borderColor = wBlueMainTitleColor.cgColor
                    cell.layer.backgroundColor = UIColor.white.cgColor
                    cell.lblTitle.textColor = wBlueMainTitleColor
                    
                }else{
                    cell.layer.borderColor =  UIColor.lightGray.cgColor
                    cell.lblTitle.textColor = UIColor.lightGray

                }
            }
        }
        else{
            cell.lblTitle.text = arrayPeriodFilter[indexPath.row].quarter_name
            
            if arrayPeriodFilter[indexPath.row].is_checked{
//                cell.layer.borderColor =  UIColor.white.cgColor
                cell.layer.backgroundColor = InvestTextColor.cgColor
                cell.lblTitle.textColor = UIColor.white

                if appDelegateObj.IsThemeLight == true{
                    
                    cell.layer.borderColor = wBlueMainTitleColor.cgColor
                    cell.layer.backgroundColor = wBlueMainTitleColor.cgColor
                    cell.lblTitle.textColor = UIColor.white
                  
                }else{
                    cell.layer.backgroundColor = InvestTextColor.cgColor
                    cell.lblTitle.textColor = UIColor.white

                }
            }
            else{
                cell.layer.borderColor =  UIColor.lightGray.cgColor
                cell.lblTitle.textColor = UIColor.lightGray

                if appDelegateObj.IsThemeLight == true{
                    cell.layer.borderColor = wBlueMainTitleColor.cgColor
                    cell.layer.backgroundColor = UIColor.white.cgColor
                    cell.lblTitle.textColor = wBlueMainTitleColor
                }else{
                    cell.layer.borderColor =  UIColor.lightGray.cgColor
                    cell.lblTitle.textColor = UIColor.lightGray

                }
            }
        }
        
        cell.lblTitle.font = cell.lblTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 13.0))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        var size1: CGSize
        
        let fontSizePass = GlobalMethods.setThemeFontSize(objectSize: 19.0)
        
        if collectionView == collectionFilterProperty{
            size1 = (arrayPropFilter[indexPath.row].p_type_name.uppercased() as AnyObject).size(withAttributes: [NSAttributedStringKey.font: UIFont(name: MEDIUEM_FONT, size: fontSizePass)!] )
        }else{
            
            size1 = (arrayPeriodFilter[indexPath.row].quarter_name.uppercased() as AnyObject).size(withAttributes: [NSAttributedStringKey.font: UIFont(name: MEDIUEM_FONT, size: fontSizePass)!] )
        }
        
        let width: CGFloat = CGFloat(size1.width + 8)
        let height: CGFloat = 30
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        
        if collectionView == collectionFilterProperty{
            var objModel = FilterPropertyDataList()
            var copyArray = [FilterPropertyDataList]()
            objModel = arrayPropFilter[indexPath.row]
            copyArray = arrayPropFilter
            let copyObj = objModel
            
            if objModel.is_checked{
                copyObj.is_checked = false
            }else{
                copyObj.is_checked = true
            }
            

            copyArray.remove(at: indexPath.row)
            copyArray.insert(copyObj, at: indexPath.row)
            
            arrayPropFilter = copyArray
            self.collectionFilterProperty.reloadData()
            //self.collectionFilterProperty.collectionViewLayout = self.alignedFlowLayout
            
            
        }
        else{
            var objModel = FilterQuarterDataList()
            var copyArray = [FilterQuarterDataList]()
            objModel = arrayPeriodFilter[indexPath.row]
            copyArray = arrayPeriodFilter
            
            let copyObj = objModel
            
            if objModel.is_checked{
                copyObj.is_checked = false
            }else{
                copyObj.is_checked = true
            }
            
            copyArray.remove(at: indexPath.row)
            copyArray.insert(copyObj, at: indexPath.row)
            
            arrayPeriodFilter = copyArray
            self.collectionFilterPeriod.reloadData()
          //  self.collectionFilterPeriod.collectionViewLayout = self.alignedFlowLayout
        }
    }
    
}

class LeftAlignedCollectionViewFlowLayout: UICollectionViewFlowLayout {
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let attributes = super.layoutAttributesForElements(in: rect)
        
        var leftMargin = sectionInset.left
        var maxY: CGFloat = -1.0
        attributes?.forEach { layoutAttribute in
            if layoutAttribute.frame.origin.y >= maxY {
                leftMargin = sectionInset.left
            }
            
            layoutAttribute.frame.origin.x = leftMargin
            
            leftMargin += layoutAttribute.frame.width + minimumInteritemSpacing
            maxY = max(layoutAttribute.frame.maxY , maxY)
        }
        
        return attributes
    }
}
