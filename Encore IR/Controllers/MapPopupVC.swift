//
//  MapPopupVC.swift
//  Encore Direct
//
//  Created by ravi on 29/06/19.
//  Copyright © 2019 ravi. All rights reserved.
//

import UIKit
import GoogleMaps

class MapPopupVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var imgBack: UIImageView!
    
    @IBOutlet var gmap: GMSMapView!
    
    //MARK:- Variable
    var centerMapCoordinate = CLLocationCoordinate2D()
    var strLocationAddress = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupInitialView()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.setUpThemeColor()
    }
    

    //MARK:- Class Methods and Functions
    func setupInitialView(){
        
        lblTopTitle.text = strLocationAddress
        
        let marker = GMSMarker()
        marker.position = centerMapCoordinate
        //            marker.icon = imageConverted
        marker.map = gmap
        gmap.camera = GMSCameraPosition.camera(withTarget: marker.position, zoom: 11)

    }
    
    func setUpThemeColor(){
        if appDelegateObj.IsThemeLight == true{
            
            print("Ligth Theme mode")
            imgBackground.image = UIImage(named: "Wht_background")
            imgBack.image = UIImage(named: "Wht_Back")
            self.view.backgroundColor = wviewBgDefaultThemeColor
            lblTopTitle.textColor = wTopTitleFontColor
            
        }
        else{
            print("Dark Theme mode")
            self.view.backgroundColor = TabViewColor
            imgBackground.image = UIImage(named: "background")
            imgBack.image = UIImage(named: "Back")
            lblTopTitle.textColor = TopTitleFontColor
      
        }
        self.setupFontSize()
    }
    func setupFontSize(){
        lblTopTitle.font = lblTopTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 20.0))
        
    }
    
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
