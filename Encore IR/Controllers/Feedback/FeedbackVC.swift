//
//  FeedbackVC.swift
//  Encore IR
//
//  Created by ravi on 08/05/19.
//  Copyright © 2019 ravi. All rights reserved.
//

import UIKit
import LGSideMenuController


class FeedbackVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UITextViewDelegate {

    @IBOutlet weak var imgBackground: UIImageView!
    
    @IBOutlet weak var viewTop: UIView!
    
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var imgMenu: UIImageView!
    @IBOutlet weak var btnMenu: UIButton!
    
    @IBOutlet weak var objScrollView: UIScrollView!
    
    
    @IBOutlet weak var txtName: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtContact: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtEmail: SkyFloatingLabelTextField!
    
    @IBOutlet weak var viewTextview: UIView!
    
    @IBOutlet weak var lblMessageTitle: UILabel!
    
    @IBOutlet weak var txtMessage: UITextView!
    
    @IBOutlet weak var viewAddImage: UIView!
    
    @IBOutlet weak var lblAddImage: UILabel!
    
    @IBOutlet weak var imgAddImage: UIImageView!
    
    
    @IBOutlet weak var btnAddImage: UIButton!
    
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    
    @IBOutlet weak var imgFooterBG: UIImageView!
    @IBOutlet weak var viewFooter: UIView!
    
    @IBOutlet weak var viewNewsFooter: UIView!
    @IBOutlet weak var imgNews: UIImageView!
    @IBOutlet weak var btnNews: UIButton!
    
    @IBOutlet weak var viewMyAssetFooter: UIView!
    @IBOutlet weak var imgAssetFooter: UIImageView!
    @IBOutlet weak var btnAssets: UIButton!
    
    @IBOutlet weak var viewNewDealFooter: UIView!
    @IBOutlet weak var imgDealFooter: UIImageView!
    @IBOutlet weak var btnDeal: UIButton!
    
    @IBOutlet weak var viewLibraryFooter: UIView!
    @IBOutlet weak var imgLibraryFooter: UIImageView!
    @IBOutlet weak var btnLibrary: UIButton!
    
    @IBOutlet weak var viewScrollBG: UIView!
    
    @IBOutlet weak var lblCounterNews: UILabel!
    @IBOutlet weak var lblCounterAssets: UILabel!
    @IBOutlet weak var lblCounterDeals: UILabel!
    @IBOutlet weak var lblCounterLibrary: UILabel!

    
    var IsImageUploaded = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupInitialView()
        self.tabBarController?.moreNavigationController.isNavigationBarHidden = true

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupTheme()
        self.setupFontSize()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.imgAddImage.image = UIImage(named: "plus-box")
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        btnSubmit.setRoundedCorners(ratio: 0.10)

        btnSubmit.titleLabel?.font = UIFont(name:DINPROBold, size: 15.0)
        btnSubmit.titleLabel?.text = strSubmit
        
        btnSubmit.titleLabel?.adjustsFontSizeToFitWidth = true
        
        //New Notification Counter
        if appDelegateObj.ObjCounter.new_news != "0" {
            lblCounterNews.isHidden = false
            lblCounterNews.text = appDelegateObj.ObjCounter.new_news
            
        }else{
            lblCounterNews.isHidden = true
        }
        
        lblCounterNews.layer.cornerRadius = lblCounterNews.frame.width/2
        lblCounterNews.layer.masksToBounds = true
        
        
        if appDelegateObj.ObjCounter.new_asset != "0" {
            lblCounterAssets.isHidden = false
            lblCounterAssets.text = appDelegateObj.ObjCounter.new_asset
            
        }else{
            lblCounterAssets.isHidden = true
        }
        lblCounterAssets.text = appDelegateObj.ObjCounter.new_asset
        
        lblCounterAssets.layer.cornerRadius = lblCounterAssets.frame.width/2
        lblCounterAssets.layer.masksToBounds = true
        
        if appDelegateObj.ObjCounter.new_deal != "0" {
            lblCounterDeals.isHidden = false
            lblCounterDeals.text = appDelegateObj.ObjCounter.new_deal
            
        }else{
            lblCounterDeals.isHidden = true
        }
        lblCounterDeals.text = appDelegateObj.ObjCounter.new_deal
        
        lblCounterDeals.layer.cornerRadius = lblCounterDeals.frame.width/2
        lblCounterDeals.layer.masksToBounds = true
        
        if appDelegateObj.ObjCounter.new_docs != "0" {
            lblCounterLibrary.isHidden = false
            lblCounterLibrary.text = appDelegateObj.ObjCounter.new_docs
            
        }else{
            lblCounterLibrary.isHidden = true
        }
        lblCounterLibrary.text = appDelegateObj.ObjCounter.new_docs
       
        lblCounterLibrary.layer.cornerRadius = lblCounterLibrary.frame.width/2
        lblCounterLibrary.layer.masksToBounds = true
        
    }
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        
        self.view.backgroundColor = TabViewColor
        
        txtName.placeholder = "Name"
        txtName.title = "Name"
        txtName.text = "\(appDelegateObj.loginUser.u_fname) \(appDelegateObj.loginUser.u_lname)"
       // txtName.font = UIFont(name: Rubic, size: 15)
        txtName.delegate = self
     
        txtContact.placeholder = "Contact Number"
        txtContact.title = "Contact Number"
        txtContact.text = appDelegateObj.loginUser.u_contact_1
     //   txtContact.font = UIFont(name: Rubic, size: 15)
        txtContact.delegate = self
      
        txtEmail.placeholder = "Email"
        txtEmail.title = "Email"
        txtEmail.text = appDelegateObj.loginUser.u_email
        //txtEmail.font = UIFont(name: Rubic, size: 15)
        txtEmail.delegate = self
        
        txtMessage.text = msgfeedbackplc
        txtMessage.delegate = self
   
    }
    
    func setupFontSize(){
        lblTopTitle.font = lblTopTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 20.0))
        txtName.font = txtName.font!.withSize(GlobalMethods.setThemeFontSize(objectSize: 15.0))
        txtContact.font = txtContact.font!.withSize(GlobalMethods.setThemeFontSize(objectSize: 15.0))
        txtEmail.font = txtEmail.font!.withSize(GlobalMethods.setThemeFontSize(objectSize: 15.0))
        lblMessageTitle.font = lblMessageTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 13.0))
        txtMessage.font = txtMessage.font!.withSize(GlobalMethods.setThemeFontSize(objectSize: 15.0))
        lblAddImage.font = lblAddImage.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 13.0))
    
        btnSubmit.titleLabel?.font = UIFont(name:MEDIUEM_FONT, size: GlobalMethods.setThemeFontSize(objectSize: 17.0))
        
        
    }
    
    func setupTheme(){
        
        
        viewScrollBG.layer.borderWidth = 1
        if appDelegateObj.IsThemeLight == true{
            viewScrollBG.layer.borderColor = wBoxBorderColor.cgColor
            self.viewScrollBG.backgroundColor = wviewBgDefaultThemeColor
        }
        else{
            viewScrollBG.layer.borderColor = kCellBackColor.cgColor
            self.viewScrollBG.backgroundColor = kCellBackColor
        }
        
        if appDelegateObj.IsThemeLight == true{
            print("Ligth Theme mode")
            
           

            imgBackground.image = UIImage(named: "Wht_background")
            self.view.backgroundColor = wviewBgDefaultThemeColor
            lblTopTitle.textColor = wBlueMainTitleColor
            
            lblCounterNews.textColor = UIColor.black
            lblCounterNews.backgroundColor = counterBgColor
            
            lblCounterAssets.textColor = UIColor.black
            lblCounterAssets.backgroundColor = counterBgColor
            
            lblCounterDeals.textColor = UIColor.black
            lblCounterDeals.backgroundColor = counterBgColor
            
            lblCounterLibrary.textColor = UIColor.black
            lblCounterLibrary.backgroundColor = counterBgColor
            
            
            
            txtName.titleColor = wLightGrayColor
            txtName.selectedTitleColor = wDarkGrayColor
            txtName.selectedLineColor = wLightGrayColor
            txtName.lineColor = wLightGrayColor
            txtName.textColor = wDarkGrayColor
            txtName.tintColor = wDarkGrayColor
          
            txtContact.titleColor = wLightGrayColor
            txtContact.selectedTitleColor = wDarkGrayColor
            txtContact.selectedLineColor = wLightGrayColor
            txtContact.lineColor = wLightGrayColor
            txtContact.textColor = wDarkGrayColor
            txtContact.tintColor = wDarkGrayColor
            
            txtEmail.titleColor = wLightGrayColor
            txtEmail.selectedTitleColor = wDarkGrayColor
            txtEmail.selectedLineColor = wLightGrayColor
            txtEmail.lineColor = wLightGrayColor
            txtEmail.textColor = wDarkGrayColor
            txtEmail.tintColor = wDarkGrayColor
            
            txtMessage.tintColor = wDarkGrayColor
            txtMessage.textColor = wDarkGrayColor
            lblMessageTitle.textColor = wDarkGrayColor
            lblAddImage.textColor = wLightGrayColor
            
            btnSubmit.backgroundColor = wBlueMainTitleColor
            
            imgMenu.image = UIImage(named: "menuBlue")
            imgFooterBG.image = UIImage(named: "Wht_FooterBG")
            imgNews.image = UIImage(named: "Wht_News")
            imgAssetFooter.image = UIImage(named: "Wht_Asset")
            imgDealFooter.image = UIImage(named: "Wht_Deal")
            imgLibraryFooter.image = UIImage(named: "Wht_Library")
            
        }
        else{
            print("Dark Theme mode")
            
            btnSubmit.backgroundColor = FilterFillColor
            
            
            self.view.backgroundColor = TabViewColor
            imgBackground.image = UIImage(named: "background")
            lblTopTitle.textColor = TopTitleFontColor
            
            lblCounterNews.textColor = UIColor.black
            lblCounterNews.backgroundColor = counterBgColor
            
            lblCounterAssets.textColor = UIColor.black
            lblCounterAssets.backgroundColor = counterBgColor
            
            lblCounterDeals.textColor = UIColor.black
            lblCounterDeals.backgroundColor = counterBgColor
            
            lblCounterLibrary.textColor = UIColor.black
            lblCounterLibrary.backgroundColor = counterBgColor
            
            txtName.titleColor = InvestTextColor
            txtName.selectedTitleColor = UIColor.white
            txtName.selectedLineColor = feedbackSepColor
            txtName.lineColor = feedbackSepColor
            txtName.textColor = UIColor.white
            txtName.tintColor = UIColor.white
            
            txtContact.titleColor = InvestTextColor
            txtContact.selectedTitleColor = UIColor.white
            txtContact.selectedLineColor = feedbackSepColor
            txtContact.lineColor = feedbackSepColor
            
            txtContact.textColor = UIColor.white
            txtContact.tintColor = UIColor.white
            
            txtEmail.titleColor = InvestTextColor
            txtEmail.selectedTitleColor = UIColor.white
            txtEmail.selectedLineColor = feedbackSepColor
            txtEmail.lineColor = feedbackSepColor
            txtEmail.textColor = UIColor.white
            txtEmail.tintColor = UIColor.white
            
            txtMessage.tintColor = UIColor.white
            txtMessage.textColor = UIColor.white
            lblMessageTitle.textColor = InvestTextColor
            lblAddImage.textColor = InvestTextColor
            
            imgMenu.image = UIImage(named: "menu")
            imgFooterBG.image = UIImage(named: "footer-background")
            imgNews.image = UIImage(named: "News-icon")
            imgAssetFooter.image = UIImage(named: "My-Assets-icon")
            imgDealFooter.image = UIImage(named: "New-Deals-icon")
            imgLibraryFooter.image = UIImage(named: "Library-icon")
            
        }
        
    }

    //MARK:- IBActions
    @IBAction func btnMenuPressed(_ sender: Any) {
        showLeftViewAnimated(sender)
    }
    
    
    @IBAction func btnAddImagePressed(_ sender: Any) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        
        alertController.addAction(UIAlertAction(title: strCamera, style: .default, handler: { (_) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePicker.sourceType = .camera
                self.present(imagePicker, animated: true, completion: nil)
            }
        }))
        
        alertController.addAction(UIAlertAction(title: strPhoto_Video_Library, style: .default, handler: { (_) in
            imagePicker.sourceType = .photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }))
                
        
        alertController.addAction(UIAlertAction(title: strCancel, style: .destructive, handler: { (_) in
            alertController.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
    @IBAction func btnSubmitPressed(_ sender: Any) {
        
        if (txtName.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: msgUserName, viewcontrolelr: self)
        }
        else if (txtContact.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: msgUserContact, viewcontrolelr: self)
        }
        else if (txtContact.text?.count)! < intMinContactLenght{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: msgUserValidContact, viewcontrolelr: self)
        }
        else if (txtEmail.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: msgEnterEmail, viewcontrolelr: self)
        }
        else if txtEmail.text?.isEmail == false
        {
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: msgEnterValidEmail, viewcontrolelr: self)
        }
        else if(txtMessage.text?.trimmingCharacters(in: .whitespaces).isEmpty)! || txtMessage.text == msgfeedbackplc
        {
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: msgEnterFeedComment, viewcontrolelr: self)
        }
        else if txtMessage.text.count < intMinFeedbackLenght
        {
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: msgEnterValidFeedComment, viewcontrolelr: self)
        }
        else{
            self.addFeedbackApi()
        }
        
    }
    
    @IBAction func btnNewsPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 0
    }
    @IBAction func btnAssetPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 1
    }
    @IBAction func btnDealPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 2
    }
    
    @IBAction func btnLibraryPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 3
    }
    
    
    //MARK:- UIImagePicker Delegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            
            // profileImage = image
            
            self.imgAddImage.image = image
            
            IsImageUploaded = true
            
            picker.dismiss(animated: true, completion: nil)
            
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) {
            return true
        }
        
        if textField == txtName{
            if (txtName.text?.count)! > intMAxNameLenght{
                return false
            }
        }
        
        if textField == txtContact {
            
            if (txtContact.text?.isNumber)!{
                
                if (txtContact.text?.count)! > intMaxContactLenght{
                    return false
                }
                
                return string.isNumber
            }
            
        }
        
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if textView == txtMessage{
            if (txtMessage.text == msgfeedbackplc)
            {
                txtMessage.text = ""
            }
            txtMessage.becomeFirstResponder() //Optional
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if textView == txtMessage{
            if (txtMessage.text == "")
            {
                txtMessage.text = msgfeedbackplc
//                btnSubmit.isEnabled = false
//                btnSubmit.alpha = 0.5

            }
            txtMessage.resignFirstResponder()
        }
        
    }
//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//
//        let  char = text.cString(using: String.Encoding.utf8)!
//        let isBackSpace = strcmp(char, "\\b")
//
//        if (isBackSpace == -92) {
//
//            if (txtMessage.text?.count)! <= intMinFeedbackLenght + 1{
//                btnSubmit.isEnabled = false
//                btnSubmit.alpha = 0.5
//            }
//            else{
//                btnSubmit.isEnabled = true
//                btnSubmit.alpha = 1.0
//            }
//
//            return true
//        }
//
//        if textView == txtMessage{
//            if (txtMessage.text?.count)! < intMinFeedbackLenght{
//                btnSubmit.isEnabled = false
//                btnSubmit.alpha = 0.5
//            }
//            else{
//                btnSubmit.isEnabled = true
//                btnSubmit.alpha = 1.0
//            }
//        }
//
//
//        return true
//    }
}
