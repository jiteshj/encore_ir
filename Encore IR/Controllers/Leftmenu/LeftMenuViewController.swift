//
//  LeftMenuViewController.swift
//  Encore IR
//
//  Created by ravi on 04/05/19.
//  Copyright © 2019 ravi. All rights reserved.
//

import UIKit


let keymenu = "Menu"
let keyImage = "Mimage"


class cellMenu: UITableViewCell {
    
    @IBOutlet weak var imgMenu: UIImageView!
    
    @IBOutlet weak var lblMenu: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        // do your thing
        
        if appDelegateObj.IsThemeLight == true{
            lblMenu.textColor = wDarkGrayColor
        }
        else{
            lblMenu.textColor = DarkThemefontColor
        }
        
        lblMenu.font = lblMenu.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 15.0))
        
    }
    
}



class LeftMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK:- Outlets

    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var viewProfileImage: UIView!
    @IBOutlet weak var imgProfileCover: UIImageView!
//    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserAddress: UILabel!
    @IBOutlet weak var objTableMenu: UITableView!
    @IBOutlet weak var viewFooter: UIView!
    @IBOutlet weak var btnLogout: UIButton!
   
    
    @IBOutlet weak var lblInvestorSince: UILabel!
    
    //MARK:- Variables

    var arrayMenu = [NSDictionary]()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
      //  self.setupInitialView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateSideMenuProfile), name: NSNotification.Name(rawValue: kPostUpdateSideMenuProfile), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.setupInitialView), name: NSNotification.Name(rawValue: kPostUpdateSideMenuTheme), object: nil)
        
//        objTableMenu.rowHeight = UITableViewAutomaticDimension
//        objTableMenu.estimatedRowHeight = 80

    }
    
    override func viewWillLayoutSubviews() {
                
        imgProfileCover.layer.borderWidth = 2
        imgProfileCover.layer.masksToBounds = false
        if appDelegateObj.IsThemeLight == true{
            imgProfileCover.layer.borderColor = wBoxBorderColor.cgColor
        }
        else{
            imgProfileCover.layer.borderColor = UIColor.white.cgColor
        }
        imgProfileCover.layer.cornerRadius = imgProfileCover.frame.height/2
        imgProfileCover.clipsToBounds = true
        
    }

    override func viewDidAppear(_ animated: Bool) {
     
        if appDelegateObj.IsThemeLight == true{
            self.view.backgroundColor = wviewBgDefaultThemeColor
            lblUserName.textColor = wBlueMainTitleColor
            lblUserAddress.textColor = wDarkGrayColor
            lblInvestorSince.textColor = wBlueMainTitleColor
            
        }else{
            self.view.backgroundColor = fontTextColor
            lblUserName.textColor = DarkThemefontColor
            lblUserAddress.textColor = DarkThemefontColor
            lblInvestorSince.textColor = DarkThemefontColor
            
        }
        
        self.setupInitialView()
    }
    
    //MARK:- Class Methods and Functions
    @objc func setupInitialView(){
    
        if appDelegateObj.IsThemeLight == true{
            
            arrayMenu.removeAll()

            arrayMenu.append([keymenu:strProfile,keyImage:"Wht_MyProfile"])

            arrayMenu.append([keymenu:strBookmark,keyImage:"Wht_Bookmarks"])
            arrayMenu.append([keymenu:"Settings",keyImage:"setting-blue"])
            arrayMenu.append([keymenu:strFeedback,keyImage:"Wht_Feedback"])
            arrayMenu.append([keymenu:strContactUs,keyImage:"Wht_ContactUs"])
            
            btnLogout.setImage(UIImage(named: "Wht_Logout"), for: .normal)
            btnBack.setImage(UIImage(named: "Wht_Back"), for: .normal)
            
            
        }
        else{
            
            arrayMenu.removeAll()
            
            arrayMenu.append([keymenu:strProfile,keyImage:"My Profile"])

            arrayMenu.append([keymenu:strBookmark,keyImage:"Bookmarks"])
            arrayMenu.append([keymenu:"Settings",keyImage:"setting-white"])
            arrayMenu.append([keymenu:strFeedback,keyImage:"Feedback"])
            arrayMenu.append([keymenu:strContactUs,keyImage:"Contact Us"])

            btnLogout.setImage(UIImage(named: "Logout"), for: .normal)
            btnBack.setImage(UIImage(named: "Back"), for: .normal)
            

        }

        if appDelegateObj.IsThemeLight == true{
            imgProfileCover.layer.borderColor = wBoxBorderColor.cgColor
        }
        else{
            imgProfileCover.layer.borderColor = UIColor.white.cgColor
        }

        if appDelegateObj.IsThemeLight == true{
            self.view.backgroundColor = wviewBgDefaultThemeColor
            lblUserName.textColor = wBlueMainTitleColor
            lblUserAddress.textColor = wDarkGrayColor
            lblInvestorSince.textColor = wBlueMainTitleColor
            
        }else{
            self.view.backgroundColor = fontTextColor
            lblUserName.textColor = DarkThemefontColor
            lblUserAddress.textColor = DarkThemefontColor
            lblInvestorSince.textColor = DarkThemefontColor
            
        }
        objTableMenu.tableFooterView = UIView()
        objTableMenu.reloadData()

        self.updateSideMenuProfile()
        
        lblUserAddress.isHidden = true
        
        self.setupFontSize()
    }
    
    func setupFontSize(){
        lblUserName.font = lblUserName.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 17.0))
        lblInvestorSince.font = lblInvestorSince.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 12.0))
        lblUserAddress.font = lblUserAddress.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 10.0))
    }
    
    
    @objc func updateSideMenuProfile()
    {
     
        lblInvestorSince.text = msgInvestorSince + " \(appDelegateObj.loginUser.v_since)"
        lblUserName.text = "\(appDelegateObj.loginUser.u_fname) \(appDelegateObj.loginUser.u_lname)"
        lblUserAddress.text = "\(appDelegateObj.loginUser.u_state), \(appDelegateObj.loginUser.u_country)"

        let imgPath = appDelegateObj.loginUser.u_pro_img
        
        if appDelegateObj.loginUser.u_profile_set == "0"{
            imgProfileCover?.setImage(string: "\(appDelegateObj.loginUser.u_fname) \(appDelegateObj.loginUser.u_lname)", color: UIColor.white, circular: true, textAttributes: [NSAttributedStringKey.font: UIFont(name:MEDIUEM_FONT, size: 50.0)!, NSAttributedStringKey.foregroundColor: UIColor.black])
        }
        else{
            imgProfileCover.sd_setImage(with: URL(string: imgPath), placeholderImage: UIImage(named: "userDefault"))
        }
        
    }
    
    func logoutUserApi()
    {
        UserDefaults.standard.removeObject(forKey: kLoginUser)
        UserDefaults.standard.removeObject(forKey: kLoginUserCheck)
        UserDefaults.standard.synchronize()
        
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Start", bundle:nil)
        let navigationController = storyBoard.instantiateViewController(withIdentifier: "ChooseNavigationController") as! ChooseNavigationController
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.window!.rootViewController = navigationController
    }
    
//    func logoutApiCall(){
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Start", bundle:nil)
//        let navigationController = storyBoard.instantiateViewController(withIdentifier: "ChooseNavigationController") as! ChooseNavigationController
//        let appdelegate = UIApplication.shared.delegate as! AppDelegate
//        appdelegate.window!.rootViewController = navigationController
//    }

    
    @IBAction func btnBackPressed(_ sender: Any) {
//        SlideNavigationController.sharedInstance().toggleLeftMenu()
      //  hideLeftView(sender)

        self.hideLeftViewAnimated(self)
    }
    
    @IBAction func btnLogoutPressed(_ sender: Any) {
        
        
        let alert = UIAlertController(title: strAppName, message: msgLogout, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Logout", style: UIAlertActionStyle.default, handler: { action in
            self.logoutApi()
            
//            self.logoutApiCall()
        }))
        
        let activeVc = UIApplication.shared.keyWindow?.rootViewController
        activeVc?.present(alert, animated: true, completion: nil)
        
        if appDelegateObj.IsRememberMe == true{
            
        }else{
            
        }
    }
    

    //MARK:- UITableView Methods
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayMenu.count;
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let tablecell = tableView.dequeueReusableCell(withIdentifier: "cellMenu", for: indexPath) as! cellMenu
        
        tablecell.imgMenu.image = UIImage(named: arrayMenu[indexPath.row][keyImage] as! String)
        tablecell.lblMenu.text = arrayMenu[indexPath.row][keymenu] as? String
        
        return tablecell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
      /*  switch indexPath.row {
        case 0:
            appDelegateObj.tabBarController?.selectedIndex = 0
            self.hideLeftViewAnimated(self)
            break
        case 1:
            appDelegateObj.tabBarController?.selectedIndex = 1
            self.hideLeftViewAnimated(self)
            break
        case 2:
            appDelegateObj.tabBarController?.selectedIndex = 2
            self.hideLeftViewAnimated(self)
            break
        case 3:
            appDelegateObj.tabBarController?.selectedIndex = 3
            self.hideLeftViewAnimated(self)
            break
        case 4:
            appDelegateObj.tabBarController?.selectedIndex = 4
            self.hideLeftViewAnimated(self)
            break
        case 5:
            appDelegateObj.tabBarController?.selectedIndex = 5
            self.hideLeftViewAnimated(self)
            break
        case 6:
            appDelegateObj.tabBarController?.selectedIndex = 6
            self.hideLeftViewAnimated(self)
            break
        case 7:
            appDelegateObj.tabBarController?.selectedIndex = 7
            self.hideLeftViewAnimated(self)
            break
 
        
        default:
            //self.navigationController?.hideLeftView(Any?.self)
            
            self.hideLeftViewAnimated(self)
            break
            
        }
        switch indexPath.row {
        case 0:
            appDelegateObj.tabBarController?.selectedIndex = 4
            self.hideLeftViewAnimated(self)
            break
        case 1:
            appDelegateObj.tabBarController?.selectedIndex = 0
            self.hideLeftViewAnimated(self)
            break
        case 2:
            appDelegateObj.tabBarController?.selectedIndex = 1
            self.hideLeftViewAnimated(self)
            break
        case 3:
            appDelegateObj.tabBarController?.selectedIndex = 2
            self.hideLeftViewAnimated(self)
            break
        case 4:
            appDelegateObj.tabBarController?.selectedIndex = 3
            self.hideLeftViewAnimated(self)
            break
        case 5:
            appDelegateObj.tabBarController?.selectedIndex = 5
            self.hideLeftViewAnimated(self)
            break
        case 6:
            appDelegateObj.tabBarController?.selectedIndex = 8
            self.hideLeftViewAnimated(self)
            break
        case 7:
            appDelegateObj.tabBarController?.selectedIndex = 6
            self.hideLeftViewAnimated(self)
            break
            
            
        default:
            //self.navigationController?.hideLeftView(Any?.self)
            
            self.hideLeftViewAnimated(self)
            break
            
        }
        */
        
        switch indexPath.row {
        case 0:
            appDelegateObj.tabBarController?.selectedIndex = 4
            self.hideLeftViewAnimated(self)
            break
        case 1:
            appDelegateObj.tabBarController?.selectedIndex = 5
            self.hideLeftViewAnimated(self)
            break
        case 2:
            appDelegateObj.tabBarController?.selectedIndex = 8
            self.hideLeftViewAnimated(self)
            break
        case 3:
            appDelegateObj.tabBarController?.selectedIndex = 6
            self.hideLeftViewAnimated(self)
            break
        case 4:
            appDelegateObj.tabBarController?.selectedIndex = 7
            self.hideLeftViewAnimated(self)
            break
            
        default:
            //self.navigationController?.hideLeftView(Any?.self)
            
            self.hideLeftViewAnimated(self)
            break
            
        }
    }
    
}
