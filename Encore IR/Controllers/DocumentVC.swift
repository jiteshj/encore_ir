//
//  DocumentVC.swift
//  EncoreIR
//
//  Created by Jaimin Patel on 21/05/19.
//  Copyright © 2019 ravi. All rights reserved.
//

import UIKit
import WebKit
import SVProgressHUD

class DocumentVC: UIViewController, WKNavigationDelegate, WKUIDelegate {
    
    
    //MARK:- Outlets
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnDownload: UIButton!
    @IBOutlet weak var objWebView: WKWebView!
    
    @IBOutlet weak var imgDownload: UIImageView!
    
    @IBOutlet weak var imgBack: UIImageView!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var btnPrev: UIButton!
    @IBOutlet weak var lblDocCount: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var hightBottomViewConstraint: NSLayoutConstraint!
    
    //MARK:- Variable
    var strUrlPath = ""
    var strDocTitle = ""
    var isMoreThenDoc = Bool()
    var arrayDocument = NSMutableArray()
    var currentIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.btnDownload.isHidden = true
        self.imgDownload.isHidden = true
        
        self.setupInitialView()
    
        if isMoreThenDoc{
            self.hightBottomViewConstraint.constant = 50
            self.bottomView.isHidden = false
            lblDocCount.text = "\(currentIndex + 1)/\(arrayDocument.count)"
            strUrlPath = arrayDocument[currentIndex] as! String
            btnPrev.isHidden = true
        }
        else{
            self.hightBottomViewConstraint.constant = 0
            self.bottomView.isHidden = true
        }
        
        self.loadDocument()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.setUpThemeColor()
    }
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        btnNext.layer.cornerRadius = btnNext.frame.size.height/2
        btnNext.layer.borderWidth = 2
        
        btnNext.layer.masksToBounds = true
        
        btnPrev.layer.cornerRadius = btnPrev.frame.size.height/2
        btnPrev.layer.borderWidth = 2
        
        btnPrev.layer.masksToBounds = true
  
    }
    
    func setUpThemeColor(){
        if appDelegateObj.IsThemeLight == true{
            
            print("Ligth Theme mode")
            imgBackground.image = UIImage(named: "Wht_background")
            imgBack.image = UIImage(named: "Wht_Back")
            self.view.backgroundColor = wviewBgDefaultThemeColor
            lblTopTitle.textColor = wTopTitleFontColor
            
            lblDocCount.textColor = wBlueMainTitleColor
            btnNext.setTitleColor(wBlueMainTitleColor, for: .normal)
            btnNext.layer.borderColor = wBlueMainTitleColor.cgColor
            btnPrev.setTitleColor(wBlueMainTitleColor, for: .normal)
            btnPrev.layer.borderColor = wBlueMainTitleColor.cgColor
            
        }
        else{
            print("Dark Theme mode")
            self.view.backgroundColor = TabViewColor
            imgBackground.image = UIImage(named: "background")
            imgBack.image = UIImage(named: "Back")
            lblTopTitle.textColor = TopTitleFontColor
            
            btnNext.layer.borderColor = UIColor.white.cgColor
            btnPrev.layer.borderColor = UIColor.white.cgColor
            
            lblDocCount.textColor = TopTitleFontColor
            btnNext.setTitleColor(TopTitleFontColor, for: .normal)
            btnPrev.setTitleColor(TopTitleFontColor, for: .normal)
            btnNext.layer.borderColor = wDarkGrayColor.cgColor
            btnPrev.layer.borderColor = wDarkGrayColor.cgColor
        }
        
        self.setupFontSize()
    }
    
    func setupFontSize(){
        lblTopTitle.font = lblTopTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 20.0))
        lblDocCount.font = lblTopTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 17.0))
        
        btnNext.titleLabel?.font = UIFont(name:DINPROBold, size: GlobalMethods.setThemeFontSize(objectSize: 17.0))
        btnPrev.titleLabel?.font = UIFont(name:DINPROBold, size: GlobalMethods.setThemeFontSize(objectSize: 17.0))
    }
    
    func loadDocument(){
        
        if arrayDocument.count == 1{
            btnNext.isHidden = true
            btnPrev.isHidden = true
        }
        else if currentIndex == 0{
            btnPrev.isHidden = true
            
            if arrayDocument.count > 1{
                btnNext.isHidden = false
            }
            
        }
        else if currentIndex == arrayDocument.count-1{
            btnNext.isHidden = true
            
            if arrayDocument.count > 1{
                btnPrev.isHidden = false
            }
        }
        else{
            btnNext.isHidden = false
            btnPrev.isHidden = false
        }
        
        
        if let url = URL(string: strUrlPath) {
            let req = URLRequest(url: url)
            objWebView?.load(req)
            self.lblTopTitle.text = strDocTitle
        }
    }
    
    
    //MARK:- IBActions
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDownloadPressed(_ sender: Any) {
        
        let pdfLoc = NSData(contentsOf: URL(string: strUrlPath)!)
        
        let oggetti  = [pdfLoc!]
        let activityViewController : UIActivityViewController = UIActivityViewController(activityItems: oggetti, applicationActivities: nil)
        present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func btnPrevPressed(_ sender: Any) {
      
            currentIndex = currentIndex - 1
       
            strUrlPath = arrayDocument[currentIndex] as! String
            lblDocCount.text = "\(currentIndex+1)/\(arrayDocument.count)"
            self.loadDocument()
    }
    
    @IBAction func btnNextPressed(_ sender: Any) {
        
            currentIndex = currentIndex + 1
        
            strUrlPath = arrayDocument[currentIndex] as! String
            lblDocCount.text = "\(currentIndex+1)/\(arrayDocument.count)"
            self.loadDocument()
    }
    
    
    //MARK:- webview documeent
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        SVProgressHUD.dismiss()
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        SVProgressHUD.show()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        SVProgressHUD.dismiss()
        print(error)
    }
    
    
}
