//
//  FinancialsVC.swift
//  EncoreIR
//
//  Created by ravi on 15/05/19.
//  Copyright © 2019 ravi. All rights reserved.
//

import UIKit
import ImageSlideshow

class CellFinDocDetail: UICollectionViewCell {
    @IBOutlet weak var imgDoc: UIImageView!
    @IBOutlet weak var lblDoc: UILabel!
    @IBOutlet weak var btnDoc: UIButton!
}

class cellQuotes: UITableViewCell{
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    
    
    @IBOutlet weak var constLblLeading: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.viewBack.layer.cornerRadius = 5
        self.viewBack.layer.masksToBounds = true
        
    }
}

class cellFinTerm: UITableViewCell{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewBack: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewBack.layer.cornerRadius = 5
       
        self.viewBack.layer.masksToBounds = true
        
    }
    
    
}

class FinancialsVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    
    //MARK:- Outlets
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnLargeFont: UIButton!

    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var imgFontBackgroung: UIImageView!
    @IBOutlet weak var imgFontBig: UIImageView!

    @IBOutlet weak var viewQuote: UIView!
    
    @IBOutlet weak var lblDesclaimer: UILabel!
    
    @IBOutlet weak var viewFinancialdoc: UIView!
    
    @IBOutlet weak var lblFinDocTitle: UILabel!
    
    
    
    @IBOutlet weak var viewFooter: UIView!
    
    @IBOutlet weak var lblFooterNotes: UILabel!
    
    @IBOutlet weak var objCollFinDocumnet: UICollectionView!
    
    @IBOutlet weak var viewQuoteHeigth: NSLayoutConstraint!
    
    @IBOutlet weak var viewFinDocHeight: NSLayoutConstraint!
    
    @IBOutlet weak var viewFooterHeight: NSLayoutConstraint!
    
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var btnMinFont: UIButton!
    @IBOutlet weak var btnMaxFont: UIButton!
    
    
    @IBOutlet weak var ViewCollection: UIView!
    
    
    
    //MARK:- Variable
    var arrayRowTitle = NSMutableArray()
    var objSection = ObjSectionDetail()
    var IsGroupDis = false
    var arrImage = [UIImage]()
    
    var strNameFinStatement = ""
    var strImageUrlFinStatement = ""
    
    var strNameIncStatement = ""
    var strImageUrlIncStatement = ""
    
    var strNameBalSheet = ""
    var strImageUrlBalSheet = ""

    var arrDocList = [financials_documentDetailCategory]()
    
    var IsPDf = false
   
    var objFinDocList = financials_documentList()
    var fontsizeDetail : CGFloat = 0
    var slideshowTransitioningDelegate: ZoomAnimatedTransitioningDelegate? = nil
    
    
    //MARK:- UIView Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupInitialView()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.setUpThemeColor()
    }
    
    
    //MARK:- Class Method and Function
    func setupInitialView(){
        lblTopTitle.text  = strFinancialHeader
  
        self.AssetFinApi(strAId: objSection.a_id, strSection_isDynamicTF: objSection.section_isDynamic, strSection_id: objSection.section_id, strC_type: objSection.c_type, strIs_group: objSection.is_group)
      
        self.viewFinancialdoc.layer.cornerRadius = 5
        self.viewFinancialdoc.layer.borderWidth = 0.5
        self.viewFinancialdoc.layer.masksToBounds = true
    
        self.viewContainer.isHidden = true

        self.objCollFinDocumnet.center = ViewCollection.center

    }
    func setUpThemeColor(){
        if appDelegateObj.IsThemeLight == true{
            
            print("Ligth Theme mode")
            imgBackground.image = UIImage(named: "Wht_background")
            imgBack.image = UIImage(named: "Wht_Back")
            self.view.backgroundColor = wviewBgDefaultThemeColor
            lblTopTitle.textColor = wTopTitleFontColor
            
            imgFontBackgroung.image = UIImage(named: "Wht_LargeFontBG")
            imgFontBig.image = UIImage(named: "Wht_LargeFont")
            
            btnMinFont.setTitleColor(wBlueMainTitleColor, for: .normal)
            btnMaxFont.setTitleColor(wBlueMainTitleColor, for: .normal)
            
            
            self.viewFinancialdoc.backgroundColor = UIColor.white
            self.viewFinancialdoc.layer.borderColor = wBlueMainTitleColor.cgColor
            
            lblFinDocTitle.textColor = wBlueMainTitleColor
            lblDesclaimer.textColor = wDarkGrayColor
            lblFooterNotes.textColor = wLightGrayColor
           
        }
        else{
            print("Dark Theme mode")
            self.view.backgroundColor = TabViewColor
            imgBackground.image = UIImage(named: "background")
            imgBack.image = UIImage(named: "Back")
            lblTopTitle.textColor = TopTitleFontColor
            
            imgFontBackgroung.image = UIImage(named: "LargeFontBG")
            imgFontBig.image = UIImage(named: "FontBig")
            
            btnMinFont.setTitleColor(UIColor.white, for: .normal)
            btnMaxFont.setTitleColor(UIColor.white, for: .normal)
            
            self.viewFinancialdoc.backgroundColor = kCellBackColor
            self.viewFinancialdoc.layer.borderColor = kCellBackColor.cgColor
            
            lblFinDocTitle.textColor = UIColor.white
            lblDesclaimer.textColor = UIColor.white
            lblFooterNotes.textColor = UIColor.white
           
            
        }
        self.setupFontSize()
    }
    func setupFontSize(){
        lblTopTitle.font = lblTopTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 20.0))
        btnMinFont.titleLabel?.font = UIFont(name:DINPRO, size: GlobalMethods.setThemeFontSize(objectSize: 16.0))
        btnMaxFont.titleLabel?.font = UIFont(name:DINPRO, size: GlobalMethods.setThemeFontSize(objectSize: 25.0))
        
        fontsizeDetail = GlobalMethods.setThemeFontSize(objectSize: 0.0)
    }
    func displayFinance(value : financials_documentList){
        
        self.objFinDocList = value
        
        if IsGroupDis == true{
            
            if value.f_disclaimer != ""{
                lblDesclaimer.text = value.f_disclaimer
                lblDesclaimer.numberOfLines = 0
                lblDesclaimer.font = UIFont(name: DINPROBItalic, size: CGFloat(13 + fontsizeDetail))
                self.viewQuote.isHidden = false
                self.viewQuote.backgroundColor = UIColor.clear
                
            }else{
                self.viewQuoteHeigth.constant = 0
                self.viewQuote.isHidden = true
                self.viewQuote.backgroundColor = UIColor.clear
                
            }
            
            if value.f_blc_sheet_img.count == 0 && value.f_income_s_img.count == 0 && value.f_summery_img.count == 0 {
                self.viewFinDocHeight.constant = 0
                
            }else{
                
                lblFinDocTitle.font = UIFont(name: DINPROBold, size: CGFloat(14 + fontsizeDetail))
                arrDocList.removeAll()
              
                if value.f_summery_img.count > 0{
                    let addDoc = financials_documentDetailCategory()
                    addDoc.doc_category = "0"
                    addDoc.documents = value.f_summery_img
                    arrDocList.append(addDoc)
                }
                if value.f_income_s_img.count > 0{
                    let addDoc = financials_documentDetailCategory()
                    addDoc.doc_category = "1"
                    addDoc.documents = value.f_income_s_img
                    arrDocList.append(addDoc)
                }
                if value.f_blc_sheet_img.count > 0{
                    let addDoc = financials_documentDetailCategory()
                    addDoc.doc_category = "2"
                    addDoc.documents = value.f_blc_sheet_img
                    arrDocList.append(addDoc)
                }
            }
            
            if value.f_footnotes != ""{
                lblFooterNotes.text = value.f_footnotes
                lblFooterNotes.numberOfLines = 0
                lblFooterNotes.font = UIFont(name: DINPROBItalic, size: CGFloat(13 + fontsizeDetail))
                self.viewFooter.isHidden = false
            }else{
                self.viewFooterHeight.constant = 0
                self.viewFooter.isHidden = true
            }
            
        }
        else{
            
            if value.f_disclaimer != ""{
                lblDesclaimer.text = value.f_disclaimer
                lblDesclaimer.numberOfLines = 0
                lblDesclaimer.font = UIFont(name: DINPROBItalic, size: CGFloat(13 + fontsizeDetail))
                self.viewQuote.isHidden = false
                self.viewQuote.backgroundColor = UIColor.clear
                
            }else{
                self.viewQuoteHeigth.constant = 0
                self.viewQuote.isHidden = true
                self.viewQuote.backgroundColor = UIColor.clear
                
            }
            
            if value.f_blc_sheet_img.count == 0 && value.f_income_s_img.count == 0 && value.f_summery_img.count == 0 {
                
                self.viewFinDocHeight.constant = 0
                
            }else{
                
                lblFinDocTitle.font = UIFont(name: DINPROBold, size: CGFloat(14 + fontsizeDetail))
                
                
                arrDocList.removeAll()
                if value.f_summery_img.count > 0{
                    
                    let addDoc = financials_documentDetailCategory()

                    addDoc.doc_category = "0"
                    addDoc.documents = value.f_summery_img
                    arrDocList.append(addDoc)
                    
                }
                if value.f_income_s_img.count > 0{
                    let addDoc = financials_documentDetailCategory()
                    addDoc.doc_category = "1"
                    addDoc.documents = value.f_income_s_img
                    arrDocList.append(addDoc)
                }
                if value.f_blc_sheet_img.count > 0{
                    let addDoc = financials_documentDetailCategory()
                    addDoc.doc_category = "2"
                    addDoc.documents = value.f_blc_sheet_img
                    arrDocList.append(addDoc)
                }
            }
            
            if value.f_footnotes != ""{
                lblFooterNotes.text = value.f_footnotes
                lblFooterNotes.numberOfLines = 0
                lblFooterNotes.font = UIFont(name: DINPROBItalic, size: CGFloat(13 + fontsizeDetail))
                self.viewFooter.isHidden = false

                
            }else{
                self.viewFooterHeight.constant = 0
                self.viewFooter.isHidden = true

            }
            
        }
        
        objCollFinDocumnet.reloadData()
        
    }
    
    func applyFontChanges(){
        
        lblDesclaimer.font = UIFont(name: DINPROBItalic, size: CGFloat(13 + fontsizeDetail))
        lblFinDocTitle.font = UIFont(name: DINPROBold, size: CGFloat(14 + fontsizeDetail))
        lblFooterNotes.font = UIFont(name: DINPROBItalic, size: CGFloat(13 + fontsizeDetail))

        
    }
    
    
    //MARK:- IBActions
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnLargeFontPressed(_ sender: Any) {
        
        if viewContainer.isHidden == true {
            viewContainer.isHidden = false
        } else {
            viewContainer.isHidden = true
        }
    }
    
    
    @IBAction func btnMinFontPressed(_ sender: Any) {
        print("font --\(fontsizeDetail)")
        
        if fontsizeDetail < 0 {
            return
        }
        else{
            fontsizeDetail = fontsizeDetail - 3
        }
        self.applyFontChanges()
    }
    
    
    @IBAction func btnMaxFontPressed(_ sender: Any) {
        print("font ++\(fontsizeDetail)")
        
        if fontsizeDetail > 6 {
            return
        }
        else{
            fontsizeDetail = fontsizeDetail + 3
        }
        self.applyFontChanges()

    }

    
    //MARK:- UICollection Delegate Method
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrDocList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellFinDocDetail", for: indexPath) as! CellFinDocDetail
        
        if arrDocList[indexPath.row].doc_category == "0"{
            if appDelegateObj.IsThemeLight{
                cell.imgDoc.image = UIImage(named: "financialSumBlue")
            }else{
                cell.imgDoc.image = UIImage(named: "Financial Summary")
            }
            
            cell.lblDoc.text = strFinStatement.uppercased()
            cell.lblDoc.numberOfLines = 2
            cell.lblDoc.font = UIFont(name:DINPROBold, size: 10.0)

        }
        if arrDocList[indexPath.row].doc_category == "1"{
            if appDelegateObj.IsThemeLight{
                cell.imgDoc.image = UIImage(named: "IncomeStatBlue")
            }else{
                cell.imgDoc.image = UIImage(named: "Income Statement")
            }
            
            cell.lblDoc.text = strIncomeStatement.uppercased()
            cell.lblDoc.numberOfLines = 2
            cell.lblDoc.font = UIFont(name:DINPROBold, size: 10.0)

        }
        if arrDocList[indexPath.row].doc_category == "2"{
            if appDelegateObj.IsThemeLight{
                cell.imgDoc.image = UIImage(named: "BalanceSheetBlue")
            }else{
                cell.imgDoc.image = UIImage(named: "Balance Sheet")
            }
            
            cell.lblDoc.text = strBalanceSheet.uppercased()
            cell.lblDoc.numberOfLines = 2
            cell.lblDoc.font = UIFont(name:DINPROBold, size: 10.0)
        }
        
        if appDelegateObj.IsThemeLight{
            cell.lblDoc.textColor = wDarkGrayColor
        }else{
            cell.lblDoc.textColor = UIColor.white
        }
        
        cell.btnDoc.tag = indexPath.row
        cell.btnDoc.addTarget(self, action: #selector(self.btnDocPressed), for: .touchUpInside)
        
        return cell
    }
    

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let numberOfItems = CGFloat(collectionView.numberOfItems(inSection: section))
        let combinedItemWidth = (numberOfItems * flowLayout.itemSize.width) + ((numberOfItems - 1)  * flowLayout.minimumInteritemSpacing)
        let padding = (collectionView.frame.width - combinedItemWidth) / 2
        return UIEdgeInsets(top: 0, left: padding, bottom: 0, right: padding)
    }
    
    @objc func btnDocPressed(sender:UIButton)  {
        print("Document\(arrDocList[sender.tag])")
        
        let objDoc = arrDocList[sender.tag]
        
        let url = URL(string:objDoc.documents[0].document)
        let strExtension = url?.pathExtension
        
        if strExtension!.lowercased() == "pdf"{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ObjVC = storyboard.instantiateViewController(withIdentifier: "DocumentVC") as! DocumentVC
            
            var myArrayPdf = NSMutableArray()
            
            for objDetails in objDoc.documents{
               myArrayPdf.add(objDetails.document)
            }
    
            ObjVC.arrayDocument = myArrayPdf
            ObjVC.isMoreThenDoc = true
            
        
           
            if arrDocList[sender.tag].doc_category == "0"{
                ObjVC.strDocTitle = "Financial Summary"
                
            }
            else if arrDocList[sender.tag].doc_category == "1"{
                ObjVC.strDocTitle = "Income Statement"
                
            }
            else if arrDocList[sender.tag].doc_category == "2"{
                ObjVC.strDocTitle = "Balance Sheet"
                
            }
           
            self.navigationController?.pushViewController(ObjVC, animated: true)
            
        }
        else{
            var arrImg = [UIImage]()
          
            
                    
                    for objDetails in objDoc.documents{
                       
                       
                        let url = URL(string:objDetails.document)
                
                        if let data = try? Data(contentsOf: url!) {
                            let image: UIImage = UIImage(data: data)!
                            arrImg.append(image)
                        }
                    }
                    
            
         
            
            
            /*let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ObjVC = storyboard.instantiateViewController(withIdentifier: "FullScreenImageVC") as! FullScreenImageVC
         
            print(arrImg)
            ObjVC.arrSlide = arrImg
          
            ObjVC.modalPresentationStyle = .overCurrentContext
            present(ObjVC, animated: false, completion: nil)*/
            
            var arryModel = [Model]()
            
            for element in arrImage {
                //imageLabel.image = element
                arryModel.append(Model(image: element, title: ""))
            }
            
            
            let fullScreenController = FullScreenSlideshowViewController()
            
            fullScreenController.closeButton.layer.cornerRadius = fullScreenController.closeButton.bounds.height/2
            fullScreenController.closeButton.layer.borderWidth = 1
            fullScreenController.closeButton.layer.borderColor = UIColor.darkGray.cgColor
            fullScreenController.closeButton.backgroundColor = UIColor.lightGray
            fullScreenController.closeButton.layer.masksToBounds = true
            
            if appDelegateObj.IsThemeLight == true{
                fullScreenController.backgroundColor = wviewBgDefaultThemeColor
            }
            else{
                fullScreenController.backgroundColor = TabViewColor
            }
            
            fullScreenController.inputs = arryModel.map { $0.inputSource }
            
            
            fullScreenController.initialPage = 0
            
            
            
            
            present(fullScreenController, animated: true, completion: nil)
   
        }
        
        
        
      /*  let docName = arrDocList[sender.tag].document
        _ = arrDocList[sender.tag].doc_name
        
        let url = URL(string:docName)

        let strExtension = url?.pathExtension
        if strExtension?.lowercased() == "pdf"{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ObjVC = storyboard.instantiateViewController(withIdentifier: "DocumentVC") as! DocumentVC
            
            ObjVC.strUrlPath = arrDocList[sender.tag].document
            //check
            
            if arrDocList[sender.tag].doc_category == "0"{
                ObjVC.strDocTitle = "Financial Summary"

            }
            else if arrDocList[sender.tag].doc_category == "1"{
                ObjVC.strDocTitle = "Income Statement"

            }
            else if arrDocList[sender.tag].doc_category == "2"{
                ObjVC.strDocTitle = "Balance Sheet"

            }
            else{
                ObjVC.strDocTitle = arrDocList[sender.tag].doc_name
            }
//            ObjVC.strDocTitle = arrDocList[sender.tag].doc_name
            self.navigationController?.pushViewController(ObjVC, animated: true)
            
        }else{
            
 
            
            if let data = try? Data(contentsOf: url!) {
                let image: UIImage = UIImage(data: data)!
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let ObjVC = storyboard.instantiateViewController(withIdentifier: "FullScreenImageVC") as! FullScreenImageVC
                
                var arrImg = [UIImage]()
                
                arrImg.append(image)
                
                print(arrImg)
                ObjVC.arrSlide = arrImg
                //        self.navigationController?.pushViewController(ObjVC, animated: true)
                
                ObjVC.modalPresentationStyle = .overCurrentContext
                present(ObjVC, animated: false, completion: nil)
                
            }
        
        }*/
    
    }
    
}
