//
//  NewDealVC.swift
//  Encore IR
//
//  Created by ravi on 06/05/19.
//  Copyright © 2019 ravi. All rights reserved.
//

import UIKit
import LGSideMenuController


class NewDealVC: UIViewController, UITableViewDelegate,UITableViewDataSource, UISearchBarDelegate {

    //MARK:- Outlets
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var imgViewBackground: UIImageView!
    @IBOutlet weak var imgMenu: UIImageView!
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var btnMenu: UIButton!
    
    @IBOutlet weak var viewSearch: UISearchBar!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var imgSearch: UIImageView!
    
    @IBOutlet weak var tblDealObj: UITableView!
    
    @IBOutlet weak var lblNoDataFound: UILabel!
    
    
    @IBOutlet weak var imgFooterBG: UIImageView!
    @IBOutlet weak var imgNewsFooter: UIImageView!
    @IBOutlet weak var imgAssetFooter: UIImageView!
    @IBOutlet weak var imgDealFooter: UIImageView!
    @IBOutlet weak var imgLibFooter: UIImageView!
    
    @IBOutlet weak var lblCounterNews: UILabel!
    @IBOutlet weak var lblCounterAssets: UILabel!
    @IBOutlet weak var lblCounterDeals: UILabel!
    @IBOutlet weak var lblCounterLibrary: UILabel!
    
    
    //MARK:- Constant & Variables
    var arrayData = [AssetProject]()
    var objRefreshControl = UIRefreshControl()
    var pageAdded = 0
    var isPagingNeed = true
    var TotalFeed = 1
    
    var arrayDataCopy = [AssetProject]()
    
    var textFieldInsideSearchBar = UITextField()
    
    //MARK:- UIView Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      //  self.tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        self.title = nil

        self.setupInitialView()
    
        self.getNewDealsFeed(strType: "1")
        
        lblNoDataFound.isHidden = true
        
        
        tblDealObj.tableFooterView = UIView()
        
        objRefreshControl.attributedTitle = NSAttributedString(string: "")
        objRefreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        objRefreshControl.tintColor = UIColor.clear
        tblDealObj.addSubview(objRefreshControl)
        
        NotificationCenter.default.addObserver(self, selector: #selector(refresh), name: NSNotification.Name(rawValue: kRefreshDeal), object: nil)

        tblDealObj.rowHeight = UITableViewAutomaticDimension
        tblDealObj.estimatedRowHeight = 270

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getUpdateCounter()
    }
    
    override func viewWillLayoutSubviews() {
        
        //New Notification Counter
        if appDelegateObj.ObjCounter.new_news != "0" {
            lblCounterNews.isHidden = false
            lblCounterNews.text = appDelegateObj.ObjCounter.new_news
            
        }else{
            lblCounterNews.isHidden = true
        }
        lblCounterNews.textColor = UIColor.black
        lblCounterNews.backgroundColor = counterBgColor
        lblCounterNews.layer.cornerRadius = lblCounterNews.frame.width/2
        lblCounterNews.layer.masksToBounds = true
        
        
        if appDelegateObj.ObjCounter.new_asset != "0" {
            lblCounterAssets.isHidden = false
            lblCounterAssets.text = appDelegateObj.ObjCounter.new_asset
            
        }else{
            lblCounterAssets.isHidden = true
        }
        lblCounterAssets.text = appDelegateObj.ObjCounter.new_asset
        lblCounterAssets.textColor = UIColor.black
        lblCounterAssets.backgroundColor = counterBgColor
        lblCounterAssets.layer.cornerRadius = lblCounterAssets.frame.width/2
        lblCounterAssets.layer.masksToBounds = true
        
        if appDelegateObj.ObjCounter.new_deal != "0" {
            lblCounterDeals.isHidden = false
            lblCounterDeals.text = appDelegateObj.ObjCounter.new_deal
            
        }else{
            lblCounterDeals.isHidden = true
        }
        lblCounterDeals.text = appDelegateObj.ObjCounter.new_deal
        lblCounterDeals.textColor = UIColor.black
        lblCounterDeals.backgroundColor = counterBgColor
        lblCounterDeals.layer.cornerRadius = lblCounterDeals.frame.width/2
        lblCounterDeals.layer.masksToBounds = true
        
        if appDelegateObj.ObjCounter.new_docs != "0" {
            lblCounterLibrary.isHidden = false
            lblCounterLibrary.text = appDelegateObj.ObjCounter.new_docs
            
        }else{
            lblCounterLibrary.isHidden = true
        }
        lblCounterLibrary.text = appDelegateObj.ObjCounter.new_docs
        lblCounterLibrary.textColor = UIColor.black
        lblCounterLibrary.backgroundColor = counterBgColor
        lblCounterLibrary.layer.cornerRadius = lblCounterLibrary.frame.width/2
        lblCounterLibrary.layer.masksToBounds = true
        
        tblDealObj.reloadData()
    }
    override func viewDidAppear(_ animated: Bool) {
        self.setUpThemeColor()
    }
    
    func setUpThemeColor(){
        if appDelegateObj.IsThemeLight == true{
            
            print("Ligth Theme mode")
            imgViewBackground.image = UIImage(named: "Wht_background")
            
            self.view.backgroundColor = wviewBgDefaultThemeColor
            lblTopTitle.textColor = wTopTitleFontColor
            // lblTopTitle.font = UIFont(name: wRubic, size: 20)
            imgMenu.image = UIImage(named: "menuBlue")
            imgSearch.image = UIImage(named: "searchBlue")
            
            imgFooterBG.image = UIImage(named: "Wht_FooterBG")
            imgNewsFooter.image = UIImage(named: "Wht_News")
            imgAssetFooter.image = UIImage(named: "Wht_Asset")
            imgDealFooter.image = UIImage(named: "Wht_DealSelected")
            imgLibFooter.image = UIImage(named: "Wht_Library")
           
            
            lblCounterNews.textColor = UIColor.black
            lblCounterNews.backgroundColor = counterBgColor
            
            lblCounterAssets.textColor = UIColor.black
            lblCounterAssets.backgroundColor = counterBgColor
            
            lblCounterDeals.textColor = UIColor.black
            lblCounterDeals.backgroundColor = counterBgColor
            
            lblCounterLibrary.textColor = UIColor.black
            lblCounterLibrary.backgroundColor = counterBgColor
            
            textFieldInsideSearchBar.textColor = wBlueMainTitleColor
            lblNoDataFound.textColor = UIColor.black
        }
        else{
            print("Dark Theme mode")
            self.view.backgroundColor = TabViewColor
            imgViewBackground.image = UIImage(named: "background")
            
            lblTopTitle.textColor = TopTitleFontColor
            //  lblTopTitle.font = UIFont(name: MEDIUEM_FONT, size: 20)
            imgMenu.image = UIImage(named: "menu")
            imgSearch.image = UIImage(named: "Search-1")
            
            imgFooterBG.image = UIImage(named: "footer-background")
            imgNewsFooter.image = UIImage(named: "News-icon")
            imgAssetFooter.image = UIImage(named: "My-Assets-icon")
            imgDealFooter.image = UIImage(named: "NewDeal-Selected")
            imgLibFooter.image = UIImage(named: "Library-icon")
            
            lblCounterNews.textColor = UIColor.black
            lblCounterNews.backgroundColor = counterBgColor
            
            lblCounterAssets.textColor = UIColor.black
            lblCounterAssets.backgroundColor = counterBgColor
            
            lblCounterDeals.textColor = UIColor.black
            lblCounterDeals.backgroundColor = counterBgColor
            
            lblCounterLibrary.textColor = UIColor.black
            lblCounterLibrary.backgroundColor = counterBgColor
        
            textFieldInsideSearchBar.textColor = UIColor.white
            lblNoDataFound.textColor = UIColor.white
        }
        self.setupFontSize()
    }
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        self.view.backgroundColor = TabViewColor
        viewSearch.isHidden = true
    
        textFieldInsideSearchBar = viewSearch.value(forKey: "searchField") as! UITextField
        textFieldInsideSearchBar.font = UIFont(name: D_DIN, size: 15)
        
    
    }
    func setupFontSize(){
        lblTopTitle.font = lblTopTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 20.0))
        lblNoDataFound.font = lblNoDataFound.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 15.0))
        
    }

    @objc func refresh(_ sender: Any) {
        //  your code to refresh tableView
        //print("Refresh start")
        
        self.getNewDealsFeed(strType: "1")

        
        objRefreshControl.endRefreshing()
        
    }
    
    @objc func btnBookmarkColl(sender:UIButton)  {
        
        let objModel = arrayData[sender.tag]
        
        let copyObj = objModel
        
        appDelegateObj.strPostId = objModel.a_id
        appDelegateObj.strPostTypeId = "3" // deal type = 3
        
        if objModel.is_bookmark == "1"{
            copyObj.is_bookmark = "0"
            self.removeBookmarkApi(strPostId: appDelegateObj.strPostId, strPostTypeId: appDelegateObj.strPostTypeId)
        }else{
            copyObj.is_bookmark = "1"
            self.addBookmarkApi(strPostId: appDelegateObj.strPostId, strPostTypeId: appDelegateObj.strPostTypeId)
        }
        
        var copyArray = arrayData
        
        copyArray.remove(at: sender.tag)
        copyArray.insert(copyObj, at: sender.tag)
        
        arrayData = copyArray
        
        self.tblDealObj.reloadData()
    }
    
    //MARK:- IBActions
    @IBAction func btnNewsPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 0
    }
    @IBAction func btnMyAssetsPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 1
    }
    @IBAction func btnLibraryPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 3
    }


    @IBAction func btnMenuPressed(_ sender: Any) {
        showLeftViewAnimated(sender)

    }
    
    @IBAction func btnSearchPressed(_ sender: Any) {
        if self.arrayData.count > 0{
            lblTopTitle.isHidden = true
            viewSearch.isHidden = false
            imgSearch.isHidden = true
            btnSearch.isHidden = true
            
            arrayDataCopy = self.arrayData
        }
    }
    
    //MARK:- UISearchbar Methods
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        self.closeSearchView()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        viewSearch.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.filterTextArray(withString: searchText)
    }
    
    func filterTextArray(withString searchText: String) {
        var result = [AssetProject]()
        
        result = arrayDataCopy.filter{ $0.a_name.range(of: searchText, options: [.caseInsensitive]) != nil
            || $0.p_type_name.range(of: searchText, options: [.caseInsensitive]) != nil }
        
        //print(result.count)
        arrayData = result
        tblDealObj.reloadData()
    }
    
    func closeSearchView(){
        viewSearch.endEditing(true)
        lblTopTitle.isHidden = false
        viewSearch.isHidden = true
        imgSearch.isHidden = false
        btnSearch.isHidden = false
        
        self.arrayData = arrayDataCopy
        
        tblDealObj.reloadData()
    }
    
    
    //MARK:- UITableView Methods
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayData.count;
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let tablecell = tableView.dequeueReusableCell(withIdentifier: "AssetsTblCell", for: indexPath) as! AssetsTblCell
        
        let objModel = arrayData[indexPath.row]
        
        tablecell.lblTitle.text = objModel.a_name.uppercased()
        tablecell.lblTitle.numberOfLines = 2
        tablecell.lblDetail.text = objModel.p_type_name
        tablecell.lblDetail.numberOfLines = 1
        
        let imgPath = objModel.a_asset_img
        tablecell.imgCell.sd_setImage(with: URL(string: imgPath), completed: nil)
        
        tablecell.lblQuater.text = "\(objModel.a_city), \(objModel.a_state)"
        
        tablecell.preservesSuperviewLayoutMargins = false
        tablecell.separatorInset = UIEdgeInsets.zero
        tablecell.layoutMargins = UIEdgeInsets.zero
        
        if objModel.is_bookmark == "0" {
//            tablecell.btnBookmark.setImage(UIImage(named: "Bookmark"), for: .normal)
            tablecell.imgBookmark.image = UIImage(named: "Bookmark")
            
        }else{
            
//            tablecell.btnBookmark.setImage(UIImage(named: "Bookmark-fill"), for: .normal)
            tablecell.imgBookmark.image = UIImage(named: "Bookmark-fill")

        }
        
//        tablecell.imgRibbon.isHidden = false

        if objModel.is_viewed == "0" {
            tablecell.imgRibbon.isHidden = false
        }else{
            tablecell.imgRibbon.isHidden = true
        }
        
        tablecell.btnBookmark.tag = indexPath.row
        tablecell.btnBookmark.addTarget(self, action: #selector(self.btnBookmarkColl), for: .touchUpInside)
        
        
        return tablecell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if arrayData[indexPath.row].is_viewed == "0"{
            
            let copyObj = arrayData[indexPath.row]
            copyObj.is_viewed = "1"
            var copyArray = arrayData
            copyArray.remove(at: indexPath.row)
            copyArray.insert(copyObj, at: indexPath.row)
            arrayData = copyArray
            self.tblDealObj.reloadData()
            
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ObjVC = storyboard.instantiateViewController(withIdentifier: "MyAssetsDetailVC") as! MyAssetsDetailVC
        ObjVC.strAssetTitle = arrayData[indexPath.row].a_name
        ObjVC.objMyAsset = arrayData[indexPath.row]
        ObjVC.strAssetId = arrayData[indexPath.row].a_id
        ObjVC.IsNewDealDetail = true
        self.navigationController?.pushViewController(ObjVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return UITableViewAutomaticDimension

    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if self.viewSearch.isHidden == true{
            
            if indexPath.row == arrayData.count - 3 {
                isPagingNeed = false
            }
            
            if !isPagingNeed && TotalFeed > 9 {
                
                //            if TotalFeed > 0 {
                isPagingNeed = true
                
                self.pageAdded += 10
                
                let strNewPage = "\(pageAdded)"
                //print("page added \(pageAdded)")
                //print("TotalFeed \(TotalFeed)  \(arrayData.count)")
                
                self.getMoreNewDealsFeed(strMorePage: strNewPage, strType: "1")
             
            }
         
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                if self.arrayData.count > indexPath.row{
                    if self.arrayData[indexPath.row].is_viewed == "0" {
                        let copyObj = self.arrayData[indexPath.row]
                        copyObj.is_viewed = "1"
                        var copyArray = self.arrayData
                        copyArray.remove(at: indexPath.row)
                        copyArray.insert(copyObj, at: indexPath.row)
                        self.arrayData = copyArray
                        self.tblDealObj.reloadData()
                        
                        self.IsReadPostApi(strPostId: copyObj.a_id, strPostTypeId: "0")
                        
                    }
                }
            }
        }

    }
    
//    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//
//        if arrayData[indexPath.row].is_viewed == "0" {
//
//            let copyObj = arrayData[indexPath.row]
//            copyObj.is_viewed = "1"
//            var copyArray = arrayData
//            copyArray.remove(at: indexPath.row)
//            copyArray.insert(copyObj, at: indexPath.row)
//            arrayData = copyArray
//            self.tblDealObj.reloadData()
//
//            self.IsReadPostApi(strPostId: copyObj.a_id, strPostTypeId: "0")
//
//        }else{
//
//        }
//
//    }
    
}
