//
//  RecentUpdateVC.swift
//  EncoreIR
//
//  Created by Trivedi Sagar on 5/11/19.
//  Copyright © 2019 ravi. All rights reserved.
//

import UIKit
import ImageSlideshow

class cellQuaterlyUpdateImage: UITableViewCell {
    
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    
    @IBOutlet weak var objCollection: UICollectionView!
    
    @IBOutlet weak var viewCollHeight: NSLayoutConstraint!
    
    @IBOutlet weak var viewCollection: UIView!
    
    var arrImgQuaterlyUpdates = [UIImage]()

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewBack.layer.cornerRadius = 5
        self.viewBack.layer.borderWidth = 0.5
        self.viewBack.layer.masksToBounds = true
    
    }
    
}


class cellRecentUpdate: UITableViewCell{
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    
    @IBOutlet weak var imgChart: UIImageView!
    
    @IBOutlet weak var btnChart: UIButton!
    
    @IBOutlet weak var viewChart: UIView!
    
    @IBOutlet weak var viewChartBG: UIView!
    
    @IBOutlet weak var viewChartHeight: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewBack.layer.cornerRadius = 5
        self.viewBack.layer.borderWidth = 0.5
        self.viewBack.layer.masksToBounds = true
        
        self.viewChartBG.layer.cornerRadius = viewChartBG.frame.width/2
        self.viewChartBG.layer.masksToBounds = true
       
        
    }
}

class RecentUpdateVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK:- Outlets
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var imgFontBig: UIImageView!
    @IBOutlet weak var btnLargeFont: UIButton!

    @IBOutlet weak var tblDetail: UITableView!
    
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var imgFontBackgroung: UIImageView!
    @IBOutlet weak var btnMinFont: UIButton!
    
    @IBOutlet weak var btnMaxFont: UIButton!
    
    @IBOutlet var lblTitleDate: UILabel!
    
    //MARK:- Variable
    var arrayRowTitle = NSMutableArray()
    var objSection = ObjSectionDetail()
    var IsGroupDis = false
    var arrImage = [UIImage]()
    var strChartName = ""
    var strImageUrl = ""
    var IsPDf = false
    var arrPdf = NSMutableArray()
    var IntCellCount = 6
    var arrConstrctImg = [UIImage]()

    var fontsizeDetail : CGFloat = 0
    var slideshowTransitioningDelegate: ZoomAnimatedTransitioningDelegate? = nil
    
    
    //MARK:- UIView Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
//        arrayRowTitle = ["DESCRIPTION","CONSTRUCTION"]
        
        self.setupInitialView()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.setUpThemeColor()
    }
    
    //MARK:- Class Method and Function
    func setupInitialView(){
        lblTopTitle.text  = strQuarterlyUpdatesTitle
        
        self.QuaterlyUpdatesApi(strAId: objSection.a_id, strSection_isDynamicTF: objSection.section_isDynamic, strSection_id: objSection.section_id, strC_type: objSection.c_type, strIs_group: objSection.is_group)
        
        viewContainer.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.popUpView), name: NSNotification.Name(rawValue: kPopUpView), object: nil)

    }
    func setUpThemeColor(){
        if appDelegateObj.IsThemeLight == true{
            
            print("Ligth Theme mode")
            imgBackground.image = UIImage(named: "Wht_background")
            imgBack.image = UIImage(named: "Wht_Back")
            self.view.backgroundColor = wviewBgDefaultThemeColor
            lblTopTitle.textColor = wTopTitleFontColor
            
            imgFontBackgroung.image = UIImage(named: "Wht_LargeFontBG")
            imgFontBig.image = UIImage(named: "Wht_LargeFont")
            
            btnMinFont.setTitleColor(wBlueMainTitleColor, for: .normal)
            btnMaxFont.setTitleColor(wBlueMainTitleColor, for: .normal)
            
            lblTitleDate.textColor = wTopTitleFontColor
          
        }
        else{
            print("Dark Theme mode")
            self.view.backgroundColor = TabViewColor
            imgBackground.image = UIImage(named: "background")
            imgBack.image = UIImage(named: "Back")
            lblTopTitle.textColor = TopTitleFontColor
            
            imgFontBackgroung.image = UIImage(named: "LargeFontBG")
            imgFontBig.image = UIImage(named: "FontBig")
            
            btnMinFont.setTitleColor(UIColor.white, for: .normal)
            btnMaxFont.setTitleColor(UIColor.white, for: .normal)
            
            lblTitleDate.textColor = TopTitleFontColor
          
        }
        self.setupFontSize()
    }
    func setupFontSize(){
        lblTopTitle.font = lblTopTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 20.0))
        btnMinFont.titleLabel?.font = UIFont(name:DINPRO, size: GlobalMethods.setThemeFontSize(objectSize: 16.0))
        btnMaxFont.titleLabel?.font = UIFont(name:DINPRO, size: GlobalMethods.setThemeFontSize(objectSize: 25.0))
        lblTitleDate.font = lblTitleDate.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 20.0))
        fontsizeDetail = GlobalMethods.setThemeFontSize(objectSize: 0.0)
    }
    @objc func popUpView(){

   /*     let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ObjVC = storyboard.instantiateViewController(withIdentifier: "FullScreenImageVC") as! FullScreenImageVC
        
        ObjVC.arrSlide = arrConstrctImg
        
        ObjVC.modalPresentationStyle = .overCurrentContext
        present(ObjVC, animated: false, completion: nil)
*/
        
        var arryModel = [Model]()
        
        
        for element in arrConstrctImg {
            //imageLabel.image = element
            arryModel.append(Model(image: element, title: ""))
        }
        
        
        let fullScreenController = FullScreenSlideshowViewController()
        
        fullScreenController.closeButton.layer.cornerRadius = fullScreenController.closeButton.bounds.height/2
        fullScreenController.closeButton.layer.borderWidth = 1
        fullScreenController.closeButton.layer.borderColor = UIColor.darkGray.cgColor
        fullScreenController.closeButton.backgroundColor = UIColor.lightGray
        fullScreenController.closeButton.layer.masksToBounds = true
        
        if appDelegateObj.IsThemeLight == true{
            fullScreenController.backgroundColor = wviewBgDefaultThemeColor
        }
        else{
            fullScreenController.backgroundColor = TabViewColor
        }
        
        fullScreenController.inputs = arryModel.map { $0.inputSource }
        
        fullScreenController.initialPage = 0
   
        present(fullScreenController, animated: true, completion: nil)
 
    }
    
    
    @objc func btnDocTbl(sender:UIButton)  {
        
        if IsPDf == false{
            
            // Your action
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ObjVC = storyboard.instantiateViewController(withIdentifier: "FullScreenImageVC") as! FullScreenImageVC
            
            print(arrImage)
            ObjVC.arrSlide = arrImage
            //        self.navigationController?.pushViewController(ObjVC, animated: true)
            
            ObjVC.modalPresentationStyle = .overCurrentContext
            present(ObjVC, animated: false, completion: nil)
            
        }else{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ObjVC = storyboard.instantiateViewController(withIdentifier: "DocumentVC") as! DocumentVC
            if arrPdf.count != 0{
                ObjVC.arrayDocument = arrPdf
                ObjVC.isMoreThenDoc = true
            }
            ObjVC.strUrlPath = arrPdf.lastObject as! String
            ObjVC.strDocTitle = strChartName.firstUppercased
            self.navigationController?.pushViewController(ObjVC, animated: true)
            
        }
        
        
    }
    
    func displayQuaterlyUpdates(value : recent_updateList, indexOrder: QuaterlyListIndex){
        
        print(value)
        print(indexOrder)
        
        lblTitleDate.text = value.q_title_date
        
        if IsGroupDis == true{
            
            let strOne = value.q_general
            let strTwo = value.q_leasing
            let strThree = value.q_construction
            let strFour = value.q_performance
            let strFive = value.q_legal
            let strSix = value.q_footnotes
            
            
            self.arrayRowTitle.removeAllObjects()
            
            self.arrayRowTitle.add(strOne)
            self.arrayRowTitle.add(strTwo)
            self.arrayRowTitle.add(strThree)
            self.arrayRowTitle.add(strFour)
            self.arrayRowTitle.add(strFive)
            self.arrayRowTitle.add(strSix)
            
            
            self.tblDetail.reloadData()
            self.strChartName = value.q_chart_imgs.doc_name //(responseValue?.data.recent_update.q_chart_imgs.doc_name)!
            self.strImageUrl =  value.q_chart_imgs.document    //(responseValue?.data.recent_update.q_chart_imgs.document)!
            
            if self.strImageUrl != ""{
                let fullNameArr = self.strImageUrl.components(separatedBy: "||")
                self.arrImage.removeAll()
                self.arrPdf.removeAllObjects()
                
                for img in fullNameArr {
                    
                    let url = URL(string:img)
                    
                    let strExtension = url?.pathExtension
                    if strExtension == "pdf"{
                        self.IsPDf = true
                        self.arrPdf.add(img)
                    }else{
                        //                        let data = try? Data(contentsOf: url!)
                        
                        if let data = try? Data(contentsOf: url!) {
                            let image: UIImage = UIImage(data: data)!
                            self.arrImage.append(image)
                            
                        }
                        
                        
                        //                        if let image = UIImage(data: data!) {
                        //                            self.arrImage.append(image)
                        //                        }
                        //                        let image: UIImage = UIImage(data: data!)!
                        //                        self.arrImage.append(image)
                    }
                    
                }
                print(arrImage.count)
                
            }
            
            self.arrConstrctImg.removeAll()
            let arrConstructionUrl = value.q_construction_img
            if arrConstructionUrl.count != 0{
                
                for urlImg in arrConstructionUrl {
                    
                    let url = URL(string:urlImg.url)
                    
                    if let data = try? Data(contentsOf: url!) {
                        let image: UIImage = UIImage(data: data)!
                        self.arrConstrctImg.append(image)
                    }
                    
                }
                
            }
            
        }
        else{
            
            let strOne = value.q_general
            let strTwo = value.q_leasing
            let strThree = value.q_construction
            let strFour = value.q_performance
            let strFive = value.q_legal
            let strSix = value.q_footnotes
            
            
            self.arrayRowTitle.removeAllObjects()
            
            self.arrayRowTitle.add(strOne)
            self.arrayRowTitle.add(strTwo)
            self.arrayRowTitle.add(strThree)
            self.arrayRowTitle.add(strFour)
            self.arrayRowTitle.add(strFive)
            self.arrayRowTitle.add(strSix)
            
            
            self.tblDetail.reloadData()
            self.strChartName = value.q_chart_imgs.doc_name //(responseValue?.data.recent_update.q_chart_imgs.doc_name)!
            self.strImageUrl =  value.q_chart_imgs.document    //(responseValue?.data.recent_update.q_chart_imgs.document)!
            
            if self.strImageUrl != ""{
                let fullNameArr = self.strImageUrl.components(separatedBy: "||")
                self.arrImage.removeAll()
                self.arrPdf.removeAllObjects()
                
                for img in fullNameArr {
                    
                    let url = URL(string:img)
                    
                    let strExtension = url?.pathExtension
                    if strExtension == "pdf"{
                        self.IsPDf = true
                        self.arrPdf.add(img)
                    }else{
//                        let data = try? Data(contentsOf: url!)
                        
                        if let data = try? Data(contentsOf: url!) {
                            let image: UIImage = UIImage(data: data)!
                            self.arrImage.append(image)

                        }

                        
//                        if let image = UIImage(data: data!) {
//                            self.arrImage.append(image)
//                        }
//                        let image: UIImage = UIImage(data: data!)!
//                        self.arrImage.append(image)
                    }
                    
                }
                print(arrImage.count)
                
            }
            
            self.arrConstrctImg.removeAll()
            let arrConstructionUrl = value.q_construction_img
            if arrConstructionUrl.count != 0{
                
                for urlImg in arrConstructionUrl {
                    
                    let url = URL(string:urlImg.url)
                    
                    if let data = try? Data(contentsOf: url!) {
                        let image: UIImage = UIImage(data: data)!
                        self.arrConstrctImg.append(image)
                    }
                    
                }
                
            }
            
        }
        
        
    }
    
    //MARK:- IBActions
    @IBAction func btnBackSel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnLargeFontPressed(_ sender: Any) {
        
        if viewContainer.isHidden == true {
            viewContainer.isHidden = false
        } else {
            viewContainer.isHidden = true
        }
    }
    
    
    @IBAction func btnMinFontPressed(_ sender: Any) {
        print("font --\(fontsizeDetail)")
        
        if fontsizeDetail < 0 {
            //            fontsizeTitle = 15
            //            fontsizeDetail = 13
        }
        else{
            fontsizeDetail = fontsizeDetail - 3
            tblDetail.reloadData()
        }
        
    }
    
    
    @IBAction func btnMaxFontPressed(_ sender: Any) {
        print("font ++\(fontsizeDetail)")

        if fontsizeDetail > 6 {
            //            fontsizeTitle = 15
            //            fontsizeDetail = 13
        }
        else{
            fontsizeDetail = fontsizeDetail + 3
            tblDetail.reloadData()
        }
        
    }
    
    
    //MARK:- UITableView Methods
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayRowTitle.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if IsGroupDis == true{
            if indexPath.row == 0{
                let tablecell = tableView.dequeueReusableCell(withIdentifier: "cellQuotes", for: indexPath) as! cellQuotes
                
                tablecell.lblTitle.text =  arrayRowTitle[indexPath.row] as? String
                tablecell.lblTitle.font = UIFont(name: DINPROBold, size: CGFloat(13 + fontsizeDetail))

                tablecell.lblTitle.numberOfLines = 0
                
                if appDelegateObj.IsThemeLight{
                    tablecell.viewBack.backgroundColor = UIColor.white
                    tablecell.lblTitle.textColor = wBlueMainTitleColor
                }else{
                    tablecell.viewBack.backgroundColor = kCellBackColor
                    tablecell.lblTitle.textColor = UIColor.white
                }
 
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    tablecell.isHidden = true
                }else{
                    tablecell.isHidden = false

                }
                
                return tablecell
                
            }
            else if indexPath.row == 1{
                
                let tablecell = tableView.dequeueReusableCell(withIdentifier: "cellRecentUpdate", for: indexPath) as! cellRecentUpdate
                
                tablecell.lblTitle.text = strLeasing
                tablecell.lblTitle.font = UIFont(name: DINPROBold, size: CGFloat(14 + fontsizeDetail))
                tablecell.lblDetail.text = arrayRowTitle[indexPath.row] as? String
                tablecell.lblDetail.font = UIFont(name: MEDIUEM_FONT, size: CGFloat(14 + fontsizeDetail))
                tablecell.lblDetail.numberOfLines = 0
                
                
                if appDelegateObj.IsThemeLight{
                    tablecell.viewBack.layer.borderColor = wBlueMainTitleColor.cgColor
                    tablecell.viewBack.backgroundColor = UIColor.white
                    tablecell.lblTitle.textColor = wBlueMainTitleColor
                    tablecell.lblDetail.textColor = wDarkGrayColor
                    tablecell.viewChartBG.backgroundColor = wBlueMainTitleColor
                }else{
                    tablecell.viewBack.layer.borderColor = kCellBackColor.cgColor
                    tablecell.viewBack.backgroundColor = kCellBackColor
                    tablecell.lblTitle.textColor = UIColor.white
                    tablecell.lblDetail.textColor = UIColor.white
                    tablecell.viewChartBG.backgroundColor = PdfBGColor
                }
               
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    tablecell.isHidden = true
                }
                else{
                    tablecell.isHidden = false
                }
                
                tablecell.viewChartHeight.constant = 0
                tablecell.viewChart.isHidden = true
                
                
                return tablecell
                
            }
            else if indexPath.row == 2{
                
                let tablecell = tableView.dequeueReusableCell(withIdentifier: "cellQuaterlyUpdateImage", for: indexPath) as! cellQuaterlyUpdateImage
                
                tablecell.lblTitle.text = strConstruction
                tablecell.lblDetail.text = arrayRowTitle[indexPath.row] as? String
                tablecell.lblDetail.numberOfLines = 0
                
                tablecell.lblTitle.font = UIFont(name: DINPROBold, size: CGFloat(14 + fontsizeDetail))
                tablecell.lblDetail.font = UIFont(name: MEDIUEM_FONT, size: CGFloat(14 + fontsizeDetail))
                
                tablecell.arrImgQuaterlyUpdates = arrConstrctImg
                
                
                if appDelegateObj.IsThemeLight{
                    tablecell.viewBack.layer.borderColor = wBlueMainTitleColor.cgColor
                    tablecell.viewBack.backgroundColor = UIColor.white
                    tablecell.lblTitle.textColor = wBlueMainTitleColor
                    tablecell.lblDetail.textColor = wDarkGrayColor
                    
                }else{
                    tablecell.viewBack.layer.borderColor = kCellBackColor.cgColor
                    tablecell.viewBack.backgroundColor = kCellBackColor
                    tablecell.lblTitle.textColor = UIColor.white
                    tablecell.lblDetail.textColor = UIColor.white
                    
                }
                
                if arrConstrctImg.count != 0 {
                    tablecell.objCollection.reloadData()
                    tablecell.isHidden = false

                }
                else{
                    tablecell.viewCollHeight.constant = 0
                    tablecell.viewCollection.isHidden = true
                    
                    if arrayRowTitle[indexPath.row] as? String == ""{
                        tablecell.isHidden = true
                    }

                }
                
                return tablecell
                
            }else if indexPath.row == 3{
                
                let tablecell = tableView.dequeueReusableCell(withIdentifier: "cellRecentUpdate", for: indexPath) as! cellRecentUpdate
                
                tablecell.lblTitle.text = strPerformance
                tablecell.lblDetail.text = arrayRowTitle[indexPath.row] as? String
                tablecell.lblDetail.numberOfLines = 0
                
                tablecell.lblTitle.font = UIFont(name: DINPROBold, size: CGFloat(14 + fontsizeDetail))
                tablecell.lblDetail.font = UIFont(name: MEDIUEM_FONT, size: CGFloat(14 + fontsizeDetail))
               
                
                if appDelegateObj.IsThemeLight{
                    tablecell.viewBack.layer.borderColor = wBlueMainTitleColor.cgColor
                    tablecell.viewBack.backgroundColor = UIColor.white
                    tablecell.lblTitle.textColor = wBlueMainTitleColor
                    tablecell.lblDetail.textColor = wDarkGrayColor
                    tablecell.viewChartBG.backgroundColor = wBlueMainTitleColor
                }else{
                    tablecell.viewBack.layer.borderColor = kCellBackColor.cgColor
                    tablecell.viewBack.backgroundColor = kCellBackColor
                    tablecell.lblTitle.textColor = UIColor.white
                    tablecell.lblDetail.textColor = UIColor.white
                    tablecell.viewChartBG.backgroundColor = PdfBGColor
                }
                
                if arrImage.count != 0 || arrPdf.count != 0{
                    tablecell.btnChart.tag = indexPath.row
                    tablecell.isHidden = false
                    tablecell.btnChart.addTarget(self, action: #selector(self.btnDocTbl), for: .touchUpInside)
                    
                    strChartName = strPerformance
                    
                    tablecell.viewChartHeight.constant = 44
                    tablecell.viewChart.isHidden = false

                }
                else{
                    tablecell.viewChartHeight.constant = 0
                    tablecell.viewChart.isHidden = true
                    
                    if arrayRowTitle[indexPath.row] as? String == ""{
                        tablecell.isHidden = true
                    }
                    else{
                        tablecell.isHidden = false
                    }

                    
                }
                
                return tablecell
                
            }else if indexPath.row == 4{
                
                let tablecell = tableView.dequeueReusableCell(withIdentifier: "cellRecentUpdate", for: indexPath) as! cellRecentUpdate
                
                tablecell.lblTitle.text = strLegal
                tablecell.lblDetail.text = arrayRowTitle[indexPath.row] as? String
                tablecell.lblDetail.numberOfLines = 0
                
                tablecell.lblTitle.font = UIFont(name: DINPROBold, size: CGFloat(14 + fontsizeDetail))
                tablecell.lblDetail.font = UIFont(name: MEDIUEM_FONT, size: CGFloat(14 + fontsizeDetail))
                
                
                if appDelegateObj.IsThemeLight{
                    tablecell.viewBack.layer.borderColor = wBlueMainTitleColor.cgColor
                    tablecell.viewBack.backgroundColor = UIColor.white
                    tablecell.lblTitle.textColor = wBlueMainTitleColor
                    tablecell.lblDetail.textColor = wDarkGrayColor
                    tablecell.viewChartBG.backgroundColor = wBlueMainTitleColor
                }else{
                    tablecell.viewBack.layer.borderColor = kCellBackColor.cgColor
                    tablecell.viewBack.backgroundColor = kCellBackColor
                    tablecell.lblTitle.textColor = UIColor.white
                    tablecell.lblDetail.textColor = UIColor.white
                    tablecell.viewChartBG.backgroundColor = PdfBGColor
                }
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    tablecell.isHidden = true
                }
                else{
                    tablecell.isHidden = false
                }
                
                tablecell.viewChartHeight.constant = 0
                tablecell.viewChart.isHidden = true
                
                
                return tablecell
                
            }
            else if indexPath.row == 5{
                
                let tablecell = tableView.dequeueReusableCell(withIdentifier: "cellFinTerm", for: indexPath) as! cellFinTerm
                tablecell.lblTitle.text = arrayRowTitle[indexPath.row] as? String
                tablecell.lblTitle.font = UIFont(name: DINPROBItalic, size: CGFloat(13 + fontsizeDetail))
                
                tablecell.lblTitle.numberOfLines = 0
                
                
                if appDelegateObj.IsThemeLight{
                    
                    tablecell.viewBack.backgroundColor = UIColor.white
                    tablecell.lblTitle.textColor = wDarkGrayColor
                    
                }else{
                    
                    tablecell.viewBack.backgroundColor = kCellBackColor
                    tablecell.lblTitle.textColor = UIColor.white
                    
                }
                
                
                return tablecell
                
            }
            else{
                let tablecell = tableView.dequeueReusableCell(withIdentifier: "cellFinTerm", for: indexPath) as! cellFinTerm
                
                if appDelegateObj.IsThemeLight{
                   
                    tablecell.viewBack.backgroundColor = UIColor.white
                    tablecell.lblTitle.textColor = wDarkGrayColor
                    
                }else{
                    
                    tablecell.viewBack.backgroundColor = kCellBackColor
                    tablecell.lblTitle.textColor = UIColor.white
                    
                }
                return tablecell
            }
            
            
        }
        else{
            if indexPath.row == 0{
                let tablecell = tableView.dequeueReusableCell(withIdentifier: "cellQuotes", for: indexPath) as! cellQuotes
                
                tablecell.lblTitle.text =  arrayRowTitle[indexPath.row] as? String
                tablecell.lblTitle.font = UIFont(name: DINPROBold, size: CGFloat(13 + fontsizeDetail))
                
                tablecell.lblTitle.numberOfLines = 0
                
                if appDelegateObj.IsThemeLight{
                    tablecell.viewBack.backgroundColor = UIColor.white
                    tablecell.lblTitle.textColor = wBlueMainTitleColor
                }else{
                    tablecell.viewBack.backgroundColor = kCellBackColor
                    tablecell.lblTitle.textColor = UIColor.white
                }
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    tablecell.isHidden = true
                }else{
                    tablecell.isHidden = false

                }
                
                return tablecell
                
            }
            else if indexPath.row == 1{
                
                let tablecell = tableView.dequeueReusableCell(withIdentifier: "cellRecentUpdate", for: indexPath) as! cellRecentUpdate
                
                tablecell.lblTitle.text = strLeasing
                tablecell.lblTitle.font = UIFont(name: DINPROBold, size: CGFloat(14 + fontsizeDetail))
                tablecell.lblDetail.text = arrayRowTitle[indexPath.row] as? String
                tablecell.lblDetail.font = UIFont(name: MEDIUEM_FONT, size: CGFloat(14 + fontsizeDetail))
                tablecell.lblDetail.numberOfLines = 0
                
                
                if appDelegateObj.IsThemeLight{
                    tablecell.viewBack.layer.borderColor = wBlueMainTitleColor.cgColor
                    tablecell.viewBack.backgroundColor = UIColor.white
                    tablecell.lblTitle.textColor = wBlueMainTitleColor
                    tablecell.lblDetail.textColor = wDarkGrayColor
                    tablecell.viewChartBG.backgroundColor = wBlueMainTitleColor
                }else{
                    tablecell.viewBack.layer.borderColor = kCellBackColor.cgColor
                    tablecell.viewBack.backgroundColor = kCellBackColor
                    tablecell.lblTitle.textColor = UIColor.white
                    tablecell.lblDetail.textColor = UIColor.white
                    tablecell.viewChartBG.backgroundColor = PdfBGColor
                }
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    tablecell.isHidden = true
                }
                else{
                    tablecell.isHidden = false
                }
                
                tablecell.viewChartHeight.constant = 0
                tablecell.viewChart.isHidden = true
                
                
                return tablecell
                
            }
            else if indexPath.row == 2{
                
                let tablecell = tableView.dequeueReusableCell(withIdentifier: "cellQuaterlyUpdateImage", for: indexPath) as! cellQuaterlyUpdateImage
                
                tablecell.lblTitle.text = strConstruction
                tablecell.lblDetail.text = arrayRowTitle[indexPath.row] as? String
                tablecell.lblDetail.numberOfLines = 0
                
                tablecell.lblTitle.font = UIFont(name: DINPROBold, size: CGFloat(14 + fontsizeDetail))
                tablecell.lblDetail.font = UIFont(name: MEDIUEM_FONT, size: CGFloat(14 + fontsizeDetail))
                
                tablecell.arrImgQuaterlyUpdates = arrConstrctImg
                
                if appDelegateObj.IsThemeLight{
                    tablecell.viewBack.layer.borderColor = wBlueMainTitleColor.cgColor
                    tablecell.viewBack.backgroundColor = UIColor.white
                    tablecell.lblTitle.textColor = wBlueMainTitleColor
                    tablecell.lblDetail.textColor = wDarkGrayColor
                    
                }else{
                    tablecell.viewBack.layer.borderColor = kCellBackColor.cgColor
                    tablecell.viewBack.backgroundColor = kCellBackColor
                    tablecell.lblTitle.textColor = UIColor.white
                    tablecell.lblDetail.textColor = UIColor.white
                    
                }
                
                if arrConstrctImg.count != 0 {
                    tablecell.objCollection.reloadData()
                    tablecell.isHidden = false
                }
                else{
                    tablecell.viewCollHeight.constant = 0
                    tablecell.viewCollection.isHidden = true
                    
                    if arrayRowTitle[indexPath.row] as? String == ""{
                        tablecell.isHidden = true
                    }

                }
                
                return tablecell
                
            }else if indexPath.row == 3{
                
                let tablecell = tableView.dequeueReusableCell(withIdentifier: "cellRecentUpdate", for: indexPath) as! cellRecentUpdate
                
                tablecell.lblTitle.text = strPerformance
                tablecell.lblDetail.text = arrayRowTitle[indexPath.row] as? String
                tablecell.lblDetail.numberOfLines = 0
                
                tablecell.lblTitle.font = UIFont(name: DINPROBold, size: CGFloat(14 + fontsizeDetail))
                tablecell.lblDetail.font = UIFont(name: MEDIUEM_FONT, size: CGFloat(14 + fontsizeDetail))
                
                
                if appDelegateObj.IsThemeLight{
                    tablecell.viewBack.layer.borderColor = wBlueMainTitleColor.cgColor
                    tablecell.viewBack.backgroundColor = UIColor.white
                    tablecell.lblTitle.textColor = wBlueMainTitleColor
                    tablecell.lblDetail.textColor = wDarkGrayColor
                    tablecell.viewChartBG.backgroundColor = wBlueMainTitleColor
                }else{
                    tablecell.viewBack.layer.borderColor = kCellBackColor.cgColor
                    tablecell.viewBack.backgroundColor = kCellBackColor
                    tablecell.lblTitle.textColor = UIColor.white
                    tablecell.lblDetail.textColor = UIColor.white
                    tablecell.viewChartBG.backgroundColor = PdfBGColor
                }
                
                if arrImage.count != 0 || arrPdf.count != 0{
                    tablecell.btnChart.tag = indexPath.row
                    tablecell.isHidden = false
                    tablecell.btnChart.addTarget(self, action: #selector(self.btnDocTbl), for: .touchUpInside)
                    
                    strChartName = strPerformance

                    tablecell.viewChartHeight.constant = 44
                    tablecell.viewChart.isHidden = false
                    
                }
                else{
                    tablecell.viewChartHeight.constant = 0
                    tablecell.viewChart.isHidden = true
                    
                    if arrayRowTitle[indexPath.row] as? String == ""{
                        tablecell.isHidden = true
                    }
                    else{
                        tablecell.isHidden = false
                    }
                }
                
                return tablecell
                
            }else if indexPath.row == 4{
                
                let tablecell = tableView.dequeueReusableCell(withIdentifier: "cellRecentUpdate", for: indexPath) as! cellRecentUpdate
                
                tablecell.lblTitle.text = strLegal
                tablecell.lblDetail.text = arrayRowTitle[indexPath.row] as? String
                tablecell.lblDetail.numberOfLines = 0
                
                tablecell.lblTitle.font = UIFont(name: DINPROBold, size: CGFloat(14 + fontsizeDetail))
                tablecell.lblDetail.font = UIFont(name: MEDIUEM_FONT, size: CGFloat(14 + fontsizeDetail))
                
                if appDelegateObj.IsThemeLight{
                    tablecell.viewBack.layer.borderColor = wBlueMainTitleColor.cgColor
                    tablecell.viewBack.backgroundColor = UIColor.white
                    tablecell.lblTitle.textColor = wBlueMainTitleColor
                    tablecell.lblDetail.textColor = wDarkGrayColor
                    tablecell.viewChartBG.backgroundColor = wBlueMainTitleColor
                }else{
                    tablecell.viewBack.layer.borderColor = kCellBackColor.cgColor
                    tablecell.viewBack.backgroundColor = kCellBackColor
                    tablecell.lblTitle.textColor = UIColor.white
                    tablecell.lblDetail.textColor = UIColor.white
                    tablecell.viewChartBG.backgroundColor = PdfBGColor
                }
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    tablecell.isHidden = true
                }
                else{
                    tablecell.isHidden = false
                }
                
                tablecell.viewChartHeight.constant = 0
                tablecell.viewChart.isHidden = true
                
                
                return tablecell
                
            }
            else if indexPath.row == 5{
                
                let tablecell = tableView.dequeueReusableCell(withIdentifier: "cellFinTerm", for: indexPath) as! cellFinTerm
                tablecell.lblTitle.text = arrayRowTitle[indexPath.row] as? String
                tablecell.lblTitle.font = UIFont(name: DINPROBItalic, size: CGFloat(13 + fontsizeDetail))
                
                tablecell.lblTitle.numberOfLines = 0
                
                if appDelegateObj.IsThemeLight{
                    
                    tablecell.viewBack.backgroundColor = UIColor.white
                    tablecell.lblTitle.textColor = wDarkGrayColor
                    
                }else{
                    
                    tablecell.viewBack.backgroundColor = kCellBackColor
                    tablecell.lblTitle.textColor = UIColor.white
                    
                }
                
                return tablecell
                
            }
            else{
                let tablecell = tableView.dequeueReusableCell(withIdentifier: "cellFinTerm", for: indexPath) as! cellFinTerm
                return tablecell
            }
            
            
        }
                
        
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
//        if indexPath.row == 2{
//
//            if arrConstrctImg.count != 0 {
//
//                // Your action
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let ObjVC = storyboard.instantiateViewController(withIdentifier: "FullScreenImageVC") as! FullScreenImageVC
//
//                ObjVC.arrSlide = arrConstrctImg
//
//                ObjVC.modalPresentationStyle = .overCurrentContext
//                present(ObjVC, animated: false, completion: nil)
//
//            }
//            else{
//
//            }
//
//        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if IsGroupDis == true{
            if indexPath.row == 0{
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    return 0
                }else{
                    return UITableViewAutomaticDimension
                }
                
            }
            else if indexPath.row == 1{
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    return 0
                }else{
                    return UITableViewAutomaticDimension
                }
                
            }
            else if indexPath.row == 2{
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    return 0
                }else{
                    return UITableViewAutomaticDimension
                }
                
            }else if indexPath.row == 3{
                
                if arrayRowTitle[indexPath.row] as? String == "" || arrImage.count == 0 || arrPdf.count == 0{
                    return 0
                }else{
                    return UITableViewAutomaticDimension
                }
                
            }else if indexPath.row == 4{
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    return 0
                }else{
                    return UITableViewAutomaticDimension
                }
                
            }
            else if indexPath.row == 5{
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    return 0
                }else{
                    return UITableViewAutomaticDimension
                }
                
            }
            else{
                return UITableViewAutomaticDimension
            }
            
            
        }
        else{
            if indexPath.row == 0{
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    return 0
                }else{
                    return UITableViewAutomaticDimension
                }
                
            }
            else if indexPath.row == 1{
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    return 0
                }else{
                    return UITableViewAutomaticDimension
                }
                
            }
            else if indexPath.row == 2{
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    
                    if arrConstrctImg.count != 0{
                        return UITableViewAutomaticDimension
                    }
                    
                    return 0
                }else{
                    return UITableViewAutomaticDimension
                }
                
            }else if indexPath.row == 3{
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    
                    if arrImage.count != 0 || arrPdf.count != 0{
                        return UITableViewAutomaticDimension
                    }
                    
                    return 0
                }else{
                    return UITableViewAutomaticDimension
                }
                
            }else if indexPath.row == 4{
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    return 0
                }else{
                    return UITableViewAutomaticDimension
                }
                
            }
            else if indexPath.row == 5{
                
                if arrayRowTitle[indexPath.row] as? String == ""{
                    return 0
                }else{
                    return UITableViewAutomaticDimension
                }
                
            }
            else{
                return UITableViewAutomaticDimension
            }
            
            
        }
        
        
    }
    
}


extension cellQuaterlyUpdateImage : UICollectionViewDataSource, UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImgQuaterlyUpdates.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellAssetDetail", for: indexPath) as! cellAssetDetail
        cell.imgNews.image = arrImgQuaterlyUpdates[indexPath.row]
        cell.pageControl.numberOfPages = arrImgQuaterlyUpdates.count
        cell.btnCounter.setTitle("\(arrImgQuaterlyUpdates.count)", for: .normal)
        cell.btnCounter.isHidden = true
        cell.imgBox.isHidden = true

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellAssetDetail", for: indexPath) as! cellAssetDetail
        Cell.pageControl.currentPage = indexPath.row
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("cell pressed")
        
       let nc1 = NotificationCenter.default
        nc1.post(name: Notification.Name(kPopUpView), object: nil)

        
        

    }
    
}

extension cellQuaterlyUpdateImage : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let itemsPerRow:CGFloat = 4
//        let hardCodedPadding:CGFloat = 5
//        let itemWidth = (collectionView.bounds.width / itemsPerRow) - hardCodedPadding
//        let itemHeight = collectionView.bounds.height - (2 * hardCodedPadding)
//        return CGSize(width: itemWidth, height: itemHeight)
        
        let approximateHeight = collectionView.frame.height
        
        let numberOfCell: CGFloat = 1   //you need to give a type as CGFloat
        let cellWidth = UIScreen.main.bounds.size.width / numberOfCell
        //        return CGSizeMake(cellWidth, cellWidth)
        return CGSize(width: cellWidth, height: approximateHeight)
    }
    
}
