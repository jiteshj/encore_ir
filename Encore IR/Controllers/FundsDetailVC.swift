//
//  FundsDetailVC.swift
//  EncoreIR
//
//  Created by Jaimin Patel on 19/05/19.
//  Copyright © 2019 ravi. All rights reserved.
//

import UIKit
import AlignedCollectionViewFlowLayout
import AlignedCollectionViewFlowLayout
import ImageSlideshow


class cellFundOverView: UITableViewCell {
    
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var lblcellTitle: UILabel!
    @IBOutlet weak var lblCreatedDate: UILabel!
    @IBOutlet weak var lblDescription: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewBack.layer.cornerRadius = 5
        self.viewBack.layer.masksToBounds = true
        self.viewBack.layer.borderWidth = 0.5
        
        self.lblCreatedDate.isHidden = true
        
        lblcellTitle.font = lblcellTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 15.0))
        lblCreatedDate.font = lblCreatedDate.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 12.0))
        lblDescription.font = lblDescription.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 13.0))

    }
    
}


class cellCollSubProject: UICollectionViewCell {
    
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var imgCell: UIImageView!
    @IBOutlet weak var lblCell: UILabel!
    
    var tempCellAttributesArray = [UICollectionViewLayoutAttributes]()
    let leftEdgeInset: CGFloat = 10

    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.imgCell.layer.cornerRadius = 3
        self.imgCell.clipsToBounds = true
        
        lblCell.font = lblCell.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 15.0))
    }
}

class FundsDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource {

    
    @IBOutlet weak var viewBackground: UIImageView!
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var viewTop: UIView!
    
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var imgBookmarkTop: UIImageView!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var btnBookmark: UIButton!
    
    @IBOutlet weak var objFundTbl: UITableView!
    
    @IBOutlet weak var collSubProject: UICollectionView!
    
    @IBOutlet weak var collHeaderView: UICollectionView!
    
    @IBOutlet weak var lblNoDataFound: UILabel!
    
    
    //MARK:- Variable
    var arrImg = [UIImage]()
    var objgroupDetail = groupDetailsList()
    var arrSectionList = [sectionlistDetail]()
    var arrProjectDetail = [projectdetailList]()
    var arrFundDetailList = [fundDetailList]()
    var countSubDeal = ""
    var objFund = AssetProject()
    var arrImgUrl = [a_asset_imgDetail]()
    var strFundId = ""

    
    @IBOutlet var collectionHeader: UIView!
    
    
    @IBOutlet var collectionFooter: UIView!
    
    let alignedFlowLayout = AlignedCollectionViewFlowLayout(horizontalAlignment: .justified, verticalAlignment: .center)
    var slideshowTransitioningDelegate: ZoomAnimatedTransitioningDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        objFundTbl.tableHeaderView = collectionHeader
        objFundTbl.tableFooterView = collectionFooter

        self.setupInitialView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpThemeColor()
    }
    

    //MARK:- Class Methods and Functions
    func setupInitialView(){
        
        lblNoDataFound.isHidden = true
        
        if strFundId != ""{
            self.getFundsDetail(strType: strFundId)

        }else{
            self.getFundsDetail(strType: objFund.a_gp_id)

        }

//        self.lblTopTitle.text = self.objgroupDetail.g_a_group_name
//        if self.objFund.is_bookmark == "1"{
//            imgBookmarkTop.image = UIImage.init(named: "Bookmark-fill")
//
//        }else{
//            imgBookmarkTop.image = UIImage.init(named: "Bookmark")
//        }
        if strFundId != ""{
            self.IsReadPostApi(strPostId: strFundId, strPostTypeId: "1")

        }else{
            self.IsReadPostApi(strPostId: objFund.a_gp_id, strPostTypeId: "1")

        }
//        self.IsReadPostApi(strPostId: objFund.a_gp_id, strPostTypeId: "1")

//        let HeigthColletion : CGFloat = CGFloat(255 * self.arrSectionList.count)
//        self.collectionFooter.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width , height: HeigthColletion)
//        self.view.setNeedsLayout()

    }
    
    func setUpThemeColor(){
        if appDelegateObj.IsThemeLight == true{
            
            print("Ligth Theme mode")
            viewBackground.image = UIImage(named: "Wht_background")
            imgBack.image = UIImage(named: "Wht_Back")
            
            self.view.backgroundColor = wviewBgDefaultThemeColor
            lblTopTitle.textColor = wTopTitleFontColor
            lblNoDataFound.textColor = UIColor.black
            
        }
        else{
            print("Dark Theme mode")
            self.view.backgroundColor = TabViewColor
            viewBackground.image = UIImage(named: "background")
            imgBack.image = UIImage(named: "Back")
            lblTopTitle.textColor = TopTitleFontColor
            lblNoDataFound.textColor = UIColor.white
            
        }
        self.setupFontSize()
    }
    func setupFontSize(){
        lblTopTitle.font = lblTopTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 20.0))
        lblNoDataFound.font = lblNoDataFound.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 15.0))
    }

    @IBAction func btBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func btnBookmarkPressed(_ sender: Any) {
        
        appDelegateObj.strPostId = objgroupDetail.g_a_id
        appDelegateObj.strPostTypeId = "4" //fund type = 4
        
        if objFund.is_bookmark == "1"{
            objFund.is_bookmark = "0"
            self.removeBookmarkApi(strPostId: appDelegateObj.strPostId, strPostTypeId: appDelegateObj.strPostTypeId)
            btnBookmark.setImage(UIImage(named: "Bookmark"), for: .normal)
        }else{
            objFund.is_bookmark = "1"
            btnBookmark.setImage(UIImage(named: "Bookmark-fill"), for: .normal)
            self.addBookmarkApi(strPostId: appDelegateObj.strPostId, strPostTypeId: appDelegateObj.strPostTypeId)
        }
        
        let nc = NotificationCenter.default
        nc.post(name: Notification.Name(kRefreshAssets), object: nil)

        
    }
    
    
    //MARK:- UITableView Methods
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrSectionList.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        
        
        if indexPath.row == 0{
            let tablecellMain = tableView.dequeueReusableCell(withIdentifier: "cellFundOverView", for: indexPath) as! cellFundOverView

            tablecellMain.lblCreatedDate.text = objgroupDetail.created_date
            
            tablecellMain.lblDescription.text = objgroupDetail.g_a_overview
            tablecellMain.lblDescription.numberOfLines = 0
            
            if appDelegateObj.IsThemeLight{
                
                tablecellMain.viewBack.backgroundColor = wviewBgDefaultThemeColor
                tablecellMain.viewBack.layer.borderColor = wBlueMainTitleColor.cgColor
                tablecellMain.lblcellTitle.textColor = wBlueMainTitleColor
                tablecellMain.lblDescription.textColor = wDarkGrayColor
                tablecellMain.lblCreatedDate.textColor = wDarkGrayColor
            }else{
                tablecellMain.viewBack.backgroundColor = kCellBackColor
                tablecellMain.viewBack.layer.borderColor = UIColor.clear.cgColor
                tablecellMain.lblcellTitle.textColor = UIColor.white
                tablecellMain.lblDescription.textColor = UIColor.white
                tablecellMain.lblCreatedDate.textColor = UIColor.white
            }

            
            return tablecellMain
        }
        else{
            let tablecell = tableView.dequeueReusableCell(withIdentifier: "cellDetailTitle", for: indexPath) as! cellDetailTitle

            tablecell.lblTitle.text = arrSectionList[indexPath.row].section_name.uppercased()
            
            if arrSectionList[indexPath.row].hide_Show == "Hide"{
                tablecell.isHidden = true
            }
            
            if appDelegateObj.IsThemeLight{
                tablecell.viewBack.backgroundColor = wviewBgDefaultThemeColor
                tablecell.viewBack.layer.borderColor = wBlueMainTitleColor.cgColor
                tablecell.lblTitle.textColor = wBlueMainTitleColor
                
            }else{
                tablecell.viewBack.backgroundColor = kCellBackColor
                tablecell.viewBack.layer.borderColor = UIColor.clear.cgColor
                tablecell.lblTitle.textColor = UIColor.white
            }
            
            return tablecell
        }
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if indexPath.row == 0{
            return
        }
        
        else if arrSectionList[indexPath.row].section_id == strUnique1{

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ObjVC = storyboard.instantiateViewController(withIdentifier: "InvestmentSummaryVC") as! InvestmentSummaryVC
            //            ObjVC.objSection = arrSectionList[indexPath.row]
            
            let ObjSectionData = ObjSectionDetail()
            ObjSectionData.a_id = objgroupDetail.g_a_id
            ObjSectionData.c_type = arrSectionList[indexPath.row].c_id
            ObjSectionData.is_group = "1"
            ObjSectionData.section_id = arrSectionList[indexPath.row].section_id
            ObjSectionData.section_isDynamic = arrSectionList[indexPath.row].section_isDynamic
            ObjSectionData.u_id = appDelegateObj.loginUser.u_id
            
            appDelegateObj.ObjSection = ObjSectionData
            
            ObjVC.objSection = ObjSectionData
            
            self.navigationController?.pushViewController(ObjVC, animated: true)
            
        }
        else if arrSectionList[indexPath.row].section_id == strUnique5{

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ObjVC = storyboard.instantiateViewController(withIdentifier: "DistributionGuideVC") as! DistributionGuideVC
            
            let ObjSectionData = ObjSectionDetail()
            ObjSectionData.a_id = objgroupDetail.g_a_id
            ObjSectionData.c_type = arrSectionList[indexPath.row].c_id
            ObjSectionData.is_group = "1"
            ObjSectionData.section_id = arrSectionList[indexPath.row].section_id
            ObjSectionData.section_isDynamic = arrSectionList[indexPath.row].section_isDynamic
            ObjSectionData.u_id = appDelegateObj.loginUser.u_id
            
            appDelegateObj.ObjSection = ObjSectionData
            
            ObjVC.objSection = ObjSectionData
            ObjVC.IsGroupDis = true
            
            self.navigationController?.pushViewController(ObjVC, animated: true)
            
        }
        else if arrSectionList[indexPath.row].section_id == strUnique3{

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ObjVC = storyboard.instantiateViewController(withIdentifier: "FinancialsVC") as! FinancialsVC
            
            let ObjSectionData = ObjSectionDetail()
            ObjSectionData.a_id = objgroupDetail.g_a_id
            ObjSectionData.c_type = arrSectionList[indexPath.row].c_id
            ObjSectionData.is_group = "1"
            ObjSectionData.section_id = arrSectionList[indexPath.row].section_id
            ObjSectionData.section_isDynamic = arrSectionList[indexPath.row].section_isDynamic
            ObjSectionData.u_id = appDelegateObj.loginUser.u_id
            
            appDelegateObj.ObjSection = ObjSectionData
            
            ObjVC.objSection = ObjSectionData
            ObjVC.IsGroupDis = true
            
            self.navigationController?.pushViewController(ObjVC, animated: true)
            
            
        }
        else {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ObjVC = storyboard.instantiateViewController(withIdentifier: "NewSectionVC") as! NewSectionVC
            
            let ObjSectionData = ObjSectionDetail()
            ObjSectionData.a_id = objgroupDetail.g_a_id
            ObjSectionData.c_type = arrSectionList[indexPath.row].section_type
            ObjSectionData.is_group = "1"
            ObjSectionData.section_id = arrSectionList[indexPath.row].section_id
            ObjSectionData.section_isDynamic = arrSectionList[indexPath.row].section_isDynamic
            ObjSectionData.u_id = appDelegateObj.loginUser.u_id
            
            appDelegateObj.ObjSection = ObjSectionData
            
            ObjVC.objSection = ObjSectionData
            ObjVC.IsGroupDis = false
            ObjVC.lblTopTitle.text = arrSectionList[indexPath.row].section_display_name

            self.navigationController?.pushViewController(ObjVC, animated: true)
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0{
            
            return UITableViewAutomaticDimension
            
        }
        else{
            if arrSectionList[indexPath.row].hide_Show == "Hide"{
                return 0
            }else{
                return UITableViewAutomaticDimension
            }
        }
        
        
    }
    
    //MARK:- UICollection Delegate Method
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collSubProject {
            return arrProjectDetail.count + arrFundDetailList.count
        }
        else if collectionView == collHeaderView{
            return arrImgUrl.count
        }
        else{
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        
        if collectionView == collSubProject {
            
            let cellFooter = collectionView.dequeueReusableCell(withReuseIdentifier: "cellCollSubProject", for: indexPath) as! cellCollSubProject
            
            if indexPath.row < arrProjectDetail.count{
                let imgPath = arrProjectDetail[indexPath.row].a_asset_img
                cellFooter.imgCell.sd_setImage(with: URL(string: imgPath), completed: nil)
                cellFooter.lblCell.text = arrProjectDetail[indexPath.row].a_name
                cellFooter.lblCell.numberOfLines = 2
                
                if appDelegateObj.IsThemeLight{
                    cellFooter.lblCell.textColor = wDarkGrayColor
                }else{
                    cellFooter.lblCell.textColor = UIColor.white
                }
            }
            else{
                
                var objModel = fundDetailList()
                
                objModel = arrFundDetailList[indexPath.row - arrProjectDetail.count]
                
                let imgPath = objModel.g_a_photos
                cellFooter.imgCell.sd_setImage(with: URL(string: imgPath), completed: nil)
                cellFooter.lblCell.text = objModel.g_a_group_name
                cellFooter.lblCell.numberOfLines = 2
                
                if appDelegateObj.IsThemeLight{
                    cellFooter.lblCell.textColor = wDarkGrayColor
                }else{
                    cellFooter.lblCell.textColor = UIColor.white
                }
            }
            
           

            return cellFooter

        }
        else{
            
            let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellAssetDetail", for: indexPath) as! cellAssetDetail
            
            let imgPath = arrImgUrl[indexPath.row].url
            Cell.imgNews.sd_setImage(with: URL(string: imgPath), completed: nil)
            
            
            
            Cell.pageControl.numberOfPages = arrImgUrl.count
            Cell.btnCounter.setTitle("+\(arrImgUrl.count)", for: .normal)
            
            Cell.btnCounter.isHidden = true
            Cell.imgBox.isHidden = true
            
            return Cell

        }
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // Your action
        
        if collectionView == collHeaderView {
          
            
            var arryModel = [Model]()
            
            for element in arrImg {
                
                arryModel.append(Model(image: element, title: ""))
            }
            
            
            let fullScreenController = FullScreenSlideshowViewController()
            
            fullScreenController.closeButton.layer.cornerRadius = fullScreenController.closeButton.bounds.height/2
            fullScreenController.closeButton.layer.borderWidth = 1
            fullScreenController.closeButton.layer.borderColor = UIColor.darkGray.cgColor
            fullScreenController.closeButton.backgroundColor = UIColor.lightGray
            fullScreenController.closeButton.layer.masksToBounds = true
            
            if appDelegateObj.IsThemeLight == true{
                fullScreenController.backgroundColor = wviewBgDefaultThemeColor
            }
            else{
                fullScreenController.backgroundColor = TabViewColor
            }
            
            fullScreenController.inputs = arryModel.map { $0.inputSource }
            
            
            fullScreenController.initialPage = 0
            
            
            
            
            present(fullScreenController, animated: true, completion: nil)

        }
        
        if collectionView == collSubProject{
            
            if indexPath.row < arrProjectDetail.count{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let ObjVC = storyboard.instantiateViewController(withIdentifier: "MyAssetsDetailVC") as! MyAssetsDetailVC
                ObjVC.strAssetTitle = objFund.a_name
                ObjVC.objMyAsset = objFund
                ObjVC.strAssetId = arrProjectDetail[indexPath.row].a_id
                ObjVC.IsNewDealDetail = false
              //  print(objFund)
                self.navigationController?.pushViewController(ObjVC, animated: true)
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let ObjVC = storyboard.instantiateViewController(withIdentifier: "FundsDetailVC") as! FundsDetailVC
                ObjVC.strFundId = arrFundDetailList[indexPath.row - arrProjectDetail.count].g_a_id
                self.navigationController?.pushViewController(ObjVC, animated: true)
            }
            
            

        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == collHeaderView {
            let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellAssetDetail", for: indexPath) as! cellAssetDetail
            Cell.pageControl.currentPage = indexPath.row

        }
    }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        let closestCell: UICollectionViewCell = self.collHeaderView.visibleCells[0]
        for cell in self.collHeaderView.visibleCells as [UICollectionViewCell] {
            let closestCellDelta = abs(closestCell.center.x - self.collHeaderView.bounds.size.width / 2.0 - self.collHeaderView.contentOffset.x)
            let cellDelta = abs(cell.center.x - self.collHeaderView.bounds.size.width/2.0 - self.collHeaderView.contentOffset.x)
            if (cellDelta < closestCellDelta){
            }
        }
        let indexPath = self.collHeaderView.indexPath(for: closestCell)
        self.collHeaderView.scrollToItem(at: indexPath!, at: .centeredHorizontally , animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    }
}


extension FundsDetailVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        if collectionView == collSubProject{
//            let approximateHeight = collSubProject.frame.height
//
//            let numberOfCell: CGFloat = 2   //you need to give a type as CGFloat
//            let cellWidth = UIScreen.main.bounds.size.width / numberOfCell
//            //        return CGSizeMake(cellWidth, cellWidth)
//            return CGSize(width: cellWidth, height: approximateHeight)
            
            let marginsAndInsets = 10 * 2 + collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right + 10 * CGFloat(2 - 1)
            let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(2)).rounded(.down)
            //        let itemHeight = ((collectionView.bounds.size.height - marginsAndInsets) / CGFloat(2)).rounded(.down)
            
//            let approximateHeight = itemWidth * CGFloat(arrProjectDetail.count)
            
            return CGSize(width: itemWidth, height: itemWidth + 20)

        }
        else {
            let approximateHeight = collHeaderView.frame.height

            let numberOfCell: CGFloat = 1   //you need to give a type as CGFloat
            let cellWidth = UIScreen.main.bounds.size.width / numberOfCell
            //        return CGSizeMake(cellWidth, cellWidth)
            return CGSize(width: cellWidth, height: approximateHeight)
        }


    }
    
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    //        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    //    }
    
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    //        return 0
    //    }
    //
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    //        return 0
    //    }
    //
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    //        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    //    }
    
    
    
}

class CollectionViewFlowLayout: UICollectionViewFlowLayout {
    var tempCellAttributesArray = [UICollectionViewLayoutAttributes]()
    let leftEdgeInset: CGFloat = 10
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let cellAttributesArray = super.layoutAttributesForElements(in: rect)
        //Oth position cellAttr is InConvience Emoji Cell, from 1st onwards info cells are there, thats why we start count from 2nd position.
        if(cellAttributesArray != nil && cellAttributesArray!.count > 1) {
            for i in 1..<(cellAttributesArray!.count) {
                let prevLayoutAttributes: UICollectionViewLayoutAttributes = cellAttributesArray![i - 1]
                let currentLayoutAttributes: UICollectionViewLayoutAttributes = cellAttributesArray![i]
                let maximumSpacing: CGFloat = 8
                let prevCellMaxX: CGFloat = prevLayoutAttributes.frame.maxX
                //UIEdgeInset 30 from left
                let collectionViewSectionWidth = self.collectionViewContentSize.width - leftEdgeInset
                let currentCellExpectedMaxX = prevCellMaxX + maximumSpacing + (currentLayoutAttributes.frame.size.width )
                if currentCellExpectedMaxX < collectionViewSectionWidth {
                    var frame: CGRect? = currentLayoutAttributes.frame
                    frame?.origin.x = prevCellMaxX + maximumSpacing
                    frame?.origin.y = prevLayoutAttributes.frame.origin.y
                    currentLayoutAttributes.frame = frame ?? CGRect.zero
                } else {
                    // self.shiftCellsToCenter()
                    currentLayoutAttributes.frame.origin.x = leftEdgeInset
                    //To Avoid InConvience Emoji Cell
                    if (prevLayoutAttributes.frame.origin.x != 0) {
                        currentLayoutAttributes.frame.origin.y = prevLayoutAttributes.frame.origin.y + prevLayoutAttributes.frame.size.height + 08
                    }
                }
                // print(currentLayoutAttributes.frame)
            }
            //print("Main For Loop End")
        }
        // self.shiftCellsToCenter()
        return cellAttributesArray
    }
    
    func shiftCellsToCenter() {
        if (tempCellAttributesArray.count == 0) {return}
        let lastCellLayoutAttributes = self.tempCellAttributesArray[self.tempCellAttributesArray.count-1]
        let lastCellMaxX: CGFloat = lastCellLayoutAttributes.frame.maxX
        let collectionViewSectionWidth = self.collectionViewContentSize.width - leftEdgeInset
        let xAxisDifference = collectionViewSectionWidth - lastCellMaxX
        if xAxisDifference > 0 {
            for each in self.tempCellAttributesArray{
                each.frame.origin.x += xAxisDifference/2
            }
        }
        
    }
}

class DynamicHeightCollectionView: UICollectionView {
    override func layoutSubviews() {
        super.layoutSubviews()
        if !__CGSizeEqualToSize(bounds.size, self.intrinsicContentSize) {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        return contentSize
    }
}
