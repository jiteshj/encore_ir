//
//  NewSectionVC.swift
//  EncoreIR
//
//  Created by ravi on 15/05/19.
//  Copyright © 2019 ravi. All rights reserved.
//

import UIKit
import ImageSlideshow

class cellNewSection: UITableViewCell{

    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var txtDetail: UITextView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewBack.layer.cornerRadius = 5
        viewBack.layer.borderWidth = 0.5
        self.viewBack.layer.masksToBounds = true
    
    }
}

class NewSectionVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDelegate, UITableViewDataSource {

    
    //MARK:- Outlets
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var imgFontBackgroung: UIImageView!
    @IBOutlet weak var imgFontBig: UIImageView!
    
    @IBOutlet weak var btnLargeFont: UIButton!
    @IBOutlet weak var objCollectionNewSection: UICollectionView!
    @IBOutlet weak var viewCollection: UIView!
    @IBOutlet weak var tblDetail: UITableView!
    
    
    @IBOutlet weak var viewContainer: UIView!
    
    @IBOutlet weak var btnMinFont: UIButton!
    
    @IBOutlet weak var btnMaxFont: UIButton!
    
    //MARK:- Variable
    var arrayRowTitle = [TextDetailList]()
    var arrayRowTitleDetail = NSMutableArray()
    var objSection = ObjSectionDetail()
    var IsGroupDis = false
    var arrImage = [UIImage]()
    var strChartName = ""
    var strImageUrl = ""
    var IsPDf = false
    var arrPdf = NSMutableArray()
    var strViewTitle = ""
    var fontsizeDetail : CGFloat = 0
    var slideshowTransitioningDelegate: ZoomAnimatedTransitioningDelegate? = nil

    //MARK:- UIView Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupInitialView()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.setUpThemeColor()
    }
    
    //MARK:- Class Method and Function
    func setupInitialView(){
        lblTopTitle.text  = strViewTitle
        
      //  btnLargeFont.isHidden = true
        
        self.NewSectionApi(strAId: objSection.a_id, strSection_isDynamicTF: objSection.section_isDynamic, strSection_id: objSection.section_id, strC_type: objSection.c_type, strIs_group: objSection.is_group)
     
        
        viewContainer.isHidden = true

    }
    
    func setUpThemeColor(){
        if appDelegateObj.IsThemeLight == true{
            
            print("Ligth Theme mode")
            imgBackground.image = UIImage(named: "Wht_background")
            imgBack.image = UIImage(named: "Wht_Back")
            self.view.backgroundColor = wviewBgDefaultThemeColor
            lblTopTitle.textColor = wTopTitleFontColor
            
            imgFontBackgroung.image = UIImage(named: "Wht_LargeFontBG")
            imgFontBig.image = UIImage(named: "Wht_LargeFont")
            
            btnMinFont.setTitleColor(wBlueMainTitleColor, for: .normal)
            btnMaxFont.setTitleColor(wBlueMainTitleColor, for: .normal)
            

            
        }
        else{
            print("Dark Theme mode")
            self.view.backgroundColor = TabViewColor
            imgBackground.image = UIImage(named: "background")
            imgBack.image = UIImage(named: "Back")
            lblTopTitle.textColor = TopTitleFontColor
            
            imgFontBackgroung.image = UIImage(named: "LargeFontBG")
            imgFontBig.image = UIImage(named: "FontBig")
            
            btnMinFont.setTitleColor(UIColor.white, for: .normal)
            btnMaxFont.setTitleColor(UIColor.white, for: .normal)
      
        }
        self.setupFontSize()
    }

    func setupFontSize(){
        lblTopTitle.font = lblTopTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 20.0))
        btnMinFont.titleLabel?.font = UIFont(name:DINPRO, size: GlobalMethods.setThemeFontSize(objectSize: 16.0))
        btnMaxFont.titleLabel?.font = UIFont(name:DINPRO, size: GlobalMethods.setThemeFontSize(objectSize: 25.0))
        
        fontsizeDetail = GlobalMethods.setThemeFontSize(objectSize: 0.0)
    }
    func displayNewSection(value : NewSectionListData){
        
        print(value)

        if IsGroupDis == true{
            
            
            
            let strOne = value.text
            
            self.arrayRowTitle.removeAll()
            
            self.arrayRowTitle.append(strOne)
            
            self.tblDetail.reloadData()
            
            let fullImageArr = value.image.value
            self.arrImage.removeAll()
            self.arrPdf.removeAllObjects()
            
            if fullImageArr != ""{
                
                let newArr = fullImageArr.components(separatedBy: ",")
                self.arrImage.removeAll()
                self.arrPdf.removeAllObjects()
                
                for img in newArr {
                    
                    let url = URL(string:img)
                    
                    let strExtension = url?.pathExtension
                    if strExtension == "pdf"{
                        self.IsPDf = true
                        self.arrPdf.add(img)
                    }else{
                        //                        let data = try? Data(contentsOf: url!)
                        
                        if let data = try? Data(contentsOf: url!) {
                            let image: UIImage = UIImage(data: data)!
                            self.arrImage.append(image)
                            
                        }
                        
                        
                    }
                    
                    print(arrImage.count)
                    
                }
                
                self.objCollectionNewSection.reloadData()
                
                self.tblDetail.tableHeaderView = self.viewCollection
                
            }
            
        } else{
            
            
            
            let strOne = value.text
            
            self.arrayRowTitle.removeAll()
            
            self.arrayRowTitle.append(strOne)
            
            self.tblDetail.reloadData()
            
            let fullImageArr = value.image.value
            self.arrImage.removeAll()
            self.arrPdf.removeAllObjects()
            
            if fullImageArr != ""{
                
                let newArr = fullImageArr.components(separatedBy: ",")
                self.arrImage.removeAll()
                self.arrPdf.removeAllObjects()
                
                for img in newArr {
                    
                    let url = URL(string:img)
                    
                    let strExtension = url?.pathExtension
                    if strExtension == "pdf"{
                        self.IsPDf = true
                        self.arrPdf.add(img)
                    }else{
                        //                        let data = try? Data(contentsOf: url!)
                        
                        if let data = try? Data(contentsOf: url!) {
                            let image: UIImage = UIImage(data: data)!
                            self.arrImage.append(image)
                            
                        }
                        
                        
                    }
                    
                    print(arrImage.count)
                    
                }
                
                if arrImage.count > 0 {
                    self.objCollectionNewSection.reloadData()
                    self.tblDetail.tableHeaderView = self.viewCollection
                }
                
            }
            
        }

    }

    //MARK:- IBActions
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnLargeFontPressed(_ sender: Any) {
        
        if viewContainer.isHidden == true {
            viewContainer.isHidden = false
        } else {
            viewContainer.isHidden = true
        }
        
    }
    
    
    @IBAction func btnMinFontPressed(_ sender: Any) {
        print("font --\(fontsizeDetail)")
        
        if fontsizeDetail < 0 {
            //            fontsizeTitle = 15
            //            fontsizeDetail = 13
        }
        else{
            fontsizeDetail = fontsizeDetail - 3
            tblDetail.reloadData()
        }
        
    }
    
    
    @IBAction func btnMaxFontPressed(_ sender: Any) {
        print("font ++\(fontsizeDetail)")
        
        if fontsizeDetail > 6 {
            //            fontsizeTitle = 15
            //            fontsizeDetail = 13
        }
        else{
            fontsizeDetail = fontsizeDetail + 3
            tblDetail.reloadData()
        }
        
    }
    
    //MARK:- UICollection Delegate Method
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellAssetDetail", for: indexPath) as! cellAssetDetail
        Cell.imgNews.image = arrImage[indexPath.row]
        Cell.pageControl.numberOfPages = arrImage.count
        Cell.btnCounter.setTitle("\(arrImage.count)", for: .normal)
        Cell.btnCounter.isHidden = true
        Cell.imgBox.isHidden = true
        return Cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        /*let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ObjVC = storyboard.instantiateViewController(withIdentifier: "FullScreenImageVC") as! FullScreenImageVC
        ObjVC.arrSlide = arrImage
      
        
        ObjVC.modalPresentationStyle = .overCurrentContext
        present(ObjVC, animated: false, completion: nil)*/
        
        var arryModel = [Model]()
        
        for element in arrImage {
            //imageLabel.image = element
            arryModel.append(Model(image: element, title: ""))
        }
        
        
        let fullScreenController = FullScreenSlideshowViewController()
        
        fullScreenController.closeButton.layer.cornerRadius = fullScreenController.closeButton.bounds.height/2
        fullScreenController.closeButton.layer.borderWidth = 1
        fullScreenController.closeButton.layer.borderColor = UIColor.darkGray.cgColor
        fullScreenController.closeButton.backgroundColor = UIColor.lightGray
        fullScreenController.closeButton.layer.masksToBounds = true
        
        if appDelegateObj.IsThemeLight == true{
            fullScreenController.backgroundColor = wviewBgDefaultThemeColor
        }
        else{
            fullScreenController.backgroundColor = TabViewColor
        }
        
        fullScreenController.inputs = arryModel.map { $0.inputSource }
        
        
        fullScreenController.initialPage = indexPath.row
        
        
        if let imageView = (collectionView.cellForItem(at: indexPath) as! cellAssetDetail).imgNews {
            slideshowTransitioningDelegate = ZoomAnimatedTransitioningDelegate(imageView: imageView, slideshowController: fullScreenController)
            fullScreenController.transitioningDelegate = slideshowTransitioningDelegate
        }
        
        fullScreenController.slideshow.currentPageChanged = { [weak self] page in
            if let imageView = (collectionView.cellForItem(at: indexPath) as! cellAssetDetail).imgNews {
                self?.slideshowTransitioningDelegate?.referenceImageView = imageView
            }
        }
        
        present(fullScreenController, animated: true, completion: nil)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellAssetDetail", for: indexPath) as! cellAssetDetail
        Cell.pageControl.currentPage = indexPath.row
    }
    
    //MARK:- UITableView Methods
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayRowTitle.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let tablecell = tableView.dequeueReusableCell(withIdentifier: "cellNewSection", for: indexPath) as! cellNewSection
        tablecell.txtTitle.text = arrayRowTitle[indexPath.row].cm_field_title
        tablecell.txtTitle.textColor = UIColor.white
        
        tablecell.txtTitle.attributedPlaceholder = NSAttributedString(string: (arrayRowTitle[indexPath.row].cm_field_title),
                                                                      attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        
        tablecell.txtDetail.text = arrayRowTitle[indexPath.row].value
        
        tablecell.txtTitle.font = UIFont(name: DINPROBold, size: CGFloat(14 + fontsizeDetail))
        tablecell.txtDetail.font = UIFont(name: MEDIUEM_FONT, size: CGFloat(14 + fontsizeDetail))
        
        tablecell.txtTitle.isEnabled = false
        tablecell.txtDetail.isEditable = false
        
        if appDelegateObj.IsThemeLight{
            tablecell.viewBack.layer.borderColor = wBlueMainTitleColor.cgColor
            tablecell.viewBack.backgroundColor = UIColor.white
            tablecell.txtTitle.textColor = wBlueMainTitleColor
            tablecell.txtDetail.textColor = wDarkGrayColor
        }else{
            tablecell.viewBack.layer.borderColor = kCellBackColor.cgColor
            tablecell.viewBack.backgroundColor = kCellBackColor
            tablecell.txtTitle.textColor = UIColor.white
            tablecell.txtDetail.textColor = UIColor.white
        }
        

        
        if arrayRowTitle[indexPath.row].cm_field_title == ""{
            tablecell.isHidden = true
        }
    
        
        return tablecell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if IsGroupDis == true{
            if indexPath.row == 0{
                
                
                if arrayRowTitle[indexPath.row].cm_field_title == ""{
                    return 0
                }else{
                    return UITableViewAutomaticDimension
                }
                
            }
            else{
                return UITableViewAutomaticDimension
            }
            
            
        }
        else{
            if indexPath.row == 0{
                
                
                if arrayRowTitle[indexPath.row].cm_field_title == ""{
                    return 0
                }else{
                    return UITableViewAutomaticDimension
                }
                
            }
            else{
                return UITableViewAutomaticDimension
            }
            
            
        }
        
    }
    
}

extension NewSectionVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let approximateHeight = collectionView.frame.size.height
        
        let numberOfCell: CGFloat = 1   //you need to give a type as CGFloat
        let cellWidth = collectionView.frame.size.width / numberOfCell
        //        return CGSizeMake(cellWidth, cellWidth)
        return CGSize(width: cellWidth, height: approximateHeight)
        
    }
    
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    //        return 0
    //    }
    //
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    //        return 0
    //    }
    //
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    //        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    //    }
    
}
