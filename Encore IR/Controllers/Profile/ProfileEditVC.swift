//
//  ProfileEditVC.swift
//  Encore IR
//
//  Created by ravi on 09/05/19.
//  Copyright © 2019 ravi. All rights reserved.
//

import UIKit

class ProfileEditVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {

    @IBOutlet weak var viewBackground: UIImageView!
    
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var viewTop: UIView!
    
    @IBOutlet weak var lblTopTitle: UILabel!
    
    @IBOutlet weak var imgOk: UIImageView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var imgBorder: UIImageView!
    @IBOutlet weak var btnOk: UIButton!
    
    @IBOutlet weak var viewProfile: UIView!
    
  
    @IBOutlet weak var btnCamera: UIButton!
    
    @IBOutlet weak var viewProfileDetail: UIView!
    
    
    @IBOutlet weak var txtName: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtContactNumber: SkyFloatingLabelTextField!
    
    @IBOutlet weak var viewProfileBG: UIView!
    
    @IBOutlet weak var btnImageEdit: UIButton!
    
    @IBOutlet weak var txtLastName: SkyFloatingLabelTextField!
    
    
    //MARK:- UIView Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupInitialView()
        
    }
    

    
    override func viewWillLayoutSubviews() {
        
        
        imgBorder.layer.borderWidth = 2
        imgBorder.layer.masksToBounds = false
        
        if appDelegateObj.IsThemeLight == true{
            imgBorder.layer.borderColor = wBoxBorderColor.cgColor
        }
        else{
            imgBorder.layer.borderColor = UIColor.white.cgColor
        }
        
       
        imgBorder.layer.cornerRadius = imgBorder.frame.height/2
        imgBorder.clipsToBounds = true
        
        btnImageEdit.layer.cornerRadius = btnImageEdit.bounds.size.height / 2
        btnImageEdit.clipsToBounds = true

    }

    //MARK:- Class Methods and Functions
    func setupInitialView(){
        
//        self.viewProfileBG.layer.cornerRadius = 5
//        self.viewProfileBG.layer.masksToBounds = true
        

        
        txtName.placeholder = strFirstName
        txtName.title = strFirstName
        txtName.text = "\(appDelegateObj.loginUser.u_fname)"
      //  txtName.font = UIFont(name: Rubic, size: 15)
        txtName.delegate = self
        
        txtLastName.placeholder = strLastName
        txtLastName.title = strLastName
        txtLastName.text = "\(appDelegateObj.loginUser.u_lname)"
      //  txtLastName.font = UIFont(name: Rubic, size: 15)
        txtLastName.delegate = self
        

        
        txtContactNumber.placeholder = strContactNumber
        txtContactNumber.title = strContactNumber
        txtContactNumber.text = appDelegateObj.loginUser.u_contact_1
      //  txtContactNumber.font = UIFont(name: Rubic, size: 15)
        txtContactNumber.delegate = self
        
        
        let imgPath = appDelegateObj.loginUser.u_pro_img
        print(imgPath)
        if appDelegateObj.loginUser.u_profile_set == "0"{
            imgBorder?.setImage(string: "\(appDelegateObj.loginUser.u_fname) \(appDelegateObj.loginUser.u_lname)", color: UIColor.white, circular: true, textAttributes: [NSAttributedStringKey.font: UIFont(name:MEDIUEM_FONT, size: 50.0)!, NSAttributedStringKey.foregroundColor: UIColor.black])
        }
        else{
            imgBorder.sd_setImage(with: URL(string: imgPath), placeholderImage: UIImage(named: "userDefault"))
        }
        
        self.viewProfileBG.layer.cornerRadius = 5
        self.viewProfileBG.layer.masksToBounds = true
        
        viewProfileBG.layer.borderWidth = 1
        if appDelegateObj.IsThemeLight == true{
            viewProfileBG.layer.borderColor = wBoxBorderColor.cgColor
            self.viewProfileBG.backgroundColor = wviewBgDefaultThemeColor
        }
        else{
            viewProfileBG.layer.borderColor = kCellBackColor.cgColor
            self.viewProfileBG.backgroundColor = kCellBackColor
        }
        viewProfileBG.clipsToBounds = true
        
        
        if appDelegateObj.IsThemeLight == true{
            print("Ligth Theme mode")
            viewBackground.image = UIImage(named: "Wht_background")
            self.view.backgroundColor = wviewBgDefaultThemeColor
            lblTopTitle.textColor = wBlueMainTitleColor
            
            imgBack.image = UIImage(named: "Wht_Back")
            
            imgOk.image = UIImage(named: "okBlue")
            
           
            txtName.textColor = wDarkGrayColor
            txtName.titleColor = wLightGrayColor
            txtName.selectedTitleColor = wDarkGrayColor
            txtName.selectedLineColor = wLightGrayColor
            txtName.lineColor = wLightGrayColor
            txtName.tintColor = wDarkGrayColor
            
            txtLastName.titleColor = wLightGrayColor
            txtLastName.selectedTitleColor = wDarkGrayColor
            txtLastName.selectedLineColor = wLightGrayColor
            txtLastName.lineColor = wLightGrayColor
            txtLastName.tintColor = wDarkGrayColor
            txtLastName.textColor = wDarkGrayColor
            
            txtContactNumber.titleColor = wLightGrayColor
            txtContactNumber.selectedTitleColor = wDarkGrayColor
            txtContactNumber.selectedLineColor = wLightGrayColor
            txtContactNumber.lineColor = wLightGrayColor
            txtContactNumber.textColor = wDarkGrayColor
            txtContactNumber.tintColor = wDarkGrayColor
            
            btnCamera.setImage(UIImage.init(named: "cameraBlue"), for: .normal)
            
        }
        else{
            print("Dark Theme mode")
            self.view.backgroundColor = TabViewColor
            viewBackground.image = UIImage(named: "background")
            lblTopTitle.textColor = TopTitleFontColor
            imgBack.image = UIImage(named: "Back")
            imgOk.image = UIImage(named: "ok")
            
            btnCamera.setImage(UIImage.init(named: "camera"), for: .normal)
            txtName.textColor = UIColor.white
            txtName.titleColor = InvestTextColor
            txtName.selectedTitleColor = UIColor.white
            txtName.selectedLineColor = feedbackSepColor
            txtName.lineColor = feedbackSepColor
            txtName.tintColor = UIColor.white
            
            txtLastName.titleColor = InvestTextColor
            txtLastName.selectedTitleColor = UIColor.white
            txtLastName.selectedLineColor = feedbackSepColor
            txtLastName.lineColor = feedbackSepColor
            txtLastName.tintColor = UIColor.white
            txtLastName.textColor = UIColor.white
            
            txtContactNumber.titleColor = InvestTextColor
            txtContactNumber.selectedTitleColor = UIColor.white
            txtContactNumber.selectedLineColor = feedbackSepColor
            txtContactNumber.lineColor = feedbackSepColor
            txtContactNumber.textColor = UIColor.white
            txtContactNumber.tintColor = UIColor.white
            
        }
        
        self.setupFontSize()
    }
    

    func setupFontSize(){
        lblTopTitle.font = lblTopTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 20.0))
        txtName.font = txtName.font!.withSize(GlobalMethods.setThemeFontSize(objectSize: 15.0))
        txtContactNumber.font = txtContactNumber.font!.withSize(GlobalMethods.setThemeFontSize(objectSize: 15.0))
        txtLastName.font = txtLastName.font!.withSize(GlobalMethods.setThemeFontSize(objectSize: 15.0))
      
    
    }
    //MARK:- IBActions
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnOkPressed(_ sender: Any) {
        
        if (txtName.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: msgUserName, viewcontrolelr: self)
        }
        else if (txtLastName.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: msgLastName, viewcontrolelr: self)
        }
        else if (txtContactNumber.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: msgUserContact, viewcontrolelr: self)
        }
        else if (txtContactNumber.text?.count)! < intMinContactLenght{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: msgUserValidContact, viewcontrolelr: self)
        }
        else{
            self.updateProfileDetail()
        }
        
    }
    
    @IBAction func btnCameraPressed(_ sender: Any) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        
        alertController.addAction(UIAlertAction(title: strCamera, style: .default, handler: { (_) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePicker.sourceType = .camera
                self.present(imagePicker, animated: true, completion: nil)
            }
        }))
        
        alertController.addAction(UIAlertAction(title: strPhoto_Video_Library, style: .default, handler: { (_) in
            imagePicker.sourceType = .photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }))
        
        alertController.addAction(UIAlertAction(title: strRemovePhoto, style: .destructive, handler: { (_) in
//            alertController.dismiss(animated: true, completion: nil)
            self.removeProfileImage()
        }))

        
        alertController.addAction(UIAlertAction(title: strCancel, style: .destructive, handler: { (_) in
            alertController.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
    @IBAction func btnImageEditPressed(_ sender: Any) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        
        alertController.addAction(UIAlertAction(title: strCamera, style: .default, handler: { (_) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePicker.sourceType = .camera
                self.present(imagePicker, animated: true, completion: nil)
            }
        }))
        
        alertController.addAction(UIAlertAction(title: strPhoto_Video_Library, style: .default, handler: { (_) in
            imagePicker.sourceType = .photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }))
        
        alertController.addAction(UIAlertAction(title: strRemovePhoto, style: .default, handler: { (_) in
            //            alertController.dismiss(animated: true, completion: nil)
            self.removeProfileImage()
        }))
        
        
        alertController.addAction(UIAlertAction(title: strCancel, style: .default, handler: { (_) in
            alertController.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    //MARK:- UIImagePicker Delegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            
            // profileImage = image
            
//            self.imgUserProfile.layer.cornerRadius = self.imgUserProfile.bounds.height/2
//            self.imgUserProfile.layer.masksToBounds = true
            self.imgBorder.image = image
            // self.imgProfile.image = image
            
            self.updateUserProfile()
            
            picker.dismiss(animated: true, completion: nil)
            
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }

    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) {
            return true
        }
        
        if textField == txtContactNumber{
            
            if (txtContactNumber.text?.isNumber)!{
                
                if (txtContactNumber.text?.count)! > intMaxContactLenght{
                    return false
                }
                
                return string.isNumber
            }
            
        }
        
        
        if textField == txtName{
            if (txtName.text?.count)! > intMAxNameLenght{
                return false
            }
        }
        
        if textField == txtLastName{
            if (txtLastName.text?.count)! > intMAxNameLenght{
                return false
            }
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
