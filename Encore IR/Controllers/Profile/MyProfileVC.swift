//
//  MyProfileVC.swift
//  Encore IR
//
//  Created by ravi on 08/05/19.
//  Copyright © 2019 ravi. All rights reserved.
//

import UIKit
import LGSideMenuController
import SDWebImage
import UserNotifications





class MyProfileVC: UIViewController,  UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    
    @IBOutlet weak var objScrollview: UIScrollView!
    @IBOutlet weak var objContentView: UIView!
    
    @IBOutlet weak var viewProfile: UIView!
    @IBOutlet weak var imgProfileCover: UIImageView!
//    @IBOutlet weak var imgProfileUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserEmailId: UILabel!
    @IBOutlet weak var lblUserContact: UILabel!
    
    
    @IBOutlet weak var viewInvestSummary: UIView!
    @IBOutlet weak var lblInvestSumTitle: UILabel!
    @IBOutlet weak var lblMyAssetsTitle: UILabel!
    @IBOutlet weak var lblAmountTitle: UILabel!
    @IBOutlet weak var lblFundedTitle: UILabel!
    @IBOutlet weak var tblSummaryObj: UITableView!
    @IBOutlet weak var viewInvestSum: UIView!
  
    @IBOutlet weak var viewNewsFooter: UIView!
    @IBOutlet weak var imgNews: UIImageView!
    @IBOutlet weak var btnNews: UIButton!
    
    @IBOutlet weak var viewMyAssetFooter: UIView!
    @IBOutlet weak var imgAssetFooter: UIImageView!
    @IBOutlet weak var btnAssets: UIButton!
    
    @IBOutlet weak var viewNewDealFooter: UIView!
    @IBOutlet weak var imgDealFooter: UIImageView!
    @IBOutlet weak var btnDeal: UIButton!
    
    @IBOutlet weak var viewLibraryFooter: UIView!
    @IBOutlet weak var imgLibraryFooter: UIImageView!
    @IBOutlet weak var btnLibrary: UIButton!
   
   
    @IBOutlet weak var imgMenu: UIImageView!
    
    @IBOutlet weak var imgEdit: UIImageView!

    
    @IBOutlet weak var lblCounterNews: UILabel!
    @IBOutlet weak var lblCounterAssets: UILabel!
    @IBOutlet weak var lblCounterDeals: UILabel!
    @IBOutlet weak var lblCounterLibrary: UILabel!
    
    @IBOutlet weak var imgViewBackground: UIImageView!
    @IBOutlet weak var imgFooterBG: UIImageView!

    //MARK:- Variable
    var profileDetail = LoginUserData()
    var arrAmountData = [LoginAmountData]()

 
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupInitialView()
        self.tabBarController?.moreNavigationController.isNavigationBarHidden = true
        

        NotificationCenter.default.addObserver(self, selector: #selector(self.updateDetail), name: NSNotification.Name(rawValue: kGetProfileUpdate), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.setupInitialView), name: NSNotification.Name(rawValue: kProfileEdit), object: nil)
        
         tblSummaryObj.tableFooterView = UIView()

    }
    override func viewWillAppear(_ animated: Bool) {
        self.getProfileDetail()
        
    }
    
    override func viewWillLayoutSubviews() {
       
        imgProfileCover.layer.borderWidth = 2
        imgProfileCover.layer.masksToBounds = false
        if appDelegateObj.IsThemeLight == true{
            imgProfileCover.layer.borderColor = wBoxBorderColor.cgColor
        }
        else{
            imgProfileCover.layer.borderColor = UIColor.white.cgColor
        }
        imgProfileCover.layer.cornerRadius = imgProfileCover.frame.height/2
        imgProfileCover.clipsToBounds = true
        
        
        //New Notification Counter
        if appDelegateObj.ObjCounter.new_news != "0" {
            lblCounterNews.isHidden = false
            lblCounterNews.text = appDelegateObj.ObjCounter.new_news

        }else{
            lblCounterNews.isHidden = true
        }
        lblCounterNews.textColor = UIColor.black
        lblCounterNews.backgroundColor = counterBgColor
        lblCounterNews.layer.cornerRadius = lblCounterNews.frame.width/2
        lblCounterNews.layer.masksToBounds = true
        
        
        if appDelegateObj.ObjCounter.new_asset != "0" {
            lblCounterAssets.isHidden = false
            lblCounterAssets.text = appDelegateObj.ObjCounter.new_asset
            
        }else{
            lblCounterAssets.isHidden = true
        }
        lblCounterAssets.text = appDelegateObj.ObjCounter.new_asset
        lblCounterAssets.textColor = UIColor.black
        lblCounterAssets.backgroundColor = counterBgColor
        lblCounterAssets.layer.cornerRadius = lblCounterAssets.frame.width/2
        lblCounterAssets.layer.masksToBounds = true
        
        if appDelegateObj.ObjCounter.new_deal != "0" {
            lblCounterDeals.isHidden = false
            lblCounterDeals.text = appDelegateObj.ObjCounter.new_deal
            
        }else{
            lblCounterDeals.isHidden = true
        }
        lblCounterDeals.text = appDelegateObj.ObjCounter.new_deal
        lblCounterDeals.textColor = UIColor.black
        lblCounterDeals.backgroundColor = counterBgColor
        lblCounterDeals.layer.cornerRadius = lblCounterDeals.frame.width/2
        lblCounterDeals.layer.masksToBounds = true
        
        if appDelegateObj.ObjCounter.new_docs != "0" {
            lblCounterLibrary.isHidden = false
            lblCounterLibrary.text = appDelegateObj.ObjCounter.new_docs
            
        }else{
            lblCounterLibrary.isHidden = true
        }
        lblCounterLibrary.text = appDelegateObj.ObjCounter.new_docs
        lblCounterLibrary.textColor = UIColor.black
        lblCounterLibrary.backgroundColor = counterBgColor
        lblCounterLibrary.layer.cornerRadius = lblCounterLibrary.frame.width/2
        lblCounterLibrary.layer.masksToBounds = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        lblTopTitle.text = strProfile
        
        if appDelegateObj.IsThemeLight == true{
            print("Ligth Theme mode")
            imgViewBackground.image = UIImage(named: "Wht_background")
            self.view.backgroundColor = wviewBgDefaultThemeColor
            lblTopTitle.textColor = wBlueMainTitleColor
          //  lblTopTitle.font = UIFont(name: wRubic, size: 20)
            imgMenu.image = UIImage(named: "menuBlue")
            imgFooterBG.image = UIImage(named: "Wht_FooterBG")
            imgNews.image = UIImage(named: "Wht_News")
            imgAssetFooter.image = UIImage(named: "Wht_Asset")
            imgDealFooter.image = UIImage(named: "Wht_Deal")
            imgLibraryFooter.image = UIImage(named: "Wht_Library")
       
            imgEdit.image = UIImage(named: "Wht_edit")
            
            lblUserName.textColor = wBlueMainTitleColor
            lblUserEmailId.textColor = wDarkGrayColor
            lblUserContact.textColor = wDarkGrayColor
            
            self.viewInvestSum.layer.cornerRadius = 5
            self.viewInvestSum.layer.masksToBounds = true
            self.viewInvestSum.backgroundColor = wviewBgDefaultThemeColor
            viewInvestSum.layer.borderWidth = 1
            if appDelegateObj.IsThemeLight == true{
                viewInvestSum.layer.borderColor = wBoxBorderColor.cgColor
            }
            else{
                viewInvestSum.layer.borderColor = UIColor.white.cgColor
            }
            viewInvestSum.clipsToBounds = true
            
            lblMyAssetsTitle.textColor = wLightGrayColor
            lblAmountTitle.textColor = wLightGrayColor
            lblFundedTitle.textColor = wLightGrayColor
            lblInvestSumTitle.textColor = wBlueMainTitleColor
    
        }
        else{
            print("Dark Theme mode")
            self.view.backgroundColor = TabViewColor
            imgViewBackground.image = UIImage(named: "background")
            lblTopTitle.textColor = TopTitleFontColor
          //  lblTopTitle.font = UIFont(name: MEDIUEM_FONT, size: 20)
            imgMenu.image = UIImage(named: "menu")
            imgFooterBG.image = UIImage(named: "footer-background")
            imgNews.image = UIImage(named: "News-icon")
            imgAssetFooter.image = UIImage(named: "My-Assets-icon")
            imgDealFooter.image = UIImage(named: "New-Deals-icon")
            imgLibraryFooter.image = UIImage(named: "Library-icon")

            imgEdit.image = UIImage(named: "edit")

            lblUserName.textColor = InvestTextColor
            lblUserEmailId.textColor = InvestTextColor
            lblUserContact.textColor = InvestTextColor
            
            self.viewInvestSum.layer.cornerRadius = 5
            self.viewInvestSum.layer.masksToBounds = true
            self.viewInvestSum.backgroundColor = kCellBackColor
            viewInvestSum.layer.borderWidth = 0.5
            if appDelegateObj.IsThemeLight == true{
                viewInvestSum.layer.borderColor = kCellBackColor.cgColor
            }
            else{
                viewInvestSum.layer.borderColor = kCellBackColor.cgColor
            }
            viewInvestSum.clipsToBounds = true
            
            lblMyAssetsTitle.textColor = InvestTextColor
            lblAmountTitle.textColor = InvestTextColor
            lblFundedTitle.textColor = InvestTextColor
            lblInvestSumTitle.textColor = wviewBgDefaultThemeColor
        }
        self.updateDetail()
        self.setupFontSize()
        tblSummaryObj.reloadData()
    }

    //MARK:- Class Methods and Functions
    @objc func setupInitialView(){
        self.sideMenuController?.isLeftViewEnabled = true
    }
    
    
    @objc func updateDetail()
    {
        lblUserName.text = "\(appDelegateObj.loginUser.u_fname) \(appDelegateObj.loginUser.u_lname)"
        lblUserEmailId.text = appDelegateObj.loginUser.u_email
        lblUserContact.text = appDelegateObj.loginUser.u_contact_1
        
        let imgPath = appDelegateObj.loginUser.u_pro_img
        if appDelegateObj.loginUser.u_profile_set == "0"{
            imgProfileCover?.setImage(string: "\(appDelegateObj.loginUser.u_fname) \(appDelegateObj.loginUser.u_lname)", color: UIColor.white, circular: true, textAttributes: [NSAttributedStringKey.font: UIFont(name:MEDIUEM_FONT, size: 50.0)!, NSAttributedStringKey.foregroundColor: UIColor.black])
        }
        else{
            imgProfileCover.sd_setImage(with: URL(string: imgPath), placeholderImage: UIImage(named: "userDefault"))
        }
        
        arrAmountData = appDelegateObj.ProAmountDetail
        tblSummaryObj.reloadData()
    }

    func setupFontSize(){
        lblTopTitle.font = lblTopTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 20.0))
        lblUserName.font = lblUserName.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 20.0))
        lblUserEmailId.font = lblUserEmailId.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 12.0))
        lblUserContact.font = lblUserContact.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 12.0))
        lblInvestSumTitle.font = lblInvestSumTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 14.0))
        lblMyAssetsTitle.font = lblMyAssetsTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 14.0))
        lblAmountTitle.font = lblAmountTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 14.0))
        lblFundedTitle.font = lblFundedTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 14.0))
    }
    
    //MARK:- IBActions
    @IBAction func btnMenuPressed(_ sender: Any) {
        showLeftViewAnimated(sender)
    }
    
    @IBAction func btnEditPressed(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ObjVC = storyboard.instantiateViewController(withIdentifier: "ProfileEditVC") as! ProfileEditVC
        self.navigationController?.pushViewController(ObjVC, animated: true)

    }
  
    
    @IBAction func btnNewsPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 0
    }
    
    @IBAction func btnAssetPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 1
    }
    
    
    @IBAction func btnDealPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 2
    }
    
    @IBAction func btnLibraryPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 3
    }
    
    //MARK:- UITableView Methods
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        var intRowCount = 0
        
        if arrAmountData.count > 0{
            intRowCount = arrAmountData.count + 1
        }
        
        return intRowCount
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let tablecell = tableView.dequeueReusableCell(withIdentifier: "investmentSummaryCell", for: indexPath) as! investmentSummaryCell
        
        
        if indexPath.row == arrAmountData.count{
            var totalAmount = 0.0
            
            for objModel in arrAmountData{
                var price = 0.0
                
                var myString = objModel.v_amount
                myString.remove(at: myString.startIndex)
                let amount  = myString.replacingOccurrences(of: ",", with: "")
                
                price = Double(amount) as! Double
                
                totalAmount = totalAmount + price
            }
            
            tablecell.lblAssets.text = "Total Amount"
         //   tablecell.lblAmount.text = String(format: "$%.2f", totalAmount)
            
            tablecell.lblAmount.text = self.getNumberFormat(strAmount: "\(totalAmount)")
            
            tablecell.lblAssets.textColor = UIColor.white
            tablecell.lblAssets.font = UIFont(name: DINPROBold, size: 13)
            tablecell.lblFunded.text = ""
        }else{
            tablecell.lblAssets.text = arrAmountData[indexPath.row].a_name
  
            var myString = arrAmountData[indexPath.row].v_amount
            myString.remove(at: myString.startIndex)
            let amount  = myString.replacingOccurrences(of: ",", with: "")
            
            tablecell.lblAmount.text = self.getNumberFormat(strAmount: amount)
            tablecell.lblFunded.text = arrAmountData[indexPath.row].dt_funded
            tablecell.lblAssets.textColor = UIColor.white
            tablecell.lblAssets.font = UIFont(name: MEDIUEM_FONT, size: 13)
            
        }
        
        return tablecell
    }
    
    
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row == arrAmountData.count{
            return
        }
      
        let objModel = arrAmountData[indexPath.row]

        
        if objModel.is_group == "0" {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ObjVC = storyboard.instantiateViewController(withIdentifier: "MyAssetsDetailVC") as! MyAssetsDetailVC
            ObjVC.strAssetTitle = arrAmountData[indexPath.row].a_name
            ObjVC.strAssetId = arrAmountData[indexPath.row].a_id
            ObjVC.IsNewDealDetail = false
            self.navigationController?.pushViewController(ObjVC, animated: true)
            
        }else{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ObjVC = storyboard.instantiateViewController(withIdentifier: "FundsDetailVC") as! FundsDetailVC
            ObjVC.strFundId = arrAmountData[indexPath.row].g_a_id
            self.navigationController?.pushViewController(ObjVC, animated: true)
                        
        }
        
    }
   
    //MARK:- Scrollview Methods

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if appDelegateObj.IsThemeLight == true{
            let verticalIndicator = scrollView.subviews.last as? UIImageView
            verticalIndicator?.image = UIImage(named: "Wht_ScrollLine")
        }
        else{
            let verticalIndicator = scrollView.subviews.last as? UIImageView
            verticalIndicator?.image = UIImage(named: "scroll-seprate-line")
        }

    }
}
