//
//  BookmarksVC.swift
//  Encore IR
//
//  Created by ravi on 08/05/19.
//  Copyright © 2019 ravi. All rights reserved.
//

import UIKit
import LGSideMenuController
import SDWebImage



class BookmarksVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    //MARK:- Outlets

    @IBOutlet weak var imgBackground: UIImageView!
    
    @IBOutlet weak var viewTitle: UIView!
    @IBOutlet weak var lblTopTitle: UILabel!
    
    @IBOutlet weak var btnMenu: UIButton!
    
    @IBOutlet weak var btnSearch: UIButton!
    
    @IBOutlet weak var viewFilter: UIView!
    
    @IBOutlet weak var viewNewsHeader: UIView!
    
    @IBOutlet weak var imgFilterNews: UIImageView!
    
    @IBOutlet weak var btnFilterNews: UIButton!
    
    @IBOutlet weak var viewAssetHeader: UIView!
    
    
    @IBOutlet weak var imgFilterAsset: UIImageView!
    
    @IBOutlet weak var btnFilterAsset: UIButton!
    
    
    @IBOutlet weak var viewDealHeader: UIView!
    
    @IBOutlet weak var imgFilterDeal: UIImageView!
    
    @IBOutlet weak var btnFilterDeal: UIButton!
    
    
    @IBOutlet weak var tblBookmarkObj: UITableView!
    
    
    @IBOutlet weak var viewFooter: UIView!
    
    @IBOutlet weak var imgFooterBg: UIImageView!
    
    
    @IBOutlet weak var viewNewsFooter: UIView!
    @IBOutlet weak var imgNews: UIImageView!
    @IBOutlet weak var btnNews: UIButton!
    
    @IBOutlet weak var viewMyAssetFooter: UIView!
    @IBOutlet weak var imgAssetFooter: UIImageView!
    @IBOutlet weak var btnAssets: UIButton!
    
    @IBOutlet weak var viewNewDealFooter: UIView!
    @IBOutlet weak var imgDealFooter: UIImageView!
    @IBOutlet weak var btnDeal: UIButton!
    
    @IBOutlet weak var viewLibraryFooter: UIView!
    @IBOutlet weak var imgLibraryFooter: UIImageView!
    @IBOutlet weak var btnLibrary: UIButton!

    
 
    
    @IBOutlet weak var imgTitleMenu: UIImageView!
    
    @IBOutlet weak var imgTitleSearch: UIImageView!
    
   
    
    @IBOutlet weak var lblNoDataFound: UILabel!
    
    @IBOutlet weak var lblCounterNews: UILabel!
    @IBOutlet weak var lblCounterAssets: UILabel!
    @IBOutlet weak var lblCounterDeals: UILabel!
    @IBOutlet weak var lblCounterLibrary: UILabel!
    
    @IBOutlet weak var viewSearchbar: UISearchBar!
    
    //MARK:- Variable

    
    var arrayNewsBookmark = [Subnews_detail]()
    var arrayMyAssetBookmark = [AssetProject]()
    var arrayNewDealsBookmark = [AssetProject]()
    
    var arrayNewsBookmarkCopy = [Subnews_detail]()
    var arrayMyAssetBookmarkCopy = [AssetProject]()
    var arrayNewDealsBookmarkCopy = [AssetProject]()

    
    var isSelectedTab = 0
    var objRefreshControl = UIRefreshControl()

    var textFieldInsideSearchBar = UITextField()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tabBarController?.moreNavigationController.isNavigationBarHidden = true
        self.setupInitialView()
        self.getNewsBookmarkFeed()
        
        lblNoDataFound.text = msgNewsNotBookmark
        lblNoDataFound.isHidden = true
        
        
        isSelectedTab = 0
        
        objRefreshControl.attributedTitle = NSAttributedString(string: "")
        objRefreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        objRefreshControl.tintColor = UIColor.clear
        tblBookmarkObj.addSubview(objRefreshControl)

    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        btnFilterNews.setRoundedCorners(ratio: 0.50)
 
        //btnFilterNews.titleLabel?.font = UIFont(name:DINPROBold, size: 12.0)
        btnFilterNews.titleLabel?.text = strNews

        btnFilterNews.titleLabel?.adjustsFontSizeToFitWidth = true
        
        
        btnFilterAsset.setRoundedCorners(ratio: 0.50)
        
       // btnFilterAsset.titleLabel?.font = UIFont(name:DINPROBold, size: 12.0)
        btnFilterAsset.titleLabel?.text = strMyAssets

        btnFilterAsset.titleLabel?.adjustsFontSizeToFitWidth = true
        
        
        btnFilterDeal.setRoundedCorners(ratio: 0.50)
        
        //dark-background   light-background
       // btnFilterDeal.titleLabel?.font = UIFont(name:DINPROBold, size: 12.0)
        btnFilterDeal.titleLabel?.text = strNews

        btnFilterDeal.titleLabel?.adjustsFontSizeToFitWidth = true

        //New Notification Counter
        if appDelegateObj.ObjCounter.new_news != "0" {
            lblCounterNews.isHidden = false
            lblCounterNews.text = appDelegateObj.ObjCounter.new_news
            
        }else{
            lblCounterNews.isHidden = true
        }
       
        lblCounterNews.layer.cornerRadius = lblCounterNews.frame.width/2
        lblCounterNews.layer.masksToBounds = true
        
        
        if appDelegateObj.ObjCounter.new_asset != "0" {
            lblCounterAssets.isHidden = false
            lblCounterAssets.text = appDelegateObj.ObjCounter.new_asset
            
        }else{
            lblCounterAssets.isHidden = true
        }
        lblCounterAssets.text = appDelegateObj.ObjCounter.new_asset
        
        lblCounterAssets.layer.cornerRadius = lblCounterAssets.frame.width/2
        lblCounterAssets.layer.masksToBounds = true
        
        if appDelegateObj.ObjCounter.new_deal != "0" {
            lblCounterDeals.isHidden = false
            lblCounterDeals.text = appDelegateObj.ObjCounter.new_deal
            
        }else{
            lblCounterDeals.isHidden = true
        }
        lblCounterDeals.text = appDelegateObj.ObjCounter.new_deal
       
        lblCounterDeals.layer.cornerRadius = lblCounterDeals.frame.width/2
        lblCounterDeals.layer.masksToBounds = true
        
        if appDelegateObj.ObjCounter.new_docs != "0" {
            lblCounterLibrary.isHidden = false
            lblCounterLibrary.text = appDelegateObj.ObjCounter.new_docs
            
        }else{
            lblCounterLibrary.isHidden = true
        }
        lblCounterLibrary.text = appDelegateObj.ObjCounter.new_docs
        
        lblCounterLibrary.layer.cornerRadius = lblCounterLibrary.frame.width/2
        lblCounterLibrary.layer.masksToBounds = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupTheme()
        tblBookmarkObj.reloadData()
    }
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        
        viewSearchbar.isHidden = true
        tblBookmarkObj.tableFooterView = UIView()
        
        textFieldInsideSearchBar = viewSearchbar.value(forKey: "searchField") as! UITextField
        textFieldInsideSearchBar.font = UIFont(name: D_DIN, size: 15)
    }
    
    func setupTheme(){
      
        if appDelegateObj.IsThemeLight == true{
            print("Ligth Theme mode")
            
            
            imgBackground.image = UIImage(named: "Wht_background")
            self.view.backgroundColor = wviewBgDefaultThemeColor
            imgTitleSearch.image = UIImage(named: "searchBlue")
            lblTopTitle.textColor = wBlueMainTitleColor
            
            lblCounterNews.textColor = UIColor.black
            lblCounterNews.backgroundColor = counterBgColor
            
            lblCounterAssets.textColor = UIColor.black
            lblCounterAssets.backgroundColor = counterBgColor
            
            lblCounterDeals.textColor = UIColor.black
            lblCounterDeals.backgroundColor = counterBgColor
            
            lblCounterLibrary.textColor = UIColor.black
            lblCounterLibrary.backgroundColor = counterBgColor
            
            btnFilterNews.borderWidth = 1
            btnFilterNews.borderColor = wBlueMainTitleColor
            
            btnFilterAsset.borderWidth = 1
            btnFilterAsset.borderColor = wBlueMainTitleColor
            
            btnFilterDeal.borderWidth = 1
            btnFilterDeal.borderColor = wBlueMainTitleColor
            
            if isSelectedTab == 0{
                btnFilterNews.backgroundColor = mainBlueThemeColor
                btnFilterAsset.backgroundColor = UIColor.clear
                btnFilterDeal.backgroundColor = UIColor.clear
                
                btnFilterNews.setTitleColor(UIColor.white, for: .normal)
                btnFilterAsset.setTitleColor(wDarkGrayColor, for: .normal)
                btnFilterDeal.setTitleColor(wDarkGrayColor, for: .normal)
                
            }else if isSelectedTab == 1{
                btnFilterNews.backgroundColor = UIColor.clear
                btnFilterAsset.backgroundColor = mainBlueThemeColor
                btnFilterDeal.backgroundColor = UIColor.clear
                
                btnFilterNews.setTitleColor(wDarkGrayColor, for: .normal)
                btnFilterAsset.setTitleColor(UIColor.white, for: .normal)
                btnFilterDeal.setTitleColor(wDarkGrayColor, for: .normal)
                
            }else{
                btnFilterNews.backgroundColor = UIColor.clear
                btnFilterAsset.backgroundColor = UIColor.clear
                btnFilterDeal.backgroundColor = mainBlueThemeColor
                
                btnFilterNews.setTitleColor(wDarkGrayColor, for: .normal)
                btnFilterAsset.setTitleColor(wDarkGrayColor, for: .normal)
                btnFilterDeal.setTitleColor(UIColor.white, for: .normal)
            }
            
            imgTitleMenu.image = UIImage(named: "menuBlue")
            imgFooterBg.image = UIImage(named: "Wht_FooterBG")
            imgNews.image = UIImage(named: "Wht_News")
            imgAssetFooter.image = UIImage(named: "Wht_Asset")
            imgDealFooter.image = UIImage(named: "Wht_Deal")
            imgLibraryFooter.image = UIImage(named: "Wht_Library")
            
            lblNoDataFound.textColor = UIColor.black
            
            textFieldInsideSearchBar.textColor = wBlueMainTitleColor
            
        }
        else{
            print("Dark Theme mode")
            
            self.view.backgroundColor = TabViewColor
            imgBackground.image = UIImage(named: "background")
            imgTitleSearch.image = UIImage(named: "Search-1")
            lblTopTitle.textColor = TopTitleFontColor
            
            btnFilterNews.borderWidth = 1
            btnFilterNews.borderColor = FilterFillColor
            
            lblCounterNews.textColor = UIColor.black
            lblCounterNews.backgroundColor = counterBgColor
            
            lblCounterAssets.textColor = UIColor.black
            lblCounterAssets.backgroundColor = counterBgColor
            
            lblCounterDeals.textColor = UIColor.black
            lblCounterDeals.backgroundColor = counterBgColor
            
            lblCounterLibrary.textColor = UIColor.black
            lblCounterLibrary.backgroundColor = counterBgColor
            
            btnFilterNews.borderWidth = 1
            btnFilterNews.borderColor = FilterFillColor
            
            btnFilterAsset.borderWidth = 1
            btnFilterAsset.borderColor = FilterFillColor
            
            btnFilterDeal.borderWidth = 1
            btnFilterDeal.borderColor = FilterFillColor
            
            btnFilterNews.setTitleColor(UIColor.white, for: .normal)
            btnFilterAsset.setTitleColor(UIColor.white, for: .normal)
            btnFilterDeal.setTitleColor(UIColor.white, for: .normal)
            
            if isSelectedTab == 0{
                btnFilterNews.backgroundColor = FilterFillColor
                btnFilterAsset.backgroundColor = UIColor.clear
                btnFilterDeal.backgroundColor = UIColor.clear
            
            }else if isSelectedTab == 1{
                btnFilterNews.backgroundColor = UIColor.clear
                btnFilterAsset.backgroundColor = FilterFillColor
                btnFilterDeal.backgroundColor = UIColor.clear
            
            }else{
                btnFilterNews.backgroundColor = UIColor.clear
                btnFilterAsset.backgroundColor = UIColor.clear
                btnFilterDeal.backgroundColor = FilterFillColor
            }
            
            
            imgTitleMenu.image = UIImage(named: "menu")
            imgFooterBg.image = UIImage(named: "footer-background")
            imgNews.image = UIImage(named: "News-icon")
            imgAssetFooter.image = UIImage(named: "My-Assets-icon")
            imgDealFooter.image = UIImage(named: "New-Deals-icon")
            imgLibraryFooter.image = UIImage(named: "Library-icon")
            
            lblNoDataFound.textColor = UIColor.white
            
            textFieldInsideSearchBar.textColor = UIColor.white
            
        }
        
        self.setupFontSize()
    }
    
    func setupFontSize(){
        lblTopTitle.font = lblTopTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 20.0))
        lblNoDataFound.font = lblNoDataFound.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 15.0))
        
        btnFilterNews.titleLabel?.font = UIFont(name:DINPROBold, size: GlobalMethods.setThemeFontSize(objectSize: 12.0))
        btnFilterAsset.titleLabel?.font = UIFont(name:DINPROBold, size: GlobalMethods.setThemeFontSize(objectSize: 12.0))
        btnFilterDeal.titleLabel?.font = UIFont(name:DINPROBold, size: GlobalMethods.setThemeFontSize(objectSize: 12.0))
    }
    
    @objc func refresh(_ sender: Any) {
        //  your code to refresh tableView
        print("Refresh start")
        
        if isSelectedTab == 0{
            self.getNewsBookmarkFeed()
        }else if isSelectedTab == 1{
            self.getMyAssestBookmarkFeed()
        }
        else{
            self.getNewDealsBookmarkFeed()
        }
        
        objRefreshControl.endRefreshing()
        
    }
    
    
    
    //MARK:- UISearchbar Methods
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        self.closeSearchView()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        viewSearchbar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.filterTextArray(withString: searchText)
    }
    
    func filterTextArray(withString searchText: String) {
      
        if isSelectedTab == 0{
           
                var result = [Subnews_detail]()
                
                result = arrayNewsBookmarkCopy.filter{ $0.n_headline.range(of: searchText, options: [.caseInsensitive]) != nil
                    || $0.n_p_dec.range(of: searchText, options: [.caseInsensitive]) != nil }
                
                print(result.count)
                arrayNewsBookmark = result
                tblBookmarkObj.reloadData()
     
        }else if isSelectedTab == 1{
                var result = [AssetProject]()
                result = arrayMyAssetBookmarkCopy.filter{ $0.a_name.range(of: searchText, options: [.caseInsensitive]) != nil
                    || $0.p_type_name.range(of: searchText, options: [.caseInsensitive]) != nil || $0.group_name.range(of: searchText, options: [.caseInsensitive]) != nil}
                
                print(result.count)
                arrayMyAssetBookmark = result
                tblBookmarkObj.reloadData()
  
        }
        else{
                var result = [AssetProject]()
                
                result = arrayNewDealsBookmarkCopy.filter{ $0.a_name.range(of: searchText, options: [.caseInsensitive]) != nil
                    || $0.p_type_name.range(of: searchText, options: [.caseInsensitive]) != nil }
                
                print(result.count)
                arrayNewDealsBookmark = result
                tblBookmarkObj.reloadData()

        }
        
    }
    func closeSearchView(){
        viewSearchbar.endEditing(true)
        lblTopTitle.isHidden = false
        imgTitleSearch.isHidden = false
        viewSearchbar.isHidden = true
      
        if isSelectedTab == 0{
            arrayNewsBookmark = arrayNewsBookmarkCopy
        }else if isSelectedTab == 1{
            arrayMyAssetBookmark = arrayMyAssetBookmarkCopy
        }
        else{
            arrayNewDealsBookmark = arrayNewDealsBookmarkCopy
        }
    
        tblBookmarkObj.reloadData()
       
    }

    //MARK:- IBActions
    @IBAction func btnMenuPessed(_ sender: Any) {
        showLeftViewAnimated(sender)
    }
    
    @IBAction func btnSearchPressed(_ sender: Any) {
        
        viewSearchbar.isHidden = false
        
        imgTitleSearch.isHidden = true
        
        lblTopTitle.isHidden = true
        
        if isSelectedTab == 0{
            arrayNewsBookmarkCopy = arrayNewsBookmark
        }else if isSelectedTab == 1{
            arrayMyAssetBookmarkCopy = arrayMyAssetBookmark
        }
        else{
            arrayNewDealsBookmarkCopy = arrayNewDealsBookmark
        }

    }
    
    
    @IBAction func btnFilterNewsPressed(_ sender: Any) {
        
        if arrayNewsBookmark.count == 0{
            self.getNewsBookmarkFeed()
           
        }
        else{
            lblNoDataFound.isHidden = true
        }
            if isSelectedTab != 0{
                isSelectedTab = 0
                
                
                if appDelegateObj.IsThemeLight{
                    btnFilterNews.backgroundColor = mainBlueThemeColor
                    btnFilterAsset.backgroundColor = UIColor.clear
                    btnFilterDeal.backgroundColor = UIColor.clear
                    
                    btnFilterNews.setTitleColor(UIColor.white, for: .normal)
                    btnFilterAsset.setTitleColor(wDarkGrayColor, for: .normal)
                    btnFilterDeal.setTitleColor(wDarkGrayColor, for: .normal)
                }
                else{
                    btnFilterNews.backgroundColor = FilterFillColor
                    btnFilterAsset.backgroundColor = UIColor.clear
                    btnFilterDeal.backgroundColor = UIColor.clear
                    
                    btnFilterNews.setTitleColor(UIColor.white, for: .normal)
                    btnFilterAsset.setTitleColor(UIColor.white, for: .normal)
                    btnFilterDeal.setTitleColor(UIColor.white, for: .normal)
                }
                tblBookmarkObj.reloadData()
            }
    }
    
    @IBAction func btnFilterAssetPressed(_ sender: Any) {
        
        if arrayMyAssetBookmark.count == 0{
            self.getMyAssestBookmarkFeed()
        }
        else{
            lblNoDataFound.isHidden = true
        }
        
            if isSelectedTab != 1{
                isSelectedTab = 1
                
                if appDelegateObj.IsThemeLight{
                    btnFilterNews.backgroundColor = UIColor.clear
                    btnFilterAsset.backgroundColor = mainBlueThemeColor
                    btnFilterDeal.backgroundColor = UIColor.clear
                    
                    btnFilterNews.setTitleColor(wDarkGrayColor, for: .normal)
                    btnFilterAsset.setTitleColor(UIColor.white, for: .normal)
                    btnFilterDeal.setTitleColor(wDarkGrayColor, for: .normal)
                }
                else{
                    btnFilterNews.backgroundColor = UIColor.clear
                    btnFilterAsset.backgroundColor = FilterFillColor
                    btnFilterDeal.backgroundColor = UIColor.clear
                    
                    btnFilterNews.setTitleColor(UIColor.white, for: .normal)
                    btnFilterAsset.setTitleColor(UIColor.white, for: .normal)
                    btnFilterDeal.setTitleColor(UIColor.white, for: .normal)
                }
             
                tblBookmarkObj.reloadData()
            }
        
    }
    
    @IBAction func btnFilterDealPressed(_ sender: Any) {
        if arrayNewDealsBookmark.count == 0{
            self.getNewDealsBookmarkFeed()
        }
        else{
            lblNoDataFound.isHidden = true
        }
            if isSelectedTab != 2{
                isSelectedTab = 2
                
                if appDelegateObj.IsThemeLight{
                    btnFilterNews.backgroundColor = UIColor.clear
                    btnFilterAsset.backgroundColor = UIColor.clear
                    btnFilterDeal.backgroundColor = mainBlueThemeColor
                    
                    btnFilterNews.setTitleColor(wDarkGrayColor, for: .normal)
                    btnFilterAsset.setTitleColor(wDarkGrayColor, for: .normal)
                    btnFilterDeal.setTitleColor(UIColor.white, for: .normal)
                }
                else{
                    btnFilterNews.backgroundColor = UIColor.clear
                    btnFilterAsset.backgroundColor = UIColor.clear
                    btnFilterDeal.backgroundColor = FilterFillColor
                    
                    btnFilterNews.setTitleColor(UIColor.white, for: .normal)
                    btnFilterAsset.setTitleColor(UIColor.white, for: .normal)
                    btnFilterDeal.setTitleColor(UIColor.white, for: .normal)
                }
                
               
                tblBookmarkObj.reloadData()
            }
        
    }
    
    @IBAction func btnNewsPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 0
    }
    
    @IBAction func btnAssetPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 1
    }
    
    @IBAction func btnDealPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 2
    }
    
    @IBAction func btnLibraryPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 3
    }
    
    @objc func btnBookmarkTbl(sender:UIButton)  {
        
        var IsArrCount1 = false
    
        if isSelectedTab == 0{
            appDelegateObj.strPostId = arrayNewsBookmark[sender.tag].n_id
            appDelegateObj.strPostTypeId = "1" //news type = 1
            
            var copyArray = arrayNewsBookmark
            copyArray.remove(at: sender.tag)
            arrayNewsBookmark = copyArray
            
            if arrayNewsBookmark.count == 0{
                IsArrCount1 = true
            }
            
            
        }else if isSelectedTab == 1{
            
            if arrayMyAssetBookmark[sender.tag].is_group == "1"
            {
                
                appDelegateObj.strPostId = arrayMyAssetBookmark[sender.tag].a_gp_id
                appDelegateObj.strPostTypeId = "4" //fund type = 4
                
                var copyArray = arrayMyAssetBookmark
                copyArray.remove(at: sender.tag)
                arrayMyAssetBookmark = copyArray
                
                if arrayMyAssetBookmark.count == 0{
                    IsArrCount1 = true
                }
                
            }
            else{
                
                appDelegateObj.strPostId = arrayMyAssetBookmark[sender.tag].a_id
                appDelegateObj.strPostTypeId = "2" //asset type = 2
                
                var copyArray = arrayMyAssetBookmark
                copyArray.remove(at: sender.tag)
                arrayMyAssetBookmark = copyArray
                
                if arrayMyAssetBookmark.count == 0{
                    IsArrCount1 = true
                }
            }
            
        }else{
            appDelegateObj.strPostId = arrayNewDealsBookmark[sender.tag].a_id
            appDelegateObj.strPostTypeId = "3" //deal type = 3
            
            var copyArray = arrayNewDealsBookmark
            copyArray.remove(at: sender.tag)
            arrayNewDealsBookmark = copyArray
            
            if arrayNewDealsBookmark.count == 0{
                IsArrCount1 = true
            }
            
        }
        
        self.removeBookmarkApi(strPostId: appDelegateObj.strPostId, strPostTypeId: appDelegateObj.strPostTypeId)
       
        if IsArrCount1 == true{
            
            if isSelectedTab == 0{
                self.getNewsBookmarkFeed()
            }else if isSelectedTab == 1{
                self.getMyAssestBookmarkFeed()
            }
            else{
                self.getNewDealsBookmarkFeed()
            }
        }
        else{
            tblBookmarkObj.reloadData()
        }
        
    }
    
    @objc func btnShareTbl(sender:UIButton)  {
        
        if isSelectedTab == 0{
            
            let objModel = arrayNewsBookmark[sender.tag]

            SDWebImageManager.shared.loadImage(
                with: URL(string: objModel.n_image),
                options: .highPriority,
                progress: nil) { (image, data, error, cacheType, isFinished, imageUrl) in
                    print(isFinished)
                    let text = objModel.n_headline
                    let textDetail = objModel.n_p_dec
                    
                    let shareAll = [image! , text, textDetail] as [Any]
                    let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = self.view
                    self.present(activityViewController, animated: true, completion: nil)
                    
            }
            
            
            
        }else if isSelectedTab == 1{
            let objModel = arrayMyAssetBookmark[sender.tag]

            SDWebImageManager.shared.loadImage(
                with: URL(string: objModel.a_asset_img),
                options: .highPriority,
                progress: nil) { (image, data, error, cacheType, isFinished, imageUrl) in
                    print(isFinished)
                    let text = objModel.a_name
                    let textDetail = objModel.a_description
                    
                    let shareAll = [image! , text, textDetail] as [Any]
                    let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = self.view
                    self.present(activityViewController, animated: true, completion: nil)
                    
            }
            
        }else{
            let objModel = arrayNewDealsBookmark[sender.tag]

            SDWebImageManager.shared.loadImage(
                with: URL(string: objModel.a_asset_img),
                options: .highPriority,
                progress: nil) { (image, data, error, cacheType, isFinished, imageUrl) in
                    print(isFinished)
                    let text = objModel.a_name
                    let textDetail = objModel.a_description
                    
                    let shareAll = [image! , text, textDetail] as [Any]
                    let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = self.view
                    self.present(activityViewController, animated: true, completion: nil)
                    
            }
        }
        
        self.removeBookmarkApi(strPostId: appDelegateObj.strPostId, strPostTypeId: appDelegateObj.strPostTypeId)
        
        tblBookmarkObj.reloadData()
    }
    
    //MARK:- UITableView Methods
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        var arrayCount = 0
        
        if isSelectedTab == 0{
            arrayCount = arrayNewsBookmark.count
        }else if isSelectedTab == 1{
            arrayCount = arrayMyAssetBookmark.count
        }
        else{
            arrayCount = arrayNewDealsBookmark.count
        }
        return arrayCount
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        
        
        if isSelectedTab == 0{
            
            let tablecell = tableView.dequeueReusableCell(withIdentifier: "NewsTableCellTop", for: indexPath) as! NewsTableCellTop

            let objModel = arrayNewsBookmark[indexPath.row]
            
            tablecell.lblTitle.text = objModel.n_headline
            tablecell.lblTitle.numberOfLines = 2
            
            tablecell.lblDetail.text = objModel.n_p_dec
            tablecell.lblDetail.numberOfLines = 4
            
            tablecell.lblDate.text = getDateFromTimeStamp(timestamp: Double(objModel.n_created_date)!, strFormat: "MMMM dd, yyyy")
            
            
            let imgPath = objModel.n_image
            tablecell.imgView.sd_setImage(with: URL(string: imgPath), completed: nil)
            
            if objModel.is_bookmark == "0" {
                if appDelegateObj.IsThemeLight == true{
                    tablecell.imgBookmark.image = UIImage(named: "Wht_Bookmark")
                }
                else{
                    tablecell.imgBookmark.image = UIImage(named: "Bookmark")
                }
            }else{
                if appDelegateObj.IsThemeLight == true{
                    tablecell.imgBookmark.image = UIImage(named: "Wht_BookmarkFill")
                }
                else{
                    tablecell.imgBookmark.image = UIImage(named: "Bookmark-fill")
                }

            }
            
            tablecell.imgRibbon.isHidden = true
            
            tablecell.btnBookmark.tag = indexPath.row
            tablecell.btnBookmark.addTarget(self, action: #selector(self.btnBookmarkTbl), for: .touchUpInside)
            
            tablecell.btnShare.tag = indexPath.row
            tablecell.btnShare.addTarget(self, action: #selector(self.btnShareTbl), for: .touchUpInside)
            
            
            tablecell.preservesSuperviewLayoutMargins = false
            tablecell.separatorInset = UIEdgeInsets.zero
            tablecell.layoutMargins = UIEdgeInsets.zero
            
            return tablecell

            
            

        }else if isSelectedTab == 1{
            
           

            guard let tablecell = tableView.dequeueReusableCell(withIdentifier: "AssetsTblCell") as? AssetsTblCell else { return UITableViewCell() }
           
            
            let objModel = arrayMyAssetBookmark[indexPath.row]
            
            let imgPath = objModel.a_asset_img
            tablecell.imgCell.sd_setImage(with: URL(string: imgPath), completed: nil)
            
            
            tablecell.preservesSuperviewLayoutMargins = false
            tablecell.separatorInset = UIEdgeInsets.zero
            tablecell.layoutMargins = UIEdgeInsets.zero
            
            if objModel.is_bookmark == "0" {
                //            tablecell.btnBookmark.setImage(UIImage(named: "Bookmark"), for: .normal)
                tablecell.imgBookmark.image = UIImage(named: "Bookmark")
                
            }else{
                //            tablecell.btnBookmark.setImage(UIImage(named: "Bookmark-fill"), for: .normal)
                tablecell.imgBookmark.image = UIImage(named: "Bookmark-fill")
                
            }
            
            if objModel.is_viewed == "0" {
                
               
                tablecell.imgRibbon.isHidden = false
                
                
            }else{
                tablecell.imgRibbon.isHidden = true
            }
            
            if objModel.is_group == "0"{
                tablecell.lblTitle.text = objModel.a_name.uppercased()
                tablecell.lblTitle.numberOfLines = 2
                tablecell.lblDetail.text = objModel.p_type_name
                tablecell.lblDetail.numberOfLines = 0
                tablecell.lblQuater.text = "\(objModel.a_city), \(objModel.a_state)"
                
            }
            else{
                tablecell.lblTitle.text = objModel.group_name.uppercased()
                tablecell.lblTitle.numberOfLines = 2
                tablecell.lblDetail.text = ""
                tablecell.lblQuater.text = ""
                
            }
            
            tablecell.btnBookmark.tag = indexPath.row
            tablecell.btnBookmark.addTarget(self, action: #selector(self.btnBookmarkTbl), for: .touchUpInside)
            
            return tablecell

        }else{
            
        
            
            let tablecell = tableView.dequeueReusableCell(withIdentifier: "AssetsTblCell", for: indexPath) as! AssetsTblCell
            
            let objModel = arrayNewDealsBookmark[indexPath.row]
            
            tablecell.lblTitle.text = objModel.a_name.uppercased()
            tablecell.lblTitle.numberOfLines = 2
            tablecell.lblDetail.text = objModel.p_type_name
            tablecell.lblDetail.numberOfLines = 1
            
            let imgPath = objModel.a_asset_img
            tablecell.imgCell.sd_setImage(with: URL(string: imgPath), completed: nil)
            
            tablecell.lblQuater.text = "\(objModel.a_city), \(objModel.a_state)"
            
            tablecell.preservesSuperviewLayoutMargins = false
            tablecell.separatorInset = UIEdgeInsets.zero
            tablecell.layoutMargins = UIEdgeInsets.zero
            
            if objModel.is_bookmark == "0" {
               
                tablecell.imgBookmark.image = UIImage(named: "Bookmark")
                
            }else{
                
                tablecell.imgBookmark.image = UIImage(named: "Bookmark-fill")
                
            }
            
            if objModel.is_viewed == "0" {
                tablecell.imgRibbon.isHidden = false
            }else{
                tablecell.imgRibbon.isHidden = true
            }
            
            tablecell.btnBookmark.tag = indexPath.row
            tablecell.btnBookmark.addTarget(self, action: #selector(self.btnBookmarkTbl), for: .touchUpInside)
            
            
            return tablecell


        }
        
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if isSelectedTab == 0{
            
            let objModel = arrayNewsBookmark[indexPath.row]

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ObjVC = storyboard.instantiateViewController(withIdentifier: "NewsDetailsVC") as! NewsDetailsVC
            ObjVC.isNewsDetail = true
            ObjVC.objNews_detail = objModel
            //  present(ObjVC, animated: true, completion: nil)
            
            self.navigationController?.pushViewController(ObjVC, animated: true)
            
            
        }else if isSelectedTab == 1{
            
            let objModel = arrayMyAssetBookmark[indexPath.row]

            if objModel.is_group == "0" {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let ObjVC = storyboard.instantiateViewController(withIdentifier: "MyAssetsDetailVC") as! MyAssetsDetailVC
                ObjVC.strAssetTitle = objModel.a_name
                ObjVC.objMyAsset = objModel
                ObjVC.strAssetId = objModel.a_id
                ObjVC.IsNewDealDetail = false
                self.navigationController?.pushViewController(ObjVC, animated: true)
                
            }else{
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let ObjVC = storyboard.instantiateViewController(withIdentifier: "FundsDetailVC") as! FundsDetailVC
                ObjVC.objFund = objModel
                self.navigationController?.pushViewController(ObjVC, animated: true)
                
            }
            
            
        }else{
            
            let objModel = arrayNewDealsBookmark[indexPath.row]

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ObjVC = storyboard.instantiateViewController(withIdentifier: "MyAssetsDetailVC") as! MyAssetsDetailVC
            ObjVC.strAssetTitle = objModel.a_name
            ObjVC.objMyAsset = objModel
            ObjVC.strAssetId = objModel.a_id
            ObjVC.IsNewDealDetail = true
            self.navigationController?.pushViewController(ObjVC, animated: true)
            
        }
        
    }
    
    
    
}
