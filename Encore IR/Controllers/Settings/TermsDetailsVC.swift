//
//  TermsDetailsVC.swift
//  Encore Direct
//
//  Created by Trivedi Sagar on 7/1/19.
//  Copyright © 2019 ravi. All rights reserved.
//

import UIKit

class TermsDetailsVC: UIViewController {
    
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var imgBack: UIImageView!
    
    @IBOutlet var txtViewDetailsTerms: UITextView!
    
    var isTerms = Bool()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupInitialView()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.setUpThemeColor()
    }

    //MARK:- Class Methods and Functions
    func setupInitialView(){
        
        if isTerms{
            lblTopTitle.text = "Terms & Services Agreement"
            self.getTerms()
        }else{
            lblTopTitle.text = "What's New"
            self.getWhatsApi()
        }
    
        self.setupFontSize()
    }
    func setUpThemeColor(){
        if appDelegateObj.IsThemeLight == true{
            
            print("Ligth Theme mode")
            imgBackground.image = UIImage(named: "Wht_background")
            imgBack.image = UIImage(named: "Wht_Back")
            self.view.backgroundColor = wviewBgDefaultThemeColor
            lblTopTitle.textColor = wTopTitleFontColor
            txtViewDetailsTerms.textColor = wDarkGrayColor
        }
        else{
            print("Dark Theme mode")
            self.view.backgroundColor = TabViewColor
            imgBackground.image = UIImage(named: "background")
            imgBack.image = UIImage(named: "Back")
            lblTopTitle.textColor = TopTitleFontColor
            txtViewDetailsTerms.textColor = TopTitleFontColor
            
        }
    }
    
    func setupFontSize(){
        lblTopTitle.font = lblTopTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 20.0))
        txtViewDetailsTerms.font = txtViewDetailsTerms.font!.withSize(GlobalMethods.setThemeFontSize(objectSize: 14.0))
    }

    //MARK:- IBActions
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
