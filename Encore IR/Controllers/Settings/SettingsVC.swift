//
//  SettingsVC.swift
//  Encore Direct
//
//  Created by Trivedi Sagar on 7/1/19.
//  Copyright © 2019 ravi. All rights reserved.
//

import UIKit
import UserNotifications

class SettingsVC: UIViewController {
    
    //MARK:- Outlets
    
    @IBOutlet weak var imgBackground: UIImageView!
    
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var imgSideMenu: UIImageView!
    
    @IBOutlet weak var imgTerms: UIImageView!
    @IBOutlet weak var viewTerms: UIView!
    @IBOutlet weak var lblTerms: UILabel!
    
    @IBOutlet weak var viewSecond: UIView!
    
    @IBOutlet weak var imgOnOff: UIImageView!
    @IBOutlet weak var btnNotification: UIButton!
    @IBOutlet weak var lblNotificationTitle: UILabel!
    
    @IBOutlet weak var imgBerometerOnOFf: UIImageView!
    @IBOutlet weak var btnBeromtrNotif: UIButton!
    @IBOutlet weak var lblBeromtrNotification: UILabel!
    
    @IBOutlet weak var imgThemeOnOFf: UIImageView!
    @IBOutlet weak var btnThemeNotif: UIButton!
    @IBOutlet weak var lblThemeNotification: UILabel!
    
    @IBOutlet weak var lblCounterNews: UILabel!
    @IBOutlet weak var lblCounterAssets: UILabel!
    @IBOutlet weak var lblCounterDeals: UILabel!
    @IBOutlet weak var lblCounterLibrary: UILabel!
    
    @IBOutlet weak var imgFontSize: UIImageView!
    @IBOutlet weak var lblFontSize: UILabel!
    @IBOutlet weak var btnFontSize: UIButton!

    @IBOutlet weak var viewVersion: UIView!
    @IBOutlet weak var lblVersionTitle: UILabel!
    @IBOutlet weak var lblVersion: UILabel!
    
    @IBOutlet weak var imgFooterBG: UIImageView!
    @IBOutlet weak var imgNewsFooter: UIImageView!
    @IBOutlet weak var imgAssetFooter: UIImageView!
    @IBOutlet weak var imgDealFooter: UIImageView!
    @IBOutlet weak var imgLibFooter: UIImageView!
 
    @IBOutlet weak var viewFont: UIView!
    
    @IBOutlet weak var viewFontInner: UIView!
    
    @IBOutlet weak var lblFontSizeInnerTitle: UILabel!
    
    @IBOutlet weak var imgSmallFont: UIImageView!
    
    @IBOutlet weak var lblFontSmallTitle: UILabel!
    
    @IBOutlet weak var lblFontMediumTitle: UILabel!
    
    @IBOutlet weak var imgMediumFont: UIImageView!
    
    @IBOutlet weak var lblFontLargeTitle: UILabel!
   
    @IBOutlet weak var imgLargeFont: UIImageView!
  
    @IBOutlet weak var imgExtraLargeFont: UIImageView!
    
    @IBOutlet weak var lblFontExtraLargeTitle: UILabel!
    
    //MARK:- Constant & Variables
    var imgCollection = [UIImage]()
    var imgPass = UIImage()
    var notiOnOff = "0"
    var bioMetricOnOff = "0"
  //  var intFontSizeSelectedIndex = 1
    
    //MARK:- UIView Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.setupInitialView()
        self.tabBarController?.moreNavigationController.isNavigationBarHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(checkNotificationSetting), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func viewDidAppear(_ animated: Bool) {
     
        if appDelegateObj.IsThemeLight == true{
            print("Ligth Theme mode")
            lblTopTitle.textColor = wBlueMainTitleColor
         
            imgBackground.image = nil
            imgBackground.backgroundColor = wviewBgDefaultThemeColor
            imgThemeOnOFf.image = UIImage(named: "Wht_On")
            
            imgFontSize.image = UIImage(named: "Wht_LargeFont")
            imgTerms.image = UIImage(named: "fileBlue")
            imgSideMenu.image = UIImage(named: "menuBlue")
            
            self.viewTerms.layer.cornerRadius = 5
            self.viewTerms.layer.masksToBounds = true
            self.viewTerms.backgroundColor = wviewBgDefaultThemeColor
            viewTerms.layer.borderWidth = 1
            viewTerms.layer.borderColor = wBoxBorderColor.cgColor
            viewTerms.clipsToBounds = true
          
            
            self.viewSecond.layer.cornerRadius = 5
            self.viewSecond.layer.masksToBounds = true
            self.viewSecond.backgroundColor = wviewBgDefaultThemeColor
            viewSecond.layer.borderWidth = 1
            viewSecond.layer.borderColor = wBoxBorderColor.cgColor
            viewSecond.clipsToBounds = true
            
            
            self.viewVersion.layer.cornerRadius = 5
            self.viewVersion.layer.masksToBounds = true
            self.viewVersion.backgroundColor = wviewBgDefaultThemeColor
            viewVersion.layer.borderWidth = 1
            viewVersion.layer.borderColor = wBoxBorderColor.cgColor
            viewVersion.clipsToBounds = true
            
            self.viewFontInner.layer.cornerRadius = 5
            self.viewFontInner.layer.masksToBounds = true
            self.viewFontInner.backgroundColor = wviewBgDefaultThemeColor
            viewFontInner.layer.borderWidth = 1
            viewFontInner.layer.borderColor = UIColor.clear.cgColor
            viewFontInner.clipsToBounds = true
            
            lblFontSizeInnerTitle.textColor = wDarkGrayColor
            lblFontSize.textColor = wDarkGrayColor
            lblFontSmallTitle.textColor = wDarkGrayColor
            lblFontMediumTitle.textColor = wDarkGrayColor
            lblFontLargeTitle.textColor = wDarkGrayColor
            lblFontExtraLargeTitle.textColor = wDarkGrayColor
       
            lblNotificationTitle.textColor = wBlueMainTitleColor
            lblBeromtrNotification.textColor = wBlueMainTitleColor
            lblThemeNotification.textColor = wBlueMainTitleColor
            lblFontSize.textColor = wBlueMainTitleColor
            lblTerms.textColor = wBlueMainTitleColor
            lblVersionTitle.textColor = wBlueMainTitleColor
            
            lblVersion.textColor = wDarkGrayColor
            btnFontSize.setTitleColor(wDarkGrayColor, for: .normal)
            
            imgFooterBG.image = UIImage(named: "Wht_FooterBG")
            imgNewsFooter.image = UIImage(named: "Wht_News")
            imgAssetFooter.image = UIImage(named: "Wht_Asset")
            imgDealFooter.image = UIImage(named: "Wht_Deal")
            imgLibFooter.image = UIImage(named: "Wht_Library")
            
            self.fontRadioButtonImageSet(index: appDelegateObj.intFontSize)
        }
        else{
            print("Dark Theme mode")
          
            imgBackground.image = UIImage(named: "background")
            
            imgFooterBG.image = UIImage(named: "footer-background")
            imgNewsFooter.image = UIImage(named: "News-icon")
            imgAssetFooter.image = UIImage(named: "My-Assets-icon")
            imgDealFooter.image = UIImage(named: "New-Deals-icon")
            imgLibFooter.image = UIImage(named: "Library-icon")
           
            
            lblTopTitle.textColor = TopTitleFontColor
          
            imgThemeOnOFf.image = UIImage(named: "Off")
            
            self.viewTerms.layer.cornerRadius = 5
            self.viewTerms.layer.masksToBounds = true
            self.viewTerms.backgroundColor = kCellBackColor
            viewTerms.layer.borderWidth = 0.5
            viewTerms.layer.borderColor = kCellBackColor.cgColor
            viewTerms.clipsToBounds = true
            
            self.viewSecond.layer.cornerRadius = 5
            self.viewSecond.layer.masksToBounds = true
            self.viewSecond.backgroundColor = kCellBackColor
            viewSecond.layer.borderWidth = 0.5
            viewSecond.layer.borderColor = kCellBackColor.cgColor
            viewSecond.clipsToBounds = true
            
            self.viewVersion.layer.cornerRadius = 5
            self.viewVersion.layer.masksToBounds = true
            self.viewVersion.backgroundColor = kCellBackColor
            viewVersion.layer.borderWidth = 0.5
            viewVersion.layer.borderColor = kCellBackColor.cgColor
            viewVersion.clipsToBounds = true
            
            self.viewFontInner.layer.cornerRadius = 5
            self.viewFontInner.layer.masksToBounds = true
            self.viewFontInner.backgroundColor = kCellBackColor
            viewFontInner.layer.borderWidth = 0.5
            viewFontInner.layer.borderColor = UIColor.clear.cgColor
            viewFontInner.clipsToBounds = true
            
            lblFontSizeInnerTitle.textColor = UIColor.white
            lblFontSize.textColor = UIColor.white
            lblFontSmallTitle.textColor = UIColor.white
            lblFontMediumTitle.textColor = UIColor.white
            lblFontLargeTitle.textColor = UIColor.white
            lblFontExtraLargeTitle.textColor = UIColor.white

            imgFontSize.image = UIImage(named: "FontBig")
            imgTerms.image = UIImage(named: "text-file-icon")
            imgSideMenu.image = UIImage(named: "menu")
            
            lblNotificationTitle.textColor = wviewBgDefaultThemeColor
            lblBeromtrNotification.textColor = wviewBgDefaultThemeColor
            lblThemeNotification.textColor = wviewBgDefaultThemeColor
            lblFontSize.textColor = wviewBgDefaultThemeColor
            lblTerms.textColor = wviewBgDefaultThemeColor
            lblVersionTitle.textColor = wviewBgDefaultThemeColor
            lblVersion.textColor = wLightGrayColor
            btnFontSize.setTitleColor(wLightGrayColor, for: .normal)
            
            self.fontRadioButtonImageSet(index: appDelegateObj.intFontSize)
        }
        
        self.checkNotificationSetting()
        
        self.updateDetail()
    }
    
    override func viewWillLayoutSubviews() {
        
        
        
        //New Notification Counter
        if appDelegateObj.ObjCounter.new_news != "0" {
            lblCounterNews.isHidden = false
            lblCounterNews.text = appDelegateObj.ObjCounter.new_news
            
        }else{
            lblCounterNews.isHidden = true
        }
        lblCounterNews.textColor = UIColor.black
        lblCounterNews.backgroundColor = counterBgColor
        lblCounterNews.layer.cornerRadius = lblCounterNews.frame.width/2
        lblCounterNews.layer.masksToBounds = true
        
        
        if appDelegateObj.ObjCounter.new_asset != "0" {
            lblCounterAssets.isHidden = false
            lblCounterAssets.text = appDelegateObj.ObjCounter.new_asset
            
        }else{
            lblCounterAssets.isHidden = true
        }
        lblCounterAssets.text = appDelegateObj.ObjCounter.new_asset
        lblCounterAssets.textColor = UIColor.black
        lblCounterAssets.backgroundColor = counterBgColor
        lblCounterAssets.layer.cornerRadius = lblCounterAssets.frame.width/2
        lblCounterAssets.layer.masksToBounds = true
        
        if appDelegateObj.ObjCounter.new_deal != "0" {
            lblCounterDeals.isHidden = false
            lblCounterDeals.text = appDelegateObj.ObjCounter.new_deal
            
        }else{
            lblCounterDeals.isHidden = true
        }
        lblCounterDeals.text = appDelegateObj.ObjCounter.new_deal
        lblCounterDeals.textColor = UIColor.black
        lblCounterDeals.backgroundColor = counterBgColor
        lblCounterDeals.layer.cornerRadius = lblCounterDeals.frame.width/2
        lblCounterDeals.layer.masksToBounds = true
        
        if appDelegateObj.ObjCounter.new_docs != "0" {
            lblCounterLibrary.isHidden = false
            lblCounterLibrary.text = appDelegateObj.ObjCounter.new_docs
            
        }else{
            lblCounterLibrary.isHidden = true
        }
        lblCounterLibrary.text = appDelegateObj.ObjCounter.new_docs
        lblCounterLibrary.textColor = UIColor.black
        lblCounterLibrary.backgroundColor = counterBgColor
        lblCounterLibrary.layer.cornerRadius = lblCounterLibrary.frame.width/2
        lblCounterLibrary.layer.masksToBounds = true
        
    }
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
       
        self.setupFontSize()
        self.viewFont.isHidden = true
        
        lblBeromtrNotification.text = strBiometricsNotification
        lblNotificationTitle.text = strPushNotification
        //lblTopTitle.font = UIFont(name: MEDIUEM_FONT, size: 20)
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        lblVersion.text = appVersion
        
       // CGFloat fontSize = self.lblTopTitle.font.pointSize;
        
        let fontSize = self.lblTopTitle.font.pointSize
    }
    
    func setupFontSize(){
        lblTopTitle.font = lblTopTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 20.0))
        
        lblTerms.font = lblTerms.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 15.0))
        lblNotificationTitle.font = lblNotificationTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 15.0))
        lblBeromtrNotification.font = lblBeromtrNotification.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 15.0))
        lblThemeNotification.font = lblThemeNotification.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 15.0))
        lblFontSize.font = lblFontSize.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 15.0))
        
        lblVersionTitle.font = lblVersionTitle.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 15.0))
        
        lblVersion.font = lblVersion.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 13.0))
    
        btnFontSize.titleLabel?.font = btnFontSize.titleLabel?.font.withSize(GlobalMethods.setThemeFontSize(objectSize: 13.0))
    
    }
    
    func setUpSmallFontSize(){
        lblTopTitle.font = lblTopTitle.font.withSize(18.0)
        
        lblTerms.font = lblTerms.font.withSize(13.0)
        lblNotificationTitle.font = lblNotificationTitle.font.withSize(13.0)
        lblBeromtrNotification.font = lblBeromtrNotification.font.withSize(13.0)
        lblThemeNotification.font = lblThemeNotification.font.withSize(13.0)
        lblFontSize.font = lblFontSize.font.withSize(13.0)
        lblVersionTitle.font = lblVersionTitle.font.withSize(13.0)
        
        lblVersion.font = lblVersion.font.withSize(11.0)
        btnFontSize.titleLabel?.font = btnFontSize.titleLabel?.font.withSize(11.0)
        self.updateSideMenu()
    }
    
    func setUpMediumFontSize(){
        lblTopTitle.font = lblTopTitle.font.withSize(20.0)
        
        lblTerms.font = lblTerms.font.withSize(15.0)
        lblNotificationTitle.font = lblNotificationTitle.font.withSize(15.0)
        lblBeromtrNotification.font = lblBeromtrNotification.font.withSize(15.0)
        lblThemeNotification.font = lblThemeNotification.font.withSize(15.0)
        lblFontSize.font = lblFontSize.font.withSize(15.0)
        lblVersionTitle.font = lblVersionTitle.font.withSize(15.0)
        
        lblVersion.font = lblVersion.font.withSize(13.0)
        btnFontSize.titleLabel?.font = btnFontSize.titleLabel?.font.withSize(13.0)
        self.updateSideMenu()
    }
    
    func setUpLargeFontSize(){
        lblTopTitle.font = lblTopTitle.font.withSize(22.0)
        
        lblTerms.font = lblTerms.font.withSize(17.0)
        lblNotificationTitle.font = lblNotificationTitle.font.withSize(17.0)
        lblBeromtrNotification.font = lblBeromtrNotification.font.withSize(17.0)
        lblThemeNotification.font = lblThemeNotification.font.withSize(17.0)
        lblFontSize.font = lblFontSize.font.withSize(17.0)
        lblVersionTitle.font = lblVersionTitle.font.withSize(17.0)
        
        lblVersion.font = lblVersion.font.withSize(15.0)
        btnFontSize.titleLabel?.font = btnFontSize.titleLabel?.font.withSize(15.0)
        self.updateSideMenu()
    }
    
    func setUpExtraLargeFontSize(){
        lblTopTitle.font = lblTopTitle.font.withSize(24.0)
        
        lblTerms.font = lblTerms.font.withSize(19.0)
        lblNotificationTitle.font = lblNotificationTitle.font.withSize(19.0)
        lblBeromtrNotification.font = lblBeromtrNotification.font.withSize(19.0)
        lblThemeNotification.font = lblThemeNotification.font.withSize(19.0)
        lblFontSize.font = lblFontSize.font.withSize(19.0)
        lblVersionTitle.font = lblVersionTitle.font.withSize(19.0)
        
        lblVersion.font = lblVersion.font.withSize(17.0)
        btnFontSize.titleLabel?.font = btnFontSize.titleLabel?.font.withSize(17.0)
        self.updateSideMenu()
    }
    
    func updateSideMenu(){
        let nc = NotificationCenter.default
        nc.post(name: Notification.Name(kPostUpdateSideMenuTheme), object: nil)
    }
    
    @objc func checkNotificationSetting(){
        
        let current = UNUserNotificationCenter.current()
        
        current.getNotificationSettings(completionHandler: { (settings) in
            if settings.authorizationStatus == .notDetermined {
                // Notification permission has not been asked yet, go for it!
            } else if settings.authorizationStatus == .denied {
                // Notification permission was previously denied, go to settings & privacy to re-enable
                
                DispatchQueue.main.async {
                    
                    if appDelegateObj.IsThemeLight == true{
                        self.imgOnOff.image = UIImage(named: "Wht_Off")
                    }
                    else{
                        self.imgOnOff.image = UIImage(named: "Off")
                    }
                    
                    self.notiOnOff = "0"
                    self.notificationOnOff(strOnOff: "0")
                }
                
            } else if settings.authorizationStatus == .authorized {
                // Notification permission was already granted
                
                DispatchQueue.main.async {
                    
                    if appDelegateObj.IsThemeLight == true{
                        self.imgOnOff.image = UIImage(named: "Wht_On")
                    }
                    else{
                        self.imgOnOff.image = UIImage(named: "On")
                    }
                    
                    self.notiOnOff = "1"
                    self.notificationOnOff(strOnOff: "1")
                    
                }
            }
        })
        
    }
    
    func updateDetail(){
        //notificaion on off
        if appDelegateObj.loginUser.u_notification_flag == "0"{
            if appDelegateObj.IsThemeLight == true{
                self.imgOnOff.image = UIImage(named: "Wht_Off")
            }
            else{
                self.imgOnOff.image = UIImage(named: "Off")
            }
            notiOnOff = "0"
        }else{
            if appDelegateObj.IsThemeLight == true{
                self.imgOnOff.image = UIImage(named: "Wht_On")
            }
            else{
                self.imgOnOff.image = UIImage(named: "On")
            }
            notiOnOff = "1"
        }
        
        //biometric on off
        if appDelegateObj.loginUser.u_biometric_authentication == "0"{
            if appDelegateObj.IsThemeLight == true{
                self.imgBerometerOnOFf.image = UIImage(named: "Wht_Off")
            }
            else{
                self.imgBerometerOnOFf.image = UIImage(named: "Off")
            }
            
            bioMetricOnOff = "0"
            
        }else{
            if appDelegateObj.IsThemeLight == true{
                self.imgBerometerOnOFf.image = UIImage(named: "Wht_On")
            }
            else{
                self.imgBerometerOnOFf.image = UIImage(named: "On")
            }
            
            bioMetricOnOff = "1"
        }
    }
   
    func fontRadioButtonImageSet(index: Int){
        
        if index == FontSizeType.Small.rawValue{
            
             btnFontSize.setTitle("Small", for: .normal)
            
            if appDelegateObj.IsThemeLight{
                imgSmallFont.image = UIImage(named: "radioSelBlue")
                imgMediumFont.image = UIImage(named: "radioUnSelBlue")
                imgLargeFont.image = UIImage(named: "radioUnSelBlue")
                imgExtraLargeFont.image = UIImage(named: "radioUnSelBlue")
            }else{
                imgSmallFont.image = UIImage(named: "radioSel")
                imgMediumFont.image = UIImage(named: "radioUnSel")
                imgLargeFont.image = UIImage(named: "radioUnSel")
                imgExtraLargeFont.image = UIImage(named: "radioUnSel")
            }
        }
        else if index == FontSizeType.Medium.rawValue{
            
             btnFontSize.setTitle("Medium", for: .normal)
            
            if appDelegateObj.IsThemeLight{
                imgSmallFont.image = UIImage(named: "radioUnSelBlue")
                imgMediumFont.image = UIImage(named: "radioSelBlue")
                imgLargeFont.image = UIImage(named: "radioUnSelBlue")
                imgExtraLargeFont.image = UIImage(named: "radioUnSelBlue")
            }else{
                imgSmallFont.image = UIImage(named: "radioUnSel")
                imgMediumFont.image = UIImage(named: "radioSel")
                imgLargeFont.image = UIImage(named: "radioUnSel")
                imgExtraLargeFont.image = UIImage(named: "radioUnSel")
            }
            
        }else if index == FontSizeType.Large.rawValue{
             btnFontSize.setTitle("Large", for: .normal)
            if appDelegateObj.IsThemeLight{
                imgSmallFont.image = UIImage(named: "radioUnSelBlue")
                imgMediumFont.image = UIImage(named: "radioUnSelBlue")
                imgLargeFont.image = UIImage(named: "radioSelBlue")
                imgExtraLargeFont.image = UIImage(named: "radioUnSelBlue")
            }else{
                imgSmallFont.image = UIImage(named: "radioUnSel")
                imgMediumFont.image = UIImage(named: "radioUnSel")
                imgLargeFont.image = UIImage(named: "radioSel")
                imgExtraLargeFont.image = UIImage(named: "radioUnSel")
            }
        }
        else if index == FontSizeType.ExtraLarge.rawValue{
             btnFontSize.setTitle("Extra Large", for: .normal)
            if appDelegateObj.IsThemeLight{
                imgSmallFont.image = UIImage(named: "radioUnSelBlue")
                imgMediumFont.image = UIImage(named: "radioUnSelBlue")
                imgLargeFont.image = UIImage(named: "radioUnSelBlue")
                imgExtraLargeFont.image = UIImage(named: "radioSelBlue")
            }else{
                imgSmallFont.image = UIImage(named: "radioUnSel")
                imgMediumFont.image = UIImage(named: "radioUnSel")
                imgLargeFont.image = UIImage(named: "radioUnSel")
                imgExtraLargeFont.image = UIImage(named: "radioSel")
            }
        }
    }
    
    
    //MARK:- IBActions
    
    @IBAction func btnTermsPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ObjVC = storyboard.instantiateViewController(withIdentifier: "TermsDetailsVC") as! TermsDetailsVC
        ObjVC.isTerms = true
        self.navigationController?.pushViewController(ObjVC, animated: true)
    }
    
    @IBAction func btnNotificationPressed(_ sender: Any) {
        
        let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)!
        UIApplication.shared.open(settingsUrl)
    
    }
    @IBAction func btnBioMtrcNotiPressed(_ sender: Any) {
        if imgBerometerOnOFf.image == UIImage(named: "On") || imgBerometerOnOFf.image == UIImage(named: "Wht_On"){
            if appDelegateObj.IsThemeLight == true{
                self.imgBerometerOnOFf.image = UIImage(named: "Wht_Off")
            }
            else{
                self.imgBerometerOnOFf.image = UIImage(named: "Off")
            }
            
            bioMetricOnOff = "0"
            appDelegateObj.loginUser.u_biometric_authentication = "0"
            self.bioMetricOnOff(strOnOff: "0")
            
        }else{
            if appDelegateObj.IsThemeLight == true{
                self.imgBerometerOnOFf.image = UIImage(named: "Wht_On")
            }
            else{
                self.imgBerometerOnOFf.image = UIImage(named: "On")
            }
            bioMetricOnOff = "1"
            appDelegateObj.loginUser.u_biometric_authentication = "1"
            self.bioMetricOnOff(strOnOff: "1")
            
        }
    }
    @IBAction func btnThemeNotiPressed(_ sender: Any) {
        
       // self.ShowAlertDisplay(titleObj: strAppName, messageObj: "Comming Soon!", viewcontrolelr: self)
        
      
        if appDelegateObj.IsThemeLight == true{
            appDelegateObj.IsThemeLight = false
            UserDefaults.standard.set("0", forKey: kIsThemeLight)
            UserDefaults.standard.synchronize()
            
            self.viewDidAppear(true)
            
            print("Dark Theme mode")
            if appDelegateObj.IsThemeLight == true{
                self.imgThemeOnOFf.image = UIImage(named: "Wht_Off")
            }
            else{
                self.imgThemeOnOFf.image = UIImage(named: "Off")
            }
            
        }
        else{
            appDelegateObj.IsThemeLight = true
            UserDefaults.standard.set("1", forKey: kIsThemeLight)
            UserDefaults.standard.synchronize()
            
            self.viewDidAppear(true)
            
            print("Light Theme mode")
            if appDelegateObj.IsThemeLight == true{
                self.imgThemeOnOFf.image = UIImage(named: "Wht_On")
            }
            else{
                self.imgThemeOnOFf.image = UIImage(named: "On")
            }
            
        }
        
        self.updateSideMenu()
        
      
    }
    
    @IBAction func btnFontsizePressed(_ sender: Any) {
       // self.ShowAlertDisplay(titleObj: strAppName, messageObj: "Coming Soon!", viewcontrolelr: self)
        
        self.setViewHideShowWithAnimarion(view: viewFont, hidden: false)
    }
    
    @IBAction func btnVersionPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ObjVC = storyboard.instantiateViewController(withIdentifier: "TermsDetailsVC") as! TermsDetailsVC
        self.navigationController?.pushViewController(ObjVC, animated: true)
    }
    
    @IBAction func btnMenuPressed(_ sender: Any) {
        showLeftViewAnimated(sender)
    }
    @IBAction func btnNewsPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 0
    }
    @IBAction func btnAssetPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 1
    }
    @IBAction func btnDealPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 2
    }
    
    @IBAction func btnLibraryPressed(_ sender: Any) {
        appDelegateObj.tabBarController?.selectedIndex = 3
    }
    
    @IBAction func btnSmallFontPressed(_ sender: Any) {
       
        if appDelegateObj.intFontSize == FontSizeType.Small.rawValue{
            return
        }
        
        appDelegateObj.intFontSize = FontSizeType.Small.rawValue
        
        self.fontRadioButtonImageSet(index: appDelegateObj.intFontSize)
        
        
        UserDefaults.standard.set(appDelegateObj.intFontSize, forKey: kFontSize)
        UserDefaults.standard.synchronize()
        
        self.setUpSmallFontSize()
    }
    @IBAction func btnMediumFontPressed(_ sender: Any) {
        
        if appDelegateObj.intFontSize == FontSizeType.Medium.rawValue{
            return
        }
        
        appDelegateObj.intFontSize = FontSizeType.Medium.rawValue
        self.fontRadioButtonImageSet(index: appDelegateObj.intFontSize)
        
        
        UserDefaults.standard.set(appDelegateObj.intFontSize, forKey: kFontSize)
        UserDefaults.standard.synchronize()
        
        self.setUpMediumFontSize()
    }
    @IBAction func btnLargeFontPressed(_ sender: Any) {
        
        if appDelegateObj.intFontSize == FontSizeType.Large.rawValue{
            return
        }
        
        
        appDelegateObj.intFontSize = FontSizeType.Large.rawValue
        self.fontRadioButtonImageSet(index: appDelegateObj.intFontSize)
        
        
        UserDefaults.standard.set(appDelegateObj.intFontSize, forKey: kFontSize)
        UserDefaults.standard.synchronize()
        
        self.setUpLargeFontSize()
    }
    @IBAction func btnExtraLargeFontPressed(_ sender: Any) {
        
        if appDelegateObj.intFontSize == FontSizeType.ExtraLarge.rawValue{
            return
        }
        
        appDelegateObj.intFontSize = FontSizeType.ExtraLarge.rawValue
        self.fontRadioButtonImageSet(index: appDelegateObj.intFontSize)
        
        UserDefaults.standard.set(appDelegateObj.intFontSize, forKey: kFontSize)
        UserDefaults.standard.synchronize()
        
        self.setUpExtraLargeFontSize()
    }
    @IBAction func btnViewFontBgPressed(_ sender: Any) {
        self.setViewHideShowWithAnimarion(view: viewFont, hidden: true)
    }
    
}
