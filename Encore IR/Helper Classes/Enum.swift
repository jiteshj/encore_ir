//
//  Enum.swift

import Foundation

enum FontSizeType : Int {
    case Small = 0
    case Medium = 1
    case Large = 2
    case ExtraLarge = 3
}

