//
//  Constant_Class.swift
//  Ride
//
//  Created by Trivedi Sagar on 08/10/18.
//  Copyright © 2018 Sagar Trivedi. All rights reserved.
//

import Foundation
import UIKit

let appDelegateObj = UIApplication.shared.delegate as! AppDelegate

let kGoogleMapKey = "AIzaSyBPggZ-_XmvTXr4ZITLME3fphFFjwItHVU"
//AIzaSyDKozT9lJ6YnwLSwpCHMx296r5lwAzm11c

//Test
//let BASE_URL = "http://52.38.87.73/test_investor/api/"

//Live
//let BASE_URL = "http://52.38.87.73/api/"

// Demo Url Link
let BASE_URL = "http://staging.ancubate.com/demo-investor/api/"


//bcc Mail reciever app support
let strbccMailReciever = "appsupport@encorewealth.bz"

let strIrTeamEmail = "irteam@encore.bz"

//~ Color Code
let chartsDarkThemeColor = UIColor(red:28.0/255.0, green:95.0/255.0 ,blue:134.0/255.0 , alpha:1.00)
let mainBlueThemeColor = UIColor(red:48.0/255.0, green:123.0/255.0 ,blue:173.0/255.0 , alpha:1.00)
let TopTitleFontColor = UIColor(red:255.0/255.0, green:255.0/255.0 ,blue:255.0/255.0 , alpha:1.00)
let accentBlueThemeColor = UIColor(red:127.0/255.0, green:183.0/255.0 ,blue:215.0/255.0 , alpha:1.00)
let chartsLightThemeColor = UIColor(red:177.0/255.0, green:211.0/255.0 ,blue:227.0/255.0 , alpha:1.00)
let logoTextColor = UIColor(red:118.0/255.0, green:113.0/255.0 ,blue:113.0/255.0 , alpha:1.00)
let fontTextColor = UIColor(red:38.0/255.0, green:44.0/255.0 ,blue:51.0/255.0 , alpha:1.00)
let fontLightTextColor = UIColor(red:38.0/255.0, green:44.0/255.0 ,blue:51.0/255.0 , alpha:0.90)
let lineViewColor = UIColor(red:140.0/255.0, green:135.0/255.0 ,blue:135.0/255.0 , alpha:1.00)
let TabViewColor = UIColor(red:38.0/255.0, green:44.0/255.0 ,blue:51.0/255.0 , alpha:1.00)
let InvestTextColor = UIColor(red:201.0/255.0, green:201.0/255.0 ,blue:201.0/255.0 , alpha:1.00)
let FilterFillColor = UIColor(red:140.0/255.0, green:135.0/255.0 ,blue:135.0/255.0 , alpha:1.00)
let feedbackSepColor = UIColor(red:238.0/255.0, green:239.0/255.0 ,blue:241.0/255.0 , alpha:0.10)
let contactTitleColor = UIColor(red:127.0/255.0, green:183.0/255.0 ,blue:215.0/255.0 , alpha:1.00)
let counterBgColor = UIColor(red:255.0/255.0, green:255.0/255.0 ,blue:255.0/255.0 , alpha:1.00)
let kCellBackColor = UIColor(red:83.0/255.0, green:90.0/255.0 ,blue:95.0/255.0 , alpha:1.0)
let viewBgDefaultThemeColor = UIColor(red:59.0/255.0, green:63.0/255.0 ,blue:68.0/255.0 , alpha:1.00)
let viewFooterDefaultThemeColor = UIColor(red:59.0/255.0, green:63.0/255.0 ,blue:68.0/255.0 , alpha:1.00)
let PlayVideoThumbColor = UIColor(red:0.0/255.0, green:0.0/255.0 ,blue:0.0/255.0 , alpha:0.50)
let BlueThemeButtonColor = UIColor(red:26.0/255.0, green:95.0/255.0 ,blue:134.0/255.0 , alpha:1.00)
let PdfBGColor = UIColor(red:140.0/255.0, green:135.0/255.0 ,blue:135.0/255.0 , alpha:1.00)
let DateFontColor = UIColor(red:201.0/255.0, green:201.0/255.0 ,blue:201.0/255.0 , alpha:1.00)
let otpFonttitleColor = UIColor(red:118.0/255.0, green:120.0/255.0 ,blue:128.0/255.0 , alpha:1.00)
let otpFontColor = UIColor(red:66.0/255.0, green:67.0/255.0 ,blue:71.0/255.0 , alpha:1.00)
let DarkThemefontColor = UIColor(red:255.0/255.0, green:255.0/255.0 ,blue:255.0/255.0 , alpha:1.00)


//~ White Theme Color Code
let wTopTitleFontColor = UIColor(red:26.0/255.0, green:95.0/255.0 ,blue:134.0/255.0 , alpha:1.00)
let wDetailFontColor = UIColor(red:118.0/255.0, green:114.0/255.0 ,blue:114.0/255.0 , alpha:1.00)
let wDateFontColor = UIColor(red:140.0/255.0, green:135.0/255.0 ,blue:135.0/255.0 , alpha:1.00)
let wviewBgDefaultThemeColor = UIColor(red:255.0/255.0, green:255.0/255.0 ,blue:255.0/255.0 , alpha:1.00)
//let wviewFooterDefaultThemeColor = UIColor(red:59.0/255.0, green:63.0/255.0 ,blue:68.0/255.0 , alpha:1.00)
let wPageControlThemeColor = UIColor(red:118.0/255.0, green:114.0/255.0 ,blue:114.0/255.0 , alpha:1.00)

let wBlueMainTitleColor = UIColor(red:26.0/255.0, green:95.0/255.0 ,blue:134.0/255.0 , alpha:1.00)
let wDarkGrayColor = UIColor(red:66.0/255.0, green:67.0/255.0 ,blue:71.0/255.0 , alpha:1.00)
let wLightGrayColor = UIColor(red:118.0/255.0, green:120.0/255.0 ,blue:128.0/255.0 , alpha:1.00)
let wBoxBorderColor = UIColor(red:175.0/255.0, green:197.0/255.0 ,blue:218.0/255.0 , alpha:1.00)



//
let intMAxNameLenght = 30
let intMinNameLenght = 0
let intMaxContactLenght = 15
let intMinContactLenght = 7
let intMinFeedbackLenght = 10

let intFontDifference = 2

//~
let strOK                   = "OK"
let strAppName              = "Encore Direct"
let MsgSomthingWrong        = "Something went wrong"
let MsgNoInternet           = "Please check internet connection"
let strCancel               = "Cancel"
let strLoading              = "Loading..."
let strCamera               = "Camera"
let strPhoto_Video_Library  = "Photo Library"
let strDone                 = "Done"
let strRemovePhoto          = "Remove photo"


// Dark font
let DINPRO              = "DINPro"
let MEDIUEM_FONT        = "DINPro-Medium"
let DINPROBold          = "DINPro-Bold"
let DINPROBItalic       = "DINPro-MediumItalic"
let DINPROLight         = "DINPro-Light"
let D_DIN               = "D-DIN"
let Rubic               = "Rubik-Regular"

//~ White Theme Font
let wRubic              = "Rubik-Regular"


//~
let kUserEmail                  = "UserEmail"
let kPassword                   = "kPassword"
let kMoveNextView               = "moveNextView"
let kIndex                      = "indexPass"
let kLoginUser                  = "loginUserDetails"
let kLoginUserCheck             = "loginUserCheck"
let kGetProfileUpdate           = "GetProfileUpdate"
let kPostUpdateSideMenuProfile  = "PostUpdateSideMenuProfile"
let kContactUsDetails           = "ContactUsDetails"
let kTouchAuthUserId            = "TouchAuthUserId"
let kTouchAuthPassword          = "TouchAuthPassword"
let kProfileEdit                = "ProfileEdit"
let kPopUpView                  = "PopUpView"
let kIsThemeLight               = "IsThemeLight"
let kRefreshNews                = "RefreshNews"
let kRefreshAssets              = "RefreshAssets"
let kRefreshDeal                = "RefreshDeal"
let kNewNotification            = "NewNotification"
let kPostUpdateSideMenuTheme    = "PostUpdateSideMenuTheme"
let kFontSize                   = "FontSize"

// login view
let strForgotPassword       = "Forgot Your Password?"
let strRememberMe           = "Remember Me"
let strUserName             = "Username"
let strPassword             = "Password"
let strSignin               = "Sign In"
let msgEnterUserName        = "Please enter username"
let msgEnterPassword        = "Please enter password"
let strReadMore             = "Read More..."

// otp view
let strSubmit               = "Submit"
let strBackToSignin         = "BACK TO SIGN IN"
let msgEnterOTP             = "Please enter otp"
let msgOTPMismatch          = "Please enter valid OTP"

//forgot password View
let strGetStared            = "Get Started"
let strNewPass              = "New Password"
let strConfPass             = "Confirm Password"
let msgEnterNewPassword     = "Please enter new password"
let msgEnterConfirmassword  = "Please enter confirm password"
let msgPasswordMismatch     = "New password and confirm password does not match"

//forgotPasswordEmail
let strEmail    = "Email"

//BACK TO SIGN IN

//menu view
let strProfile              = "My Profile"
let strBookmark             = "Bookmarks"
let strFeedback             = "Feedback"
let strContactUs            = "Contact Us"
let msgInvestorSince        = "Investor since"

//bookmark view
let strNews                     = "News"
let strMyAssets                 = "My Assets"
let strNewDeals                 = "New Deals"
let msgNewsNotBookmark          = "No news bookmarked"
let msgassetsNotBookmark        = "No assets bookmarked"
let msgdealsNotBookmark         = "No deals bookmarked"


// profile edit
let msgUserName                 = "Please enter first name"
let msgLastName                 = "Please enter last name"
let msgUserContact              = "Please enter contact number"
let msgUserValidContact         = "Please enter valid contact number"
let strFirstName                = "First Name"
let strLastName                 = "Last Name"
let strContactNumber            = "Contact Number"
let strBiometricsNotification   = "BIOMETRIC AUTHENTICATION"
let strPushNotification         = "PUSH NOTIFICATIONS"


//"First Name"

//feedback
let msgEnterEmail = "Please enter email"
let msgEnterValidEmail = "Enter valid email id"
let msgEnterFeedComment = "Please enter feedback"
let msgfeedbackplc = "Type your message"
let msgEnterValidFeedComment = "Please enter minimum 10 characters"
let msgFeedbackSubmitted = "Thank you for submitting your feedback!"

//investment Summary
let strQuarterlyUpdates = "Quarterly Updates"
let strQuarterlyUpdatesTitle = "Quarter-End Update"
let strLEGAL_ISSUE_AND_CLAIMS = "LEGAL ISSUE AND CLAIMS"
let strInvSumm = "Investment Summary"
let strQupdates = "QUARTERLY UPDATES"

//Distribution Guide
//let strDistribuGuidance = "Distribution Guidance"
let strDistribuGuidance = "Investment Guidance"
let strDevTimeline = "DEVELOPMENT TIMELINE"
let strPropExitStategy = "PROPOSED EXIT STRATEGY"
let strDistGuidanceCell = "DISTRIBUTION GUIDANCE"
let strDistGuidance = "INVESTMENT GUIDANCE"

// financial
let strFinDocTitle = "FINANCIAL DOCUMENTS"
let strFinancial = "FINANCIALS"
let strFinancialHeader = "Financials"

//Asset Detail
let strUnique1 = "uniqid_1" //Quaterly-End Updates
let strUnique5 = "uniqid_5" //Investment guidance
let strUnique3 = "uniqid_3" //Financials


// Document
let strDocument = "Document"
let strFinStatement = "Financial \nStatement"
let strIncomeStatement = "Income \nStatement"
let strBalanceSheet = "Balance \nSheet"



// Quaterly Updates
let strPerformance = "PERFORMANCE"
let strLeasing = "LEASING"
let strConstruction = "CONSTRUCTION"
let strLegal = "LEGAL"

//contact  Investment Director
let strContactDesignation = "Investment Director"
let strMob = "Mob:"
let strOffice = "Office:"

// logout
let msgLogout = "Are you sure you want to logout?"

//profile update
let msgProfileUpdate = "Profile picture update successfully."
let msgProfileRemove = "Profile picture removed successfully."
let msgProfileDetailUpdate = "Profile successfully updated."

//Profile successfully updated

