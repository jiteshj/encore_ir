//
//  Extension_Class.swift
//  Ride
//
//  Created by Trivedi Sagar on 08/10/18.
//  Copyright © 2018 Sagar Trivedi. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration

extension UIViewController{
    
    func checkErrorTypeNetworkLost(error:Error) -> Bool {
        if  (error as NSError).code == -1005
        {
            return true
        }
        if  (error as NSError).code == -1001
        {
            return true
        }
        return false
    }
    
    func ShowAlertDisplay(titleObj:String, messageObj:String, viewcontrolelr:UIViewController)
    {
        let AlertObj = UIAlertController.init(title:titleObj, message: messageObj, preferredStyle: UIAlertControllerStyle.alert)
        AlertObj.addAction(UIAlertAction.init(title: strOK, style: UIAlertActionStyle.default, handler: nil))
        viewcontrolelr.present(AlertObj, animated: true, completion: nil)
    }
    
    func setUserDefault(ObjectToSave : AnyObject?  , KeyToSave : String)
    {
        let defaults = UserDefaults.standard
        if (ObjectToSave != nil)
        {
            defaults.set(ObjectToSave!, forKey: KeyToSave)
        }
        UserDefaults.standard.synchronize()
    }
    
    func removeUserDefault(KeyObj : String)
    {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: KeyObj)
        UserDefaults.standard.synchronize()
    }
    
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    func getDayNameFromDate(dateString:String)->String{
        
        let f:DateFormatter = DateFormatter()
        f.timeZone = NSTimeZone.local
        f.dateFormat = "yyyy-MM-dd"
        let MMMdate = f.date(from: dateString)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat  = "EEEE"//"EE" to get short style
        // let dayInWeek = dateFormatter.string(from: (MMMdate)!)//"Sunday"
        let dayInWeek = dateFormatter.string(from: MMMdate!)
        return dayInWeek
    }
    func getCurrentMonthNameFromDate()-> String{
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM"
        let nameOfMonth = dateFormatter.string(from: now)
        
        return nameOfMonth
    }
    func changeDateFormat(dateString:String,strCur:String,strNew:String)->String{
        
        let dateFormatterOld = DateFormatter()
        dateFormatterOld.timeZone = NSTimeZone(abbreviation: "UTC")! as TimeZone
        dateFormatterOld.dateFormat = strCur
        let date = dateFormatterOld.date(from: dateString)
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.system
        dateFormatter.dateFormat = strNew
        let returnString = dateFormatter.string(from: date!)
        
        return returnString
        
    }
    
    func getDateFromTimeStamp(timestamp:Double) -> String{
        let date = Date(timeIntervalSince1970: timestamp)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        
        //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    
    func getDateFromTimeStamp(timestamp:Double,strFormat:String) -> String{
        let date = Date(timeIntervalSince1970: timestamp)
        let dateFormatterOld = DateFormatter()
        dateFormatterOld.timeZone = NSTimeZone(abbreviation: "UTC")! as TimeZone

        
        /*
         RFC3339DateFormatter.locale = Locale(localeIdentifier: "en_US_POSIX")
         RFC3339DateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
         RFC3339DateFormatter.timeZone = TimeZone(forSecondsFromGMT: 0)
         
         var locale = NSLocale(localeIdentifier: "en/US")
         dateFormatter.locale = locale
         */
        
        //Set timezone that you want
        let dateFormatter = DateFormatter()
//        dateFormatter.timeZone = NSTimeZone.system
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)

        dateFormatter.dateFormat = strFormat
        dateFormatter.dateStyle = .medium
        //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    
    
    func calculateTimeDifference(dateStr:String) -> String {
        
        let f:DateFormatter = DateFormatter()
        
        f.timeZone = NSTimeZone.local
        
        f.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let now = f.string(from: NSDate() as Date)
        
        let startDate = f.date(from: dateStr)
        
        let endDate = f.date(from: now)
        
        let calendar: NSCalendar = NSCalendar.current as NSCalendar
        
        let calendarUnits = NSCalendar.Unit.weekOfMonth.union( NSCalendar.Unit.day).union(NSCalendar.Unit.hour).union(NSCalendar.Unit.minute).union(NSCalendar.Unit.second).union(NSCalendar.Unit.month).union(NSCalendar.Unit.year)
        
        let dateComponents = calendar.components(calendarUnits, from: startDate!, to: endDate!, options: [])
        
        let weeks = abs(Int32(dateComponents.weekOfMonth!))
        let days = abs(Int32(dateComponents.day!))
        let hours = abs(Int32(UInt32(dateComponents.hour!)))
        let min = abs(Int32(UInt32(dateComponents.minute!)))
        let sec = abs(Int32(UInt32(dateComponents.second!)))
        let months = abs(Int32(UInt32(dateComponents.month!)))
        let years = abs(Int32(UInt32(dateComponents.year!)))
        
        var timeAgo = "just now"
        if (sec > 0){
            if (sec > 1) {
                timeAgo = "\(sec) seconds ago"
                
            } else {
                timeAgo = "\(sec) second ago"
            }
            
        }
        if (min > 0){
            if (min > 1) {
                timeAgo = "\(min) minutes ago"
            } else {
                timeAgo = "\(min) minute ago"
            }
        }
        if(hours > 0){
            if (hours > 1) {
                timeAgo = "\(hours) hours ago"
            } else {
                timeAgo = "\(hours) hour ago"
            }
        }
        if (days > 0) {
            if (days > 1) {
                timeAgo = "\(days) days ago"
            } else {
                timeAgo = "Yesterday"
            }
        }
        if(weeks > 0){
            if (weeks > 1) {
                timeAgo = "\(weeks) weeks ago"
            } else {
                timeAgo = "\(weeks) week ago"
            }
        }
        if(months > 0){
            if (months > 1) {
                timeAgo = "\(months) months ago"
            } else {
                timeAgo = "\(months) month ago"
            }
        }
        if(years > 0){
            if (years > 1) {
                timeAgo = "\(years) years ago"
            } else {
                timeAgo = "\(years) year ago"
            }
        }
        
        //  print("timeAgo is===> \(timeAgo)")
        return timeAgo;
        
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func convertToDict(text:String) -> NSDictionary {
        
        var dictonary:NSDictionary?
        
        if let data = text.data(using: String.Encoding.utf8) {
            
            do {
                dictonary = try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject] as! NSDictionary
                
            } catch let error as NSError {
                print(error)
            }
        }
        return dictonary!
    }
    
    func setViewHideShowWithAnimarion(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })
    }
    
    
    func setViewHideShowWithAnimarionRightToLeft(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.2, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })
    }
    
    func getCurrentDate_Time(strFormat:String) -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = strFormat
        let result = formatter.string(from: date)
        return result
    }
    
    func verifyUrl(urlString: String?) -> Bool {
        guard let urlString = urlString,
            let url = URL(string: urlString) else {
                return false
        }
        
        return UIApplication.shared.canOpenURL(url)
    }
    
    func getNumberFormat(strAmount: String)-> String{
        
        
        if strAmount.count > 0{
            //en_US, es_ES
            let dNumber = Double(strAmount)
            let formatter = NumberFormatter()
            formatter.numberStyle = .currency
            formatter.maximumFractionDigits = 2
            formatter.locale = Locale(identifier: "en_US")
            
            return formatter.string(from: NSNumber(value: dNumber!))!
        }else{
            return strAmount
        }
        
       
    }
    


    
}
extension UIView{
    func dropShadow(color:UIColor, opacity:Float, radius:Float) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        self.layer.shadowRadius = CGFloat(radius)
    }
}
extension String{
    
    var length : Int {
        return self.characters.count
    }
    //Validate Email
    var isEmail: Bool {
        do {
            let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}", options: .caseInsensitive)
            return regex.firstMatch(in: self as String, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.length)) != nil
        } catch {
            return false
        }
    }
    
    var isNumber : Bool{
        let invalidCharSet = CharacterSet(charactersIn: "1234567890").inverted as CharacterSet
        let filtered : String = self.components(separatedBy: invalidCharSet).joined(separator: "")
        return (self == filtered)
    }
    
    func toImage() -> UIImage? {
        if let data = Data(base64Encoded: self, options: .ignoreUnknownCharacters){
            return UIImage(data: data)
        }
        return nil
    }
    
    public func isImage() -> Bool {
        // Add here your image formats.
        let imageFormats = ["jpg", "jpeg", "png", "gif"]
        
        if let ext = self.getExtension() {
            return imageFormats.contains(ext)
        }
        
        return false
    }
    
    public func getExtension() -> String? {
        let ext = (self as NSString).pathExtension
        
        if ext.isEmpty {
            return nil
        }
        
        return ext
    }
    
    public func isURL() -> Bool {
        return URL(string: self) != nil
    }
    
    enum RegularExpressions: String {
        case phone = "^\\s*(?:\\+?(\\d{1,3}))?([-. (]*(\\d{3})[-. )]*)?((\\d{3})[-. ]*(\\d{2,4})(?:[-.x ]*(\\d+))?)\\s*$"
    }
    
    func isValid(regex: RegularExpressions) -> Bool {
        return isValid(regex: regex.rawValue)
    }
    
    func isValid(regex: String) -> Bool {
        let matches = range(of: regex, options: .regularExpression)
        return matches != nil
    }
    
    func onlyDigits() -> String {
        let filtredUnicodeScalars = unicodeScalars.filter{CharacterSet.decimalDigits.contains($0)}
        return String(String.UnicodeScalarView(filtredUnicodeScalars))
    }
    
    func makeAColl() {
        if isValid(regex: .phone) {
            if let url = URL(string: "tel://\(self.onlyDigits())"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    
    func capitalizedFirst() -> String {
        let first = self[self.startIndex ..< self.index(startIndex, offsetBy: 1)]
        let rest = self[self.index(startIndex, offsetBy: 1) ..< self.endIndex]
        return first.uppercased() + rest.lowercased()
    }
    
    func capitalizedFirst(with: Locale?) -> String {
        let first = self[self.startIndex ..< self.index(startIndex, offsetBy: 1)]
        let rest = self[self.index(startIndex, offsetBy: 1) ..< self.endIndex]
        return first.uppercased(with: with) + rest.lowercased(with: with)
    }
    
   
    var htmlToAttributedString: NSAttributedString? {
        guard
            let data = self.data(using: .utf8)
            else { return nil }
        do {
            return try NSAttributedString(data: data, options: [
                NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html,
                NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue
                ], documentAttributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
            return  nil
        }
    }
    
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
//    func isValid(regex: String) -> Bool {
//        let matches = range(of: regex, options: .regularExpression)
//        return matches != nil
//    }
    
    
    
}
extension Int {
    var degreesToRadians: Double { return Double(self) * .pi / 180 }
}
extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}
extension Date{
    func dayNumberOfWeek() -> Int?{
        return Calendar.current.dateComponents([.weekday], from: self).weekday
    }
    
    /// Returns a Date with the specified days added to the one it is called with
    func add(years: Int = 0, months: Int = 0, days: Int = 0, hours: Int = 0, minutes: Int = 0, seconds: Int = 0) -> Date{
        var targetDay: Date
        targetDay = Calendar.current.date(byAdding: .year, value: years, to: self)!
        targetDay = Calendar.current.date(byAdding: .month, value: months, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .day, value: days, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .hour, value: hours, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .minute, value: minutes, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .second, value: seconds, to: targetDay)!
        return targetDay
    }
    
//    func getTimeWithFormat(date:Date) -> String {
//
//        let dateFormatter1 = DateFormatter()
//        dateFormatter1.dateStyle = .medium
//        dateFormatter1.timeStyle = .none
//        dateFormatter1.dateFormat = GENERAL_DATE_FORMAT
//
//        let dateprint = dateFormatter1.string(from: date)
//        
//        return dateprint
//    }
    
    func getDateFormat(strFormat:String) -> DateFormatter {
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateStyle = .medium
        dateFormatter1.timeStyle = .none
        dateFormatter1.dateFormat = strFormat
        return dateFormatter1
    }
    
    func hmsFrom(seconds: Int, completion: @escaping ( _ hours: Int,  _ minutes: Int, _ seconds: Int)->()) {
        
        completion(seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
        
    }
    
    func offsetFrom(date : Date) -> Bool{
        
        if date < Date(){
            return false
        }
        
        let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second]
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: date, to: self);
        
        if let day = difference.day, day > 0 { return false }
        else if let hour = difference.hour, hour > 0 { return false }
        else if let minute = difference.minute, minute <= 5 && minute > 0 { return true }
        else if let second = difference.second, second > 0 { return false }
        return false
    }
    
    
}
extension UIApplication {
    class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return getTopViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return getTopViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }
}
protocol Utilities {
}
extension NSObject:Utilities{
    
    
    enum ReachabilityStatus {
        case notReachable
        case reachableViaWWAN
        case reachableViaWiFi
    }
    
    var currentReachabilityStatus: ReachabilityStatus {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return .notReachable
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return .notReachable
        }
        
        if flags.contains(.reachable) == false {
            // The target host is not reachable.
            return .notReachable
        }
        else if flags.contains(.isWWAN) == true {
            // WWAN connections are OK if the calling application is using the CFNetwork APIs.
            return .reachableViaWWAN
        }
        else if flags.contains(.connectionRequired) == false {
            // If the target host is reachable and no connection is required then we'll assume that you're on Wi-Fi...
            return .reachableViaWiFi
        }
        else if (flags.contains(.connectionOnDemand) == true || flags.contains(.connectionOnTraffic) == true) && flags.contains(.interventionRequired) == false {
            // The connection is on-demand (or on-traffic) if the calling application is using the CFSocketStream or higher APIs and no [user] intervention is needed
            return .reachableViaWiFi
        }
        else {
            return .notReachable
        }
    }
    
}

extension UIButton {
    func underlineButton(text: String) {
        let titleString = NSMutableAttributedString(string: text)
        let fontBig = UIFont(name:DINPRO, size: 12.0)
        titleString.addAttribute(NSAttributedStringKey.font, value: fontBig!, range: NSMakeRange(0, text.count))
        titleString.addAttribute(NSAttributedStringKey.foregroundColor, value: BlueThemeButtonColor, range: NSMakeRange(0, text.count))

        titleString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSMakeRange(0, text.count))
        self.setAttributedTitle(titleString, for: .normal)
    }
}

extension UIImageView {
    static func fromGif(frame: CGRect, resourceName: String) -> UIImageView? {
        guard let path = Bundle.main.path(forResource: resourceName, ofType: "gif") else {
            print("Gif does not exist at that path")
            return nil
        }
        let url = URL(fileURLWithPath: path)
        guard let gifData = try? Data(contentsOf: url),
            let source =  CGImageSourceCreateWithData(gifData as CFData, nil) else { return nil }
        var images = [UIImage]()
        let imageCount = CGImageSourceGetCount(source)
        for i in 0 ..< imageCount {
            if let image = CGImageSourceCreateImageAtIndex(source, i, nil) {
                images.append(UIImage(cgImage: image))
            }
        }
        let gifImageView = UIImageView(frame: frame)
        gifImageView.animationImages = images
        return gifImageView
    }
}

@IBDesignable extension UIButton {
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}

extension UIButton {
    func setRoundedCorners(ratio: Double?) {
//        if let r = ratio {
//            self.layer.cornerRadius = self.frame.size.width*CGFloat(r) // for specific corner ratio
//        } else {
//
//            // circle
//            // i would put a condition, as width and height differ:
//            if(self.frame.size.width == self.frame.size.height) {
//                self.layer.cornerRadius = self.frame.size.width/2 // for circles
//            } else {
//                //
//            }
//        }
//        self.layer.borderWidth = 1
        self.layer.cornerRadius = self.frame.size.height*CGFloat(ratio!) // for specific corner ratio
        
        self.clipsToBounds = true
    }
}

extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}

extension UIImage {
    class func outlinedEllipse(size: CGSize, color: UIColor, lineWidth: CGFloat = 1.0) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        
        context.setStrokeColor(color.cgColor)
        context.setLineWidth(lineWidth)
        let rect = CGRect(origin: .zero, size: size).insetBy(dx: lineWidth * 0.5, dy: lineWidth * 0.5)
        context.addEllipse(in: rect)
        context.strokePath()
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    
}

extension UILabel {
    
    func calculateMaxLines() -> Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
        return linesRoundedUp
    }
    
}

extension StringProtocol {
    var firstUppercased: String {
        return prefix(1).uppercased() + dropFirst()
    }
    var firstCapitalized: String {
        return String(prefix(1)).capitalized + dropFirst()
    }
}
