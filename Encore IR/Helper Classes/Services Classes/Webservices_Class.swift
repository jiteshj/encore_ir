//
//  Webservices_Class.swift
//  Encore IR
//
//  Created by Trivedi Sagar on 15/05/19.
//  Copyright © 2018 Sagar Trivedi. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SVProgressHUD
import SDWebImage


enum APIPath : String
{
    case login = "user/login"
    case news = "news/news-list"
    case addBookmark = "bookmark/add"
    case removeBookmark = "bookmark/remove"
    case user_Profile = "user/profile-detail"
    case update_Profile         = "user/edit-profile-detail"
    case updateProfileImage     = "user/update-profile-image"
    case removeProfileImage     = "user/remove-profile-img"
    case projectManageList      = "project/project-manage-list"
    case change_noti            = "user/change_noti"
    case biometric_authentication = "user/biometric_authentication"
    case addfeed                = "feed/add-feed"
    case get_asset_details      = "asset/get-asset-details"
    case group_fund             = "asset/group-fund"
    case asset_manage_list      = "asset/asset-manage-list"
    case logout                 = "user/logout"
    case count                 = "asset/count-new"
    case viewed                 = "asset/asset-viewed"
    case check_login             = "user/check-login"
    case newsDetail             = "news/get-news-details"


    case forgotPass = "user/forgot-password"
    case resetPassword = "user/reset-password"
    case getDocument = "document/get-document-list"
    case getAssetList = "asset/get-asset-list"
    case getNewsFeedBookMark = "bookmark/getNews"
    case getMyAssetBookMark = "bookmark/getProject"
    case getNewDealsBookMark = "bookmark/getDeal"
    
    case getFilterProperty = "asset/get-property-list"
    case term = "term"
    case termAgree = "term-agree"
    case whatsNew = "whats-new"
}

extension UIViewController{
    
    func addBookmarkApi(strPostId:String,strPostTypeId:String)
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            /*
             
             id
             u_id
             id_type
             */
            
            SVProgressHUD.show()
            let param = ["id":strPostId,"u_id":appDelegateObj.loginUser.u_id,"id_type":strPostTypeId] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.addBookmark.rawValue
            
            //            let strAuth = "Bearer " + appDelegateObj.loginUser.token
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            //print(header)
            
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<BookmarkModel>) in
                    
                    SVProgressHUD.dismiss()
                    
                    //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            //                            let responseValue = response.result.value
                            
                            //                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                            
                        else
                        {
                            // //print(response.result.value?.message)
//                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.addBookmarkApi(strPostId:appDelegateObj.strPostId , strPostTypeId:appDelegateObj.strPostTypeId)
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
    func removeBookmarkApi(strPostId:String,strPostTypeId:String)
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            /*
             
             id
             u_id
             id_type
             */
            
            SVProgressHUD.show()
            let param = ["id":strPostId,"u_id":appDelegateObj.loginUser.u_id,"id_type":strPostTypeId] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.removeBookmark.rawValue
            
            //            let strAuth = "Bearer " + appDelegateObj.loginUser.token
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            //print(header)
            
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<BookmarkModel>) in
                    
                    SVProgressHUD.dismiss()
                    
                    //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            //                            let responseValue = response.result.value
                            
                            //                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                            
                        else
                        {
                            // //print(response.result.value?.message)
//                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.removeBookmarkApi(strPostId:appDelegateObj.strPostId , strPostTypeId:appDelegateObj.strPostTypeId)
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
    func getProfileDetail()
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
          
            let param = ["u_id":appDelegateObj.loginUser.u_id] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.user_Profile.rawValue
            
            //            let strAuth = "Bearer " + appDelegateObj.loginUser.token
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            print(header)
            
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<SignInServices>) in
                    
//                    SVProgressHUD.dismiss()
                    
                    print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            let responseValue = response.result.value
                            
                            //                            let profileDatadetail : LoginData = (responseValue?.data)!
                            
                            appDelegateObj.loginUser = (responseValue?.data.user_data)!
                            
                            appDelegateObj.loginCordDetails = responseValue!.data.codinatoer_detail
                            
                            appDelegateObj.loginAssociatDetails = responseValue!.data.associative_detail
                            
                            appDelegateObj.ProAmountDetail = responseValue!.data.amountData
                            
                            
                            let nc = NotificationCenter.default
                            nc.post(name: Notification.Name(kPostUpdateSideMenuProfile), object: nil)
                            
                            let nc1 = NotificationCenter.default
                            nc1.post(name: Notification.Name(kGetProfileUpdate), object: nil)
                            
                            let nc2 = NotificationCenter.default
                            nc2.post(name: Notification.Name(kContactUsDetails), object: nil)
                            
                            
                            //
                            
                            do {
                                
                                let userDefaults = UserDefaults.standard
                                //                            let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: appDelegateObj.loginUser)
                                let encodedData: Data = try NSKeyedArchiver.archivedData(withRootObject: appDelegateObj.loginUser, requiringSecureCoding: false)
                                
                                userDefaults.set(encodedData, forKey: kLoginUser)
                                userDefaults.synchronize()
                                
                                
                            } catch {
                                fatalError("Can't encode data: \(error)")
                                
                            }
                            //                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                            
                        else
                        {
                            // //print(response.result.value?.message)
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.getProfileDetail()
                        }
                        else
                        {
//                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
    func getUpdateCounter()
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            /*
             
             
             u_id
             
             */
            
//            SVProgressHUD.show()
            let param = ["u_id":appDelegateObj.loginUser.u_id] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.count.rawValue
            
            //            let strAuth = "Bearer " + appDelegateObj.loginUser.token
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            //print(header)
            
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<updateCounter>) in
                    
//                    SVProgressHUD.dismiss()
                    
                    //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            let responseValue = response.result.value
                            
                            appDelegateObj.ObjCounter = (responseValue?.data)!
                           
                            self.viewWillLayoutSubviews()
                            
                        }else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                            
                        else
                        {
                            // //print(response.result.value?.message)
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.getUpdateCounter()
                        }
                        else
                        {
//                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
    
    func IsReadPostApi(strPostId:String,strPostTypeId:String)
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            /*
             type
             a_id
             u_id
             is_viewed
             */
            
//            SVProgressHUD.show()
            let param = ["a_id":strPostId,"u_id":appDelegateObj.loginUser.u_id,"is_viewed":"1","type":strPostTypeId] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.viewed.rawValue
            
            //            let strAuth = "Bearer " + appDelegateObj.loginUser.token
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            //print(header)
            
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<ViewDetails>) in
                    
//                    SVProgressHUD.dismiss()
                    
                    //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            //                            let responseValue = response.result.value
                            
                            //                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                            self.getUpdateCounter()

                        }else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                            
                        else
                        {
                            // //print(response.result.value?.message)
                            //                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.IsReadPostApi(strPostId: strPostId , strPostTypeId: strPostTypeId)
                        }
                        else
                        {
//                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
}

extension LoginVC{
    func loginApiCall(strUserName:String, strPassword: String)
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            SVProgressHUD.show()
            let param = ["u_email":strUserName,"u_password":strPassword,"notificationToken":appDelegateObj.deviceToken] as [String : Any]
            
            //   //print(param)
            
            let strAPIPath = BASE_URL + APIPath.login.rawValue
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: nil)
                .responseObject { (response: DataResponse<SignInServices>) in
                    
                    SVProgressHUD.dismiss()
                    
                     //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            if appDelegateObj.IsRememberMe == true{
                                
                                UserDefaults.standard.set(strUserName, forKey: kUserEmail);
                                UserDefaults.standard.set(strPassword, forKey: kPassword);
                                UserDefaults.standard.synchronize();
                                
                            }
                            
                            self.setUserDefault(ObjectToSave: strUserName as AnyObject, KeyToSave: kTouchAuthUserId)
                            
                            self.setUserDefault(ObjectToSave: strPassword as AnyObject, KeyToSave: kTouchAuthPassword)
                            
                            //  self.setUserDefault(ObjectToSave: "1" as AnyObject, KeyToSave: kTouchIdEnable)
                            
                            
                            let responseValue = response.result.value
                            
                            appDelegateObj.loginUser = (responseValue?.data.user_data)!
                            
                            
                            appDelegateObj.loginCordDetails = responseValue!.data.codinatoer_detail
                            
                            appDelegateObj.loginAssociatDetails = responseValue!.data.associative_detail
                            
                            appDelegateObj.ProAmountDetail = responseValue!.data.amountData
                            
                            //print(appDelegateObj.loginUser)
                            
                            let nc = NotificationCenter.default
                            nc.post(name: Notification.Name(kPostUpdateSideMenuProfile), object: nil)
                            
                            let nc1 = NotificationCenter.default
                            nc1.post(name: Notification.Name(kGetProfileUpdate), object: nil)
                            
                            do {
                                
                                let userDefaults = UserDefaults.standard
                                //                            let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: appDelegateObj.loginUser)
                                let encodedData: Data = try NSKeyedArchiver.archivedData(withRootObject: appDelegateObj.loginUser, requiringSecureCoding: false)
                                
                                userDefaults.set(encodedData, forKey: kLoginUser)
                                userDefaults.synchronize()
                                
                                self.setUserDefault(ObjectToSave: "1" as AnyObject, KeyToSave: kLoginUserCheck)
                                
                                
                                if appDelegateObj.loginUser.u_tc_agree == "0"{
                                    self.imgLogo.isHidden = true
                                    self.viewTerms.isHidden = false
                                    self.getTerms()
                                }else{
                                    self.viewTerms.isHidden = true
                                    self.MoveToDashboard()
                                }
                                
                            } catch {
                                fatalError("Can't encode data: \(error)")
                                
                            }
                            
                        }
                            
                        else
                        {
                            // //print(response.result.value?.message)
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.loginApiCall(strUserName: strUserName, strPassword: strPassword)
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
    
    func forgotApiCall()
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            SVProgressHUD.show()
            let param = ["forgot_field":txtUsername.text!] as [String : Any]
            
            //   //print(param)
            
            let strAPIPath = BASE_URL + APIPath.forgotPass.rawValue
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: nil)
                .responseObject { (response: DataResponse<ForgotServices>) in
                    
                    SVProgressHUD.dismiss()
                    
                    //  //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            let responseValue = response.result.value?.data
                            
                            self.forgotApiResponse(strOtp: (responseValue?.otp)!, strUser: (responseValue?.u_id)!)
                            
                        }
                            
                        else
                        {
                            // //print(response.result.value?.message)
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.forgotApiCall()
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
    func userExistApiCall()
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            SVProgressHUD.show()
            let param = ["u_email":txtUsername.text!] as [String : Any]
            
            //   //print(param)
            
            let strAPIPath = BASE_URL + APIPath.check_login.rawValue
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: nil)
                .responseObject { (response: DataResponse<ForgotServices>) in
                    
                    SVProgressHUD.dismiss()
                    
                    //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            let responseValue = response.result.value?.data
                            
                            if responseValue?.otp != ""{
                                
                                //password not set
                                self.forgotApiResponse(strOtp: (responseValue?.otp)!, strUser: (responseValue?.u_id)!)
                                
                            }else{
                                //password set
                                self.IsUserRegister = true
                                self.checkUserRegisterResponce()
                                
                            }
                            
                            
                        }
                        else
                        {
                            // //print(response.result.value?.message)
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.forgotApiCall()
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
    func getTerms()
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            /*
             
             
             u_id
             
             */
            
            SVProgressHUD.show()
            let param = ["u_id":appDelegateObj.loginUser.u_id] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.term.rawValue
            
            //            let strAuth = "Bearer " + appDelegateObj.loginUser.token
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            //print(header)
            
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<getTermsDetails>) in
                    
                    SVProgressHUD.dismiss()
                    
                    //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            //print("Get Terms")
                            
                            let responseValue = response.result.value?.data
                            
                            if responseValue?.result.count == 0 {
                                
                            }
                            else{
                                self.txtDetailTerms.text = responseValue?.result[0].terms
                                self.strUserId = (responseValue?.result[0].id)!
                            }
                            
                        }else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                            
                        else
                        {
                            // //print(response.result.value?.message)
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.getTerms()
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
    func agreeTerms()
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            /*
             
             
             u_id
             
             */
            
            SVProgressHUD.show()
            let param = ["u_id":appDelegateObj.loginUser.u_id] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.termAgree.rawValue
            
            //            let strAuth = "Bearer " + appDelegateObj.loginUser.token
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            //print(header)
            
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<TermsDetails>) in
                    
                    SVProgressHUD.dismiss()
                    
                    //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            //print("Agree Terms")
                            self.viewTerms.isHidden = true
                            self.MoveToDashboard()
                            
                        }else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                            
                        else
                        {
                            // //print(response.result.value?.message)
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.agreeTerms()
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }

    
}

extension ForgotPasswordVC{
    func resetApiCall()
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            SVProgressHUD.show()
            let param = ["u_password":txtNewPass.text!,"u_id":strUserid] as [String : Any]
            
            //   //print(param)
            
            let strAPIPath = BASE_URL + APIPath.resetPassword.rawValue
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: nil)
                .responseObject { (response: DataResponse<ResetServices>) in
                    
                    SVProgressHUD.dismiss()
                    
                    //  //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            let responseValue = response.result.value?.message
                            
                            self.resetApiResponse(strMsg: responseValue!)
                            
                        }
                            
                        else
                        {
                            // //print(response.result.value?.message)
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.resetApiCall()
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
}
extension NewsVC{
    func getNewsFeed()
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
        
//            SVProgressHUD.show()
            let param = ["page":strPage,"u_id":appDelegateObj.loginUser.u_id] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.news.rawValue
        
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            //print(header)

            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<News_Main>) in
                    
//                    SVProgressHUD.dismiss()
                    
                    //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            let responseValue = response.result.value
                            
                            self.TotalFeed = Int(responseValue!.data.news_detail.count)

//                            self.arrDealDetail = (responseValue?.data.Deal_detail)!
//                            
//                            self.objCollectionView.reloadData()
                            
                            self.arrNews_detail = (responseValue?.data.news_detail)!
                            
                            self.objTableView.reloadData()
                            
                        }else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                            
                        else
                        {
                            // //print(response.result.value?.message)
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.getNewsFeed()
                        }
                        else
                        {
//                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
    func getMoreNewsFeed(strMorePage: String)
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            SVProgressHUD.show()
            let param = ["page":strMorePage,"u_id":appDelegateObj.loginUser.u_id] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.news.rawValue
            
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            //print(header)
            
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<News_Main>) in
                    
                    SVProgressHUD.dismiss()
                    
                    //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            let responseValue = response.result.value
                            
                            self.TotalFeed = Int(responseValue!.data.news_detail.count)
                            
                            if responseValue?.data.news_detail.count == 0{
                                self.isPagingNeed = true
                            }
                            else{
                                self.isPagingNeed = false
                            
                                self.arrNews_detail.append(contentsOf: (responseValue?.data.news_detail)!)
                            
                                self.objTableView.reloadData()
                            }
                            
                        }else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                            
                        else
                        {
                            // //print(response.result.value?.message)
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.getNewsFeed()
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
}
extension MyAssetsVC{
    
    func getMoreAssetsFeed(strPage:String,strType:String, strTimeFrame:String, strProType:String, isNeedToCallFilter: Bool) {
        
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
//            SVProgressHUD.show()
            let param = ["page":strPage,"u_id":appDelegateObj.loginUser.u_id,"type":"0","timeframe":strTimeFrame,"a_sector":strProType] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.getAssetList.rawValue
            
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<AssetsServices>) in
                    
//                    SVProgressHUD.dismiss()
                    
                    //print(response)
//                    self.arrayData.removeAll()
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            let responseValue = response.result.value
                            
                            self.TotalFeed = Int(responseValue!.data.total_project)!
                            
                            if responseValue?.data.project_list.count == 0{
                                self.isPagingNeed = true
                            }
                            else{
                                self.isPagingNeed = false

//                                var arrCopyResult = self.arrayData
                                //print("arry  \(self.arrayData)")

                                for newList in (responseValue?.data.project_list)! {
                                    if !self.arrayData.contains(newList) {
                                        self.arrayData.append(newList)
//                                        arrCopyResult.append(newList)
                                        
                                    }
                                }
                                
                                self.tblAssetsobj.isHidden = false
                                self.lblNoDataFound.isHidden = true
                                
//                                self.arrayData.removeAll()
//                                self.arrayData = arrCopyResult
                                //print("arry append  \(self.arrayData)")
                                self.tblAssetsobj.reloadData()

                            }
                            
                        }else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                            
                        else
                        {
                            // //print(response.result.value?.message)
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
//                            self.getAssetsFeed(strType: strType, strTimeFrame: strTimeFrame, strProType: strProType, isNeedToCallFilter: isNeedToCallFilter)
                        }
                        else
                        {
//                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
        
    }
    
    func getAssetsFeed(strType:String, strTimeFrame:String, strProType:String, isNeedToCallFilter: Bool)
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            SVProgressHUD.show()
            let param = ["page":"0","u_id":appDelegateObj.loginUser.u_id,"type":strType,"timefram":strTimeFrame,"a_sector":strProType] as [String : Any]
            
            print(param)
            
            let strAPIPath = BASE_URL + APIPath.getAssetList.rawValue
            
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<AssetsServices>) in
                    
                    SVProgressHUD.dismiss()
                    
                    print(response)
                    self.arrayData.removeAll()
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            let responseValue = response.result.value
                            
                            self.pageAdded = 0
                            
                            self.isPagingNeed = true

                            self.TotalFeed = Int(responseValue!.data.total_project)!
                            
                            self.arrayData = (responseValue?.data.project_list)!
                            
                            if self.arrayData.count == 0{
                                self.lblNoDataFound.isHidden = false
                                
                                self.tblSummaryObj.reloadData()
                                self.tblAssetsobj.reloadData()
                                
                                self.tblAssetsobj.isHidden = true
                                self.tblSummaryObj.isHidden = true
                                
                                self.hightViewInvestConstraint.constant = 100
                                self.hightMainViewConstraint.constant = self.hightViewInvestConstraint.constant
                                
                            }
                            else{
                                self.tblAssetsobj.isHidden = false
                                self.tblSummaryObj.isHidden = false
                                
                                self.lblNoDataFound.isHidden = true
                               
                                self.tblSummaryObj.reloadData()
                                self.tblAssetsobj.reloadData()
                                self.tblAssetsobjCopy.reloadData()
                                
                                self.hightViewInvestConstraint.constant = 150 + 75
                            
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                                    self.hightMainViewConstraint.constant = self.tblAssetsobj.contentSize.height + 150 + 75 + 20
                                }
                            }
                            
                            if isNeedToCallFilter{
                               self.getPropertyFilterData()
                            }
                        
                        }else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                            
                        else
                        {
                            // //print(response.result.value?.message)
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.getAssetsFeed(strType: strType, strTimeFrame: strTimeFrame, strProType: strProType, isNeedToCallFilter: isNeedToCallFilter)
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
    func getPropertyFilterData()
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            SVProgressHUD.show()
            let param = ["u_id":appDelegateObj.loginUser.u_id] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.getFilterProperty.rawValue
            
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<FilterAssestServices>) in
                    
                    SVProgressHUD.dismiss()
                    
                    //  //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            let responseValue = response.result.value
                            
                            self.arrayPropFilter = (responseValue?.data.property_list)!
                            
                            self.arrayPeriodFilter = (responseValue?.data.quarter_list)!
                            
                            self.collectionFilterProperty.reloadData()
                            self.collectionFilterPeriod.reloadData()
                            
                            self.collectionFilterProperty.collectionViewLayout = self.alignedFlowLayout
                            self.collectionFilterPeriod.collectionViewLayout = self.alignedFlowLayout2
                            
                            
                            self.hightFilterCollProConstraint.constant = self.collectionFilterProperty.collectionViewLayout.collectionViewContentSize.height + 60
                            
                            self.hightQarterCollConstraint.constant = self.collectionFilterPeriod.collectionViewLayout.collectionViewContentSize.height + 60
                            
                        }
                        else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                            
                        else
                        {
                            // //print(response.result.value?.message)
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.getPropertyFilterData()
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
}

extension MyAssetsDetailVC{
    
    func getAssetsDetail(strType:String)
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            SVProgressHUD.show()
            let param = ["u_id":appDelegateObj.loginUser.u_id,"a_id":strType] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.get_asset_details.rawValue
            
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<AssetsDetailsServices>) in
                    
                    SVProgressHUD.dismiss()
                    
                   // //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            let responseValue = response.result.value

                            self.assetDetail = (responseValue?.data.asset_detail)!
                            self.setUpBookMark()
                            
                            self.projectDetail = (responseValue?.data.project_detail)!
                            
                            self.arrImgUrl = (responseValue?.data.image_list.a_asset_img)!
                            
                            if self.arrImgUrl.count > 0{
                                self.arrImg.removeAll()
                                for img in self.arrImgUrl {
                                    
                                    let url = URL(string:img.url)
//                                    let data = try? Data(contentsOf: url!)
//                                    let image: UIImage = UIImage(data: data!)!
//                                    self.arrImg.append(image)
                                    
                                    if let data = try? Data(contentsOf: url!) {
                                        let image: UIImage = UIImage(data: data)!
                                        self.arrImg.append(image)
                                        
                                    }
                                }
                            }
                                                        
                            self.arrayRowTitle.removeAll()
                            self.arrayRowTitle.append((responseValue?.data.project_section_list.first)!)
                            self.arrayRowTitle.append(contentsOf: (responseValue?.data.project_section_list)!)

                            self.tblDetail.reloadData()
                            self.objCollectionAssets.reloadData()
                            
                            self.mapLocationUpdate()
                            
                        }
                        else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                            
                        else
                        {
                            // //print(response.result.value?.message)
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.getAssetsDetail(strType: strType)
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
}



extension FundsDetailVC{
    
    func getFundsDetail(strType:String)
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            SVProgressHUD.show()
            let param = ["u_id":appDelegateObj.loginUser.u_id,"group_id":strType] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.group_fund.rawValue
            
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<FundsServices>) in
                    
                    SVProgressHUD.dismiss()
                    
                    //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            let responseValue = response.result.value
                            
                            self.objgroupDetail = (responseValue?.data.group_detail)!
                            
                            self.arrSectionList.removeAll()
                        self.arrSectionList.append((responseValue?.data.section_list.first)!)
                            
                            self.arrSectionList.append(contentsOf: (responseValue?.data.section_list)!)

                            self.countSubDeal = (responseValue?.data.total_sub_deal)!
                            
                            self.arrProjectDetail = (responseValue?.data.project_detail)!
                            
                            self.arrFundDetailList = (responseValue?.data.funds_detail)!

                            self.arrImgUrl.removeAll()
                            self.arrImgUrl = (responseValue?.data.image_list.fund_img)!
                            
                            if self.arrImgUrl.count > 0{
                                self.arrImg.removeAll()
                                for img in self.arrImgUrl {
                                    
                                    let url = URL(string:img.url)
                                    //                                    let data = try? Data(contentsOf: url!)
                                    //                                    let image: UIImage = UIImage(data: data!)!
                                    //                                    self.arrImg.append(image)
                                    
                                    if let data = try? Data(contentsOf: url!) {
                                        let image: UIImage = UIImage(data: data)!
                                        self.arrImg.append(image)
                                        
                                    }
                                }
                            }
                            
                            //print(self.arrImgUrl.count)
                            //print(self.arrImg.count)

                            
                            self.objFundTbl.reloadData()

                            self.collSubProject.reloadData()
                            self.collSubProject.layoutIfNeeded()
                            self.collSubProject.collectionViewLayout = self.alignedFlowLayout

                            self.collHeaderView.reloadData()

                            self.collectionFooter.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width , height: self.collSubProject.contentSize.height + 20)
                            self.collectionFooter.layoutIfNeeded()
                            
                            
//                            let HeigthColletion : CGFloat = CGFloat(255 * self.arrSectionList.count)
//                            self.collectionFooter.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width , height: HeigthColletion)

                            self.objFundTbl.reloadData()


//                            self.setupInitialView()
                            
                            self.lblTopTitle.text = self.objgroupDetail.g_a_group_name
                            if self.objFund.is_bookmark == "1"{
                                if appDelegateObj.IsThemeLight == true{
                                    self.imgBookmarkTop.image = UIImage(named: "Wht_BookmarkFill")
                                }
                                else{
                                    self.imgBookmarkTop.image = UIImage(named: "Bookmark-fill")
                                }
                            }else{
                                if appDelegateObj.IsThemeLight == true{
                                    self.imgBookmarkTop.image = UIImage(named: "Wht_Bookmark")
                                }
                                else{
                                    self.imgBookmarkTop.image = UIImage(named: "Bookmark")
                                }
                            }

                            
                        }
                        else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                            
                        else
                        {
                            // //print(response.result.value?.message)
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.getFundsDetail(strType: strType)
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
}


extension NewDealVC{
    func getNewDealsFeed(strType:String)
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            SVProgressHUD.show()
            let param = ["page":"0","u_id":appDelegateObj.loginUser.u_id,"type":strType] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.getAssetList.rawValue
            
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<AssetsServices>) in
                    
                    SVProgressHUD.dismiss()
                    
                    //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            let responseValue = response.result.value
                            
                            self.arrayData = (responseValue?.data.project_list)!
                           
                            if self.arrayData.count == 0{
                                self.lblNoDataFound.isHidden = false
                            }
                            else{
                                self.tblDealObj.reloadData()
                            }
                        }
                        else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                        else
                        {
                            // //print(response.result.value?.message)
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.getNewDealsFeed(strType: strType)
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
    func getMoreNewDealsFeed(strMorePage: String, strType:String)
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            SVProgressHUD.show()
            let param = ["page":strMorePage,"u_id":appDelegateObj.loginUser.u_id,"type":strType] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.getAssetList.rawValue
            
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<AssetsServices>) in
                    
                    SVProgressHUD.dismiss()
                    
                    //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            let responseValue = response.result.value
                            
//                            self.arrayData = (responseValue?.data.project_list)!
//
//                            if self.arrayData.count == 0{
//                                self.lblNoDataFound.isHidden = false
//                            }
//                            else{
//                                self.tblDealObj.reloadData()
//                            }
                            
                            self.TotalFeed = Int(responseValue!.data.total_project)!
                            
                            if responseValue?.data.project_list.count == 0{
                                self.isPagingNeed = true
                            }
                            else{
                                self.isPagingNeed = false
                                
                                //                                var arrCopyResult = self.arrayData
                                //print("arry  \(self.arrayData)")
                                
                                for newList in (responseValue?.data.project_list)! {
                                    if !self.arrayData.contains(newList) {
                                        self.arrayData.append(newList)
                                        //                                        arrCopyResult.append(newList)
                                        
                                    }
                                }
                                
                                //                                self.arrayData.removeAll()
                                //                                self.arrayData = arrCopyResult
                                //print("arry append  \(self.arrayData)")
                                self.tblDealObj.reloadData()
                                
                            }
                        }
                        else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                        else
                        {
                            // //print(response.result.value?.message)
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.getNewDealsFeed(strType: strType)
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
}
//extension LibraryVC{
//    func getDocumentFeed()
//    {
//        view.endEditing(true)
//        if currentReachabilityStatus != .notReachable
//        {
//
//            SVProgressHUD.show()
//            let param = ["page":"0","u_id":appDelegateObj.loginUser.u_id] as [String : Any]
//
//            //print(param)
//
//            let strAPIPath = BASE_URL + APIPath.getDocument.rawValue
//
//            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
//
//            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
//                .responseObject { (response: DataResponse<LibraryServices>) in
//
//                    SVProgressHUD.dismiss()
//
//                    //print(response)
//
//                    if response.result.value != nil {
//
//                        if response.result.value?.status_code == "200"{
//
//                            let responseValue = response.result.value
//
//                            self.arrayData = (responseValue?.setting.document_detail)!
//
//                            if self.arrayData.count == 0{
//                                self.lblNoDataFound.isHidden = false
//                            }
//                            else{
//                                self.tblLibraryObj.reloadData()
//                                self.collLibraryObj.reloadData()
//                            }
//                        }
//                        else if response.result.value?.status_code == "401"{
//                            appDelegateObj.logoutAndMoveToLoginView()
//                        }
//                        else
//                        {
//                            // //print(response.result.value?.message)
//                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
//
//                        }
//
//                    }
//                }
//                .responseJSON { response in
//                    switch response.result {
//                    case .success:
//                        break
//                    case .failure(let error):
//                        if self.checkErrorTypeNetworkLost(error: error)
//                        {
//                            self.getDocumentFeed()
//                        }
//                        else
//                        {
//                            SVProgressHUD.dismiss()
//
//                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
//
//                            //showAlert(error.localizedDescription)
//                        }
//                        break
//                    }
//            }
//
//        }
//        else{
//            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
//        }
//    }
//
//    func getMoreDocumentFeed(strMorePage: String)
//    {
//        view.endEditing(true)
//        if currentReachabilityStatus != .notReachable
//        {
//
//            SVProgressHUD.show()
//            let param = ["page":"0","u_id":appDelegateObj.loginUser.u_id] as [String : Any]
//
//            //print(param)
//
//            let strAPIPath = BASE_URL + APIPath.getDocument.rawValue
//
//            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
//
//            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
//                .responseObject { (response: DataResponse<LibraryServices>) in
//
//                    SVProgressHUD.dismiss()
//
//                    //print(response)
//
//                    if response.result.value != nil {
//
//                        if response.result.value?.status_code == "200"{
//
//                            let responseValue = response.result.value
//
////                            self.arrayData = (responseValue?.setting.document_detail)!
////
////                            if self.arrayData.count == 0{
////                                self.lblNoDataFound.isHidden = false
////                            }
////                            else{
////                                self.tblLibraryObj.reloadData()
////                                self.collLibraryObj.reloadData()
////                            }
//
//                            self.TotalFeed = Int(responseValue!.data.total_document)!
//
//                            if responseValue?.data.document_detail.count == 0{
//                                self.isPagingNeed = true
//                            }
//                            else{
//                                self.isPagingNeed = false
//
//                                self.arrayData.append(contentsOf: (responseValue?.data.document_detail)!)
//
//                                self.tblLibraryObj.reloadData()
//                                self.collLibraryObj.reloadData()
//                            }
//
//                        }
//                        else if response.result.value?.status_code == "401"{
//                            appDelegateObj.logoutAndMoveToLoginView()
//                        }
//                        else
//                        {
//                            // //print(response.result.value?.message)
//                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
//
//                        }
//
//                    }
//                }
//                .responseJSON { response in
//                    switch response.result {
//                    case .success:
//                        break
//                    case .failure(let error):
//                        if self.checkErrorTypeNetworkLost(error: error)
//                        {
//                            self.getDocumentFeed()
//                        }
//                        else
//                        {
//                            SVProgressHUD.dismiss()
//
//                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
//
//                            //showAlert(error.localizedDescription)
//                        }
//                        break
//                    }
//            }
//
//        }
//        else{
//            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
//        }
//    }
//
//}

extension DocumentMainVC{
    func getDocumentFeed()
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            SVProgressHUD.show()
            let param = ["page":"0","u_id":appDelegateObj.loginUser.u_id] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.getDocument.rawValue
            
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<DocumentMain>) in
                    
                    SVProgressHUD.dismiss()
                    
                    //print(response)
                    
                    self.arrayData.removeAll()
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            let responseValue = response.result.value
                            
                            self.arrayData.append(contentsOf: (responseValue?.setting.folder_data)!)
                            self.arrayData.append(contentsOf: (responseValue?.setting.saprate_data)!)
                                                        
                            if self.arrayData.count == 0{
                                self.lblNoDataFound.isHidden = false
                            }
                            else{
                                
                                self.lblNoDataFound.isHidden = true
                                
                                DispatchQueue.main.async{
                                        self.collLibraryObj.reloadData()
                                        self.tblLibraryObj.reloadData()
                                }
                            }
                        }
                        else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                        else
                        {
                            // //print(response.result.value?.message)
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.getDocumentFeed()
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }

    
}

extension BookmarksVC{
    func getNewsBookmarkFeed()
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            SVProgressHUD.show()
            let param = ["page":"0","u_id":appDelegateObj.loginUser.u_id] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.getNewsFeedBookMark.rawValue
            
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<BookMarkNewsServices>) in
                    
                    SVProgressHUD.dismiss()
                    
                    //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            let responseValue = response.result.value
                            
                            self.arrayNewsBookmark = (responseValue?.data.news_detail)!
                            self.arrayNewsBookmarkCopy = (responseValue?.data.news_detail)!

                        
                            if self.arrayNewsBookmark.count == 0{
                                self.lblNoDataFound.text = msgNewsNotBookmark
                                self.lblNoDataFound.isHidden = false
                            }
                            else{
                                self.lblNoDataFound.isHidden = true
                            }
                            
                            
                            if appDelegateObj.IsThemeLight{
                                self.btnFilterNews.backgroundColor = mainBlueThemeColor
                                self.btnFilterNews.setTitleColor(UIColor.white, for: .normal)
                                self.btnFilterNews.backgroundColor = mainBlueThemeColor
                            }
                            else{
                                self.btnFilterNews.backgroundColor = FilterFillColor
                                self.btnFilterNews.setTitleColor(UIColor.white, for: .normal)
                                self.btnFilterNews.backgroundColor = FilterFillColor
                            }

                            self.tblBookmarkObj.reloadData()
        
                            let nc = NotificationCenter.default
                            nc.post(name: Notification.Name(kRefreshNews), object: nil)

                        }
                        else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                        else
                        {
                            // //print(response.result.value?.message)
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.getNewsBookmarkFeed()
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
    func getMyAssestBookmarkFeed()
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            SVProgressHUD.show()
            let param = ["page":"0","u_id":appDelegateObj.loginUser.u_id,"id_type":"3"] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.getMyAssetBookMark.rawValue
            
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<BookMarkProjectServices>) in
                    
                    SVProgressHUD.dismiss()
                    
                    //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            let responseValue = response.result.value
                            
                            self.arrayMyAssetBookmark = (responseValue?.data.project_detail)!
                            self.arrayMyAssetBookmarkCopy = (responseValue?.data.project_detail)!

                            
                            if self.arrayMyAssetBookmark.count == 0{
                                self.lblNoDataFound.text = msgassetsNotBookmark
                                self.lblNoDataFound.isHidden = false
                            }else{
                                self.lblNoDataFound.isHidden = true
                            }
                           
                            self.tblBookmarkObj.reloadData()
                            
                            let nc = NotificationCenter.default
                            nc.post(name: Notification.Name(kRefreshAssets), object: nil)

                        }
                        else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                            
                        else
                        {
                            // //print(response.result.value?.message)
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.getMyAssestBookmarkFeed()
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
    func getNewDealsBookmarkFeed()
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            SVProgressHUD.show()
            let param = ["page":"0","u_id":appDelegateObj.loginUser.u_id,"id_type":"3"] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.getNewDealsBookMark.rawValue
            
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<BookMarkDealServices>) in
                    
                    SVProgressHUD.dismiss()
                    
                    //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            let responseValue = response.result.value
                            
                            self.arrayNewDealsBookmark = (responseValue?.data.deal_detail)!
                            self.arrayNewDealsBookmarkCopy = (responseValue?.data.deal_detail)!

                            
                            if self.arrayNewDealsBookmark.count == 0{
                                self.lblNoDataFound.text = msgdealsNotBookmark
                                self.lblNoDataFound.isHidden = false
                            }
                            else{
                                self.lblNoDataFound.isHidden = true
                            }
                            self.tblBookmarkObj.reloadData()
                            
                            let nc = NotificationCenter.default
                            nc.post(name: Notification.Name(kRefreshDeal), object: nil)

                            
                        }
                        else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                        else
                        {
                            // //print(response.result.value?.message)
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.getNewDealsBookmarkFeed()
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
}


extension SettingsVC{
    
    
    func notificationOnOff(strOnOff:String)
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            /*
             
             
             u_id
             u_notification_flag
             */
            
//            SVProgressHUD.show()
            let param = ["u_id":appDelegateObj.loginUser.u_id,"u_notification_flag":strOnOff] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.change_noti.rawValue
            
            //            let strAuth = "Bearer " + appDelegateObj.loginUser.token
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            //print(header)
            
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<BookmarkModel>) in
                    
//                    SVProgressHUD.dismiss()
                    
                    //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            //                            let responseValue = response.result.value
                            //print((response.value?.message)!)
                            //                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                            
                        else
                        {
                            //print((response.value?.message)!)
                            
                            // //print(response.result.value?.message)
                            //                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.notificationOnOff(strOnOff:self.notiOnOff)
                        }
                        else
                        {
//                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
    func bioMetricOnOff(strOnOff:String)
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            /*
             
             
             u_id
             u_notification_flag
             */
            
            SVProgressHUD.show()
            let param = ["u_id":appDelegateObj.loginUser.u_id,"u_biometric_authentication":strOnOff] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.biometric_authentication.rawValue
            
            //            let strAuth = "Bearer " + appDelegateObj.loginUser.token
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            //print(header)
            
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<BookmarkModel>) in
                    
                    SVProgressHUD.dismiss()
                    
                    //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            //                            let responseValue = response.result.value
                            //print((response.value?.message)!)
                            //                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                            
                        else
                        {
                            //print((response.value?.message)!)
                            
                            // //print(response.result.value?.message)
                            //                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.bioMetricOnOff(strOnOff:self.bioMetricOnOff)
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
}


extension ProfileEditVC{
    
    func updateUserProfile(){
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            /*
             "{
             u_id
             image
             }"
             */
            
            
            SVProgressHUD.show()
            let param = ["u_id":appDelegateObj.loginUser.u_id] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.updateProfileImage.rawValue
            
            //            let strAuth = "Bearer " + appDelegateObj.loginUser.token
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            //print(header)
            
            Alamofire.upload(multipartFormData:
                {
                    (multipartFormData) in
                    multipartFormData.append(UIImageJPEGRepresentation(self.imgBorder.image!, 0.1)!, withName: "image", fileName: "file.jpeg", mimeType: "image/jpeg")
                    for (key, value) in param
                    {
                        multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                    }
            }, to:strAPIPath,headers:header)
            { (result) in
                switch result {
                case .success(let upload,_,_ ):
                    upload.uploadProgress(closure: { (progress) in
                        ////print progress
                    })
                    upload.responseObject{ (response: DataResponse<ProfileimageUpdate>) in
                        
                        SVProgressHUD.dismiss()
                        
                        //print(response)
                        
                        if response.result.value != nil {
                            
                            if response.result.value?.status == "1"{
                                
                                let responseValue = response.result.value
                                
                                //print(response.result.value!.message)
                                
                                
                                let alert = UIAlertController(title: strAppName, message: (response.value?.message)!, preferredStyle: UIAlertControllerStyle.alert)
                                
                                alert.addAction(UIAlertAction(title: strOK, style: UIAlertActionStyle.default, handler: { action in
                                    
                                    appDelegateObj.loginUser.u_pro_img = (responseValue?.data.u_pro_img)!
                                    
                                    //print(appDelegateObj.loginUser)
                                    
                                    let nc = NotificationCenter.default
                                    nc.post(name: Notification.Name(kPostUpdateSideMenuProfile), object: nil)
                                    
                                    let nc1 = NotificationCenter.default
                                    nc1.post(name: Notification.Name(kGetProfileUpdate), object: nil)
                                    
                                    self.navigationController?.popViewController(animated: true)

                                    
                                }))
                                self.present(alert, animated: true, completion: nil)
                                
                                //                                do {
                                //
                                //                                    let userDefaults = UserDefaults.standard
                                //                                    let encodedData: Data = try NSKeyedArchiver.archivedData(withRootObject: appDelegateObj.loginUser, requiringSecureCoding: false)
                                //                                    userDefaults.set(encodedData, forKey: kLoginUser)
                                //                                    userDefaults.synchronize()
                                //
                                //
                                //                                } catch {
                                //                                    fatalError("Can't encode data: \(error)")
                                //
                                //                                }
                                
                            }
                            else
                            {
                                //print(response.result.value!.message)
                                self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                                
                            }
                            
                        }
                    }
                    
                case .failure(let error):
                    if self.checkErrorTypeNetworkLost(error: error)
                    {
                        
                    }
                    else
                    {
                        SVProgressHUD.dismiss()
                        
                        self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                        
                        //showAlert(error.localizedDescription)
                    }
                    break
                }
            }
            
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
    
    func removeProfileImage()
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            /*
             
             
             u_id
             
             */
            
            SVProgressHUD.show()
            let param = ["u_id":appDelegateObj.loginUser.u_id] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.removeProfileImage.rawValue
            
            //            let strAuth = "Bearer " + appDelegateObj.loginUser.token
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            //print(header)
            
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<ProfileimageUpdate>) in
                    
                    SVProgressHUD.dismiss()
                    
                    //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            //                            let responseValue = response.result.value
                            
                            //                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                            let alert = UIAlertController(title: strAppName, message: (response.value?.message)!, preferredStyle: UIAlertControllerStyle.alert)

                            alert.addAction(UIAlertAction(title: strOK, style: UIAlertActionStyle.default, handler: { action in
                                
                                self.imgBorder.image = UIImage(named: "userDefault")
                                
//                                appDelegateObj.loginUser.u_pro_img = ""
//
//                                let imgPath = appDelegateObj.loginUser.u_pro_img
//                                //print(imgPath)
//                                self.imgBorder.sd_setImage(with: URL(string: imgPath), placeholderImage: UIImage(named: "userDefault"))
                                
                                self.getProfileDetail()

                                
//                                let nc = NotificationCenter.default
//                                nc.post(name: Notification.Name(kPostUpdateSideMenuProfile), object: nil)
//
//                                let nc1 = NotificationCenter.default
//                                nc1.post(name: Notification.Name(kGetProfileUpdate), object: nil)
                                
                                self.navigationController?.popViewController(animated: true)

                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                        }else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                            
                        else
                        {
                            // //print(response.result.value?.message)
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.removeProfileImage()
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
    func updateProfileDetail()
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            /*
             
             
             u_id
             u_fname
             u_lname
             u_contact_1
             */
            let strContact = self.txtContactNumber.text
            
            let parsedContact = strContact!.replacingOccurrences(of: "+1 ", with: "")

            
            
            SVProgressHUD.show()
            let param = ["u_id":appDelegateObj.loginUser.u_id,"u_fname":txtName.text!,"u_lname":txtLastName.text!,"u_contact_1":parsedContact] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.update_Profile.rawValue
            
            //            let strAuth = "Bearer " + appDelegateObj.loginUser.token
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            //print(header)
            
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<SignInServices>) in
                    
                    SVProgressHUD.dismiss()
                    
                    //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            
                            //                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            // create the alert
                            let alert = UIAlertController(title: strAppName, message: (response.value?.message)!, preferredStyle: UIAlertControllerStyle.alert)
                            
                            alert.addAction(UIAlertAction(title: strOK, style: UIAlertActionStyle.default, handler: { action in
                                
                                let nc1 = NotificationCenter.default
                                nc1.post(name: Notification.Name(kProfileEdit), object: nil)

                                self.navigationController?.popViewController(animated: true)
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                        }else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                            
                        else
                        {
                            // //print(response.result.value?.message)
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.updateProfileDetail()
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
}


extension FeedbackVC{
    
    func addFeedbackApi()
    {
        view.endEditing(true)
        
        
        if currentReachabilityStatus != .notReachable
        {
            /*
             
             feed_name
             feed_mobile
             feed_email
             feed_comment
             u_id
             
             */
            
            
            SVProgressHUD.show()
            let param = ["u_id":appDelegateObj.loginUser.u_id,"feed_name":txtName.text!,"feed_mobile":txtContact.text!,"feed_email":txtEmail.text!,"feed_comment":txtMessage.text!] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.addfeed.rawValue
            
            //            let strAuth = "Bearer " + appDelegateObj.loginUser.token
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            //print(header)
            
            Alamofire.upload(multipartFormData:
                {
                    (multipartFormData) in
                    
                    if self.IsImageUploaded == true{
                        multipartFormData.append(UIImageJPEGRepresentation(self.imgAddImage.image!, 0.1)!, withName: "feed_image", fileName: "file.jpeg", mimeType: "image/jpeg")
                        //print("image sent")
                    }else{
                        //print("image not available")
                    }
                    for (key, value) in param
                    {
                        multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)

                    }
            }, to:strAPIPath,headers:header)
            { (result) in
                switch result {
                case .success(let upload,_,_ ):
                    upload.uploadProgress(closure: { (progress) in
                        ////print progress
                    })
                    upload.responseObject{ (response: DataResponse<BookmarkModel>) in
                        
                        SVProgressHUD.dismiss()
                        
                        //print(response)
                        
                        if response.result.value != nil {
                            
                            if response.result.value?.status_code == "200"{
                                
                                //let responseValue = response.result.value
                                
                                //                            self.txtName.text = ""
                                //                            self.txtContact.text = ""
                                //                            self.txtEmail.text = ""
                                self.txtMessage.text = ""
                                self.imgAddImage.image = UIImage(named: "plus-box")
                                self.IsImageUploaded = false
                                
                                self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                                
                            }else if response.result.value?.status_code == "401"{
                                appDelegateObj.logoutAndMoveToLoginView()
                            }
                                
                            else
                            {
                                // //print(response.result.value?.message)
                                self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                                
                            }
                            
                        }
                    }
                case .failure(let error):
                    if self.checkErrorTypeNetworkLost(error: error)
                    {
                        self.addFeedbackApi()
                    }
                    else
                    {
                        SVProgressHUD.dismiss()
                        
                        self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                        
                        //showAlert(error.localizedDescription)
                    }
                    break
                }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
}

extension InvestmentSummaryVC {
    
    func AssetInvSummApi(strAId:String,strSection_isDynamicTF:String,strSection_id:String,strC_type:String,strIs_group:String)
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            /*
             u_id
             a_id
             section_isDynamic  //false
             section_id
             c_type
             is_group   //Group 1, or 0
             */
            
            SVProgressHUD.show()
            let param = ["u_id":appDelegateObj.loginUser.u_id,"a_id":strAId,"section_isDynamic":strSection_isDynamicTF,"section_id":strSection_id,"c_type":strC_type,"is_group":strIs_group] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.asset_manage_list.rawValue
            
            //            let strAuth = "Bearer " + appDelegateObj.loginUser.token
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            //print(header)
            
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<GetInvstSummList>) in
                    
                    SVProgressHUD.dismiss()
                    
                    //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            let responseValue = response.result.value
                            
                            //                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            let strOne = responseValue?.data.recent_update.q_general
                            let strTwo = responseValue?.data.recent_update.g_i_legal
                            let strThree = responseValue?.data.recent_update.q_footnotes
                            
                            self.arrayRowTitle.removeAllObjects()
                            
                            self.arrayRowTitle.add(strOne!)
                            self.arrayRowTitle.add(strTwo!)
                            self.arrayRowTitle.add(strThree!)

                            self.tblInvesrmentSummary.reloadData()
                            self.strChartName = (responseValue?.data.recent_update.q_chart_imgs.doc_name)!
                            self.strImageUrl = (responseValue?.data.recent_update.q_chart_imgs.document)!
                            
                            self.lblTitleDate.text = responseValue?.data.recent_update.q_title_date
                            
                            if self.strImageUrl != ""{
                                let fullNameArr = self.strImageUrl.components(separatedBy: "||")
                                self.arrImage.removeAll()
                                self.arrPdf.removeAllObjects()
                                
                                for img in fullNameArr {
                                    
                                    let url = URL(string:img)
                                    
                                    let strExtension = url?.pathExtension
                                    if strExtension == "pdf"{
                                        self.IsPDf = true
                                        self.arrPdf.add(img)
                                    }else{
//                                        let data = try? Data(contentsOf: url!)
//                                        let image: UIImage = UIImage(data: data!)!
//                                        self.arrImage.append(image)
                                        
                                        if let data = try? Data(contentsOf: url!) {
                                            let image: UIImage = UIImage(data: data)!
                                            self.arrImage.append(image)
                                            
                                        }
                                    }
                                    
                                    
                                }
                            }
                            
                        }else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                            
                        else
                        {
                            // //print(response.result.value?.message)
                            //                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.AssetInvSummApi(strAId: self.objSection.a_id, strSection_isDynamicTF: self.objSection.section_isDynamic, strSection_id: self.objSection.section_id, strC_type: self.objSection.c_type, strIs_group: self.objSection.is_group)
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
    
    
}

extension DistributionGuideVC{
    
    func AssetDisGuidenceApi(strAId:String,strSection_isDynamicTF:String,strSection_id:String,strC_type:String,strIs_group:String)
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            /*
             u_id
             a_id
             section_isDynamic  //false
             section_id
             c_type
             is_group   //Group 1, or 0
             */
            
            SVProgressHUD.show()
            let param = ["u_id":appDelegateObj.loginUser.u_id,"a_id":strAId,"section_isDynamic":strSection_isDynamicTF,"section_id":strSection_id,"c_type":strC_type,"is_group":strIs_group] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.asset_manage_list.rawValue
            
            //            let strAuth = "Bearer " + appDelegateObj.loginUser.token
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            //print(header)
            
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<GetDistGuidanceList>) in
                    
                    SVProgressHUD.dismiss()
                    
                    //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            let responseValue = response.result.value
                            
                           
                            self.displayDisGuidence(value : (responseValue?.data.distribuance_guidance)!)
                            
                        }
                        else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                        else
                        {
                            // //print(response.result.value?.message)
                            //                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            
                            self.AssetDisGuidenceApi(strAId:appDelegateObj.ObjSection.a_id , strSection_isDynamicTF:appDelegateObj.ObjSection.section_isDynamic, strSection_id: appDelegateObj.ObjSection.section_id, strC_type: appDelegateObj.ObjSection.c_type, strIs_group: appDelegateObj.ObjSection.is_group)
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
}

extension FinancialsVC{
    
    func AssetFinApi(strAId:String,strSection_isDynamicTF:String,strSection_id:String,strC_type:String,strIs_group:String)
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            /*
             u_id
             a_id
             section_isDynamic  //false
             section_id
             c_type
             is_group   //Group 1, or 0
             */
            
            SVProgressHUD.show()
            let param = ["u_id":appDelegateObj.loginUser.u_id,"a_id":strAId,"section_isDynamic":strSection_isDynamicTF,"section_id":strSection_id,"c_type":strC_type,"is_group":strIs_group] as [String : Any]
            
            print(param)
            
            let strAPIPath = BASE_URL + APIPath.asset_manage_list.rawValue
            
            //            let strAuth = "Bearer " + appDelegateObj.loginUser.token
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
           
            
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<FinDocList>) in
                    
                    SVProgressHUD.dismiss()
                    
                //    print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            let responseValue = response.result.value
                            
                            //                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            self.displayFinance(value: (responseValue?.data.financials_document)!)
                        }
                        else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                            
                        else
                        {
                            // //print(response.result.value?.message)
                            //                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            
                            self.AssetFinApi(strAId:appDelegateObj.ObjSection.a_id , strSection_isDynamicTF:appDelegateObj.ObjSection.section_isDynamic, strSection_id: appDelegateObj.ObjSection.section_id, strC_type: appDelegateObj.ObjSection.c_type, strIs_group: appDelegateObj.ObjSection.is_group)
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
}

extension RecentUpdateVC{
    
    func QuaterlyUpdatesApi(strAId:String,strSection_isDynamicTF:String,strSection_id:String,strC_type:String,strIs_group:String)
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            /*
             u_id
             a_id
             section_isDynamic  //false
             section_id
             c_type
             is_group   //Group 1, or 0
             */
            
            SVProgressHUD.show()
            let param = ["u_id":appDelegateObj.loginUser.u_id,"a_id":strAId,"section_isDynamic":strSection_isDynamicTF,"section_id":strSection_id,"c_type":strC_type,"is_group":strIs_group] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.asset_manage_list.rawValue
            
            //            let strAuth = "Bearer " + appDelegateObj.loginUser.token
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            //print(header)
            
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<QuaterlyList>) in
                    
                    SVProgressHUD.dismiss()
                    
                    //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            let responseValue = response.result.value
                            
                            self.displayQuaterlyUpdates(value: (responseValue?.data.recent_update)!, indexOrder: (responseValue?.data.index)!)
                            
                        }
                        else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            
                            self.QuaterlyUpdatesApi(strAId:appDelegateObj.ObjSection.a_id , strSection_isDynamicTF:appDelegateObj.ObjSection.section_isDynamic, strSection_id: appDelegateObj.ObjSection.section_id, strC_type: appDelegateObj.ObjSection.c_type, strIs_group: appDelegateObj.ObjSection.is_group)
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
}

extension NewSectionVC{
    
    func NewSectionApi(strAId:String,strSection_isDynamicTF:String,strSection_id:String,strC_type:String,strIs_group:String)
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            /*
             u_id
             a_id
             section_isDynamic  //false
             section_id
             c_type
             is_group   //Group 1, or 0
             */
            
            SVProgressHUD.show()
            let param = ["u_id":appDelegateObj.loginUser.u_id,"a_id":strAId,"section_isDynamic":strSection_isDynamicTF,"section_id":strSection_id,"c_type":strC_type,"is_group":strIs_group] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.asset_manage_list.rawValue
            
            //            let strAuth = "Bearer " + appDelegateObj.loginUser.token
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            //print(header)
            
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<NewSectionList>) in
                    
                    SVProgressHUD.dismiss()
                    
                    //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            let responseValue = response.result.value
                            
                            //                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                            self.displayNewSection(value: (responseValue?.data)!)
                            
                        }
                        else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                            
                        else
                        {
                            // //print(response.result.value?.message)
                            //                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            
                            self.NewSectionApi(strAId:appDelegateObj.ObjSection.a_id , strSection_isDynamicTF:appDelegateObj.ObjSection.section_isDynamic, strSection_id: appDelegateObj.ObjSection.section_id, strC_type: appDelegateObj.ObjSection.c_type, strIs_group: appDelegateObj.ObjSection.is_group)
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
}

extension LeftMenuViewController{
    
    func logoutApi()
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            /*
             
             
             u_id
             
             */
            
            SVProgressHUD.show()
            let param = ["u_id":appDelegateObj.loginUser.u_id] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.logout.rawValue
            
            //            let strAuth = "Bearer " + appDelegateObj.loginUser.token
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            //print(header)
            
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<LogoutServices>) in
                    
                    SVProgressHUD.dismiss()
                    
                    //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            //                            let responseValue = response.result.value
                            
                            //                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            self.logoutUserApi()
                            
                        }else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                            
                        else
                        {
                            // //print(response.result.value?.message)
                            //                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.addBookmarkApi(strPostId:appDelegateObj.strPostId , strPostTypeId:appDelegateObj.strPostTypeId)
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
}

extension ForgotPasswordEmailVC{
    
    func forgotApiCall()
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            SVProgressHUD.show()
            let param = ["forgot_field":txtEmail.text!] as [String : Any]
            
            //   //print(param)
            
            let strAPIPath = BASE_URL + APIPath.forgotPass.rawValue
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: nil)
                .responseObject { (response: DataResponse<ForgotServices>) in
                    
                    SVProgressHUD.dismiss()
                    
                      //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            let responseValue = response.result.value?.data
                            
                            self.forgotApiResponse(strOtp: (responseValue?.otp)!, strUser: (responseValue?.u_id)!)
                            
                        }
                            
                        else
                        {
                            // //print(response.result.value?.message)
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.forgotApiCall()
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
    func userExistApiCall()
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            SVProgressHUD.show()
            let param = ["u_email":txtEmail.text!] as [String : Any]
            
            //   //print(param)
            
            let strAPIPath = BASE_URL + APIPath.check_login.rawValue
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: nil)
                .responseObject { (response: DataResponse<ForgotServices>) in
                    
                    SVProgressHUD.dismiss()
                    
                    //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            let responseValue = response.result.value?.data

                            if responseValue?.otp != ""{
                                
                                //password not set
                                self.forgotApiResponse(strOtp: (responseValue?.otp)!, strUser: (responseValue?.u_id)!)
                                
                            }else{
                                //password set
                                self.MoveToLogin()

                            }
                            
                            
                        }
                        else
                        {
                            // //print(response.result.value?.message)
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.forgotApiCall()
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
}


extension NewsDetailsVC {
    
    func newsDetailApi(strNId:String)
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            /*
             
             n_id
             u_id
             
             */
            
            SVProgressHUD.show()
            let param = ["u_id":appDelegateObj.loginUser.u_id,"n_id":strNId] as [String : Any]
            
            //print(param)
            
            let strAPIPath = BASE_URL + APIPath.newsDetail.rawValue
            
            //            let strAuth = "Bearer " + appDelegateObj.loginUser.token
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            //print(header)
            
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<NewsDetailModel>) in
                    
                    SVProgressHUD.dismiss()
                    
                    //print(response)
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            
                            let responseValue = response.result.value

                            self.objNews_detail = (responseValue?.data.result)!
//                            self.viewDidLoad()
//                            self.viewWillAppear(true)
//                            self.view.setNeedsDisplay()
                            self.setupInitialView()

                        }
                            
                        else
                        {
                            // //print(response.result.value?.message)
                            //                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.newsDetailApi(strNId: strNId)
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
}

extension TermsDetailsVC{
    func getTerms()
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {

            SVProgressHUD.show()
            let param = ["u_id":appDelegateObj.loginUser.u_id] as [String : Any]
            
          //  //print(param)
            
            let strAPIPath = BASE_URL + APIPath.term.rawValue
            
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<getTermsDetails>) in
                    
                    SVProgressHUD.dismiss()
                
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                            //print("Get Terms")
                            
                            let responseValue = response.result.value?.data
                            
                            if responseValue?.result.count == 0 {
                                
                            }
                            else{
                                self.txtViewDetailsTerms.text = responseValue?.result[0].terms
                            }
                            
                        }else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                            
                        else
                        {
                            // //print(response.result.value?.message)
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.getTerms()
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
    
    func getWhatsApi()
    {
        view.endEditing(true)
        if currentReachabilityStatus != .notReachable
        {
            
            SVProgressHUD.show()
            let param = ["u_id":appDelegateObj.loginUser.u_id] as [String : Any]
            
            //  //print(param)
            
            let strAPIPath = BASE_URL + APIPath.whatsNew.rawValue
            
            let header = ["u_access_token":appDelegateObj.loginUser.u_access_token,"Content-Type":"application/x-www-form-urlencoded"]
            
            Alamofire.request(strAPIPath,method:.post , parameters:param, headers: header)
                .responseObject { (response: DataResponse<getWhatsNew>) in
                    
                    SVProgressHUD.dismiss()
                    
                    if response.result.value != nil {
                        
                        if response.result.value?.status_code == "200"{
                           
                            
                            let responseValue = response.result.value?.data
                            
                            if responseValue?.result.count == 0 {
                                
                            }
                            else{
                               // self.txtViewDetailsTerms.text = responseValue?.result
                                
                                let responseString = responseValue?.result
                                
                                self.txtViewDetailsTerms.text = responseString?.htmlToString
                            }
                            
                        }else if response.result.value?.status_code == "401"{
                            appDelegateObj.logoutAndMoveToLoginView()
                        }
                            
                        else
                        {
                            // //print(response.result.value?.message)
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: (response.value?.message)!, viewcontrolelr: self)
                            
                        }
                        
                    }
                }
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        break
                    case .failure(let error):
                        if self.checkErrorTypeNetworkLost(error: error)
                        {
                            self.getTerms()
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                            
                            self.ShowAlertDisplay(titleObj: strAppName, messageObj: error.localizedDescription, viewcontrolelr: self)
                            
                            //showAlert(error.localizedDescription)
                        }
                        break
                    }
            }
            
        }
        else{
            self.ShowAlertDisplay(titleObj: strAppName, messageObj: MsgNoInternet, viewcontrolelr: self)
        }
    }
}
