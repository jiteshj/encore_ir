//
//  Response_Class.swift
//  Encore IR
//
//  Created by Trivedi Sagar on 15/05/19.
//  Copyright © 2018 Sagar Trivedi. All rights reserved.
//

import Foundation
import UIKit
import AlamofireJsonToObjects
import EVReflection

/*****  Common *****/
class ERServices: EVNetworkingObject {
    var status = ""
    var error = ""
    var message = ""
}

/*****  Login *****/
class SignInServices: EVNetworkingObject {
    var status = ""
    var status_code = ""
    var status_key = ""
    var message = ""
   // var setting = [UserSettingData]()
    var data = LoginData()
}
class LoginData: EVNetworkingObject{
    var user_data = LoginUserData()
    var amountData = [LoginAmountData]()
    var codinatoer_detail = LoginCodinatorDetail()
    var associative_detail = LoginAssociativeDetails()
}
class UserSettingData : EVNetworkingObject{
    
}
class LoginUserData: EVNetworkingObject{
    var u_id = ""
    var u_fname = ""
    var u_lname = ""
    var u_email = ""
    var u_contact_1 = ""
    var u_contact_2 = ""
    var u_contact_3 = ""
    var u_country = ""
    var u_state = ""
    var u_city = ""
    var v_zip = ""
    var v_since = ""
    var u_address_1 = ""
    var u_address_2 = ""
    var u_pro_img = ""
    var u_password = ""
    var u_notification_flag = ""
    var u_biometric_authentication = ""
    var u_allocate_pro = ""
    var a_id = ""
    var u_co_ordinator = ""
    var u_associative = ""
    var i_secondary_investor = ""
    var i_secondary_investor_pro = ""
    var u_access_token = ""
    var u_device_token = ""
    var t_firebase_token = ""
    var u_d_id = ""
    var u_device_type = ""
    var u_online_status = ""
    var u_device_Ip = ""
    var u_otp_verification = ""
    var u_status = ""
    var u_created_date = ""
    var u_updated_date = ""
    var u_profile_set = ""
    var u_tc_agree = ""
    var en_secondary_investor_status = ""
}
class LoginAmountData: EVNetworkingObject{
    var i_assets_id = ""
    var a_name = ""
    var a_id = ""
    var v_amount = ""
    var dt_funded = ""
    var is_group = ""
    var g_a_id = ""


}
class LoginCodinatorDetail: EVNetworkingObject{
    var c_id = ""
    var c_fname = ""
    var c_lname = ""
    var c_email = ""
    var c_contact = ""
    var c_assign_project = ""
    var c_image = ""
    var c_o_contact = ""

}
class LoginAssociativeDetails: EVNetworkingObject{
    var a_id = ""
    var a_fname = ""
    var a_lname = ""
    var a_email = ""
    var a_contact = ""
    var a_assign_project = ""
    var a_image = ""
    var a_o_contact = ""

}
/*****  End Login *****/
/*****  Forgot *****/
class ForgotServices: EVNetworkingObject {
    var status = ""
    var status_code = ""
    var status_key = ""
    var message = ""
    // var setting = [UserSettingData]()
    var data = ForgotData()
    
}
class ForgotData: EVNetworkingObject{
    var otp = ""
    var u_id = ""
}
/*****  End Forgot *****/

/*****  Reset *****/
class ResetServices: EVNetworkingObject {
    var status = ""
    var status_code = ""
    var status_key = ""
    var message = ""
    // var setting = [UserSettingData]()
    var data = ForgotData()
}
class ResetData: EVNetworkingObject{
    var u_id = ""
}
/*****  End Reset *****/

/*****  Library *****/
class LibraryServices: EVNetworkingObject {
    var status = ""
    var status_code = ""
    var status_key = ""
    var message = ""
    var setting = LibrarySetting()
    var data = LibrarySetting()
}
class LibrarySetting: EVNetworkingObject{
    var document_detail = [LibraryDetails]()
    var total_document = ""
}
class LibraryDetails: EVNetworkingObject{
    var doc_id = ""
    var doc_name = ""
    var doc_desc = ""
    var doc_type = ""
    var document = ""
    var d_created_date = ""
    var created_by = ""
    var is_viewed = ""
}
/*****  End Library *****/

/*****  Document main *****/
class DocumentMain: EVNetworkingObject {
    var status = ""
    var status_code = ""
    var status_key = ""
    var message = ""
    var setting = DocumentMainSetting()
}
class DocumentMainSetting: EVNetworkingObject{
    var saprate_data = [DocumentMainData]()
    var folder_data = [DocumentMainData]()
}
//class folder_dataDetails: EVNetworkingObject{
//    var folder_name = ""
//    var id = ""
//    var data = [saprate_dataDetails]()
//}
class saprate_dataDetails: EVNetworkingObject{
    var doc_id = ""
    var doc_name = ""
    var doc_desc = ""
    var doc_type = ""
    var document = ""
    var d_created_date = ""
    var created_by = ""
    var is_viewed = ""
    var f_id = ""
    var project_id = ""
    var d_created_by = ""
    var d_updated_date = ""
    var d_update_by = ""

}

class DocumentMainData: EVNetworkingObject{
    var doc_id = ""
    var doc_name = ""
    var doc_desc = ""
    var doc_type = ""
    var document = ""
    var d_created_date = ""
    var created_by = ""
    var is_viewed = ""
    var f_id = ""
    var project_id = ""
    var d_created_by = ""
    var d_updated_date = ""
    var d_update_by = ""
    
    var folder_name = ""
    var id = ""
    var data = [saprate_dataDetails]()
    
    var is_new = ""

}
/*****  End Document main *****/

/*
 {
 "status": true,
 "status_code": "200",
 "status_key": "success",
 "message": "Successfully fetch data!",
 "setting": {
 "folder_data": {
 "1": {
 "folder_name": "My Folder",
 "id": 18,
 "data": [
 {
 "doc_id": "77",
 "f_id": "18",
 "doc_name": "",
 "doc_desc": "",
 "document": "http://52.38.87.73/assets/uploads/documents/All_Er_Daily_Status_6-22-2019_ER61$$1561783124.pdf",
 "doc_type": "pdf",
 "project_id": "18,20,21,29,31,32,33,34,37,48,64",
 "d_created_date": "2019-06-28 23:38:49",
 "d_created_by": "1",
 "d_updated_date": "0000-00-00 00:00:00",
 "d_update_by": "0"
 },
 {
 "doc_id": "78",
 "f_id": "18",
 "doc_name": "",
 "doc_desc": "",
 "document": "http://52.38.87.73/assets/uploads/documents/All_Er_Daily_Status_6-21-2019_ER78$$1561783124.pdf",
 "doc_type": "pdf",
 "project_id": "18,20,21,29,31,32,33,34,37,48,64",
 "d_created_date": "2019-06-28 23:38:49",
 "d_created_by": "1",
 "d_updated_date": "0000-00-00 00:00:00",
 "d_update_by": "0"
 }
 ]
 }
 },
 "saprate_data": [
 {
 "doc_id": "63",
 "f_id": null,
 "doc_name": "",
 "doc_desc": "",
 "document": "http://52.38.87.73/assets/uploads/documents/2018Q4_EC-InvestorReport_Bedford_1561557102.pdf",
 "doc_type": "pdf",
 "project_id": "18",
 "d_created_date": "2019-06-26 08:52:11",
 "d_created_by": "10",
 "d_updated_date": "0000-00-00 00:00:00",
 "d_update_by": "0"
 },
 {
 "doc_id": "2",
 "f_id": null,
 "doc_name": "2018Q3_EC-InvestorReport_FriscoSquare",
 "doc_desc": "2018Q3_EC-InvestorReport_FriscoSquare.pdf",
 "document": "http://52.38.87.73/assets/uploads/documents/documents1544515227.pdf",
 "doc_type": "pdf",
 "project_id": "21",
 "d_created_date": "2018-12-11 13:30:27",
 "d_created_by": "1",
 "d_updated_date": "2019-05-29 05:32:45",
 "d_update_by": "1"
 },
 {
 "doc_id": "4",
 "f_id": null,
 "doc_name": "2018Q3_EC-InvestorReport_SoCongress",
 "doc_desc": "2018Q3_EC-InvestorReport_SoCongress.pdf",
 "document": "http://52.38.87.73/assets/uploads/documents/documents1544515274.pdf",
 "doc_type": "pdf",
 "project_id": "41",
 "d_created_date": "2018-12-11 13:31:14",
 "d_created_by": "1",
 "d_updated_date": "2019-06-13 06:02:38",
 "d_update_by": "1"
 },
 {
 "doc_id": "5",
 "f_id": null,
 "doc_name": "2018Q3_EC-InvestorReport_TwoForest",
 "doc_desc": "2018Q3_EC-InvestorReport_TwoForest.pdf ",
 "document": "http://52.38.87.73/assets/uploads/documents/documents1544515298.pdf",
 "doc_type": "pdf",
 "project_id": "40",
 "d_created_date": "2018-12-11 13:31:38",
 "d_created_by": "1",
 "d_updated_date": "2019-06-13 06:01:46",
 "d_update_by": "1"
 }
 ]
 },
 "data": []
 }
 */


/*****  Bookmark *****/
class BookMarkNewsServices: EVNetworkingObject {
    var status = ""
    var status_code = ""
    var status_key = ""
    var message = ""
    //var setting = LibrarySetting()
    var data = BookmarkNewsData()
}
class BookmarkNewsData: EVNetworkingObject{
    var news_detail = [Subnews_detail]()
}

class BookMarkProjectServices: EVNetworkingObject {
    var status = ""
    var status_code = ""
    var status_key = ""
    var message = ""
    //var setting = LibrarySetting()
    var data = BookmarkProjectData()
}
class BookmarkProjectData: EVNetworkingObject{
    var project_detail = [AssetProject]()
    var total_project = ""
}

class BookMarkDealServices: EVNetworkingObject {
    var status = ""
    var status_code = ""
    var status_key = ""
    var message = ""
    //var setting = LibrarySetting()
    var data = BookmarkDealsData()
}
class BookmarkDealsData: EVNetworkingObject{
    var deal_detail = [AssetProject]()
    var total_deal = ""
}


/*****  Bookmark End *****/

/*****  Assets *****/
class FilterAssestServices: EVNetworkingObject {
    var status = ""
    var status_code = ""
    var status_key = ""
    var message = ""
    //  var setting = []
    var data = FilterData()
}
class FilterData: EVNetworkingObject{
    var property_list = [FilterPropertyDataList]()
    var quarter_list = [FilterQuarterDataList]()
}
class FilterPropertyDataList : EVNetworkingObject{
    var p_type_id = ""
    var p_type_name = ""
    var is_checked : Bool = true
}
class FilterQuarterDataList : EVNetworkingObject{
    var quarter_id = ""
    var quarter_name = ""
    var is_checked : Bool = false
}

/*****  Assets *****/
class AssetsServices: EVNetworkingObject {
    var status = ""
    var status_code = ""
    var status_key = ""
    var message = ""
  //  var setting = []
    var data = AssetsServicesData()
}
class AssetsServicesData: EVNetworkingObject{
    var project_list = [AssetProject]()
    var total_project = ""
}
class AssetProject: EVNetworkingObject{
    var a_id = ""
    var a_name = ""
    var p_type_name = ""
    var a_description = ""
    var a_created_date = ""
    var a_gp_id = ""
    var created_by = ""
    var g_a_group_name = ""
    var a_asset_img = ""
    var a_asset_overview = ""
    var a_ownership = ""
    var a_city = ""
    var a_state = ""
    var a_deal_status = ""
    var q_period_q = ""
    var q_period_y = ""
    var is_group = ""
    var group_name = ""
    var is_viewed = ""
    var is_favourite = ""
    var is_bookmark = ""
    
    var g_i_period_y = ""
    var g_i_period_q = ""
    var amount = ""
    var date_funded = ""

}
/*****  End Assets *****/

/*****  Funds *****/
class FundsServices: EVNetworkingObject {
    var status = ""
    var status_code = ""
    var status_key = ""
    var message = ""
    //  var setting = []
    var data = FundServicesData()
}
class FundServicesData: EVNetworkingObject{
    var group_detail = groupDetailsList()
    var section_list = [sectionlistDetail]()
    var project_detail = [projectdetailList]()
    var image_list = image_listFundDetail()
    var funds_detail = [fundDetailList]()
    var total_sub_deal = ""
}
class groupDetailsList: EVNetworkingObject{
    
    var g_a_id = ""
    var g_a_group_name = ""
    var g_a_photos = ""
    var g_a_overview = ""
    var g_a_desc = ""
    var g_a_status = ""
    var created_date = ""

}
class fundDetailList: EVNetworkingObject{
    var g_a_id = ""
    var g_p_id = ""
    var g_a_group_name = ""
    var g_a_photos = ""
    var g_a_overview = ""
    var g_a_desc = ""
    var g_a_status = ""
    var g_i_period_y = ""
    var g_i_period_q = ""
}

class image_listFundDetail: EVNetworkingObject{
    var fund_img = [a_asset_imgDetail]()
}

class sectionlistDetail: EVNetworkingObject{
    
    var c_id = ""
    var section_id = ""
    var section_type = ""
    var section_name = ""
    var section_screen_name = ""
    var section_display_name = ""
    var section_isDynamic = ""
    var section_category = ""
    var hide_Show = ""
    
}

class projectdetailList: EVNetworkingObject{
    
    var a_id = ""
    var a_name = ""
    var p_type_name = ""
    var a_created_date = ""
    var a_gp_id = ""
    var created_by = ""
    var g_a_group_name = ""
    var a_asset_img = ""
    
}
/*****  End Funds *****/

/*****  Filter *****/
class Filter_Model: EVObject{
    var filterTitle: String = ""
    var isSelected: Bool = false
}
/*****  Filter Login *****/


/*****  News  *****/
class News_Main: EVObject{
    
    var status = ""
    var status_code = ""
    var status_key = ""
    var message = ""
    var setting = [NewsSettingDetails]()
    var data = NewsDataDetails()
}

class NewsSettingDetails: EVObject{
    
}

class NewsDataDetails: EVObject{
    var Deal_detail = [AssetProject]()
    var news_detail = [Subnews_detail]()
}

class SubDeal_detail: EVObject{
    
    var a_id = ""
    var a_name = ""
    var a_description = ""
    var n_type = ""
    var p_type_name = ""
    var a_created_date = ""
    var a_asset_img = ""
    var is_bookmark = ""
    var a_gp_id = ""
    var is_group = ""
    var g_a_group_name = ""
    var is_viewed = ""

}

class Subnews_detail: EVObject{
    var n_id = ""
    var n_headline = ""
    var n_p_dec = ""
    var n_type = ""
    var n_created_date = ""
    var n_image = ""
    var n_link = ""
    var is_bookmark = ""
    var n_thumb = ""
    var n_extension = ""
    var n_video = ""
    var is_viewed = ""
    var id_type = ""
    var n_is_video_news = ""
    var n_video_link = ""



}


/*****  End News  *****/

/*****  Add NewsDetail  *****/

class NewsDetailModel: EVNetworkingObject{
    
    var status = ""
    var status_code = ""
    var status_key = ""
    var message = ""
//    var setting = NSMutableArray()
    var data = NewsDetailData()
    
}

class NewsDetailData: EVNetworkingObject{
    var result = Subnews_detail()
}


/*****  End NewsDetail  *****/


/***** ADD remove Bookmark *****/
class BookmarkModel: EVNetworkingObject{
   
    var status = ""
    var status_code = ""
    var status_key = ""
    var message = ""
    var setting = NSMutableArray()
    var data = NSMutableArray()
    
}
/*****  End ADD remove Bookmark *****/


/*****  Get Profile *****/
class ProfileModel: EVNetworkingObject{
    
    var status = ""
    var status_code = ""
    var status_key = ""
    var message = ""
//    var setting = [ProSettingDetails]()
    var data = ProfileDataDetails()
    
}

class ProSettingDetails: EVNetworkingObject{
    
}

class ProfileDataDetails: EVNetworkingObject{
//    var secondary_investor = [ProSecInvestorDetails]()
    var amountData = [ProAmountDataDetails]()
    var user_data = ProUserDataDetails()
    var codinatoer_detail = ProCodinatoerDetails()
    var associative_detail = ProAssociativeDetails()

}

class ProSecInvestorDetails: EVNetworkingObject{
    //null
}

class ProAmountDataDetails: EVNetworkingObject{
    
    var i_assets_id = ""
    var a_name = ""
    var a_id = ""
    var v_amount = ""
    var dt_funded = ""
    
}
class ProUserDataDetails: EVNetworkingObject{
    
    var u_id = ""
    var u_fname = ""
    var u_lname = ""
    var u_email = ""
    var u_contact_1 = ""
    var u_contact_2 = ""
    var u_contact_3 = ""
    var u_country = ""
    var u_state = ""
    var u_city = ""
    var v_zip = ""
    var u_address_1 = ""
    var u_address_2 = ""
    var u_pro_img = ""
    var u_password = ""
    var u_notification_flag = ""
    var u_biometric_authentication = ""
    var u_allocate_pro = ""
    var a_id = ""
    var u_co_ordinator = ""
    var u_associative = ""
    var i_secondary_investor = ""
    var i_secondary_investor_pro = ""
    var u_access_token = ""
    var u_device_token = ""
    var t_firebase_token = ""
    var u_d_id = ""
    var u_device_type = ""
    var u_online_status = ""
    var u_device_Ip = ""
    var u_otp_verification = ""
    var u_status = ""
    var u_profile_set = ""
    var u_tc_agree = ""
    var en_secondary_investor_status = ""

}
class ProCodinatoerDetails: EVNetworkingObject{
    
    var c_id = ""
    var c_fname = ""
    var c_lname = ""
    var c_email = ""
    var c_contact = ""
    var c_o_contact = ""
    var c_assign_project = ""
    var c_image = ""
    
}
class ProAssociativeDetails: EVNetworkingObject{
    
    var a_id = ""
    var a_fname = ""
    var a_lname = ""
    var a_email = ""
    var a_contact = ""
    var a_o_contact = ""
    var a_image = ""
    
}
/*****  End Get Profile *****/


/*****  Profile image update *****/

class ProfileimageUpdate: EVNetworkingObject {
    var status = ""
    var status_code = ""
    var status_key = ""
    var message = ""
    //    var setting = ""
    var data = ProfileimageUpdateDetail()
}

class ProfileimageUpdateDetail: EVNetworkingObject {
    var move_upload_file = ""
    var u_id = ""
    var u_pro_img = ""
    var upload_status = ""
}

/*****  End Profile image update *****/

/*****  Assets Details *****/
class AssetsDetailsServices: EVNetworkingObject {
    var status = ""
    var status_code = ""
    var status_key = ""
    var message = ""
    //  var setting = []
    var data = AssetsDetailsServicesData()
}
class AssetsDetailsServicesData: EVNetworkingObject{
    var project_detail = project_detailDetail()
    var asset_detail = asset_detailDetail()
    var image_list = image_listDetail()
    var project_section_list = [project_section_listDetail]()
}

class project_detailDetail: EVNetworkingObject{
    var a_id = ""
    var asset_name = ""
    var a_city = ""
    var a_state = ""
}

class asset_detailDetail: EVNetworkingObject{
    var sector = ""
    var a_asset_overview = ""
    var a_ownership = ""
    var a_build_year = ""
    var a_asset_location = ""
    var a_asset_address = ""
    var a_status = ""
    var a_asset_desc = ""
    var a_y_renovated = ""
    var a_created_date = ""
    var latitude = ""
    var longitude = ""
    var bookmark = ""
}

class image_listDetail: EVNetworkingObject{
    var a_asset_img = [a_asset_imgDetail]()
}

class a_asset_imgDetail: EVNetworkingObject{
    var url = ""
}

class project_section_listDetail: EVNetworkingObject{
    
    var c_id = ""
    var section_id = ""
    var section_name = ""
    var section_type = ""
    var section_screen_name = ""
    var section_display_name = ""
    var section_isDynamic = ""
    var section_category = ""
    var hide_Show = ""
    
}

/*****  End Assets Details *****/

/*****  Dis Guidance Detail *****/

class GetDistGuidanceList: EVNetworkingObject {
    var status = ""
    var status_code = ""
    var status_key = ""
    var message = ""
    //    var setting = ""
    var data = GetAssetManageListData()
}

class GetAssetManageListData: EVNetworkingObject {
    var distribuance_guidance = DistGuidenceDetail()
}

class DistGuidenceDetail: EVNetworkingObject {
    
    var d_chart_img = financials_documentDetail()
    var d_general_update = ""
    var d_praposed_stratagy = ""
    var d_distribution_guidance = ""
    var d_disclaimer = ""
    var d_footnotes = ""
    var d_development_timeline = ""

}

/*****  End Dis Guidance Detail *****/

/*****  Invest Summ Detail *****/

class GetInvstSummList: EVNetworkingObject {
    var status = ""
    var status_code = ""
    var status_key = ""
    var message = ""
    //    var setting = ""
    var data = GetInvstSummListData()
}

class GetInvstSummListData: EVNetworkingObject {
    var recent_update = recent_updateDetail()
}

class recent_updateDetail: EVNetworkingObject {
    
    var q_chart_imgs = financials_documentDetail()
    var q_period_y = ""
    var q_period_q = ""
    var q_general = ""
    var g_i_legal = ""
    var q_footnotes = ""
    var q_title_date = ""
    var q_construction_img = [q_construction_imgUrl]()
}

/*****  End Invest Summ Detail *****/


/*****  Financial Doc Detail *****/

class FinDocList: EVNetworkingObject {
    var status = ""
    var status_code = ""
    var status_key = ""
    var message = ""
    //    var setting = ""
    var data = FinDocListData()
}

class FinDocListData: EVNetworkingObject {
    var financials_document = financials_documentList()
}

class financials_documentList: EVNetworkingObject {
    
    var f_income_s_img = [financials_documentDetail]()
    var f_blc_sheet_img = [financials_documentDetail]()
    var f_summery_img = [financials_documentDetail]()
    var f_disclaimer = ""
    var f_footnotes = ""
    
}

class financials_documentDetail: EVNetworkingObject {
    
    var document = ""
    var doc_name = ""
    
}

class financials_documentDetailCategory: EVNetworkingObject {
    var doc_category = ""
    var documents = [financials_documentDetail]()
}

/*****  End Financial Doc Detail *****/


/*****  Quaterly Updates *****/

class QuaterlyList: EVNetworkingObject {
    var status = ""
    var status_code = ""
    var status_key = ""
    var message = ""
    //    var setting = ""
    var data = QuaterlyListData()
}

class QuaterlyListData: EVNetworkingObject {
    var recent_update = recent_updateList()
    var index = QuaterlyListIndex()
}

class recent_updateList: EVNetworkingObject {
    
    var q_chart_imgs = financials_documentDetail()
    var q_period_y = ""
    var q_period_q = ""
    var q_general = ""
    var q_construction = ""
    var q_leasing = ""
    var q_legal = ""
    var q_performance = ""
    var q_footnotes = ""
    var q_construction_img = [q_construction_imgUrl]()
    var q_title_date = ""
    
}

class q_construction_imgUrl: EVNetworkingObject {
    var url = ""
}

class QuaterlyListIndex :EVNetworkingObject{
    
    var q_legal = ""
    var q_leasing = ""
    var q_construction = ""
    var q_performance = ""

}

/*****  End Quaterly Updates *****/

/*****  New Section *****/

class NewSectionList: EVNetworkingObject {
    var status = ""
    var status_code = ""
    var status_key = ""
    var message = ""
    //    var setting = ""
    var data = NewSectionListData()
}

class NewSectionListData: EVNetworkingObject {
    var text = TextDetailList()
    var image = ImageDetailList()

}

class TextDetailList: EVNetworkingObject {
    
    var c_id = ""
    var value = ""
    var cm_field_title = ""
    var cm_field_type = ""
    var c_name = ""
    
}

class ImageDetailList: EVNetworkingObject {
    
    var c_id = ""
    var value = ""
    var cm_field_title = ""
    var cm_field_type = ""
    var c_name = ""
    
}


/*****  End New Section *****/

/*****  Section obj *****/
class ObjSectionDetail: EVNetworkingObject{
    
    var u_id = ""
    var a_id = ""
    var section_isDynamic = ""
    var section_id = ""
    var c_type = ""
    var is_group = ""
    var section_display_name = ""

    
}
/*****  End Section obj *****/

/*****  Logout *****/
class LogoutServices: EVNetworkingObject {
    var status = ""
    var status_code = ""
    var status_key = ""
    var message = ""
    // var setting = ""
//    var data = ""
}
/***** End Logout *****/

/*****  Counter Detail *****/

class updateCounter: EVNetworkingObject {
    var status = ""
    var status_code = ""
    var status_key = ""
    var message = ""
    //    var setting = ""
    var data = counterGet()
}

class counterGet: EVNetworkingObject {
    var new_asset = ""
    var new_deal = ""
    var new_news = ""
    var new_docs = ""
} 

/*****  End Counterl *****/


/*****  View Detail *****/

class ViewDetails: EVNetworkingObject {
    var status = ""
    var status_code = ""
    var status_key = ""
    var message = ""
    //    var setting = ""
    var data = dataGet()
}

class dataGet: EVNetworkingObject {
    var liked = ""
    var data = dataDetails()
}

class dataDetails: EVNetworkingObject {
    var a_id = ""
    var u_id = ""
    var is_viewed = ""
    var type = ""
}

/*****  End View *****/


/***** Agree Terms *****/

class TermsDetails: EVNetworkingObject {
    var status = ""
    var status_code = ""
    var status_key = ""
    var message = ""
    //    var setting = ""
//    var data = ""
}


/*****  End Agree Terms *****/

/***** Get Terms *****/

class getTermsDetails: EVNetworkingObject {
    var status = ""
    var status_code = ""
    var status_key = ""
    var message = ""
    //    var setting = ""
    var data = getTermsData()
}

class getTermsData: EVNetworkingObject {
    var result = [resultTerms]()
}

class resultTerms: EVNetworkingObject {
    var id = ""
    var terms = ""
}

/*****  End Get Terms *****/


/***** Get Whats New *****/

class getWhatsNew: EVNetworkingObject {
    var status = ""
    var status_code = ""
    var status_key = ""
    var message = ""
    //    var setting = ""
    var data = getWhatsData()
}

class getWhatsData: EVNetworkingObject {
    var result = ""
}

class resultWhatsNew: EVNetworkingObject {
    var id = ""
    var version_name = ""
    var history = ""
}

/*****  End Whats New *****/
