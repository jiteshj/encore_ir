//
//  GlobalMethods.swift

import UIKit
import SafariServices
import SVProgressHUD

class GlobalMethods: NSObject {
    
    //MARK: - Set fontsize
    class func setThemeFontSize(objectSize: CGFloat)-> CGFloat{
        
        var fontSize = objectSize
        
        if appDelegateObj.intFontSize == FontSizeType.Small.rawValue{
            fontSize = fontSize - CGFloat(intFontDifference)
        }else if appDelegateObj.intFontSize == FontSizeType.Large.rawValue{
            fontSize = fontSize + CGFloat(intFontDifference)
        }else if appDelegateObj.intFontSize == FontSizeType.ExtraLarge.rawValue{
            fontSize = fontSize + CGFloat(intFontDifference) + CGFloat(intFontDifference)
        }
        
        return fontSize
    }
   
}
