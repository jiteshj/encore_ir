//
//  AppDelegate.swift
//  Encore IR
//
//  Created by ravi on 04/05/19.
//  Copyright © 2019 ravi. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import LGSideMenuController
import GoogleMaps
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate  {
    
    var window: UIWindow?
    
    var loginUser = LoginUserData()
    var ProAmountDetail = [LoginAmountData]()
    var loginCordDetails = LoginCodinatorDetail()
    var loginAssociatDetails = LoginAssociativeDetails()
    var isShowToAnimation = false
    var isShowSplashScreen = true

    
    var tabBarController : TabbarVC?
    var strPostId = ""
    var strPostTypeId = ""
    
    var ObjSection = ObjSectionDetail()
    var ObjCounter = counterGet()
    var IsThemeLight = false
    var IsRememberMe = false
    
    var intFontSize = FontSizeType.Medium.rawValue

    
    /* DEVICE TOKEN FOR PUSH NOTIFICATION */
    var deviceToken = ""
    var PushNotification_FCMToken = ""
    var googleAPIKEY = "AIzaSyBZLukT2E8YCltS_QfVMUzuW28gCxvsNic"
    //AIzaSyDKozT9lJ6YnwLSwpCHMx296r5lwAzm11c

    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        #if arch(i386) || arch(x86_64)
        //simulator
        deviceToken = "12345678"
        
        #else
        //device
        #endif
        
        
        //google
        GMSServices.provideAPIKey(googleAPIKEY)
        
        
        if let option = launchOptions {
            let info = option[UIApplicationLaunchOptionsKey.remoteNotification]
            if (info != nil) {
                print("notification data\(info!)")
            }}
        
        // For notification
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        // FCM
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        
        registerForPushNotifications()
        
        IQKeyboardManager.shared.enable = true
        
        if UserDefaults.standard.object(forKey: kLoginUser) != nil{
            
            appDelegateObj.ObjCounter.new_news = "0"
            appDelegateObj.ObjCounter.new_asset = "0"
            appDelegateObj.ObjCounter.new_deal = "0"
            appDelegateObj.ObjCounter.new_docs = "0"

            
            do {
                
                let userDefaults = UserDefaults.standard
                let decoded  = userDefaults.object(forKey: kLoginUser) as! Data
                //            let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! LoginUserData
                let decodedTeams = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded)
                appDelegateObj.loginUser = decodedTeams as! LoginUserData
                
                if appDelegateObj.loginUser.u_tc_agree != "0"{
                    self.MoveToDashboard()
                }else{
                    
                }
                
            } catch {
                print(error)
            }
            
            
        }
        
        if UserDefaults.standard.object(forKey: kIsThemeLight) != nil{
            let userDefaults = UserDefaults.standard
            let decoded  = userDefaults.object(forKey: kIsThemeLight) as! String
            if decoded == "1" {
                appDelegateObj.IsThemeLight = true
                print("Ligth Theme mode")
            }else{
                appDelegateObj.IsThemeLight = false
                print("Dark Theme mode")
            }
        }
        else{
            //Default Theme
            appDelegateObj.IsThemeLight = false
            print("Dark Theme mode")
        }
        
        
        /*----- set font size -----*/
        if UserDefaults.standard.object(forKey: kFontSize) != nil{
            let userDefaults = UserDefaults.standard
            let decoded  = userDefaults.object(forKey: kFontSize) as! Int
            appDelegateObj.intFontSize = decoded
        }else{
            UserDefaults.standard.set(appDelegateObj.intFontSize, forKey: kFontSize)
            UserDefaults.standard.synchronize()
        }
        
        
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func MoveToDashboard(){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let navigationController = storyboard.instantiateViewController(withIdentifier: "NavigationController") as! UINavigationController
        navigationController.setViewControllers([storyboard.instantiateViewController(withIdentifier: "TabbarVC")], animated: false)
        
        let mainViewController = storyboard.instantiateInitialViewController() as! MainViewController
        mainViewController.rootViewController = navigationController
        mainViewController.leftViewPresentationStyle = .slideAbove
        
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = mainViewController
        
        UIView.transition(with: window, duration: 0.1, options: [.transitionCrossDissolve], animations: nil, completion: nil)
        
    }
    
    func logoutAndMoveToLoginView(){
        
        UserDefaults.standard.removeObject(forKey: kLoginUser)
        UserDefaults.standard.removeObject(forKey: kLoginUserCheck)
        UserDefaults.standard.synchronize()
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Start", bundle:nil)
        let navigationController = storyBoard.instantiateViewController(withIdentifier: "ChooseNavigationController") as! ChooseNavigationController
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.window!.rootViewController = navigationController
        
    }
    
    // The callback to handle data message received via FCM for devices running iOS 10 or above.
    func applicationReceivedRemoteMessage(_ remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    // MARK:For Push Notification
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        InstanceID.instanceID().instanceID(handler: { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                appDelegateObj.deviceToken = result.token
                appDelegateObj.PushNotification_FCMToken = "\(result.token)"
                
                
            }
        })
        
        Messaging.messaging().apnsToken = deviceToken
        
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        
        
        completionHandler([.alert, .sound])
        
        
        
    }
    
    @available(iOS 10.0, *)
    private func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
    {
        //let aps = response.notification.request.content.userInfo["aps"] as? NSDictionary
        
        if let messageData = response.notification.request.content.userInfo["gcm.notification.data"] {
            let dict = convertToDictionary(text: messageData as! String)
            if ((dict!["type"] as? NSString)?.integerValue) != nil {
                //self.PresentVC(status: status,data: dict!)
            }
        }
        
        UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
        
        switch response.actionIdentifier {
            
        case "action1":
            print("Action First Tapped")
        case "action2":
            print("Action Second Tapped")
        default:
            break
        }
        completionHandler(UIBackgroundFetchResult.newData)
        
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        
        print("user clicked on the notification")
        
        //                            UIApplication.shared.applicationIconBadgeNumber = 0
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNewNotification) , object: nil, userInfo: response.notification.request.content.userInfo)

        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) { // Change `2.0` to the desired number of seconds.
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNewNotification) , object: nil, userInfo: response.notification.request.content.userInfo)

        }
        
        
        completionHandler()
        
//        if let messageData = response.notification.request.content.userInfo["gcm.notification.data"] {
//            let dict = convertToDictionary(text: messageData as! String)
//            if ((dict!["type"] as? NSString)?.integerValue) != nil {
//                //self.PresentVC(status: status,data: dict!)
//            }
//        }
//        //playSound(fileName: "Beep.wav")
//        completionHandler()
    }
    
    //    @available(iOS 10.0, *)
    //    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
    //
    //    }
    
    // MARK:- FCM Receive Data Message -
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        if let messageID = userInfo["speed_"] {
            print("Message ID: \(messageID)")
        }
        print(userInfo)
        
    }
    
    
    // it called after App terminated
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        if let messageID = userInfo[""] {
            print("Message ID: \(messageID)")
        }
        
        print(userInfo)
        
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNewNotification) , object: nil, userInfo: userInfo)

        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    
    @objc func tokenRefreshNotification(notification: NSNotification) {
        // NOTE: It can be nil here
        
        InstanceID.instanceID().instanceID(handler: { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                
                let refreshedToken = result.token
                
                appDelegateObj.PushNotification_FCMToken  = "\(refreshedToken)"
                print("fcm token is ",appDelegateObj.PushNotification_FCMToken )
                
                print("InstanceID token: \(String(describing: refreshedToken))")
                self.connectToFcm()
                
                
            }
        })
        
    }
    
    func connectToFcm() {
        
        InstanceID.instanceID().instanceID(handler: { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                
                //                let refreshedToken = result.token
                
                Messaging.messaging().shouldEstablishDirectChannel = true
                
            }
        })
        
        
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        
        InstanceID.instanceID().instanceID(handler: { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                
                let refreshedToken = result.token
                
                appDelegateObj.PushNotification_FCMToken = refreshedToken
                
            }
        })
        
        //        if let refreshedToken = InstanceID.instanceID().token() {
        //            print("InstanceID token: \(refreshedToken)")
        //            appDelegateObj.PushNotification_FCMToken = refreshedToken
        //        }
        Messaging.messaging().isAutoInitEnabled = true
        
        
        print(appDelegateObj.PushNotification_FCMToken)
        
        Messaging.messaging().subscribe(toTopic: "evt_\(appDelegateObj.PushNotification_FCMToken)") { error in
            
            print("Subscribed to topic")
        }
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func pushNotificationHandler(userInfo: Dictionary<AnyHashable,Any>) {
        print("HANDLER")
        
        // Parse the aps payload
        let apsPayload = userInfo["aps"] as! [String: AnyObject]
        print(apsPayload)
        // Play custom push notification sound (if exists) by parsing out the "sound" key and playing the audio file specified
        // For example, if the incoming payload is: { "sound":"tarzanwut.aiff" } the app will look for the tarzanwut.aiff file in the app bundle and play it
        //        if let mySoundFile : String = apsPayload["sound"] as? String {
        //            playSound(fileName: mySoundFile)
        //        }
        
        
        //playSound(fileName: "Beep.wav")
    }
    // Play the specified audio file with extension
    //    func playSound(fileName: String) {
    //        var sound: SystemSoundID = 0
    //        if let soundURL = Bundle.main.url(forAuxiliaryExecutable: fileName) {
    //            AudioServicesCreateSystemSoundID(soundURL as CFURL, &sound)
    //            AudioServicesPlaySystemSound(sound)
    //        }
    //    }
    
    func registerForPushNotifications() {
        UNUserNotificationCenter.current() // 1
            .requestAuthorization(options: [.alert, .sound, .badge]) { // 2
                granted, error in
                print("Permission granted: \(granted)") // 3
        }
    }
    
    
    //    func speedTopicSubscribe(strImiNo: String){
    //        Messaging.messaging().subscribe(toTopic: "speed_\(strImiNo)") { error in
    //            print("Subscribed to topic Speed")
    //        }
    //    }
    
    
}

